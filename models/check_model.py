from __future__ import print_function
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.model_selection import KFold
from keras.models import load_model
import numpy as np
import os


# need to know fold to know training and test data
def load_my_model(path):
    model = load_model(path)
    for layer in model.output_layers:
        print(layer.output.op.type)

    model.compile(optimizer='adam', loss='mse')
    # model.summary()
    return model


# Get data files
def load_data():
    data_folder = "/home/daniel/Projects/pyprocschooter/data/"
    targets_file = data_folder + "df4_time_team0ratio.npy".format()

    weapon_file = data_folder + "df4_weapons.npy"
    weapons = np.load(weapon_file)
    map_file = data_folder + "df4_maps.npy"
    maps = np.load(map_file)
    inputs = [weapons, maps]
    targets = np.load(targets_file)
    targets = [targets[:, 0], targets[:, 1]]
    return inputs, targets


def print_scores(inputs, targets, model, fold):
    index = 0
    metrics = []
    kf = KFold(n_splits=10, shuffle=True, random_state=0)  # NOTE SEEDING THE DATA SPLIT!!!

    for train_index, test_index in kf.split(targets[0]):
        if index == fold:
            print("fold ", index)
            # print("Output, mse, mae, r2")
            test_input = [inputs[0][test_index], inputs[1][test_index]]
            predictions = model.predict(test_input)
            for out in range(2):
                pred = predictions[:, out]
                true = targets[out][test_index]
                mse = mean_squared_error(true, pred)
                mae = mean_absolute_error(true, pred)
                r2 = r2_score(true, pred)
                print("{0},{1:.4f},{2:.4f},{3:.3f}".format(out, mse, mae, r2))
                metrics.extend([mse, mae, r2])
            avg_mse = (metrics[0] + metrics[3]) / 2
            avg_mae = (metrics[1] + metrics[4]) / 2
            avg_r2 = (metrics[2] + metrics[5]) / 2
            metrics.extend([avg_mse, avg_mae, avg_r2])
        index += 1
    return metrics


def check_model(fold, path):
    model = load_my_model(path)
    inputs, targets = load_data()
    metrics = print_scores(inputs, targets, model, fold)
    return metrics


def single_main():
    f = 0
    p = os.getcwd() + "/models/c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_{0}.h5".format(f)
    fold_metrics = check_model(f, p)
    print("mse 0 , mae 0 , r2 0 , mse 1, mae 1, r2 1, avg mse, avg mae, avg r2")
    strings = ["{0:.4f}".format(i) for i in fold_metrics]
    print(",".join(strings))
    return fold_metrics


def all_folds_main(name):
    all_metrics = []
    for f in range(10):
        p = os.getcwd() + "/models/{0}-fold_{1}.h5".format(name, f)
        fold_metrics = check_model(f, p)
        all_metrics.append(fold_metrics)
    header = "mse 0, mae 0, r2 0, mse 1, mae 1, r2 1, avg mse, avg mae, avg r2"
    print(header)
    lines = [header]
    for m in all_metrics:
        line = ",".join(["{0:.4f}".format(i) for i in m])
        print(line)
        lines.append(line + "\n")
    return lines

if __name__ == "__main__":
    # single_main()
    model_name = "c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu"
    lines = all_folds_main(model_name)
    with open(os.getcwd() + "/models/checked-{0}.csv".format(model_name), 'w') as out:
        out.writelines(lines)
