from __future__ import print_function
import subprocess
import os
from common.common import list_to_string
import time

curr_dir = os.getcwd()
sim_dir = curr_dir + "/MatchSimulator/"
settings_file = sim_dir + "SettingFiles\\experiment_settings.csv"
simulator_file = sim_dir + "simulator.x86_64"
match_result_file = sim_dir + "LogFiles\Level1\Level_1_Run_1_Summary.txt"

run_simulator_cmd = simulator_file + " -nographics"
seconds_until_next_check = 2


def evaluate_character(deap_ind):
    print("removing previous log files")
    result = os.system("rm ./MatchSimulator/LogFiles\\\Level*\\\*")
    if result != 0:
        print("Did not remove anything")
    print("evaluating")
    set_classes(deap_ind[0])
    os.chdir(sim_dir)
    # result = os.system(run_simulator_cmd)
    # result = subprocess.call(run_simulator_cmd, shell=True)
    start = time.time()
    popen1 = subprocess.Popen(run_simulator_cmd, stdout=subprocess.PIPE, shell=True)
    # time.sleep(0.5)
    # set_classes([0.5] * 16)
    # popen2 = subprocess.Popen(run_simulator_cmd, stdout=subprocess.PIPE, shell=True)
    # popen2.wait()
    popen1.wait()
    end = time.time() - start
    print("time for simulations: ", end)
    result = 0
    if result != 0:
        print("ERROR: Did not run the game!!")
        return None
    else:
        if not os.path.exists(match_result_file):
            print("{0} does not exist. WTF".format(match_result_file))
            return None
        match_results = parse_summary_file()
        print("Match results: duration: {0}, balance: {1}".format(match_results[0], match_results[1]))


def set_classes(param_array):
    team1 = list_to_string(param_array[0:8])
    team2 = list_to_string(param_array[8:])
    
    with open(settings_file, 'r') as inpt:
        lines = inpt.readlines()
    lines[1] = team1 + ",\n"
    lines[2] = team2 + ",\n"
    
    with open(settings_file, 'w') as output:
        output.writelines(lines)
    print("Characters written to file")


def parse_summary_file():
    if not os.path.exists(match_result_file):
        print("Why did the summary file stop existing??")
        return -1, -1

    duration = -1
    team0score = -1
    with open(match_result_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            if line.startswith("TotalTime"):
                duration = elems[1]
            elif line.startswith("Team0Kills"):
                team0score = elems[2]
            if duration >= 0 and team0score >= 0:  # if we have both values forget about the loop
                break
    return duration, team0score


if __name__ == "__main__":
    test_deap_ind = [[0.5] * 16]
    t0 = time.time()
    evaluate_character(test_deap_ind)
    t1 = time.time()
    print("time taken: {0:.2f} s.".format(t1 - t0))