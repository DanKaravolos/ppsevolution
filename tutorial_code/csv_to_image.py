import cv2
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

color_dict = {
    '0': [255, 255, 255],
    '1': [128, 128, 128],
    '2': [0, 0, 0],
    'base1': [255, 128, 0],
    'base2': [128, 0, 128],
    'armor': [0, 0, 128],
    'health': [255, 0, 0],
    'damage': [0, 128, 128],
    'stairs': [192, 192, 192]
}

tile_dict = {
    '0': color_dict['0'],
    '1': color_dict['1'],
    '2': color_dict['2'],
    '0Y': color_dict['base1'],
    '0Z': color_dict['base2'],
    'Y': color_dict['base1'],
    'Z': color_dict['base2'],
    'S': color_dict['stairs'],
    '0S': color_dict['stairs'],
    '0A': color_dict['armor'],
    '1A': color_dict['armor'],
    '0H': color_dict['health'],
    '1H': color_dict['health'],
    '0D': color_dict['damage'],
    '1D': color_dict['damage']
}


# returns an RGB image from an asci map input. Optionally save or show the results
def image_from_asci_map(asci_map, new_size=316, border_size=2, show=False, show_now=False,
                        save=False, target_path=None):
    rows, columns = asci_map.shape
    img = np.zeros((rows, columns, 3), dtype=np.uint8)

    # manually set the bases
    img[-5:, 0:5] = color_dict['base1']
    img[0:5, -5:] = color_dict['base2']

    # color pixels based on asci map
    for r in range(rows):
        for c in range(columns):
            img[r, c] = tile_dict[asci_map[r, c]]

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # resize based on user input
    big_img = cv2.resize(img, (new_size, new_size), interpolation=cv2.INTER_NEAREST)

    # add black border
    if border_size > 0:
        b_size = border_size
        big_img = cv2.copyMakeBorder(big_img, b_size, b_size, b_size, b_size, cv2.BORDER_CONSTANT, value=[0, 0, 0])
        # print(big_img.shape)

    # Save the image to disk and or show result
    if save:
        # target_path = image_path.replace(".png", "_{0}b.png".format(new_size + 2 * b_size))
        # target_path = "/home/daniel/Projects/ppsEvolution/map_evolution/maps/test.png"

        cv2.imwrite(target_path, big_img)
    if show:
        plt.figure(figsize=(4, 4))
        big_img = cv2.cvtColor(big_img, cv2.COLOR_RGB2BGR)
        plt.imshow(big_img, 'gray')
        plt.tight_layout()
        plt.xticks([])
        plt.yticks([])
        if show_now:
            plt.show()
    return img


if __name__ == "__main__":
    import tutorial_code.grid_functions as gf
    asci_map = gf.csv_to_array("tiny_map.csv")
    tp = "test3.6.png"
    image_from_asci_map(asci_map, new_size=316, border_size=2, show=True, save=True, target_path=tp)
