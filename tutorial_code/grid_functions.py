import re
import numpy as np
from collections import defaultdict, deque
from queue import PriorityQueue
from copy import deepcopy


def csv_to_array(csv_file):
    row_list = []
    with open(csv_file) as f:
        content = f.readlines()
        for line in content[1:]:
            if not line.startswith('-'):
                my_row = re.split('[,|]', line.rstrip("\r\n"))
                row_list.append(my_row)
        grid_map = np.asarray(row_list, dtype='U2')
        t1_base = (grid_map.shape[0] - 1, 0)
        t2_base = (0, grid_map.shape[1] - 1)
        grid_map[t1_base] = 'Y'
        grid_map[t2_base] = 'Z'
    return grid_map

'''
Below are listed the generic grid functions, where it is assumed that the grid is a 2D Numpy array.
'''


def to_string(np_grid):
    grid_str = "\n".join([",".join(line) for line in np_grid])
    return grid_str


def print_map(np_grid):
    print(to_string(np_grid))
    print("\n")


def contains_coord(coord, np_grid):
    return 0 <= coord[0] < np_grid.shape[0] and 0 <= coord[1] < np_grid.shape[1]


def get_neighbors(coord, np_grid=None, moore=False):
    north = (coord[0] - 1, coord[1])
    east = (coord[0], coord[1] + 1)
    south = (coord[0] + 1, coord[1])
    west = (coord[0], coord[1] - 1)
    neighbors = [north, east, south, west]
    if moore:
        northeast = (coord[0] - 1, coord[1] + 1)
        southeast = (coord[0] + 1, coord[1] + 1)
        southwest = (coord[0] + 1, coord[1] - 1)
        northwest = (coord[0] - 1, coord[1] - 1)
        neighbors.extend([northeast, southeast, southwest, northwest])

    if np_grid is not None:
        return [n for n in neighbors if contains_coord(n, np_grid)]
    return neighbors


# builds a dictionary of tile types from a 2D numpy array
def build_dict(np_grid):
    nr_rows, nr_columns = np_grid.shape
    tile_dict = defaultdict(list)
    for r in range(nr_rows):
        for c in range(nr_columns):
            if len(np_grid[r, c]) == 1:
                tile_dict[np_grid[r, c]].append((r, c))
            else:
                tile_dict[np_grid[r, c][0]].append((r, c))
                tile_dict[np_grid[r, c][1]].append((r, c))
    return tile_dict


# breadth_first_search starts at a coordinate $current_coord$ and uses breadth-first search or "floodfill" to find a
# certain tile type $goal_type$. The tile types that it can pass/traverse are listed in $traversables$.
# The visited nodes are listed in $visited$ (you can count these) and the $open_list$ is a list of nodes that the
# algorithm has seen nearby and can still explore.
def bfs(np_grid, current_coord, goal_type, traversables):
    return breadth_first_search(np_grid, current_coord, goal_type, traversables, [], deque())


def breadth_first_search(np_grid, current_coord, goal_type, traversables, visited, open_list):
    if type(goal_type) is not list:
        goal_type = [goal_type]

    # append current coord to visited, if this is the tiletype we are looking for we are done! :)
    visited.append(current_coord)
    if np_grid[current_coord] in goal_type:
        return True

    # add all feasible neighbors to the open list. If the open list is still empty after this, we are also done :(
    nbs = get_neighbors(current_coord, np_grid)
    for n in nbs:
        if np_grid[n] in goal_type:  # check if we can "see" the goal
            visited.append(n)  # maybe we want to do something with the visited nodes?
            return True
        else:
            n_floor_type = np_grid[n][0]  # note we are only checking for floors: 0,1,2. No pickups!
            if n_floor_type in traversables and n not in open_list and n not in visited:
                open_list.append(n)
    if not open_list:
        return False

    # apparently we are not done! Time for recursion!
    new_coord = open_list.popleft()
    return breadth_first_search(np_grid, new_coord, goal_type, traversables, visited, open_list)


def compute_cost(current_type, next_type):
    if current_type == "0" and next_type == "1":
        return 100  # since moving from 0 to 1 is basically impossible, we assign a high cost
    if current_type == "1" and next_type == "0":
        return 2  # we prefer not dropping down, because being up gives an advantage
    return 1  # if we are going from 0 to S or from S to 1, we are happy!


def manhattan_distance(coord1, coord2):
    return abs(coord2[0] - coord1[0]) + abs(coord2[1] - coord1[1])


def compute_heuristic(np_grid, coord, goal_type_list):
    # get goal locations.
    # if you create a dictionary of tile types, this function can be made more efficient
    goal_coords = []
    rows, cols = np_grid.shape
    for r in range(rows):
        for c in range(cols):
            tile = np_grid[r, c]
            if tile in goal_type_list:
                goal_coords.append((r, c))
    distances = [manhattan_distance(coord, goal) for goal in goal_coords]
    return min(distances)


# returns path and cost of path from start to any goal_type
def astar(np_grid, start_coord, goal_type, traversables):

    # make sure that we can find goal and allow both single and multiple goal types
    if type(goal_type) is not list:
        goal_type = [goal_type]
    for g in goal_type:
        if g not in traversables:
            traversables.append(g)

    # check if the goals are in the grid. if not, we cannot do A*
    if not any([g in np_grid for g in goal_type]):
        return [], -1  # return an empty path and an impossible cost.
        # Tip for usage: an empty list is false in an if-statement. So you can do:
        # if path: do something with path, else: assign negative value (because goal or path does not exist).

    # setting up variables for non-recursive A*.
    frontier = PriorityQueue()
    frontier.put(start_coord, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start_coord] = None
    cost_so_far[start_coord] = 0

    current = None
    goal_found = False
    while not frontier.empty():
        current = frontier.get()

        if np_grid[current] in goal_type:
            goal_found = True
            break

        for next_coord in get_neighbors(current, np_grid):
            if np_grid[next_coord] not in traversables:  # do not try to pass through untraversable tiles
                continue
            new_cost = cost_so_far[current] + compute_cost(np_grid[current], np_grid[next_coord])
            if next_coord not in cost_so_far or new_cost < cost_so_far[next_coord]:
                cost_so_far[next_coord] = new_cost
                priority = new_cost + compute_heuristic(np_grid, next_coord, goal_type)
                frontier.put(next_coord, priority)
                came_from[next_coord] = current

    if not goal_found:
        return [], -1

    # retrieving path from our dictionary of shortest paths to a tile.
    # Note that the start tile will be included to the path
    path = [current, came_from[current]]
    while path[-1] != start_coord:
        path.append(came_from[path[-1]])
    path.reverse()
    return path, cost_so_far[current]


def check_connection_between(np_grid, current_coord, goal_type, traversables):
    path, cost = astar(np_grid, current_coord, goal_type, traversables)
    if len(path) > 0:
        return True
    else:
        path1, cost1 = astar(np_grid, current_coord, goal_type, traversables)
        return False

if __name__ == "__main__":
    asci_map = csv_to_array("tiny_map.csv")
    print_map(asci_map)
    # path, cost = astar(asci_map, (asci_map.shape[0] - 1, 0), 'Z', ['0', '1', '0S', '0D', '0H', '0A'])
    path, cost = astar(asci_map, (asci_map.shape[0] - 1, 0), 'Z', ['0', '1', 'S'])

    # visualization:
    print("path:", path)
    print("cost to nearest goal: ", cost)
    path_map = deepcopy(asci_map)
    for p in path:
        path_map[p] += "P"
    print_map(path_map)
