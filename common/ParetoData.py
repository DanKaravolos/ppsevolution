import numpy as np
from collections import defaultdict


class ParetoData:
    def __init__(self, name, fitness1, fitness2, score, duration, sum_sq_fit, individual, is_best):
        self.map_name = name
        self.fit_score = float(fitness1)
        self.fit_duration = float(fitness2)
        self.score = float(score)
        self.duration = float(duration)
        self.fitness = float(sum_sq_fit)
        self.individual = individual
        self.is_best = is_best


class ParetoSummary:
    def __init__(self, csv_data_list, mode, runs_per_map=10):
        self.raw = csv_data_list
        self.content_type = mode  # possible values: class, map, duo
        self.runs_per_map = runs_per_map
        self.per_map_data = self.parse_raw_data()
        self.score_minmax, self.duration_minmax = self.get_gameplay_minmax()
        self.fit_score_minmax, self.fit_duration_minmax = self.get_fitness_minmax()

    def parse_raw_data(self):
        map_count = 1
        map_base = "Custom_"
        per_map_dict = defaultdict(list)

        run_count = 0
        run_data = []
        for elems in self.raw:
            # every run is marked with a header that starts with num
            if elems[0] == "num":
                map_name = map_base + str(map_count)
                if run_data:  # only for the first line. prevent adding empty list
                    per_map_dict[map_name].append(run_data)
                    run_data = []
                    run_count += 1
                # every map had ten runs
                if run_count != 0 and run_count % self.runs_per_map == 0:
                    map_count += 1
                continue

            ind_type = elems[1]
            if "dominated" in ind_type:
                continue  # we do not want to store dominated inds in our pareto front
            # so now we are sure we have a pareto front individual. is it the best?
            is_best = "best" in elems[1]
            if self.content_type == "duo":
                fit_score = elems[2]
                fit_time = elems[3]
                pred_score = elems[4]
                pred_time = elems[5]
                # fitness_avg = elems[6]
                sqrt_sum_fitness = elems[7]
                individual = self.parse_individual(elems[8:])
                data = ParetoData(map_name, fit_score, fit_time, pred_score, pred_time, sqrt_sum_fitness, individual, is_best)
            elif self.content_type == "map":
                fit_score = elems[2]
                fit_time = elems[3]
                pred_score = elems[4]
                pred_time = elems[5]
                # avg fitness was not stored in current results
                sqrt_sum_fitness = elems[6]
                individual = self.parse_individual(elems[7:])
                data = ParetoData(map_name, fit_score, fit_time, pred_score, pred_time, sqrt_sum_fitness, individual, is_best)
            elif self.content_type == "class":
                fit_score = elems[2]
                fit_time = elems[3]
                pred_score = elems[4]
                pred_time = elems[5]
                # avg fitness was not stored in current results
                sqrt_sum_fitness = elems[6]
                # individual = self.parse_individual(elems[7:])
                individual = []  # individual was not stored in current results
                data = ParetoData(map_name, fit_score, fit_time, pred_score, pred_time, sqrt_sum_fitness, individual, is_best)
            run_data.append(data)

        # the final run does not have a header to mark its end
        per_map_dict[map_name].append(run_data)
        return per_map_dict

    def parse_individual(self, ind_elems):

        if self.content_type == "class":
            return [float(param) for param in ind_elems]
        elif self.content_type == "map":
            return ind_elems
        elif self.content_type == "duo":
            map_ind = ind_elems[-1]
            class_ind = [float(param) for param in ind_elems[:-1]]
            return [class_ind, map_ind]

    def get_gameplay_minmax(self):
        score_dict = {}
        duration_dict = {}
        # For min max over all runs:
        # for m, runs in self.per_map_data.items():
        #     min_score = 1
        #     max_score = 0
        #     min_dur = 1
        #     max_dur = 0
        #
        #     for run in runs:
        #         for pdata in run:
        #             if pdata.score < min_score:
        #                 min_score = pdata.score
        #             if pdata.duration < min_dur:
        #                 min_dur = pdata.duration
        #             if pdata.score > max_score:
        #                 max_score = pdata.score
        #             if pdata.duration > max_dur:
        #                 max_dur = pdata.duration
        #     score_dict[m] = np.asarray([min_score, max_score])
        #     duration_dict[m] = np.asarray([min_dur, max_dur])

        # For min max per run:
        for m, runs in self.per_map_data.items():
            scores = [[min(run, key=lambda p: p.score).score, max(run, key=lambda p: p.score).score] for run in runs]
            durations = [[min(run, key=lambda p: p.duration).duration, max(run, key=lambda p: p.duration).duration] for run in runs]
            score_dict[m] = np.asarray(scores)
            duration_dict[m] = np.asarray(durations)
        return score_dict, duration_dict

    def get_fitness_minmax(self):
        score_dict = {}
        duration_dict = {}
        # For min max per run:
        for m, runs in self.per_map_data.items():
            scores = [[min(run, key=lambda p: p.fit_score).fit_score, max(run, key=lambda p: p.fit_score).fit_score] for run in runs]
            durations = [[min(run, key=lambda p: p.fit_duration).fit_duration, max(run, key=lambda p: p.fit_duration).fit_duration] for run in runs]
            score_dict[m] = np.asarray(scores)
            duration_dict[m] = np.asarray(durations)
        return score_dict, duration_dict
