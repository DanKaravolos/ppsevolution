import numpy as np


class TF2MatchData():
    def __init__(self, match_map, dim_values, dims, map_names):
        map_num = int(match_map.rsplit("\\", 1)[1].split("_")[1]) - 1
        self.map = map_names[map_num].replace("_Run_0_Map.csv", "")
        self.game_metrics = dim_values
        self.gmn = dims


class TF2MatchSummary():

    def __init__(self, data_list):
        self.data = data_list
        self.game_metric_names = data_list[0].gmn
        self.game_metric_data = np.mean(np.asarray([dp.game_metrics for dp in self.data], dtype=np.float), axis=0)
        self.conf_intervals = self.compute_ci()

    def compute_ci(self):
        stds = np.std(np.asarray([dp.game_metrics for dp in self.data], dtype=np.float), axis=0)
        N = len(self.data)
        cis = []
        for i in range(len(self.game_metric_data)):
            ci_low = self.game_metric_data[i] - 1.96*(stds[i]/N)
            ci_hi = self.game_metric_data[i] + 1.96*(stds[i]/N)
            cis.append([ci_low, ci_hi])
        return cis

    def get_header(self):
        cis = ["CI low {0}, CI high {0}".format(i) for i in self.game_metric_names]
        line = [*self.game_metric_names, *cis]
        str_line = ",".join(line)
        return str_line

    def to_string(self):
        cis = ["{0:.3f}, {1:.3f}".format(*ci) for ci in self.conf_intervals]
        line = ["{0:3f}".format(t) for t in self.game_metric_data] + cis
        str_line = ",".join(line)
        return str_line
