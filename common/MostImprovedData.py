

class MostImprovedData:

    def __init__(self, most_improved_dict):
        self.data = most_improved_dict
        print("MostImprovedData: Assuming per map per duration improvements.")
        self.map_names = sorted(self.data.keys(), key=lambda x: int(x.replace("Custom_", "")))
        self.times = sorted(self.data[self.map_names[0]].keys(), reverse=True)  # Short, Medium, Long
