import os
from platform import python_version
from tqdm import tqdm
import numpy as np
import re
import itertools
import sys
from collections import defaultdict
from map_evolution.visualization.map_to_image import image_from_asci_map

PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3

if PY3:
    string_type = str
else:
    string_type = basestring


def flatten(list_2d):
    return list(itertools.chain.from_iterable(list_2d))


def list_to_string(individual):
    return ",".join(["{:.6f}".format(w) if not isinstance(w, string_type) else w for w in individual])


def list_to_string3d(individual):
    return ",".join(["{:.3f}".format(w) if not isinstance(w, string_type) else w for w in individual])


def get_files_in_folder(folder):
    files = [folder + fi for fi in os.listdir(folder) if os.path.isfile(folder + "/" + fi)]
    return files


def get_specific_files_in_folder(folder, contain_string):
    files = [folder + fi for fi in os.listdir(folder) if os.path.isfile(folder + "/" + fi) and contain_string in fi]
    return files


def denormalize_time(time):
    time_min = 163.0
    time_max = 600.0
    orig = time * (time_max - time_min) + time_min
    return orig


def normalize_time(orig):
    time_min = 163.0
    time_max = 600.0
    n_time = (orig - time_min) / (time_max - time_min)
    return n_time


def define_fitness_weight(fit_string):
    if "maximize" in fit_string:
        print("maximizing '{0}'".format(fit_string))
        return 1
    else:
        print("minimizing '{0}'".format(fit_string))
        return -1


# ##used for init and select in order to parse settings from file
def parse_nn_settings(settings_file):
    print("Parsing neural net settings from ", settings_file)
    model_dict = defaultdict(list)
    with open(settings_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines:
            elems = line.rstrip().split(",", 1)
            model_path = os.getcwd() + "/" + elems[1]
            model_dict[elems[0]].append(model_path)
            print("Adding to target {0}, model {1}".format(elems[0], model_path))
    return model_dict


def parse_match_settings(csv_file):
    match_dict = {}
    with open(csv_file) as f:
        content = f.readlines()
        for line in content[1:]:
            row = re.split('[,]', line.rstrip("\r\n"))
            if len(row) > 1:
                match_key = "{0}_{1}".format(row[0], row[1])
                match_dict[match_key] = np.asarray(row[2:], dtype=np.float32).reshape((1, 16))
    return match_dict


def parse_targets(settings_file):
    print("\nParsing target objective values from ", settings_file)
    target_dict = {}
    with open(settings_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines:
            elems = line.rstrip().split(",", 1)
            target_dict[elems[0]] = float(elems[1])
            print("Objective {0}: {1}".format(elems[0], elems[1]))
    return target_dict


# individuals are equal if they evaluate to the same string representation
def is_equal_match(match1, match2):
    match_ind1 = list_to_string(match1)
    match_ind2 = list_to_string(match2)
    return match_ind1 == match_ind2


def is_equal_map(map1, map2):
    try:
        map_ind1 = map1.to_string()
        map_ind2 = map2.to_string()
    except AttributeError as error:
        print(error)
        print("Trying element 0 of individual")
        map_ind1 = map1[0].to_string()
        map_ind2 = map2[0].to_string()
    return map_ind1 == map_ind2


def is_equal_dual(dual_ind1, dual_ind2):
    equal_match = is_equal_match(dual_ind1[0], dual_ind2[0])
    equal_map = is_equal_map(dual_ind1[1], dual_ind2[1])
    return equal_match and equal_map


# individuals are equal if they have the same fitness (for pareto front?)
def is_equal_fitness(match1, match2):
    match_ind1 = list_to_string3d(match1.fitness.values)
    match_ind2 = list_to_string3d(match2.fitness.values)
    return match_ind1 == match_ind2


# individuals are equal if the parameters are within a certain distance
def match_within_threshold(match1, match2, threshold=1e-2):
    sq_distances = [(match1[i] - match2[i])**2 for i in range(len(match1))]
    dist = np.sqrt(sum(sq_distances))
    return dist <= threshold


def csv_to_array(csv_file):
    row_list = []
    with open(csv_file, 'r') as f:
        content = f.readlines()
        for line in content:
            if not line.startswith('-') and not line.startswith("Seed"):
                my_row = re.split('[,|]', line.rstrip("\r\n"))
                row_list.append(my_row)
    arr = np.asarray(row_list) if python_version()[0] == str(3) else np.asarray(row_list, dtype='S2')
    return arr


def inline_map_to_array(map_string_line):
    map_lines = map_string_line.replace("_", ",").split(";")
    arr = np.asarray(map_lines) if python_version()[0] == str(3) else np.asarray(map_lines, dtype='S2')
    return arr


def save_initial_maps(args, population, run, is_dual):
    target_file = args.result_file.replace("results", "results/initial_pop") +\
                  "-{0}-run_{1}".format(args.map_name, run) + "-initial_maps.txt"
    with open(target_file, 'w')as out:
        for i, deap_ind in tqdm(enumerate(population)):
            if is_dual:
                deap_ind[1].visualize_bases()
                str_ind = deap_ind[1].to_string(inline=True)
                deap_ind[1].undo_visualize_bases()
            else:
                deap_ind[0].visualize_bases()
                str_ind = deap_ind[0].to_string(inline=True)
                # image_from_asci_map(deap_ind[0].to_matrix(), show_now=False, save=True, target_path=target_file.replace("s.txt", "-{0}.png".format(i)))
                deap_ind[0].undo_visualize_bases()
            out.write(str_ind + "\n")
    print("Saved initial maps to ", target_file)


def save_initial_classes(args, population, run, is_dual):
    if is_dual:
        target_file = args.result_file.replace("results", "results/initial_pop") + \
                      "-{0}-run_{1}".format(args.map_name, run) + "-initial_classes.txt"
    else:
        map_name = args.nn_data.rsplit("/", 1)[1].split(".", 1)[0]
        target_file = args.result_file.replace("results", "results/initial_pop") +\
                      "-{0}-run_{1}".format(map_name, run) + "-initial_classes.txt"
    with open(target_file, 'w')as out:
        for deap_ind in tqdm(population):
            if is_dual:
                str_ind = list_to_string(deap_ind[0])
            else:
                str_ind = list_to_string(deap_ind)
            out.write(str_ind + "\n")
    print("Saved initial classes to ", target_file)


def save_initial_dual_pop(args, population, run):
    target_file = args.result_file.replace("results", "results/initial_pop") + \
                  "-{0}-run_{1}".format(args.map_name, run) + "-initial_dual_pop.txt"
    with open(target_file, 'w')as out:
        for deap_ind in population:
            str_ind0 = list_to_string(deap_ind[0])
            deap_ind[1].visualize_bases()
            str_ind1 = deap_ind[1].to_string(inline=True)
            out.write(str_ind0 + "," + str_ind1 + "\n")
    print("Saved initial population to ", target_file)


# target_to_index = {"0.085": 0, "0.315": 1, "1": 2}
# index_to_target = {0: "0.085", 1: "0.315", 2: "1"}
match_file = os.getcwd() + "/data/all_5_class_matches.csv"
match_to_param_dict = parse_match_settings(match_file)
matches = [list_to_string(match_to_param_dict[m][0]) for m in sorted(match_to_param_dict)]
match_names = [m for m in sorted(match_to_param_dict)]
param_to_match_dict = dict(zip(matches, match_names))
