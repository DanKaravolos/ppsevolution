import numpy as np
from character_evolution.evolution import weapon_parameters as wp


class EvolvedIndividualSummary():
    def __init__(self, evolved_data_list):
        # initial_len = len(evolved_data_list)
        # evolved_data_list = [dp for dp in evolved_data_list if dp.has_gt]
        # if len(evolved_data_list) < initial_len:
        #     print("Ignoring Duos without GT runs...")

        self.data = evolved_data_list
        self.game_metric_names = self.data[0].game_metric_names
        self.desired = self.data[0].desired_vals
        self.nr_dim = len(self.game_metric_names)

        # Collect desired values
        self.diff_between_a_p = [dp.diff_between_a_p for dp in self.data]
        self.diff_between_a_d = [dp.diff_between_a_d for dp in self.data]
        self.diff_between_p_d = [dp.diff_between_p_d for dp in self.data]

        self.a_is_d = np.asarray([dp.a_is_d for dp in self.data])
        self.a_is_p = np.asarray([dp.a_is_p for dp in self.data])
        self.p_is_d = np.asarray([dp.p_is_d for dp in self.data])
        self.actual = np.mean([dp.means for dp in self.data], axis=0)

        # Compure desired summaries
        # mean deviation from predicted and desired and actual
        self.mean_dap = np.mean(self.diff_between_a_p, axis=0)
        self.mean_dad = np.mean(self.diff_between_a_d, axis=0)
        self.mean_dpd = np.mean(self.diff_between_p_d, axis=0)

        # how often are a, p, d in the same class
        self.ad_count = [(self.a_is_d[:, i]).sum() / self.a_is_d.shape[0] for i in range(self.a_is_d.shape[1])]
        self.ap_count = [(self.a_is_p[:, i]).sum() / self.a_is_p.shape[0] for i in range(self.a_is_p.shape[1])]
        self.pd_count = [(self.p_is_d[:, i]).sum() / self.p_is_d.shape[0] for i in range(self.p_is_d.shape[1])]

    def get_header(self):
        line = ["abs(a-p) {0}".format(t) for t in self.game_metric_names]\
             + ["abs(a-d) {0}".format(t) for t in self.game_metric_names]\
             + ["abs(p-d) {0}".format(t) for t in self.game_metric_names]\
             + ["class a == p {0}".format(t) for t in self.game_metric_names] \
             + ["class a == d {0}".format(t) for t in self.game_metric_names] \
             + ["class p == d {0}".format(t) for t in self.game_metric_names]\

        str_line = ",".join(line)
        return str_line

    def to_string(self):
        line = [*self.mean_dap, *self.mean_dad, *self.mean_dpd, *self.ap_count, * self.ad_count, *self.pd_count]
        str_line = ",".join(["{0:4f}".format(t) for t in line])
        return str_line
