# from __future__ import division
import numpy as np
from keras.models import load_model
from common.parse_csv_maps import parse_array, csv_to_npy
from deap import tools
from collections import defaultdict
import warnings


class FitnessFunction:
    # Internal variables
    def __init__(self, individual_type, fitness_metric, target_dict=None, match=[]):
        self.individual_type = individual_type
        self.neural_net_dict = defaultdict(list)
        self.np_map = ""  # used for character class evolution
        self.np_match = np.expand_dims(np.asarray(match), axis=0)   # used for map evolution
        self.pickup_neighborhood = 0
        print("pickup neighborhood = 0")
        self.fitness = fitness_metric
        if target_dict is None and ("target" in self.fitness or "error" in self.fitness):
            print("Warning! You have set a target fitness, but not a dict for target values. Are you sure?")
        self.target_values = target_dict
        self.nr_outputs = {}
        self.multi_objective = "multi" in self.fitness
        self.prediction_mode = "regression"
        print("Prediction mode", self.prediction_mode)

    # WRAPPER FUNCTION
    def evaluate(self, individual):
        # ind_match = individual[0]
        if self.individual_type == "map" or self.individual_type == "dual":
            ind_map = individual[1] if self.individual_type == "dual" else individual[0]
            # if the map is invalid, there is no need for prediction
            if ind_map.invalid:
                return tuple([max(ind_map.unreachable_tiles, 1)] * len(self.target_values))

        fitness = self.compute_fitness(individual, self.fitness)
        # guarantee that output is a tuple
        if isinstance(fitness, tuple):
            return fitness
        else:
            return fitness,

    def compute_fitness(self, individual, fit_func):
        if self.prediction_mode == "classification" and "target" not in fit_func:
            raise ValueError("Error in compute_fitness! Prediction mode is classification, "
                             "expected 'target' or 'multi_target' as fitness function, not {0}".format(fit_func))

        if "maximize" in fit_func or "minimize" in fit_func:
            return self.regression_maximize(individual)
        elif "target" in fit_func:
            return self.absolute_error(individual)
        elif "squared_error" in fit_func:
            return self.regression_mse(individual)
        else:
            print("What is your fitness function???")
            return -999

    # initialization functions   ############################################################
    def prepare_nn_and_data(self, nn_model_dict, map_file=""):
        # set global neural network
        for target, models in iter(nn_model_dict.items()):
            for model in models:
                print("Loading NN for", target)
                neural_net = load_model(model)
                # neural_net.compile(optimizer='adam', loss='mse')  # not necessary for usage anymore
                neural_net.summary()
                self.neural_net_dict[target].append(neural_net)
                output_size = neural_net.layers[-1].get_output_shape_at(0)[1]
                # assuming all the networks of the same target have the same output size
                self.nr_outputs[target] = output_size
                network_mode = "classification" if output_size > 1 else "regression"
                if network_mode != self.prediction_mode:
                    print("Switching mode from {0} to {1}".format(self.prediction_mode, network_mode))
                    self.prediction_mode = network_mode
        print("Done loading neural networks")

        if self.individual_type == "character" and not map_file:
            return ValueError("In prepare_nn_and_data, map_file cannot be empty when evolving characters")
        if map_file:
            print("Loading map")
            self.np_map = csv_to_npy(map_file)
            if len(self.np_map.shape) == 3:
                self.np_map = np.expand_dims(self.np_map, axis=0)
            # if "mlp" in nn or "c_sizes" not in nn:
            #     print("loading an MLP. Input will be flattened")
            #     self.np_map = np.reshape(input_maps, (self.np_map.shape[0], -1))
            #     self.np_map = np.concatenate((np.zeros((self.np_map.shape[0], 16)), self.np_map), axis=1)
            #     print(self.np_map.shape)

    # UTILITY FUNCTIONS   ############################################################

    # def get_best_individual(population):
    #     best = max(population, key=lambda x: x.fitness)
    #     return best

    def get_best_individual(self, population, check_validity=False, return_predictions=False):
        if check_validity and self.individual_type != "character":
            population = [ind for ind in population if not ind[1].invalid]
        # best_valid = max(population, key=lambda ind: ind.fitness)
        best_valid = tools.selBest(population, 1)
        if return_predictions:
            preds = self.nn_evaluate_individual(best_valid)
            return best_valid, preds
        else:
            return best_valid

    #  nn_evaluate_individual returns a dictionary of predictions for each target
    #  the targets and number of targets are defined by the model_settings file in init_and_select
    def nn_evaluate_individual(self, deap_individual):
        if self.individual_type == "character":
            return self.nn_evaluate_character(deap_individual)
        elif self.individual_type == "dual":
            return self.nn_evaluate_dual(deap_individual)
        elif self.individual_type == "map":
            return self.nn_evaluate_map(deap_individual)
        else:
            return ValueError("In nn_evaluate_individual: Unknown individual_type {0}".format(self.individual_type))

    def nn_evaluate_character(self, deap_individual):
        # convert individual to numpy and convert single value to fake batch
        np_match = np.expand_dims(np.asarray(deap_individual), axis=0)
        # individual readable for NN:
        nn_individual = [np_match, self.np_map]
        return self.nn_evaluate(nn_individual)

    def nn_evaluate_dual(self, deap_individual):
        # convert individual to numpy
        np_match = np.asarray(deap_individual[0])
        np_match = np.expand_dims(np_match, axis=0)
        # convert single value to fake batch
        np_map = parse_array(deap_individual[1].to_matrix(), self.pickup_neighborhood)
        np_map = np.expand_dims(np_map, axis=0)
        # individual readable for NN:
        nn_individual = [np_match, np_map]
        return self.nn_evaluate(nn_individual)

    def nn_evaluate_map(self, deap_individual):
        # convert single value to fake batch
        np_map = parse_array(deap_individual[0].to_matrix(), self.pickup_neighborhood)
        np_map = np.expand_dims(np_map, axis=0)
        # individual readable for NN:
        nn_individual = [self.np_match, np_map]
        return self.nn_evaluate(nn_individual)

    def nn_evaluate(self, nn_individual):
        predictions = {}
        for target, models in iter(self.neural_net_dict.items()):
            target_preds = [model.predict(nn_individual)[0] for model in models]  # since batch size is 1: output only first
            if self.prediction_mode == "regression":
                predictions[target] = np.mean(target_preds)
            else:  # mode == "classification"
                # currently I do not know how to handle classification aggregation for multiple models
                if len(target_preds) != 1:
                    raise ValueError(
                        "In nn_evaluate_individual, length of target_preds is {0}, which is >1. But no aggregation method is implemented.".format(
                            len(target_preds)))
                predictions[target] = np.argmax(target_preds[0])
        return predictions

    def nn_str_prediction(self, deap_individual, separator=",", with_name=True):
        predictions = self.nn_evaluate_individual(deap_individual)
        if with_name:
            str_pred = separator.join(["{0}{2}{1:.6f}".format(k, v, separator) for k, v in sorted(predictions.items())])
        else:
            str_pred = separator.join(["{0:.6f}".format(v) for k, v in sorted(predictions.items())])
        return str_pred


    # FITNESS FUNCTIONS   ############################################################

    def regression_maximize(self, individual):
        warnings.warn("NOTE: Regression_maximize/minimize has not been tested!")
        predictions = self.nn_evaluate_individual(individual)
        if self.multi_objective:
            return [predictions[obj] for obj in sorted(predictions.keys())]
        else:
            return sum([v for v in predictions.values()])

    def regression_mse(self, individual):
        # warnings.warn("NOTE: Regression_mse has not been tested!")
        pred = self.nn_evaluate_individual(individual)
        deviations = [(self.target_values[t] - pred[t])**2 for t in sorted(self.target_values.keys())]
        if self.multi_objective:
            # warnings.warn("Warning! You are using multi objective, but you have defined MSE as fitness function."
            #               "Returning the squared deviation per objective.")
            return tuple(deviations)
        else:
            return np.mean(deviations)

    def absolute_error(self, individual):
        # warnings.warn("NOTE: function absolute error has not been tested!")
        pred = self.nn_evaluate_individual(individual)
        # un_norm_deviations = [np.absolute(self.target_values[t] - pred[t]) for t in sorted(self.target_values.keys())]
        deviations = [np.absolute(self.target_values[t] - pred[t]) / self.nr_outputs[t] for t in sorted(self.target_values.keys())]

        if self.multi_objective:
            return tuple(deviations)
        else:
            if len(deviations) > 1:
                warnings.warn("Warning! You are using single objective, "
                              "but you have multiple targets in regression_target? "
                              "Returning the sum of deviations from the targets.")
            return sum(deviations)
