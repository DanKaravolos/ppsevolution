import matplotlib.pyplot as plt

box_colors = {
    "Short": (44/255.0,181/255.0,229/255.0, 0.6),
    "Medium": (244/255.0, 62/255.0, 169/255.0, 0.6),
    "Long": (1.0, 131/255.0, 30/255.0, 0.6)}

time_goals = {"Short": 0.14, "Medium": 0.36, "Long": 0.72}
times = ["Short", "Medium", "Long"]


def change_font_settings():
    params = {
              # 'axes.axisbelow': 'true',
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '24',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)
    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'