import os
from tqdm import tqdm

tag = "result"
# tag = "game_input"

# result_dir = "/home/daniel/Projects/pyprocschooter/df6_baselines"
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/Level_Ant_CIG_net-{0}.csv".format(tag)

# result_dirs = [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_GAUSS_10/ICCC_Level_{0}_Gauss_10/results/".format(i +1) for i in range(4)]
# result_dirs += [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_GAUSS_10/ICCC_Level_Ant_Gauss_10/results/"]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/ICCCMaps_Gauss_10-{0}.csv".format(tag)
#
# result_dirs = [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_TF2_1/ICCC_Level_{0}_TF2_1/results/".format(i +1) for i in range(4)]
# result_dirs += [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_TF2_1/ICCC_Level_Ant_TF2_1/results/"]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/ICCCMaps_TF2_1-{0}.csv".format(tag)

result_dirs = [os.getcwd() + "/character_evolution/Results/GEN_MAPS_GAUSS_1/Level_{0}_Gauss_1/results/".format(i +1) for i in range(10)]
summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/GenMaps_Gauss_1-{0}.csv".format(tag)

# result_dirs = [os.getcwd() + "/character_evolution/Results/GEN_MAPS_TF2_10/Level_{0}_TF2_10/results/".format(i +1) for i in range(10)]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/GenMaps_TF2_10-{0}.csv".format(tag)

# result_dirs = [os.getcwd() + "/character_evolution/Results/CUSTOM_GAUSS_1/Custom_{0}_Gauss_1/results/".format(i +1) for i in range(10)]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/Custom_Gauss_1-{0}.csv".format(tag)

# result_dirs = [os.getcwd() + "/character_evolution/Results/CUSTOM_TF2_1/Custom_{0}_TF2_1/results/".format(i +1) for i in range(10)]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/Custom_TF2_1-{0}.csv".format(tag)


# result_dirs = [os.getcwd() + "/character_evolution/Results/CIGNET_1/CIGNET_Custom_{0}_Gauss_1/results/".format(i +1) for i in range(10)]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/CIGNET_Custom_Gauss_1-{0}.csv".format(tag)

# result_dirs = [os.getcwd() + "/character_evolution/Results/CIGNET_1/CIGNET_Level_{0}_Gauss_1/results/".format(i +1) for i in range(10)]
# summary_file = "/home/daniel/Projects/ppsEvolution/character_evolution/CIGNET_GenMap_Gauss_1-{0}.csv".format(tag)


all_csvs = []
for result_dir in result_dirs:
    all_csvs += sorted([result_dir + "/" + fi for fi in os.listdir(result_dir) if "-{0}.csv".format(tag) in fi])
count = 0
with open(summary_file, 'w') as out:
    for res_file in tqdm(all_csvs):
        with open(res_file, 'r') as inpt:
            if tag == "result" and count > 0:
                lines = inpt.readlines()[1:]
            else:
                lines = inpt.readlines()
            out.writelines(lines)
            count += 1
