import os
from tqdm import tqdm

tags = ["result", "game_input"]
nr_runs = [1]
for tag in tags:
    for run in nr_runs:
        # result_dirs1 = [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_GAUSS_10/ICCC_Level_{0}_Gauss_10/results/".format(i +1) for i in range(10)]
        # result_dirs1 += [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_GAUSS_10/ICCC_Level_Ant_GAUSS_10/results/"]
        # summary_file1 = "/home/daniel/Projects/ppsEvolution/character_evolution/ICCCMaps_Gauss_10-{0}.csv".format(tag)

        # result_dirs2 = [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_TF2_{1}/ICCC_Level_{0}_TF2_{1}/results/".format(i + 1, run) for i in range(4)]
        # result_dirs2 += [os.getcwd() + "/character_evolution/Results/ICCC_MAPS_TF2_{0}/ICCC_Level_Ant_TF2_{0}/results/".format(run)]
        # summary_file2 = "/home/daniel/Projects/ppsEvolution/character_evolution/ICCCMaps_TF2_{1}-{0}.csv".format(tag, run)

        result_dirs3 = [os.getcwd() + "/character_evolution/Results/GEN_MAPS_MLP_GAUSS_{1}/Level_{0}_MLP_Gauss_{1}/results/".format(i + 1, run) for i in range(10)]
        summary_file3 = "/home/daniel/Projects/ppsEvolution/character_evolution/GenMaps_MLP_Gauss_{1}-{0}.csv".format(tag, run)

        result_dirs4 = [os.getcwd() + "/character_evolution/Results/GEN_MAPS_MLP_TF2_{1}/Level_{0}_MLP_TF2_{1}/results/".format(i + 1, run) for i in range(10)]
        summary_file4 = "/home/daniel/Projects/ppsEvolution/character_evolution/GenMaps_MLP_TF2_{1}-{0}.csv".format(tag, run)

        result_dirs5 = [os.getcwd() + "/character_evolution/Results/CUSTOM_MLP_GAUSS_{1}/Custom_{0}_MLP_Gauss_{1}/results/".format(i + 1, run) for i in range(10)]
        summary_file5 = "/home/daniel/Projects/ppsEvolution/character_evolution/Custom_MLP_Gauss_{1}-{0}.csv".format(tag, run)

        result_dirs6 = [os.getcwd() + "/character_evolution/Results/CUSTOM_MLP_TF2_{1}/Custom_{0}_MLP_TF2_{1}/results/".format(i + 1, run) for i in range(10)]
        summary_file6 = "/home/daniel/Projects/ppsEvolution/character_evolution/Custom_MLP_TF2_{1}-{0}.csv".format(tag, run)
        all_dirs = [result_dirs3, result_dirs4, result_dirs5, result_dirs6]
        all_summaries = [summary_file3, summary_file4, summary_file5, summary_file6]

        for result_dirs, summary_file in zip(all_dirs, all_summaries):
            all_csvs = []
            for result_dir in result_dirs:
                all_csvs += sorted([result_dir + "/" + fi for fi in os.listdir(result_dir) if "-{0}.csv".format(tag) in fi])
            count = 0
            with open(summary_file, 'w') as out:
                for res_file in tqdm(all_csvs):
                    with open(res_file, 'r') as inpt:
                        if tag == "result" and count > 0:
                            lines = inpt.readlines()[1:]
                        else:
                            lines = inpt.readlines()
                        out.writelines(lines)
                        count += 1
