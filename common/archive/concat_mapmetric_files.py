import os
from tqdm import tqdm

# tag = "MapMetrics"
#
# result_dirs = [os.getcwd() + "/character_evolution/minimaps_and_map_metrics/"]
# summary_file = os.getcwd() + "/character_evolution/{0}.csv".format(tag)


tag = "both"

result_dirs = ["/home/daniel/Projects/pyprocschooter/df5_baselines"]
summary_file = os.getcwd() + "/character_evolution/MLP_scores.csv"


all_csvs = []
for result_dir in result_dirs:
    all_csvs += sorted([result_dir + "/" + fi for fi in os.listdir(result_dir) if tag in fi])

count = 0
with open(summary_file, 'w') as out:
    for res_file in tqdm(all_csvs):
        with open(res_file, 'r') as inpt:
            lines = inpt.readlines()
            if tag == "MapMetrics":
                lines[1] = "filename," + lines[1]
                lines[2] = res_file.rsplit('/', 1)[1] + "," + lines[2]
                if count > 0:
                    lines = lines[2:]  # skip headers
                else:
                    lines = lines[1:]
            out.writelines(lines)
            count += 1
