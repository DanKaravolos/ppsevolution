import os

folder = os.getcwd() + "/map_evolution/gt_comparison/"
csv1_path = folder + "results_20runs_crs0.2-gt_comp_means-euclid.csv"
csv2_path = folder + "reversed_crs0.2-gt_comp_means-euclid.csv"

result_path = folder + "crs0.2-gt_comp_means-merged.csv"
with open(result_path, 'a') as fout:
    for line in open(csv1_path):
        fout.write(line)

    fin = open(csv2_path)
    fin.next()
    for line in fin:
        fout.write(line)

csv1_path = folder + "results_20runs_crs0.2-gt_comp_per_map-euclid.csv"
csv2_path = folder + "reversed_crs0.2-gt_comp_per_map-euclid.csv"

result_path = folder + "crs0.2-gt_comp_per_map-merged.csv"
with open(result_path, 'a') as fout:
    for line in open(csv1_path):
        fout.write(line)

    fin = open(csv2_path)
    fin.next()
    for line in fin:
        fout.write(line)
