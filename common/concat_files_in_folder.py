'''
You can use this file to concatenate game_input and result files into one file. These files are used for
doing validation runs with the game and computing result metrics (via evaluate_evolved_weapons) respectively.
'''

import os


def parse(folder, keyword):
    print("Searching for {0} in folder: {1} ".format(keyword, folder))
    files = [folder + f for f in os.listdir(folder) if keyword in f and not "summary" in f]
    out_name = folder + "summary-{0}.csv".format(keyword)
    print("Writing to ", out_name)
    with open(out_name, 'w') as out:
        for fi in sorted(files):
            print("Adding ", fi)
            with open(fi, 'r') as inpt:
                lines = inpt.readlines()
                for line in lines:
                    if not line.startswith("tag"):
                        out.write(line)
                        # out.write("\n")
    print("done.")


def parse_mo_folders(folder, keyword):
    print("Searching for {0} in folder: {1} ".format(keyword, folder))
    files = []
    for subs in sorted(os.listdir(folder)):
        if os.path.isdir(folder + subs):
            sub = folder + subs
            for subsub in os.listdir(sub):
                if os.path.isdir(sub + "/" + subsub):
                    sub_fo = sub + "/" + subsub
                    subfiles = [sub_fo + "/" +f for f in sorted(os.listdir(sub_fo)) if keyword in f and not "summary" in f]
                    files.extend(subfiles)

    out_name = folder + "summary-{0}.csv".format(keyword)
    print("Writing to ", out_name)
    with open(out_name, 'w') as out:
        for fi in sorted(files):
            print("Adding ", fi)
            with open(fi, 'r') as inpt:
                lines = inpt.readlines()
                for line in lines:
                    if not line.startswith("tag"):
                        out.write(line)
                        # out.write("\n")
    print("done.")


if __name__ == "__main__":
    here = "/home/daniel/Projects/ppsEvolution/"
    # fol = "/home/daniel/Projects/ppsEvolution/character_evolution/Results/character_evolution/Scout_Heavy_BestParams/"
    # fol = "/home/daniel/Projects/ppsEvolution/character_evolution/Results/character_evolution/Scout_Heavy_MO2/"
    # fol = "/home/daniel/Projects/ppsEvolution/map_evolution/Results/Scout_Heavy_Best/p100/game_input/"
    # fol = "/home/daniel/Projects/ppsEvolution/map_evolution/Results/Scout_Heavy_MO/results/"
    # fol = "/home/daniel/Projects/ppsEvolution/dual_evolution/Results/Scout_Heavy_MO/results/game_input/"

    # fol = here + "MR_Results/character_evolution-MR/ScoutHeavy-SOEA_MR/"
    # fol = here + "MR_Results/character_evolution-MR/ScoutHeavy-MOEA_MR/"
    # fol = here + "character_evolution/Results/character_evolution-MR/ScoutHeavy_SOEA_MR/results/"
    # fol = here + "character_evolution/Results/character_evolution-MR/ScoutHeavy_MOEA_MR/"
    # fol = here + "character_evolution/Results/character_evolution/ScoutHeavy-MOEA_replicate/results/"

    # fol = here + "MR_Results/map_evolution-MR/ScoutHeavy-SOEA_MR/results/"
    # fol = here + "MR_Results/map_evolution-MR/ScoutHeavy_MOEA_MR/results/"

    # fol = here + "MR_Results/dual_evolution-MR/ScoutHeavy-SOEA_MR/results/"
    # fol = here + "MR_Results/dual_evolution-MR/ScoutHeavy-MOEA_MR/results/"
    # fol = here + "map_evolution/Results/ScoutHeavy-SOEA_MR/results/"
    fol = here + "MutationInitTest/ScoutHeavy_SOEA-4Mut/results/"

    # parse(fol, "game_input")
    # parse(fol, "best_ind-result")
    parse(fol, "result")
    # parse_mo_folders(fol, "best_ind-result")
    # parse_mo_folders(fol, "game_input")