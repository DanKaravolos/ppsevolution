from deap import tools


def tourn_selection(individuals, k, tournsize, elites):
    chosen = tools.selBest(individuals, elites)
    chosen.extend(tools.selTournament(individuals, k - elites, tournsize))
    return chosen


def roulette_selection(individuals, k, elites):
    chosen = tools.selBest(individuals, elites)
    chosen.extend(tools.selRoulette(individuals, k - elites))
    return chosen


def elite_tour_selection(individuals, k, tournsize, n_elites):
    elites = tools.selBest(individuals, n_elites)
    plebs = tools.selTournament(individuals, k - n_elites, tournsize)
    return elites, plebs


def elite_roulette_selection(individuals, k, n_elites):
    elites = tools.selBest(individuals, n_elites)
    plebs = tools.selRoulette(individuals, k - n_elites)
    return elites, plebs
