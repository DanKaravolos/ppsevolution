import os
import pickle
from collections import defaultdict
from common.TF2MatchData import TF2MatchSummary, TF2MatchData


def parse_gt_data(gt_file, game_input_file, dims):
    print("Parsing data")
    with open(gt_file, 'rb') as inpt:
        df = pickle.load(inpt)
        gt_df = df.loc[:, dims]
        gt_df['time'] = (gt_df['time'] - 150) / 450
        # gt_df['file'] = gt_df['file']

    map_names = []
    with open(game_input_file, "r") as gi:
        lines = gi.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            map_name = elems[0][1:] if elems[0].startswith("/") else elems[0]
            map_names.append(map_name)
    data_list = [TF2MatchData(dp[0], dp[1:], dims[1:], map_names) for dp in gt_df.values]
    return data_list


def create_per_map_summary(parsed_gt_data, output_file, write):
    print("making per map summary")
    output_file = output_file.replace("evaluation", "per_map_summary")
    data_dict = defaultdict(list)
    summary_dict = defaultdict(TF2MatchSummary)
    for dp in parsed_gt_data:
        data_dict[dp.map].append(dp)

    if write:
        with open(output_file, "w") as out:
            write_header = True
            for data_map, data_list in data_dict.items():
                summary = TF2MatchSummary(data_list)
                if write_header:
                    out.write("map,")
                    out.write(summary.get_header() + "\n")
                    write_header = False
                line = "{0},{1}\n".format(data_map, summary.to_string())
                out.write(line)
                summary_dict[data_map] = summary
    else:
        for data_map, data_list in data_dict.items():
            summary_dict[data_map] = TF2MatchSummary(data_list)
    return summary_dict


def evaluate_original_classes(game_input_file, gt_file, output_file, dims, write=False):
    data = parse_gt_data(gt_file, game_input_file, dims)
    pms = create_per_map_summary(data, output_file, write)
    return pms


def main():
    # gt_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutHeavy/GT_summary-OriginalTF2.pkl"
    # game_input_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutHeavy/tf2_class_scout_vs_heavy-game_input.csv"
    # output_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutHeavy/OriginalTF2_ScoutHeavy-evaluation.csv"

    # gt_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutSniper/GT_summary-OriginalTF2_ScoutSniper.pkl"
    # game_input_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutSniper/tf2_class_scout_vs_sniper-game_input.csv"
    # output_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_ScoutSniper/OriginalTF2_ScoutSniper-evaluation.csv"

    gt_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_HeavySniper/GT_summary-OriginalTF2_HeavySniper.pkl"
    game_input_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_HeavySniper/tf2_class_heavy_vs_sniper-game_input.csv"
    output_file = os.getcwd() + "/Results/character_evolution/OriginalTF2_HeavySniper/OriginalTF2_HeavySniper-evaluation.csv"

    dims = ('file', 'team0ratio', 'time')
    return evaluate_original_classes(game_input_file, gt_file, output_file, dims, write=True)


if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main()
