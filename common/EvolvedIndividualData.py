import numpy as np
from character_evolution.evolution import weapon_parameters as wp
import warnings

bin_edges = {
    "time": [0, 0.28, 0.43, 1.01],
    "team0ratio": [0, 0.14, 0.29, 0.44, 0.59, 0.74, 0.89, 1.01],
    "life_time_t0ratio": [0, 0.11, 0.22, 0.33, 0.44, 0.56, 0.67, 0.78, 0.89, 1],
    "combat_time_t0ratio": [0, 0.09, 0.18, 0.27, 0.36, 0.45, 0.55, 0.64, 0.73, 0.82, 0.91, 1],
    "pos_ent_t0ratio": [0.42, 0.452, 0.484, 0.516, 0.548, 0.58],
    "death_ent_t0ratio": [0, 0.11, 0.22, 0.33, 0.44, 0.56, 0.67, 0.78, 0.89, 1],
    "life_time_mean": [0, 0.03, 0.06, 0.09, 0.12, 0.15],
    "combat_time_mean": [0, 0.03, 0.06, 0.09, 0.12, 0.15],
    "pos_ent_mean": [0.5, 0.57, 0.64, 0.71, 0.78, 0.85],
    "death_ent_mean": [0.15, 0.22, 0.29, 0.36, 0.44, 0.51, 0.58, 0.65],
    "total_pos_ent": [0.7, 0.74, 0.78, 0.82, 0.86, 0.9],
    "total_death_ent": [0.4, 0.47, 0.54, 0.61, 0.68, 0.75]
    }

time_class_names = {0: "Short", 1: "Medium", 2: "Long"}


class EvolvedIndividual:
    def __init__(self, for_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets,
                 damage_class_only=True, orig_c1="", orig_c2="", from_classification=False):
        # input values
        self.cls = from_classification
        self.dmg_only = damage_class_only
        self.fitness = float(fitness)
        self.game_metric_names = game_metric_names
        self.gmn = self.game_metric_names
        self.predictions = [float(p) for p in game_metrics]
        self.prediction_classes = classify_target_values(self.gmn, self.predictions, False, translate=True)
        self.pred_class_centers = compute_class_centers(self.predictions, self.gmn) if self.cls else []
        self.initial_map = for_map
        self.params1 = param_list1
        self.params2 = param_list2
        self.original_class1 = orig_c1
        self.original_class2 = orig_c2
        self.desired_vals = parse_targets(targets) if not isinstance(targets, dict) else targets  # each element looks like this: "target {game_metric}: {float}"
        self.desired_classes = self.get_desired_classes()
        self.des_class_centers = compute_class_centers([self.desired_vals[n] for n in self.gmn], self.gmn) if self.cls else []
        self.p_is_d = [self.desired_classes[self.gmn[idx]] == self.prediction_classes[idx] for idx in range(len(self.gmn))]

        # inferred values
        # self.tf2_classes = wp.tf2_dmg_classes if damage_class_only else sorted(wp.tf2_weapon_param_dict.keys())
        # self.distances = None
        # self.dist_to_originals = ()
        # self.match_dist_to_original = -1
        # self.dist_to_originals_sum = -1
        # self.dist_to_originals_avg = -1
        # self.nearest = ""
        # self.changed_params = []

        # self.infer_parameter_metrics()

        # GT input values via method
        self.has_gt = False
        self.values = []
        self.means = []
        self.stds = []

        # GT inferred values
        self.intervals = []
        self.diff_between_a_p = []
        self.diff_between_a_d = []
        self.diff_between_p_d = self.compute_pred_error_from_desired()
        self.a_is_d = []
        self.a_is_p = []
        self.actual_classes = []
        self.pred_within_ci = []

    def add_gt_values(self, measurements):
        if len(measurements) < 1:
            warnings.warn("Len(measurements) < 1. Cannot compute GT mean. {0}, {1}".format(self.initial_map, self.desired_classes))
            return
        # GT input values via method
        self.values = measurements   # if not self.cls else classify_measurements(self.game_metric_names, measurements)
        self.means = np.mean(self.values, axis=0)
        self.stds = np.std(self.values, axis=0)
        self.intervals = self.compute_ci()
        self.diff_between_a_p = self.compute_prediction_error()
        self.diff_between_a_d = self.compute_error_from_desired()
        self.actual_classes = classify_target_values(self.game_metric_names, self.means, self.cls)
        self.pred_within_ci = self.compute_pred_within_ci()
        self.a_is_d = [self.desired_classes[self.gmn[idx]] == self.actual_classes[idx] for idx in range(len(self.gmn))]
        self.a_is_p = [self.actual_classes[idx] == self.prediction_classes[idx] for idx in range(len(self.gmn))]

        self.has_gt = True

    # this computes the 95% confidence interval
    def compute_ci(self):
        if len(self.values) == 0:
            return []
        z_factor = 1.96  # change this if you want another conf.interval
        cis = []
        sq_sample_size = np.sqrt(self.values.shape[0])
        for dim in range(self.values.shape[1]):
            margin = z_factor * self.stds[dim] / sq_sample_size
            cis.append([self.means[dim] - margin, self.means[dim] + margin])
        return cis

    def compute_pred_within_ci(self):
        return [self.intervals[i][0] <= self.predictions[i] < self.intervals[i][1]
                for i in range(len(self.predictions))]

    def compute_prediction_error(self):
        if self.cls:
            return [np.abs(self.pred_class_centers[i] - self.means[i]) for i in range(len(self.means))]
        else:
            try:
                return [np.abs(self.predictions[i] - self.means[i]) for i in range(len(self.means))]
            except TypeError:
                print("WTF")

    def compute_error_from_desired(self):
        if self.cls:
            return [np.abs(self.des_class_centers[i] - self.means[i]) for i in range(len(self.means))]
        else:
            return [np.abs(self.desired_vals[self.game_metric_names[i]] - self.means[i]) for i in range(len(self.means))]

    def compute_pred_error_from_desired(self):
        if self.cls:
            return [np.abs(self.des_class_centers[i] - self.pred_class_centers[i]) for i in range(len(self.predictions))]
        else:
            return [np.abs(self.desired_vals[self.game_metric_names[i]] - self.predictions[i]) for i in range(len(self.predictions))]

    def get_desired_classes(self):
        if not self.cls:
            return classify_desired_values(self.game_metric_names, self.desired_vals)
        else:
            desired_classes = {}
            for target in self.game_metric_names:
                val = self.desired_vals[target]
                if target == "time":
                    desired_classes[target] = time_class_names[val]
                elif target == "team0ratio":
                    desired_classes[target] = ("Balanced" if val == 3 else str(val))  # "Unbalanced"
            return desired_classes

    def get_header(self, external_line_number=True):
        # tf2_classes1 = ",".join("dist P1 {0}".format(t) for t in self.tf2_classes)
        # tf2_classes2 = ",".join("dist P2 {0}".format(t) for t in self.tf2_classes)
        if external_line_number:
            header = "num,map,"
        else:
            header = "map,"
        predictions = ["p {0}".format(p) for p in self.game_metric_names]
        p_classes = ["p class {0}".format(p) for p in self.game_metric_names]
        desired = ["d {0}".format(d) for d in self.game_metric_names]
        desired_classes = ["d class {0}".format(d) for d in self.game_metric_names]
        p_is_d = ["p==d {0}".format(d) for d in self.game_metric_names]
        header = header + ",".join(predictions + desired + p_classes + desired_classes)

        if self.has_gt:
            gt_text = ["{0} mean, {0} std, {0} CI low, {0} CI high".format(d) for d in self.game_metric_names]
            dev_text = ["{0} abs(a-p), {0} abs(a-d), {0} abs(p-d)".format(d) for d in self.game_metric_names]
            true_classes = ["a class {0}".format(d) for d in self.game_metric_names]
            within_ci = ["{0} within CI".format(d) for d in self.game_metric_names]
            a_is_d = ["a==d {0}".format(d) for d in self.game_metric_names]
            a_is_p = ["a==p {0}".format(d) for d in self.game_metric_names]
            header = header + "," + ",".join(gt_text + within_ci + true_classes + dev_text + a_is_d + a_is_p)
        else:
            header = header + "," * 22
        header = header + "," + ",".join(p_is_d)
        return header

    def to_string(self, add_new_line=True):
        predictions = ["{0:.4f}".format(p) for p in self.predictions]
        p_classes = [p if isinstance(p, str) else str(p) for p in self.prediction_classes]
        desired = ["{0}".format(self.desired_vals[k]) for k in self.game_metric_names]
        desired_classes = ["{0}".format(self.desired_classes[k]) for k in self.game_metric_names]
        pd = [str(eq) for eq in self.p_is_d]
        line = ",".join([e for e in [self.initial_map] + predictions + desired + p_classes + desired_classes])

        if self.has_gt:
            gt_vals = ["{0:.4f},{1:.4f},{2:.4f},{3:.4f}".format(self.means[i], self.stds[i], *self.intervals[i])
                       for i in range(len(self.game_metric_names))]
            dev_vals = ["{0:.4f},{1:.4f},{2:4f}".format(self.diff_between_a_p[i], self.diff_between_a_d[i],
                        self.diff_between_p_d[i]) for i in range(len(self.game_metric_names))]
            true_classes = [p if isinstance(p, str) else str(p) for p in self.actual_classes]
            within_ci = [str(answer) for answer in self.pred_within_ci]
            ad = [str(eq) for eq in self.a_is_d]
            ap = [str(eq) for eq in self.a_is_p]
            line = line + "," + ",".join([e for e in gt_vals + within_ci + true_classes + dev_vals + ad + ap])
        else:
            line = line + "," * 22
        line = line + "," + ",".join(pd)
        if add_new_line:
            line += "\n"
        return line


def parse_targets(targets, return_list=False):  # each element looks like this: "target {game_metric}: {float}"
    if return_list:
        return [float(t.split(":")[1]) for t in targets]

    desired_dict = {}
    for t in targets:
        elems = t.split(":")
        key = elems[0].replace("target ", "")
        desired_dict[key] = float(elems[1])
    return desired_dict


def classify_target_values(game_metrics, values_to_classify, classification_based, translate=True):
    if classification_based:
        return classify_target_values_classification(game_metrics, values_to_classify, translate=translate)
    else:
        return classify_target_values_regression(game_metrics, values_to_classify, translate=translate)


def classify_target_values_classification(game_metrics, values_to_classify, translate=True):
    classes = []
    for target in range(len(game_metrics)):
        p = values_to_classify[target]
        p_class = round(p)
        if translate:
            if game_metrics[target] == "time":
                classes.append(time_class_names[p_class])
            elif game_metrics[target] == "team0ratio":
                name = "Balanced" if p_class == 3 else str(p_class)  # "Unbalanced"
                classes.append(name)
            else:
                classes.append(p_class)
        else:
            classes.append(p_class)
    return classes


def classify_target_values_regression(game_metrics, values_to_classify, translate=True):
    classes = []
    for target in range(len(game_metrics)):
        p = values_to_classify[target]
        metric = game_metrics[target]
        bins = bin_edges[metric]
        p_class = 0 if p <= bins[0] else len(bins) - 2  # -2 because: -1 for last element and -1 for starting at 0.
        for b in range(len(bins) - 1):
            if bins[b] < p <= bins[b + 1]:
                p_class = b
                break
        if translate:
            if metric == "time":
                classes.append(time_class_names[p_class])
            elif metric == "team0ratio":
                name = "Balanced" if p_class == 3 else str(p_class)  # "Unbalanced"
                classes.append(name)
            else:
                classes.append(p_class)
        else:
            classes.append(p_class)
    return classes


def classify_desired_values(game_metrics, desired_value_dict, translate=True):
    classes = {}
    for game_metric in game_metrics:
        p = desired_value_dict[game_metric]
        bins = bin_edges[game_metric]
        p_class = 0 if p <= bins[0] else len(bins) - 2  # -2 because: -1 for last element and -1 for starting at 0.
        for b in range(len(bins) - 1):
            if bins[b] < p <= bins[b + 1]:
                p_class = b
                break
        if translate:
            if game_metric == "time":
                classes[game_metric] = (time_class_names[p_class])
            elif game_metric == "team0ratio":
                name = "Balanced" if p_class == 3 else str(p_class)  # "Unbalanced"
                classes[game_metric] = name
            else:
                classes[game_metric] = p_class
        else:
            classes[game_metric] = p_class
    return classes


# Initially, I was computing the difference between predictions, actual and desired for classification in class space
# But then you cannot compare classification with regression, so I now compute class centers and pretend classification
# is the same as regression
def classify_measurements(game_metrics, measurements):
    classes = []
    for target in range(len(game_metrics)):
        classified_target = []
        m_per_target = measurements[:, target]
        for m in m_per_target:
            metric = game_metrics[target]
            bins = bin_edges[metric]
            m_class = 0 if m <= bins[0] else len(bins) - 2  # -2 because: -1 for last element and -1 for starting at 0.
            for b in range(len(bins) - 1):
                if bins[b] < m <= bins[b + 1]:
                    m_class = b
                    break
            classified_target.append(m_class)
        classes.append(classified_target)
    classes = np.asarray(classes).transpose()
    return classes


def compute_class_centers(list_of_classes, target_names):
    centers = []
    for idx in range(len(list_of_classes)):
        edges = bin_edges[target_names[idx]]
        # fc_val = list_of_classes[idx]
        c_val = int(list_of_classes[idx])
        try:
            center = (edges[c_val] + edges[c_val + 1]) / 2
        except IndexError:
            print("IndexError in edges of {0}. Index {1} does not exist".format(target_names[idx], c_val + 1))
            center = edges[c_val]
        centers.append(center)
    return centers


