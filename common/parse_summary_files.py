from __future__ import print_function
import pandas as pd
import os
from os import path
import csv
from itertools import compress
import numpy as np


def get_files_in_folder(data_folder, keyword):
    data_folder_names = [folder for folder in os.listdir(data_folder) if path.isdir(path.join(data_folder, folder))]
    folder_file_dict = {}
    for folder_name in data_folder_names:
        folder = path.join(path.join(data_folder, folder_name), "LogFiles")
        sub_folders = [sub for sub in os.listdir(folder) if path.isdir(path.join(folder, sub))]
        file_list = []
        for sub in sub_folders:
            spath = os.path.join(folder, sub)
            files = [path.join(spath, f) for f in os.listdir(spath) if path.isfile(path.join(spath, f))]
            keyword_files = [f for f in files if keyword in f]
            file_list.extend(keyword_files)
        folder_file_dict[folder_name] = file_list
    return data_folder_names, folder_file_dict


def parse_sum_file(folder, fi):
    with open(fi, 'r') as infile:
        csv_res = csv.reader(infile, delimiter=',')
        for row in csv_res:
            if row[0] == 'Seed':
                seed = int(row[1])
            elif row[0] == 'TotalTime':
                time = int(float(row[1]))
            elif row[0] == 'Team0Kills':
                team0ratio = float(row[2])
                if np.isnan(team0ratio):
                    print("Team0Ratio is NaN. Returning []")
                    return []
            elif row[0] == 'Team1Kills':
                team1ratio = float(row[2])
            elif row[0] == 'Team0Params':
                team0params = [float(par) for par in row[1:] if par != '']
            elif row[0] == 'Team1Params':
                team1params = [float(par) for par in row[1:] if par != '']
            elif row[0] == 'TotalKills':  # safety. but this should not occur...
                total_kills = int(row[1])
                if total_kills != 20:
                    # print(fi, " does not have 20 kills, but I will allow it for now...")
                    # print("TotalKills({0}) != 20. Time {1}. for {2}".format(total_kills, time, fi))
                    return []

    if len(team0params) != 8:
        # print("wtf team0?")
        team0params = team0params[:8]
    if len(team1params) != 8:
        # print("Wtf team 1?")
        team1params = team1params[:8]
    return [folder, fi, seed, time, team0ratio, team1ratio, *team0params, *team1params, team0params, team1params]


def parse_summary_files(folder, file_dict, players_switch_sides):
    files = file_dict[folder]
    sum_data = []
    nr_unparsed = 0
    # Collect all data. Games that did not end will have an empty data list
    mask = []
    level_nums = []
    should_remove = set()
    for i in range(len(files)):
        fi = files[i]
        level_num = int(fi.rsplit("_", 4)[1])
        level_nums.append(level_num)
        parsed = parse_sum_file(folder, fi)
        if parsed:
            sum_data.append(parsed)
            mask.append(True)
        else:
            sum_data.append(parsed) # add for filtering later.
            mask.append(False)
            nr_unparsed += 1
            if level_num % 2 == 0:
                should_remove.add(level_num)
                should_remove.add(level_num - 1)
            else:
                should_remove.add(level_num)
                should_remove.add(level_num + 1)

    sort_sum_data = sorted(zip(level_nums, sum_data, mask), key=lambda x: x[0])
    level_nums, sum_data, mask = zip(*sort_sum_data)
    sum_data = list(sum_data)
    mask = list(mask)
    if players_switch_sides:
        # Filter out datapoints that should be removed, because their counterpart does not exist
        # sort_sum_data = sorted(zip(level_nums, sum_data, mask), key=lambda x: x[0])
        # level_nums, sum_data, mask = zip(*sort_sum_data)
        # sum_data = list(sum_data)
        # mask = list(mask)

        # removed code:
        # for i in range(len(level_nums) - 1):
        #     # if level_nums[i] != sort_sum_data[i][0]:
        #     #     print("Error while filtering bad data. {0} is not {1}.".format(i, sort_sum_data[i][0]))
        #     if level_nums[i] % 2 == 1:
        #         if not mask[i] and mask[i + 1]:
        #             mask[i + 1] = False
        #     elif level_nums[i] % 2 == 0:
        #         if not mask[i] and mask[i - 1]:
        #             mask[i - 1] = False
        for i in sorted(should_remove):
            # print(i)
            mask[i - 1] = False

        # if we ended with an uneven level number, don't use it, because we do not have its counterpart
        if level_nums[-1] % 2 == 1:
            mask[len(level_nums) - 1] = False
            should_remove.add(len(level_nums) - 1)
            print("Removing last datapoint because uneven. This should not occur.")
        elif level_nums[-1] % 2 == 0 and mask[-1] and not mask[-2]:
            mask[len(level_nums) - 1] = False
            should_remove.add(len(level_nums) - 1)
            print("Removing last datapoint because the one before last was bad")

    # apply mask to data and compute what we lost
    clean_data = list(compress(sum_data, mask))
    # removed_data = list(compress(sum_data, np.invert(mask)))
    nr_removed = len(files) - len(clean_data)
    # should_have_removed = len(should_remove)
    # print("removed: {0}, should have removed: {1}".format(nr_removed, should_have_removed))
    return clean_data, nr_unparsed, nr_removed


def create_datafile(data_list):
    df = pd.DataFrame(data_list, columns=['folder', 'file', 'seed', 'time', 'team0ratio', 'team1ratio',
                                          't0_hp', 't0_speed', 't0_damage', 't0_acc', 't0_clip', 't0_rof', 't0_bullets',
                                          't0_range',
                                          't1_hp', 't1_speed', 't1_damage', 't1_acc', 't1_clip', 't1_rof', 't1_bullets',
                                          't1_range',
                                          't0', 't1'])
    df.to_pickle(os.path.join(data_folder, "GT_summary-{0}.pkl".format(data_folder_name)))
    df.to_csv(os.path.join(data_folder, "GT_summary-{0}.csv".format(data_folder_name)))


def main(folder, players_switch_sides):
    print("Getting Files...")
    folders, folder_file_dict = get_files_in_folder(folder, "Summary")
    data_list = []
    nr_collected_total = 0
    nr_unparsed_total = 0
    nr_removed_total = 0
    for fold in folders:
        print("Parsing {0}...".format(fold))
        data, nr_unparsed, nr_removed = parse_summary_files(fold, folder_file_dict, players_switch_sides)
        nr_collected = len(data)
        data_list.extend(data)
        print("collected: {0}, unparsed: {1}, removed: {2}".format(nr_collected, nr_unparsed, nr_removed))
        nr_collected_total += nr_collected
        nr_unparsed_total += nr_unparsed
        nr_removed_total += nr_removed
    create_datafile(data_list)
    return nr_collected_total, nr_unparsed_total, nr_removed_total


if __name__ == "__main__":
    # Thesis way:
    # data_folder_name = "OriginalTF2"
    # data_folder_name = "ScoutHeavy_BestParams"
    # data_folder_name = "ScoutHeavy_BestParamsNewBuild"
    # data_folder_name = "OriginalTF2"
    # data_folder_name = "OriginalTF2_ScoutSniper"
    # data_folder_name = "OriginalTF2_HeavySniper"
    # data_folder_name = "Tog-P100-SH_MapGeneration"
    # data_folder_name = "Tog-P100-MO-MapGen"
    # data_folder_name = "Orchestration"
    # data_folder_name = "Orchestration-MOEA"
    # data_folder = os.path.join(os.getcwd(), "data", data_folder_name)
    # minor revisions:
    # data_folder_name = "map_evolution-SOEA-GT"
    # data_folder_name = "dual_evolution-SOEA-GT"
    # data_folder_name = "character_evolution-SOEA-GT"
    # data_folder_name = "character_evolution-MOEA-GT"
    # data_folder = "/home/daniel/Projects/ppsEvolution/character_evolution/Results/character_evolution-MR/{0}/".format(data_folder_name)

    # data_folder_name = "dual_evolution-MOEA-GT"
    # data_folder = os.getcwd() + "/MR_Results/dual_evolution-MR/{0}/".format(data_folder_name)

    data_folder_name = "Mut0-GT"
    data_folder = os.getcwd() + "/MutationInitTest/{0}/".format(data_folder_name)

    remove_data_counterparts = False
    num_collected, num_unparsed, num_removed = main(data_folder, remove_data_counterparts)
    total = num_collected + num_removed
    print("Total files: {0}, unparsed: {1} ({2:.2f}), removed: {3} ({4:.2f}), remaining: {5} ({6:.2f})".format(total,
          num_unparsed, num_unparsed / total,
          num_removed, num_removed / total,
          num_collected, num_collected / total))
