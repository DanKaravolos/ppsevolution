import numpy as np
import os
import re
from tqdm import *


def csv_to_array(csv_file, map_starts_at_first_line=False):  # evolved maps have no line with seed, but start immediately at the first line
    row_list = []
    with open(csv_file) as f:
        content = f.readlines()
        content_lines = content if map_starts_at_first_line else content[1:]
        for line in content_lines:
            if not line.startswith('-'):
                my_row = re.split('[,|]', line.rstrip("\r\n"))
                row_list.append(my_row)
    return np.asarray(row_list)


def parse_array(array, neighbor_val=0):
    if neighbor_val != 0:
        print("WARNING! neighborhood is not set to 0! Why?! This must be old code! Neighborhood is not used anymore!!!")
    ground_map = np.zeros(array.shape)
    first_floor_map = np.zeros(array.shape)
    second_floor_map = np.zeros(array.shape)
    hp_map = np.zeros(array.shape)
    armor_map = np.zeros(array.shape)
    dd_map = np.zeros(array.shape)
    cover_map = np.zeros(array.shape)
    stair_map = np.zeros(array.shape)
    for x in range(array.shape[0]):
        for y in range(array.shape[1]):
            # geometry maps
            if "0" in array[x, y]:
                ground_map[x, y] = 1
            if "1" in array[x, y]:
                first_floor_map[x, y] = 1
            if "2" in array[x, y]:
                second_floor_map[x, y] = 1
    # pickups in a second loop to take into account closed cells of geometry for influence map
    for x in range(array.shape[0]):
        for y in range(array.shape[1]):
            # pickup maps
            if "A" in array[x, y]:
                armor_map[x, y] = 1
                # paint_neighborhood(armor_map, np.array([x, y]), second_floor_map, neighbor_val)
            if "C" in array[x, y]:
                cover_map[x, y] = 1
                # paint_neighborhood(cover_map, np.array([x, y]), second_floor_map, neighbor_val)
            if "D" in array[x, y]:
                dd_map[x, y] = 1
                # paint_neighborhood(dd_map, np.array([x, y]), second_floor_map, neighbor_val)
            if "H" in array[x, y]:
                hp_map[x, y] = 1
                # paint_neighborhood(hp_map, np.array([x, y]), second_floor_map, neighbor_val)
            if "S" in array[x, y]:
                stair_map[x, y] = 1

    full_map = np.stack((ground_map, first_floor_map, second_floor_map, armor_map, cover_map, dd_map, hp_map, stair_map), axis=-1)
    return full_map


def csv_to_npy(csv_file):
    # print("powerup_neighbor_value:", powerup_neighbor_value)
    asci_array = csv_to_array(csv_file)
    full_map = parse_array(asci_array)
    return full_map


def parse_map_files(map_files):
    npy_maps = [csv_to_npy(map_file) for map_file in tqdm(map_files)]
    return np.asarray(npy_maps)


def parse_map():
    single_map = os.getcwd() + "/data/maps/custom_maps/Custom_9_Run_0_Map.csv"
    npy_map = csv_to_npy(single_map)
    np.save(single_map.replace(".csv", ".npy"), npy_map)


def parse_folder(folder, key="_Map", save_per_file=False):
    all_csvs = [folder + "/" + fi for fi in os.listdir(folder) if key in fi]
    if save_per_file:
        for single_map in tqdm(all_csvs):
            npy_map = csv_to_npy(single_map)
            np.save(single_map.replace(".csv", ".npy"), npy_map)
    else:
        np_maps = parse_map_files(all_csvs)
        np.save(folder + ".npy", np_maps)


if __name__ == "__main__":
    parse_map()
    # fo = os.getcwd() + "/data/maps/pcg_workshop/"
    # fo = os.getcwd() + "/data/maps/"
    fo = os.getcwd() + "/data/maps/custom_maps/"
    # parse_folder(fo)

