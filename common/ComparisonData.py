import numpy as np

# evolved_data = EvolvedIndividualSummary
# tf_data = TFMatchData


class ComparisonData:
    def __init__(self, evolved_data, tf_data):
        self.ea_data = evolved_data
        self.tf_data = tf_data
        self.gmn = self.ea_data.game_metric_names

        # Values for GT improvement
        self.ea_actual = self.ea_data.actual
        self.tf_actual = self.tf_data.game_metric_data
        self.desired = [evolved_data.desired[key] for key in sorted(evolved_data.desired.keys())]

        self.tf_distance_to_target, self.tf_distances = self.compute_distance(self.tf_actual, self.desired)
        self.ea_distance_to_target, self.ea_distances = self.compute_distance(self.ea_actual, self.desired)
        self.improvement = self.compute_improvement()
        self.per_metric_improvements = self.compute_per_metric_improvement()

        self.most_improved = self.compute_most_improved_run()
        self.breakpoint = ""

    def compute_distance(self, actual, desired):
        # abs_dist = np.asarray([np.abs(actual[i] - desired[i]) for i in range(len(desired))])
        euc_dist = np.sqrt(sum([((actual[i] - desired[i]) ** 2) for i in range(len(desired))]))
        euc_distances = [np.sqrt(((actual[i] - desired[i]) ** 2)) for i in range(len(desired))]
        return euc_dist, euc_distances

    def compute_improvement(self):
        # rel_change = [(self.tf_distance_to_target[t] - self.ea_distance_to_target[t]) / self.tf_distance_to_target[t] for t in range(len(self.desired))]
        rel_change = (self.tf_distance_to_target - self.ea_distance_to_target) / self.tf_distance_to_target
        return rel_change

    def compute_per_metric_improvement(self):
        rel_change = [(self.tf_distances[t] - self.ea_distances[t]) / self.tf_distances[t] for t in range(len(self.desired))]
        return rel_change

    def compute_most_improved_run(self):
        # print("test")
        improvements_per_run = []
        for run in self.ea_data.data:
            actual = run.means
            run_dist, _ = self.compute_distance(actual, self.desired)
            run_improvement = (self.tf_distance_to_target - run_dist) / self.tf_distance_to_target
            improvements_per_run.append(run_improvement)
        most_improved = np.argmax(improvements_per_run)
        # print("TEST - most improved: {0}, improvement: {1}".format(most_improved, improvements_per_run[most_improved]))
        return most_improved

    def get_header(self):
        header = ["total improvement", "most improved run"]
        elems = ["{0} improvement, evolved dist to target {0}, initial dist to target {0}, evolved {0}, initial TF2 {0}".format(metric)
                 for metric in self.gmn]
        str_line = ",".join(header + elems)
        return str_line

    def to_string(self):
        nums = [self.improvement, self.most_improved]
        for i,  ea_dist, tf_dist, ea_ac, tf_ac in zip(self.per_metric_improvements, self.ea_distances, self.tf_distances,
                                                     self.ea_actual, self.tf_actual):
            nums.extend([i, ea_dist, tf_dist, ea_ac, tf_ac])
        str_nums = ["{0:.2f}".format(n) for n in nums]
        str_line = ",".join(str_nums)
        return str_line
