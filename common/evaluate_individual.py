from collections import defaultdict
# from common.EvolvedIndividualSummary import EvolvedIndividualSummary


def create_per_duration_summary(parsed_ea_inds, output_file, summary_class, write=True):
    print("Creating per duration summary")
    output_file = output_file.replace("evaluation", "per_duration_summary")
    data_dict = {"Short": [], "Medium": [], "Long": []}
    summary_dict = defaultdict(summary_class)
    for dp in parsed_ea_inds:
        data_dict[dp.desired_classes["time"]].append(dp)

    for duration, data_list in data_dict.items():
        summary_dict[duration] = summary_class(data_list)

    if write:
        with open(output_file, "w") as out:
            write_header = True
            for duration in data_dict.keys():
                summary = summary_dict[duration]
                if write_header:
                    out.write("Desired duration,")
                    out.write(summary.get_header() + "\n")
                    write_header = False
                line = "{0},{1}\n".format(duration, summary.to_string())
                out.write(line)
    else:
        print("Not writing to file.")
    return summary_dict


def create_per_map_summary(parsed_ea_inds, output_file, summary_class, write=True):
    print("Creating per map summary")
    output_file = output_file.replace("evaluation", "per_map_summary")
    data_dict = defaultdict(list)
    summary_dict = defaultdict(summary_class)
    for dp in parsed_ea_inds:
        data_dict[dp.initial_map].append(dp)

    for data_map, data_list in data_dict.items():
        summary_dict[data_map] = summary_class(data_list)

    if write:
        with open(output_file, "w") as out:
            write_header = True
            for data_map, data_list in data_dict.items():
                summary = summary_dict[data_map]
                if write_header:
                    out.write("map,")
                    out.write(summary.get_header() + "\n")
                    write_header = False
                line = "{0},{1}\n".format(data_map, summary.to_string())
                out.write(line)
    else:
        print("Not writing to file.")
    return summary_dict


def create_per_duration_per_map_summary(parsed_ea_inds, output_file, summary_class, write=True):
    print("Creating per duration per map summary")
    output_file = output_file.replace("evaluation", "per_duration_per_map_summary")
    data_dict = defaultdict(list)
    summary_dict = defaultdict(dict)
    for dp in parsed_ea_inds:
        data_dict[dp.initial_map].append(dp)

    for data_map, data_list in data_dict.items():
        duration_dict = {"Short": [], "Medium": [], "Long": []}
        # organize data into time dict
        for dp in data_list:
            duration_dict[dp.desired_classes["time"]].append(dp)
        # summarize data in each time dict
        for duration, duration_list in duration_dict.items():
            duration_dict[duration] = summary_class(duration_list)
        summary_dict[data_map] = duration_dict

    if write:
        with open(output_file, "w") as out:
            write_header = True
            for data_map in data_dict.keys():
                for duration in duration_dict.keys():
                    summary = summary_dict[data_map][duration]
                    if write_header:
                        out.write("map, duration,")
                        out.write(summary.get_header() + "\n")
                        write_header = False
                    line = "{0},{1},{2}\n".format(data_map, duration, summary.to_string())
                    out.write(line)
    else:
        print("Not writing to file.")
    return summary_dict


def write_output(ea_inds, out_file):
    with open(out_file, 'w') as out:
        count = 1
        header = ea_inds[0].get_header()
        out.write(header + "\n")
        for ea_cls in ea_inds:
            line = ea_cls.to_string()
            out.write("{0},{1}".format(count, line))
            count += 1