from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import keras.backend as K
from keras.preprocessing import image
import cv2
import PIL.Image as pim
import os

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # use a specific GPU
print("using GPU " + os.environ["CUDA_VISIBLE_DEVICES"])

all_params = ["HP", "Speed", "Damage", "Acc", "ClipSize", "RateOfFire", "NrBullets", "Range",
              "HP", "Speed", "Damage", "Acc", "ClipSize", "RateOfFire", "NrBullets", "Range"]

# This function requires a compiled model and the path to the image.
# You can specify the layer you want to visualize the heatmap for or let the function search for the last "conv" layer.
# Additionally, you can specify your own preprocessed image, whether or not to show the heatmap separately
# and whether or not to save the image + heatmap (and where) and how intense the heatmap overlay should be.
# If no location is specified, '{image_path}-CAM.png' will be used.
# You can also enlarge or shrink the final heatmap output
def make_cam(model, image_path, weapons, layer_name="", input_image=None, show_heatmap=True,
             save_image=True, target_path="", intensity=0.4, enlarge_factor=1, class_index=-1):

    # if image is not specified, load image from disc
    if input_image is None:
        input_image = image.load_img(image_path)
        input_image = image.img_to_array(input_image) / 255.0
        input_image = np.expand_dims(input_image, axis=0)

    # if no layer_name is specified, find the last "conv" layer
    if not layer_name:
        c_layers = [layer.name for layer in model.layers if "conv" in layer.name]
        layer_name = c_layers[-1]

    # get classification
    if class_index < 0:
        preds = model.predict([weapons, input_image])
        class_index = np.argmax(preds[0])
        print("\npredicted: ", class_index)
        print("output 0 (useful regression): ", preds[0])
    else:
        print("visualizing specific class index:", class_index)
    # This is the model output
    class_output = model.output[:, class_index]

    # This the output feature map of the layer with 'layer_name'
    last_conv_layer = model.get_layer(layer_name)
    print("Computing activation map of {}".format(layer_name))

    # This is the gradient of the most likely class with regard to
    # the output feature map of `block5_conv3`
    grads = K.gradients(class_output, last_conv_layer.output)[0]
    nr_channels = grads.shape[-1]

    # This is a vector of shape (nr_channels,), where each entry
    # is the mean intensity of the gradient over a specific feature map channel
    pooled_grads = K.mean(grads, axis=(0, 1, 2))

    # This function allows us to access the values of the quantities we just defined:
    # `pooled_grads` and the output feature map of `layer_name`,
    # given a sample image
    iterate = K.function([model.input[0], model.input[1]], [pooled_grads, last_conv_layer.output[0]])

    # These are the values of these two quantities, as Numpy arrays,
    # given our sample image of two elephants
    pooled_grads_value, conv_layer_output_value = iterate([weapons, input_image])

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the elephant class
    for i in range(nr_channels):
        conv_layer_output_value[:, :, i] *= pooled_grads_value[i]

    # The channel-wise mean of the resulting feature map
    # is our heatmap of class activation
    heatmap = np.mean(conv_layer_output_value, axis=-1)

    # for visualization, normalize to [0,1]
    heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(heatmap) + 1e-5

    plt.matshow(heatmap)
    # plt.matshow(heatmap, cmap="viridis")
    plt.savefig(target_path + "-{0}-c_{1}-heatmap.png".format(layer_name, class_index))
    if show_heatmap:
        plt.show()

    if save_image:
        if not target_path:
            target_path = image_path.rsplit('.', 1)[0]
        target_path += "-{0}-c_{1}".format(layer_name, class_index)
        # save_cam_image_from_image_path(heatmap, image_path, target_path, intensity, enlarge_factor)
        save_cam_image_from_image_path(heatmap, img_path, target_path, intensity, enlarge_factor)


def make_weapon_cam(model, image_path, weapons, weapon_layer_name="", input_image=None,
    show_heatmap=True, class_index=-1):

    # if image is not specified, load image from disc
    if input_image is None:
        input_image = image.load_img(image_path)
        input_image = image.img_to_array(input_image) / 255.0
        input_image = np.expand_dims(input_image, axis=0)

    # if no layer_name is specified, find the first "dense" layer
    if not weapon_layer_name:
        d_layers = [layer.name for layer in model.layers if "dense" in layer.name]
        weapon_layer_name = d_layers[0]

    # get classification
    if class_index < 0:
        preds = model.predict([weapons, input_image])
        class_index = np.argmax(preds[0])
        print("\npredicted: ", class_index)
        print("output 0 (useful regression): ", preds[0])
    else:
        print("visualizing specific class index:", class_index)
    # This is the model output
    class_output = model.output[:, class_index]

    # This the output feature map of the layer with 'layer_name'
    weapon_layer = model.get_layer(weapon_layer_name)
    print("Computing activation map of {}".format(weapon_layer_name))

    # This is the gradient of the most likely class with regard to
    # the output feature map of `block5_conv3`
    grads = K.gradients(class_output, weapon_layer.output)[0]
    nr_channels = grads.shape[-1]

    # This is a vector of shape (nr_channels,), where each entry
    # is the mean intensity of the gradient over a specific feature map channel
    pooled_grads = K.mean(grads, axis=(0, 1))

    # This function allows us to access the values of the quantities we just defined:
    # `pooled_grads` and the output feature map of `layer_name`,
    # given a sample image
    iterate = K.function([model.input[0], model.input[1]], [pooled_grads, weapon_layer.output[0]])

    # These are the values of these two quantities, as Numpy arrays,
    # given our sample image of two elephants
    pooled_grads_value, w_layer_output_value = iterate([weapons, input_image])

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the elephant class
    for i in range(nr_channels):
        w_layer_output_value[i] *= pooled_grads_value

    # The channel-wise mean of the resulting feature map
    # is our heatmap of class activation
    heatmap = w_layer_output_value
    # for visualization, normalize to [0,1]
    # heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(np.abs(heatmap)) + 1e-5
    print("normalized gradients: ", ["{:.2f}".format(h) for h in heatmap])
    if show_heatmap:
        plt.figure()
        plt.bar(xrange(len(heatmap)), heatmap)
        ax = plt.gca()
        plt.grid()
        ax.set_xticks(xrange(len(heatmap)))
        ax.set_xticklabels(all_params, rotation=45, fontsize=12)
        plt.show()


def save_cam_image(heatmap, img, target_path="", heatmap_intensity=0.4, enlarge_factor=1):
    # We use cv2 to load the original image
    # arr = np.asarray(img * 255,dtype=np.uint8)
    timg = image.array_to_img(img, scale=True)
    timg.save("./cam_temp.png")

    img = cv2.imread("./cam_temp.png")

    # We resize the heatmap to have the same size as the original image
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))

    # We convert the heatmap to RGB
    heatmap = np.uint8(255 * heatmap)

    # We apply the heatmap to the original image
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)

    # 0.4 here is a heatmap intensity factor
    superimposed_img = heatmap * heatmap_intensity + img

    # resize based on user input
    superimposed_img = cv2.resize(superimposed_img, (enlarge_factor * img.shape[1], enlarge_factor * img.shape[0]))

    # Save the image to disk
    target_path = target_path + "-CAM.png"
    cv2.imwrite(target_path, superimposed_img)
    # cv2.imshow("Superimposed", superimposed_img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()


def save_cam_image_from_image_path(heatmap, img_path, target_path="", heatmap_intensity=0.4, enlarge_factor=1):
    # We use cv2 to load the original image
    # img = cv2.imread(img_path)
    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    # We resize the heatmap to have the same size as the original image
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))

    # We convert the heatmap to RGB
    heatmap = np.uint8(255 * heatmap)

    # We apply the heatmap to the original image
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    # heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_WINTER)

    # heatmap_intensity (orig =0.4) here is a heatmap intensity factor
    superimposed_img = heatmap * heatmap_intensity + 0.25 * img

    # resize based on user input
    superimposed_img = cv2.resize(superimposed_img, (enlarge_factor * img.shape[1], enlarge_factor * img.shape[0]),
                                  interpolation=cv2.INTER_NEAREST)

    # Save the image to disk
    target_path = target_path + "-CAM.png"
    cv2.imwrite(target_path, superimposed_img)


def get_class_from_file(path, index):
    with open(path, 'r') as inpt:
        lines = inpt.readlines()
        class_line = lines[index]
        param_arr = class_line.rstrip().split(",")
        param_arr = [float(p) for p in param_arr]
        param_arr = np.asarray(param_arr)
        param_arr = np.expand_dims(param_arr, axis=0)
    return param_arr


if __name__ == "__main__":
    # example usage with normal network
    from keras.models import load_model
    K.set_learning_phase(False)

    # load model
    model_path = "/home/daniel/Projects/ppsEvolution/models/c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_3.h5"
    model = load_model(model_path)
    model.summary()

    # load input data
    img_name = map_name = "map_antonios_valid"
    # img_name = map_name = "Level_1_Run_0_Map"

    input_path = "/home/daniel/Projects/ppsEvolution/data/test_data1/{}.npy".format(map_name)
    img_path = "/home/daniel/Projects/ppsEvolution/data/test_data1/{}.png".format(map_name)
    # img = image.load_img(img_path, grayscale=False)
    # x = image.img_to_array(img) / 255.0
    img_num = 0
    x = np.load(input_path)
    print("img shape: ", x.shape)

    classes_path = os.getcwd() + "/class_activation_maps/4_class_matches_raw.csv"
    for i in range(10):
        weapon_index = i
        weapons = get_class_from_file(classes_path, weapon_index)
        print("weapons: ", weapons)

        # define output
        target_path = "./class_activation_maps/TF2_class_maps/{}".format("Custom-ea_cnn_{0}-w_{1}".format(img_name, weapon_index))

        specific_node = 1  #-1  # change this to show the activation for a specific class instead of the model's prediction
        make_cam(model, img_path, weapons, layer_name="conv2d_16", input_image=x, class_index=specific_node,
                 target_path=target_path, intensity=0.7, enlarge_factor=100, save_image=True, show_heatmap=False)

        make_weapon_cam(model, img_path, weapons, weapon_layer_name="dense_7",
                        input_image=x, class_index=specific_node, show_heatmap=False)
