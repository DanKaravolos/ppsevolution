import numpy as np
import os
from character_evolution.preprocessing.path_info import map_path


full_data = np.load(map_path)

nr_maps = 1
sample_data = full_data[-nr_maps:]
np.save(os.getcwd() + "/data/sample_{0}-maps.npy".format(nr_maps), sample_data)

