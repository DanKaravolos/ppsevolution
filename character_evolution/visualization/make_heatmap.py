import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

dimensions = {"death": 0, "pos": 1, "shot": 2}
title_dimensions = {"death": "Deaths", "pos": "Positions", "shot": "Shots"}


def plot_both_teams(data_path, data_index, attribute):
    data = np.load(data_path)
    heatmap = data[data_index, 0, dimensions[attribute]]
    heatmap = np.mean(heatmap, axis=0)
    plt.figure(0)
    plt.subplot(211)
    sns.heatmap(heatmap, cmap='viridis')
    plt.title("Team {0}, {1}".format(0, title_dimensions[attribute]))

    plt.subplot(212)
    hm1 = np.mean(data[data_index, 1, dimensions[attribute]], axis=0)
    sns.heatmap(hm1, cmap='viridis')

    plt.title("Team {0}, {1}".format(1, title_dimensions[attribute]))
    plt.show()


def plot_both_teams_rb(data_path, data_index, attribute):
    data = np.load(data_path)
    heatmap = data[data_index, 0, dimensions[attribute]]
    heatmap = np.mean(heatmap, axis=0)
    hm1 = np.mean(data[data_index, 1, dimensions[attribute]], axis=0)
    plt.figure(0)
    plt.subplot(121)
    plt.imshow(heatmap, cmap='Reds', alpha=1)
    plt.colorbar()
    plt.title("Team {0}, {1}".format(0, title_dimensions[attribute]))
    # plt.show()
    plt.subplot(122)
    plt.imshow(hm1, cmap='Blues', alpha=1)
    plt.colorbar()
    plt.title("Team {0}, {1}".format(1, title_dimensions[attribute]))
    plt.show()


def plot_both_teams_one_map(data_path, data_index, attribute):
    data = np.load(data_path)
    heatmap = data[data_index, 0, dimensions[attribute]]
    heatmap = np.mean(heatmap, axis=0)
    hm1 = np.mean(data[data_index, 1, dimensions[attribute]], axis=0)
    # plt.figure(0)
    # sns.heatmap(heatmap, cmap='Reds', alpha=0.5)

    # sns.heatmap(hm1, cmap='Blues')
    # plt.title("{1}".format(1, title_dimensions[attribute]), alpha=0.5)
    # plt.show()
    plt.imshow(hm1, cmap='Blues', alpha=0.7)
    plt.colorbar()
    plt.imshow(heatmap, cmap='Reds', alpha=0.7)
    plt.colorbar()

    plt.show()

# def plot_both_teams_one_map(data_path, data_index, attribute):
#     data = np.load(data_path)
#     heatmap = data[data_index, 0, dimensions[attribute]]
#     heatmap = np.mean(heatmap, axis=0)
#     fig, ax = plt.subplots()
#     # plt.figure(0)
#     sns.heatmap(heatmap, cmap='Reds')
#
#     plt.title("{1}".format(1, title_dimensions[attribute]))
#     # plt.show()
#
#     w, h = fig.canvas.get_width_height()
#     img = np.frombuffer(fig.canvas.buffer_rgba(), np.uint8).reshape(h, w, -1).copy()
#
#
#     hm1 = np.mean(data[data_index, 1, dimensions[attribute]], axis=0)
#     sns.heatmap(hm1, cmap='Blues')
#     img2 = np.frombuffer(fig.canvas.buffer_rgba(), np.uint8).reshape(h, w, -1).copy()
#
#     img[img[:, :, -1] == 0] = 0
#     img2[img2[:, :, -1] == 0] = 0
#
#     fig.clf()
#     plt.figure(2)
#     plt.imshow(np.maximum(img, img2))
#     plt.subplots_adjust(0, 0, 1, 1)
#     plt.axis("off")
#     plt.show()


def plot_specific_team(data_path, data_index, team, attribute):
    data = np.load(data_path)
    heatmap = data[data_index, team, dimensions[attribute]]
    heatmap = np.mean(heatmap, axis=0)
    plt.figure(0)
    sns.heatmap(heatmap, cmap='viridis')
    plt.title("Team {0}, {1}".format(team, title_dimensions[attribute]))


if __name__ == "__main__":
    data_file = "/home/daniel/Projects/ppsEvolution/groundtruths/mse_exp-gt_data2_heightmaps.npy"
    item_nr = 299
    dim = "pos"
    # team_nr = 0
    # plot_specific_team(data_file, item_nr, team_nr, dim)
    # plot_both_teams(data_file, item_nr, dim)
    # plot_both_teams_one_map(data_file, item_nr, dim)

    # plot_both_teams_rb(data_file, item_nr, dim)

    plot_both_teams_rb(data_file, item_nr, "death")
    plot_both_teams_rb(data_file, item_nr, "pos")
    plot_both_teams_rb(data_file, item_nr, "shot")