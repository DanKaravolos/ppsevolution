# import matplotlib.pyplot as plt
# import seaborn as sns
# import numpy as np
#

def minmax(time, is_stdev=False):
    return(time * (600 - 160) + 160) if not is_stdev else time * (600 - 160)



def load_data():
    path = "/home/daniel/Projects/pyprocschooter/data/df4_time_team0ratio.npy"
    targets = np.load(path)
    durations = targets[:, 0]
    scores = targets[:, 1]

    print("Time mean: {0:.2f}, std: {1:.2f}".format(minmax(np.min(durations)), minmax(np.max(durations))))
    sho_time = 200
    lo_time = 500
    shorts = []
    longs = []
    for ti in durations:
        oti = minmax(ti)
        if oti < sho_time:
            shorts.append(oti)
        elif oti > lo_time:
            longs.append(oti)
    print("match durations <{0}: {1:.2f}. durations > {2}: {3:.2f}".format(sho_time, len(shorts)/durations.shape[0]
                                                                           , lo_time, len(longs)/durations.shape[0]
                                                                           ))


    print("Time mean: {0:.2f}, std: {1:.2f}".format(np.mean(durations), np.std(durations)))
    print("Time mean: {0:.2f}, std: {1:.2f}".format(minmax(np.mean(durations)), minmax(np.std(durations), True)))
    print("Score mean: {0:.2f}, std: {1:.2f}".format(np.mean(scores), np.std(scores)))
    return durations, scores
#
#
# def barplot(durations, scores):
#     indices = np.arange(len(durations))
#     fig1 = plt.figure(1)
#     ax = plt.gca()
#     ax.grid(True)
#     plt.hist(durations, 20)
#     plt.savefig("data_plot.png", bbox_inches='tight')  #, pad_inches=0.1)
#     # axes[1] = plt.bar(indices, scores)
#
#
#     # for ax in axes.flatten():
#     #     ax.set_ylim([0, 1])
#     #     # ax.set_xlim([0, indices[-1][-1] + 1])
#     #     # ax.set_xticklabels([])
#     #     # # ax.set_xticks(np.arange(0, indices[-1][-1] + 1, 0.25 if whiskerplot else 0.5))
#     #     # ax.set_xticks([])
#     #     # ax.set_yticks(np.arange(0, 1.1, 0.1))
#     #     ax.grid(True)
#
#
#     plt.show()
#
#
# def change_font_settings():
#     params = {
#               # 'axes.axisbelow': 'true',
#               'font.weight': 'normal',
#               'axes.labelweight': 'normal',
#               'font.size': '16',
#               # 'text.usetex': 'false',
#               }  # this sets the radial lines
#     plt.rcParams.update(params)
#
#     plt.rcParams['font.serif'].insert(0, 'Times New Roman')
#     # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
#     plt.rcParams['font.family'] = 'serif'
#
# if __name__ == "__main__":
#     # sns.set()
#     # sns.palplot(sns.color_palette())
#     change_font_settings()
#     durations, scores = load_data()
#     barplot(durations, scores)
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np


# def plot_time_and_score(data_frame):
#     # plot time
#     # color = "#808080"  # sniper blue: "#0494bc"
#     color = "#0494bc"
#     time_data = data_frame["time"]
#     fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6), sharey=True)
#     plt.tight_layout()
#     time_data.plot.hist(ax=axes[0], grid=True, bins=20, color=color)
#     axes[0].set_xlabel("Time (s)")
#
#     # plot score
#     t1_data = data_frame["team0ratio"]
#     nr_bins = 10
#     t1_data.plot.hist(ax=axes[1], grid=True, bins=nr_bins, color=color)
#     axes[1].set_xticks(np.arange(0, 1.01, 2/nr_bins))
#     axes[1].set_xticklabels(["{:.1f}".format(num) for num in np.arange(0, 1.01, 2.0 / nr_bins)])
#     axes[1].set_xlabel("Team 1 Score")
#     plt.savefig("time_and_score.png", bbox_inches="tight")
#     plt.show()


def plot_time_and_score(data_frame):
    # plot time
    # color = "#808080"  # sniper blue: "#0494bc"
    color = "#0494bc"
    time_data = data_frame["time"]
    plt.figure(figsize=(6, 6))
    plt.tight_layout()
    ax = plt.gca()
    time_data.plot.hist(ax=ax, grid=True, bins=20, color=color, zorder=4)  #, edgecolor='k'
    ax.set_xlabel("Time (s)")
    plt.savefig("time.png", bbox_inches="tight")
    # plot score
    t1_data = data_frame["team0ratio"]
    nr_bins = 10
    plt.figure(figsize=(6, 6))
    plt.tight_layout()
    ax = plt.gca()
    t1_data.plot.hist(ax=ax, grid=True, bins=nr_bins, color=color, zorder=4)
    ax.set_xticks(np.arange(0, 1.01, 2/nr_bins))
    ax.set_xticklabels(["{:.1f}".format(num) for num in np.arange(0, 1.01, 2.0 / nr_bins)])
    ax.set_xlabel("Score")
    plt.savefig("score.png", bbox_inches="tight")
    plt.show()


def change_font_settings():
    params = {
              # 'axes.axisbelow': 'true',
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '28',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'


if __name__ == "__main__":
    data_file = "/home/daniel/Projects/pyprocschooter/data/summary_df4.pkl"
    data_frame = pd.read_pickle(data_file)

    load_data()
    change_font_settings()
    # plot_time_and_score(data_frame)