import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.spines import Spine
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
import matplotlib.patches as mpatches
colors = [(44/255.0,181/255.0,229/255.0), (244/255.0, 62/255.0, 169/255.0), (1.0, 131/255.0, 30/255.0)]


def radar_factory(num_vars, frame='circle'):
    """Create a radar chart with `num_vars` axes.

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle' | 'polygon'}
        Shape of frame surrounding axes.

    """
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)
    # rotate theta such that the first axis is at the top
    theta += np.pi/2
    # daniel: shift top 1/16 to the left (so class1 starts left) (for regular plot and sym18)
    # theta += np.pi/8

    # daniel: make top split w1 and w2 in the middle  ( for sym)
    theta += np.pi / 16


    def draw_poly_patch(self):
        verts = unit_poly_verts(theta)
        return plt.Polygon(verts, closed=True, edgecolor='k')

    def draw_circle_patch(self):
        # unit circle centered on (0.5, 0.5)
        return plt.Circle((0.5, 0.5), 0.5)

    patch_dict = {'polygon': draw_poly_patch, 'circle': draw_circle_patch}
    if frame not in patch_dict:
        raise ValueError('unknown value for `frame`: %s' % frame)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1
        # define draw_frame method
        draw_patch = patch_dict[frame]

        def fill(self, *args, **kwargs):
            """Override fill so that line is closed by default"""
            closed = kwargs.pop('closed', True)
            return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super(RadarAxes, self).plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.concatenate((x, [x[0]]))
                y = np.concatenate((y, [y[0]]))
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(np.degrees(theta), labels)  # fontname='Comic Sans MS'

        def _gen_axes_patch(self):
            return self.draw_patch()

        def _gen_axes_spines(self):
            if frame == 'circle':
                return PolarAxes._gen_axes_spines(self)
            # The following is a hack to get the spines (i.e. the axes frame)
            # to draw correctly for a polygon frame.

            # spine_type must be 'left', 'right', 'top', 'bottom', or `circle`.
            spine_type = 'circle'
            verts = unit_poly_verts(theta)
            # close off polygon by repeating first vertex
            verts.append(verts[0])
            path = Path(verts)

            spine = Spine(self, spine_type, path)
            spine.set_transform(self.transAxes)
            return {'polar': spine}

    register_projection(RadarAxes)
    return theta


def unit_poly_verts(theta):
    """Return vertices of polygon for subplot axes.

    This polygon is circumscribed by a unit circle centered at (0.5, 0.5)
    """
    x0, y0, r = [0.5] * 3
    verts = [(r*np.cos(t) + x0, r*np.sin(t) + y0) for t in theta]
    return verts


def parse_csv(csv_file):
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()
        csv_data = [[], ("Generated Level", [[], [], []]), ("Designed Level", [[], [], []])]
        for line in lines[3:]:
            elems = line.rstrip().split(",")
            csv_data[0].append(elems[0])
            # data from generated level
            csv_data[1][1][0].append(float(elems[1]))
            csv_data[1][1][1].append(float(elems[3]))
            csv_data[1][1][2].append(float(elems[5]))
            # data from designed level
            csv_data[2][1][0].append(float(elems[7]))
            csv_data[2][1][1].append(float(elems[9]))
            csv_data[2][1][2].append(float(elems[11]))
    return csv_data


def all_in_one_plot(theta, data, show):
    spoke_labels = data.pop(0)

    fig, axes = plt.subplots(figsize=(9, 9), nrows=1, ncols=2,
                             subplot_kw=dict(projection='radar'))
    # fig.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05)
    pal = sns.color_palette()
    colors = [pal[0], pal[1], pal[2]]
    # Plot the four cases from the example data on separate axes
    for ax, (title, case_data) in zip(axes.flatten(), data):
        plt.figure()
        ax.set_rgrids([0.2, 0.4, 0.6, 0.8])

        ax.set_rgrids([0.25, 0.5, 0.75])
        ax.set_title(title, weight='bold', size='medium', position=(0.5, 1.1),
                     horizontalalignment='center', verticalalignment='center')
        for d, color in zip(case_data, colors):
            ax.plot(theta, d, color=color)
            ax.fill(theta, d, facecolor=color, alpha=0.25)
        ax.set_varlabels(spoke_labels)

    # add legend relative to top-left plot
    ax = axes[0]
    labels = ('Short', "Medium", "Long")
    legend = ax.legend(labels, loc=(0.9, .95),
                       labelspacing=0.1, fontsize='small')

    # fig.text(0.5, 0.965, '5-Factor Solution Profiles Across Four Scenarios',
    #          horizontalalignment='center', color='black', weight='bold',
    #          size='large')
    fig.subplots_adjust(hspace=1)
    if show:
        plt.show()


def separate_plots(theta, data, show):
    spoke_labels = data.pop(0)

    # fig.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05)
    # pal = sns.color_palette()
    # colors = [pal[0], pal[1], pal[2]]
    # Plot the four cases from the example data on separate axes
    for (title, case_data) in data:
        fig, ax = plt.subplots(nrows=1, ncols=1,
                                 subplot_kw=dict(projection='radar'))
        # if fig == plt.figure(1):
        #     ax.set_rgrids([0.2, 0.4, 0.6, 0.8])
        # else:
        ax.set_rgrids([0.25, 0.5, 0.75])
        # ax.set_title(title, weight='bold', size='large', position=(0.5, 1.1),
        #              horizontalalignment='center', verticalalignment='center')
        for d, color in zip(case_data, colors):
            ax.plot(theta, d, color=color)
            ax.fill(theta, d, facecolor=color, alpha=0.25)
        ax.set_varlabels(spoke_labels)

        # add legend relative to top-left plot
        # ax1 = plt.figure(1).axes[0]
    legend_handles = [
        mpatches.Patch(color=colors[0], label='Short', fc=colors[0] + (0.25,)),
        mpatches.Patch(color=colors[1], label='Medium', fc=colors[1] + (0.25,)),
        mpatches.Patch(color=colors[2], label='Long', fc=colors[2] + (0.25,))]
    ax.legend(loc=(0.9, .95), labelspacing=0.1, handles=legend_handles)


    ax1 = plt.figure(1).axes[0]
    plt.savefig("rad_generated_sc1.png", bbox_inches="tight", dpi=300)  # data[0][0] + ".png"
    ax2 = plt.figure(2).axes[0]
    plt.savefig("rad_designed_sc1.png", bbox_inches="tight", dpi=300)

    # # add legend relative to top-left plot
    # ax = plt.figure(1).axes[0]
    # labels = ('Short', "Medium", "Long")
    # legend = ax.legend(labels, loc=(0.75, .95),
    #                    labelspacing=0.1)

    # fig.text(0.5, 0.965, '5-Factor Solution Profiles Across Four Scenarios',
    #          horizontalalignment='center', color='black', weight='bold',
    #          size='large')
    if show:
        plt.show()


def change_font_settings():
    params = {'legend.fontsize': 'large',
              'figure.figsize': (9,9),
              'axes.labelsize': 'large',
              'axes.titlesize': 'large',
              'xtick.labelsize': 'large',  # this sets the parameter labels
              'ytick.labelsize': 'medium',
              # 'font.family': 'serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              'font.size': '19',
              'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)
    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'



if __name__ == '__main__':
    # sns.set()
    # sns.set_palette("colorblind")
    N = 16
    radar_frame = radar_factory(N, frame='circle')

    data = parse_csv("radial_plot_sym.csv")
    change_font_settings()
    # all_in_one_plot(data)
    separate_plots(radar_frame, data, show=True)
