import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.patches as mpatches


# colors = [(44/255.0,181/255.0,229/255.0), (244/255.0, 62/255.0, 169/255.0), (1.0, 131/255.0, 30/255.0)]
# [sns.color_palette("colorblind")[5], "#f43ea9", "#ff831e"]
colors = ["#000000", "#000000", "#000000"]
box_colors = [(44/255.0,181/255.0,229/255.0, 0.6), (244/255.0, 62/255.0, 169/255.0, 0.6), (1.0, 131/255.0, 30/255.0, 0.6)]
# box_colors = [(1, 1, 1, 0.1), (1, 1, 1, 0.1), (1, 1, 1, 0.1)]


def parse_csv(csv_file):
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()
        time_list = []
        tp_list = []
        score_list = []
        sp_list = []
        for line in lines[1:]:
            elems = line.rstrip().split(",")

            pred_time = float(elems[2])
            ucb_time = float(elems[3]) if float(elems[3]) < 0.99999 else 0.99999
            lcb_time = float(elems[4]) if float(elems[4]) < 0.99999 else 0.995
            time_list.append([lcb_time, ucb_time])
            tp_list.append(pred_time)

            pred_score = float(elems[5])
            ucb_score = float(elems[6])
            lcb_score = float(elems[7])
            score_list.append([lcb_score, ucb_score])
            sp_list.append(pred_score)
    return time_list, tp_list, score_list, sp_list


def make_boxplot(csv_file, label, show=True):
    time_list, tp_list, score_list, sp_list = parse_csv(csv_file)

    time_bounds = [time_list[0:10], time_list[10:20], time_list[20:]]
    time_preds = [tp_list[0:10], tp_list[10:20], tp_list[20:]]

    score_bounds = [score_list[0:10], score_list[10:20], score_list[20:]]
    score_preds = [sp_list[0:10], sp_list[10:20], sp_list[20:]]

    # box_indices = [np.arange(1, 6, 0.5), np.arange(6, 11, 0.5), np.arange(11, 16, 0.5)]
    box_indices = [np.arange(1, 31, 3), np.arange(2, 32,3), np.arange(3, 33, 3)]
    boxplot_graph(time_bounds, time_preds, score_bounds, score_preds, box_indices, label)

    plt.savefig(csv_file.replace("csv", "png"), bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def boxplot_graph(time_bounds, time_preds, score_bounds, score_preds, indices, label):
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), sharey=True)

    time_goals = [[0.11] * 10, [0.33] * 10, [0.99] * 10]

    for ti in range(len(time_bounds)):
        # axes[0].plot(indices[ti], time_goals[ti], c='#000000', zorder=7, alpha=0.6)  # target
        axes[0].scatter(indices[ti], time_goals[ti], c='#808080', zorder=7, alpha=0.9, marker='d', s=58)  # target
        # axes[0].boxplot(time_bounds[ti], whis=0, showfliers=False, usermedians=[-1] * 20, positions=indices[ti],
        #                 showbox=not whiskerplot)
        tb = [t[1] - t[0] for t in time_bounds[ti]]
        bot = [t[0] for t in time_bounds[ti]]
        axes[0].bar(indices[ti], tb, 0.5, bottom=bot, edgecolor='k', fc=box_colors[ti], zorder=5)

        axes[0].scatter(indices[ti], time_preds[ti], color=colors[ti], zorder=7)

    # axes[0].set_title('Time', fontsize=fs)
    axes[0].set_ylabel("Duration")

    for si in range(len(score_bounds)):
        # axes[1].plot(indices[si], [0.5] * 10, color='k', zorder=7, alpha=0.6)  # target
        axes[1].scatter(indices[si], [0.5] * 10, color='#808080', zorder=7, alpha=0.9, marker='d', s=58)  # target
        # axes[1].boxplot(score_bounds[si], whis=0, showfliers=False, usermedians=[-1] * 20, positions=indices[si],
        #                 showbox=not whiskerplot)
        sb = [t[1] - t[0] for t in score_bounds[si]]
        bot = [t[0] for t in score_bounds[si]]
        axes[1].bar(indices[si], sb, 0.5, bottom=bot, edgecolor='k', fc=box_colors[si], zorder=5)

        axes[1].scatter(indices[si], score_preds[si], color=colors[si], zorder=7)


    # axes[1].set_title('Score', fontsize=fs)
    axes[1].set_ylabel("Score")
    params = {
              'font.size': '18',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    for ax in axes.flatten():
        ax.set_ylim([0, 1])
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.set_yticklabels([0, "", "", "", "", 0.5, "", "", "", "", 1])


        ax.set_xlim([0, indices[-1][-1] + 1])
        # ax.set_xticks(np.arange(0, indices[-1][-1] + 1, 0.25 if whiskerplot else 0.5))
        # ax.set_xticks([])
        ax.set_xticklabels([])
        ax.set_xticks([i + 0.5 for i in indices[2]])
        ax.set_xticks([i for i in indices[1]], minor=True)
        ax.set_xticklabels([label + str(i + 1) for i in range(10)], minor=True)


        ax.grid(True)

    # plt.show()


# def plot_legend():
#     legend = ["Short", "Medium", "Long"]
#     lfig = plt.figure(figsize=(6, 2))
#     lax = plt.gca()
#     plt.rcParams['font.size'] = '14'
#     legend_handles = [mpatches.Patch(label=legend[l], fc=box_colors[l], edgecolor='k', lw=1) for l in
#                       legend]  # reversed(legend)
#     lax.legend(loc=(0, 0), handles=legend_handles, ncol=2, fancybox=False, edgecolor='w')
#     lax.axis('off')
#     plt.savefig("pcgw_boxplot_legend.png", bbox_inches='tight')


def change_font_settings():
    params = {
              'axes.axisbelow': 'true',
              # 'font.family': 'sans-serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '20',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'


if __name__ == "__main__":
    # sns.set()

    # sns.palplot(sns.color_palette())
    # sns.palplot(sns.color_palette("colorblind"))
    # plt.show()
    # sns.set_palette(pal)
    # colors = sns.color_palette()[:5]

    change_font_settings()
    # #ICCC SETTINGS:
    # make_boxplot("Graph1_UCB.csv", show=True)
    # make_boxplot("Graph2_UCB.csv", show=False)

    #PCG WORKSHOP:
    print("PCG WORKSHOP YEAH")
    make_boxplot("Custom_Gauss_UCB.csv", "D", show=True)
    make_boxplot("Custom_TF2_UCB.csv", "D",show=False)
    make_boxplot("GenMaps_Gauss_UCB.csv", "G", show=False)
    make_boxplot("GenMaps_TF2_UCB.csv", "G" , show=False)