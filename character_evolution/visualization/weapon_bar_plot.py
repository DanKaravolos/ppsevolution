import matplotlib.pyplot as plt
# import seaborn as sns
import numpy as np
import matplotlib.patches as mpatches

# colors = [(44/255.0,181/255.0,229/255.0), (244/255.0, 62/255.0, 169/255.0), (1.0, 131/255.0, 30/255.0)]
# [sns.color_palette("colorblind")[5], "#f43ea9", "#ff831e"]
colors = ["#000000", "#000000", "#000000"]
box_colors = [(44/255.0,181/255.0,229/255.0, 0.6), (244/255.0, 62/255.0, 169/255.0, 0.6), (1.0, 131/255.0, 30/255.0, 0.6)]
# box_colors = [(1, 1, 1, 0.1), (1, 1, 1, 0.1), (1, 1, 1, 0.1)]


def parse_csv(csv_file):
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()

        header = lines[0].rstrip().split(",")
        header = header[1:]
        list_of_elements = [[] for i in range(len(header))]

        for line in lines[1:]:  #3 lines: short, medium, long
            elems = line.rstrip().split(",")
            elems = elems[1:]
            # list_of_elements[0].append(elems[0])
            for i in range(0, len(elems)):
                list_of_elements[i].append(float(elems[i]))

    elems_per_time = [[el[ti] for el in list_of_elements] for ti in range(len(list_of_elements[0]))]
    return header, elems_per_time


def change_font_settings():
    params = {
              'axes.axisbelow': 'true',
              # 'font.family': 'sans-serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '14',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'


def make_single_boxplot(header, elems, errors, filename, show=True):

    print("have elems")
    indices = [np.arange(1, (len(elems[0])) * 3 + 1, 3), np.arange(2, (len(elems[0])) * 3 + 1, 3), np.arange(3,len(elems[0]) * 3 + 1, 3)]
    # for ti in range(len(elems)):
    #     fi = indices[ti]
    #     fi[1:] += 1
    plt.figure(figsize=(11, 2))
    # plt.axis('tight')
    bar_width = 1
    spacing = np.arange(len(elems[0]))
    for ti in range(len(elems)):
        ti_elems = elems[ti]
        ti_error = errors[ti]
        ti_indices = indices[ti] + spacing
        plt.bar(ti_indices, ti_elems, edgecolor='k', fc=box_colors[ti], zorder=5, width=bar_width, yerr=ti_error,
                error_kw=dict(lw=1, ecolor='black', capthick=1, capsize=2, zorder=11))

    ax = plt.gca()
    # ax.set_xticklabels([], minor=False)
    ax.set_xticks(indices[1] + spacing, minor=True)
    # ax.set_xticklabels(header, minor=True, rotation=30, ha='right', rotation_mode='anchor')
    ax.set_xticklabels(header, minor=True, rotation_mode='anchor')
    ax.tick_params(axis='x', direction='in')
    ax.set_xticks(indices[2] + 0.5 *bar_width + spacing)
    ax.set_xticklabels('')
    ax.set_xlim([indices[0][0] - 0.5 * bar_width, indices[2][-1] + 0.5 * bar_width + spacing[-1]])
    ax.set_ylim([0,1])
    # ax.tick_params(axis='x', which='minor', bottom='on')
    plt.grid()
    plt.savefig(filename.replace("csv", "png"), bbox_inches='tight', pad_inches=0.1)
    plot_legend()
    if show:
        plt.show()


def plot_legend():
    legend = ["$d_{t}=0.11$", "$d_{t}=0.33$", "$d_{t}=1$"]
    lfig = plt.figure(figsize=(3, 1))
    lax = plt.gca()
    plt.rcParams['font.size'] = '18'
    legend_handles = [mpatches.Patch(label=legend[l], fc=box_colors[l], edgecolor='k', lw=1) for l in
                      range(len(legend))]  # reversed(legend)
    lax.legend(loc=(0, 0), handles=legend_handles, ncol=3, fancybox=False, edgecolor='w')
    lax.axis('off')
    plt.savefig("pcgw_weapon_bar_legend.png", bbox_inches='tight')


def make_double_boxplot(header, elems, errors, filename, show=True):
    test = [el[:8] for el in elems]
    make_single_boxplot(header[:8], [el[:8] for el in elems], [el[:8] for el in errors], filename.replace(".png", "_1.png"), show=True)
    make_single_boxplot(header[8:], [el[8:] for el in elems], [el[8:] for el in errors],filename.replace(".png", "_2.png"), show=True)

if __name__ == "__main__":
    change_font_settings()

    #PCG WORKSHOP:
    print("PCG WORKSHOP YEAH")
    filename = "weapon_stats.csv"
    header, elems = parse_csv(filename)
    err_head, errors = parse_csv("weapon_stats_bars.csv")
    make_single_boxplot(header, elems, errors, filename.replace("csv", "png"), show=False)
    make_double_boxplot(header, elems, errors, filename.replace("csv", "png"), show=True)
    # make_boxplot("weapon_stats2.csv", show=False)
