import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
import matplotlib as mpl
import mpl_toolkits.axisartist.floating_axes as floating_axes

label_dict = {
    'Scattergun': "Scout (Scattergun)",
    'Shotgun': "Engineer (Shotgun)",
    'Flamethrower': "Pyro (Flamethrower)",
    'Minigun': "Heavy (Minigun)",
    'Syringe Gun': "Medic (Syringe Gun)",
    'Sniper Rifle': "Sniper (Sniper Rifle)",
    'Revolver': "Spy (Revolver)",
    'Rocket': "Soldier (Rocket)",
    'Grenade': "Demoman (Grenade)",
    'Other': "Other"
}


def make_palette():
    # plt.figure(2)
    # sns.palplot(sns.color_palette())
    pal = sns.color_palette()

    color_dict1 = {
        'Scattergun': "#c48efd",
        'Flamethrower': pal[3],
        'Minigun': "#fddc5c",
        'Shotgun': "#894585",
        'Syringe Gun': pal[2],
        'Sniper Rifle': pal[0],
        'Rifle': pal[9],
        'Revolver': pal[7],
        'Rocket': "#af884a",
        'Grenade': "#7f684e",
        'Other': "#505050",
    }

    color_dict = {
        'Scattergun': "#edc951",
        'Shotgun': "#eb6841",
        'Flamethrower': "#cc333f",
        'Minigun': "#eae69f",
        'Syringe Gun': "#6e8d4b",
        'Sniper Rifle': "#0494bc",
        'Revolver': "#5e4c8b",
        'Rocket': "#885b47",
        'Grenade': "#c69d67",
        'Other': "#a7b9c3"   #"#c8dce8",  #"#a7b9c3"
    }
    return color_dict


def change_font_settings():
    params = {
              'axes.axisbelow': 'true',
              # 'font.family': 'sans-serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '22',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'


def parse_csv(csv_file):
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()
        total = []
        t1 = []
        t2 = []
        legend = []

        for line in lines[1:]:
            elems = line.rstrip().split(",")
            if elems[0] == "short" or elems[0] == "med" or elems[0] == "long":
                legend.append(elems[2].rstrip())
                total.append(float(elems[4]))
                t1.append(float(elems[8]))
                t2.append(float(elems[12]))
    return [legend[0:10], total[0:10], total[10:20], total[20:30], \
           t1[0:10], t1[10:20], t1[20:30], \
           t2[0:10], t2[10:20], t2[20:30]]


def stackplot_graph(data, legend, color_dict, result_file):
    fig = plt.figure(1, figsize=(12,5))
    ax = plt.gca()
    step_size = 1
    indices = np.arange(0, len(data), step_size)

    idx = 0
    for stack in data:
        bottom = 0
        weapon = 0
        for item in stack:
            plt.bar(idx, item, bottom=bottom, color=color_dict[legend[weapon]], edgecolor="k")
            bottom += item
            weapon += 1
        idx += step_size

    ax.set_xticks([2.5, 5.5], minor=False)
    ax.set_xticks(indices, minor=True)
    ax.set_xticklabels([])
    ax.set_xticklabels(["short", "med", "long", "short", "med\nPlayer 1", "long", "short", "med\nPlayer 2", "long"], minor=True)
    ax.set_yticklabels(["0%", "20%", "40%", "60%", "80%", "100%"])

    # for tick in ax.get_xticklabels(minor=True):
    #     tick.set_rotation(45)

    # ax.legend(legend, loc='upper right',
    #           labelspacing=0.1, fontsize='medium')

    plt.tick_params(axis='x', which='major', length=56, width=1.5)
    # Shrink current axis by 20%
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    # ax.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.axis('tight')
    plt.xlim([-0.5, indices[-1]+ 0.5])
    plt.ylim([0,1])
    plt.savefig(result_file, bbox_inches='tight')

    plot_legend(legend)
    plt.show()


def plot_legend(legend):
    lfig = plt.figure(figsize=(6, 2))
    lax = plt.gca()
    plt.rcParams['font.size'] = '14'
    legend_handles = [mpatches.Patch(label=label_dict[l], fc=colors[l], edgecolor='k', lw=1) for l in
                      reversed(legend)]  # reversed(legend)
    lax.legend(loc=(0, 0), handles=legend_handles, ncol=1, fancybox=False, edgecolor='w')
    lax.axis('off')
    plt.savefig("legend3.png", bbox_inches='tight')

    # plt.rcParams['font.size'] = '22'
    plt.savefig("legend4.png", bbox_inches='tight')


def make_stackplot(filename, color_dict, result_file, show=True):
    data = parse_csv(filename)
    legend = data.pop(0)
    stackplot_graph(data, legend, color_dict, result_file)

if __name__ == "__main__":
    # sns.set()
    # pal = "colorblind"  # deep, muted, pastel, bright, dark, and colorblind.
    # sns.palplot(sns.color_palette())
    # sns.palplot(sns.color_palette())
    # plt.show()
    # sns.set_palette(pal)
    colors = make_palette()

    change_font_settings()
    make_stackplot("stackedbox_map3a.csv", colors, "classes_map3.png", show=True)
    change_font_settings()
    make_stackplot("stackedbox_map5a.csv", colors, "classes_map5.png", show=True)
