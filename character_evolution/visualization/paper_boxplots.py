import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


# colors = [(44/255.0,181/255.0,229/255.0), (244/255.0, 62/255.0, 169/255.0), (1.0, 131/255.0, 30/255.0)]
# [sns.color_palette("colorblind")[5], "#f43ea9", "#ff831e"]
colors = ["#000000", "#000000", "#000000"]
box_colors = [(44/255.0,181/255.0,229/255.0, 0.6), (244/255.0, 62/255.0, 169/255.0, 0.6), (1.0, 131/255.0, 30/255.0, 0.6)]
# box_colors = [(1, 1, 1, 0.1), (1, 1, 1, 0.1), (1, 1, 1, 0.1)]


def parse_csv(csv_file):
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()
        time_list = []
        tp_list = []
        score_list = []
        sp_list = []
        for line in lines[1:]:
            elems = line.rstrip().split(",")

            pred_time = float(elems[2])
            ucb_time = float(elems[3])
            lcb_time = float(elems[4])
            time_list.append([lcb_time, ucb_time])
            tp_list.append(pred_time)

            pred_score = float(elems[5])
            ucb_score = float(elems[6])
            lcb_score = float(elems[7])
            score_list.append([lcb_score, ucb_score])
            sp_list.append(pred_score)
    return time_list, tp_list, score_list, sp_list


def make_boxplot(csv_file, show=True):
    time_list, tp_list, score_list, sp_list = parse_csv(csv_file)

    time_bounds = [time_list[0:20], time_list[20:40], time_list[40:]]
    time_preds = [tp_list[0:20], tp_list[20:40], tp_list[40:]]

    score_bounds = [score_list[0:20], score_list[20:40], score_list[40:]]
    score_preds = [sp_list[0:20], sp_list[20:40], sp_list[40:]]

    box_indices = [np.arange(1, 11, 0.5), np.arange(11, 21, 0.5), np.arange(21, 31, 0.5)]
    boxplot_graph(time_bounds, time_preds, score_bounds, score_preds, box_indices)

    plt.savefig(csv_file.replace("csv", "png"), bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def boxplot_graph(time_bounds, time_preds, score_bounds, score_preds, indices):

    fs = 10  # fontsize
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), sharey=True)

    time_goals = [[0.085] * 20, [0.315] * 20, [0.99] * 20]

    for ti in range(len(time_bounds)):
        axes[0].plot(indices[ti], time_goals[ti], c='#000000', zorder=7, alpha=0.6)
        # axes[0].boxplot(time_bounds[ti], whis=0, showfliers=False, usermedians=[-1] * 20, positions=indices[ti],
        #                 showbox=not whiskerplot)
        tb = [t[1] - t[0] for t in time_bounds[ti]]
        bot = [t[0] for t in time_bounds[ti]]
        axes[0].bar(indices[ti], tb, 0.5, bottom=bot, edgecolor='k', fc=box_colors[ti], zorder=5)

        axes[0].scatter(indices[ti], time_preds[ti], color=colors[ti],zorder=6)

    # axes[0].set_title('Time', fontsize=fs)
    axes[0].set_ylabel("Duration")

    for si in range(len(score_bounds)):
        axes[1].plot(indices[si], [0.5] * 20, color='k', zorder=7, alpha=0.6)
        # axes[1].boxplot(score_bounds[si], whis=0, showfliers=False, usermedians=[-1] * 20, positions=indices[si],
        #                 showbox=not whiskerplot)
        sb = [t[1] - t[0] for t in score_bounds[si]]
        bot = [t[0] for t in score_bounds[si]]
        axes[1].bar(indices[si], sb, 0.5, bottom=bot, edgecolor='k', fc=box_colors[si], zorder=5)

        axes[1].scatter(indices[si], score_preds[si], color=colors[si], zorder=6)


    # axes[1].set_title('Score', fontsize=fs)
    axes[1].set_ylabel("Score")
    for ax in axes.flatten():
        ax.set_ylim([0, 1])
        ax.set_xlim([0, indices[-1][-1] + 1])
        ax.set_xticklabels([])
        # ax.set_xticks(np.arange(0, indices[-1][-1] + 1, 0.25 if whiskerplot else 0.5))
        ax.set_xticks([])
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.set_yticklabels([0,"","","","",0.5,"","","","",1])
        ax.grid(True)

    # plt.show()

def change_font_settings():
    params = {
              'axes.axisbelow': 'true',
              # 'font.family': 'sans-serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              'axes.labelweight': 'normal',
              'font.size': '26',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'


if __name__ == "__main__":
    # sns.set()

    # sns.palplot(sns.color_palette())
    # sns.palplot(sns.color_palette("colorblind"))
    # plt.show()
    # sns.set_palette(pal)
    # colors = sns.color_palette()[:5]

    change_font_settings()
    #ICCC SETTINGS:
    make_boxplot("Graph1_UCB.csv", show=True)
    make_boxplot("Graph2_UCB.csv", show=False)
