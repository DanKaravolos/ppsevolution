import numpy as np
from collections import defaultdict
# from character_evolution.postprocessing.EvolvedWeaponData import EvolvedClass


class GridCellData:
    def __init__(self, mut_prob, cross_prob, evolved_class_list):
        self.mut = mut_prob
        self.cross = cross_prob
        self.data = evolved_class_list
        self.metric_names = self.data[0].game_metric_names
        self.per_map_summary = self.compute_map_summary()
        self.per_duration_summary = self.compute_duration_summary()

        self.breakpoint = 0

    def get_header(self):
        return "mut_gene_prob, cross_prob, " + self.data[0].get_header(external_line_number=False)

    def to_string(self):
        data_list = ["{0:.2f},{1:.2f},{2}".format(self.mut, self.cross, d.to_string(add_new_line=False)) for d in self.data]
        return "\n".join(data_list)

    def get_summary_header(self):
        return "mut_gene_prob, cross_prob, grouped per, fitness, " + \
               ",".join(["abs(p-d) {0}".format(met) for met in self.metric_names]) + \
               ", dist to originals sum, dist to originals avg, dist to original 1, dist to original 2"

    def get_summary_string(self):
        mp_str = "{0:.2f},{1:.2f}".format(self.mut, self.cross)

        def dict_to_str(my_dict):
            lines = []
            for key, val in my_dict.items():
                vals = ["{:.6f}".format(v) for v in val]
                line = ",".join([mp_str, key, *vals])
                lines.append(line)
            return lines

        time_lines = dict_to_str(self.per_duration_summary)
        map_lines = dict_to_str(self.per_map_summary)
        return "\n".join(time_lines + map_lines)

    def compute_map_summary(self):
        grouped_per_map = defaultdict(list)
        # collect
        for dp in self.data:
            vals_to_summarize = [dp.fitness, *dp.diff_between_p_d, dp.dist_to_originals_sum, dp.dist_to_originals_avg, *dp.dist_to_originals]
            grouped_per_map[dp.map].append(vals_to_summarize)

        avg_per_map = {}
        for key, raw_values in grouped_per_map.items():
            # check
            nr_items = len(raw_values)
            if nr_items != 30 and nr_items != 10:
                print("Warning in GridCellData!  ({2}, {3})  Number of items grouped per {0} is {1}, not 10 or 30! Is this okay?".format(key, nr_items,
                      self.mut, self.cross))
            # process
            # avg_per_map[key] = np.mean(raw_values, axis=0)
            if nr_items >= 10:
                avg_per_map[key] = np.mean(raw_values, axis=0)
            else:
                print("GridCellData: Ignoring values with length <10")
                avg_per_map[key] = [np.nan] * len(raw_values[0])
        return avg_per_map

    def compute_duration_summary(self):
        grouped_per_duration = defaultdict(list)
        # collect
        for dp in self.data:
            class_name = dp.desired_classes["time"]
            vals_to_summarize = [dp.fitness, *dp.diff_between_p_d, dp.dist_to_originals_sum, dp.dist_to_originals_avg, *dp.dist_to_originals]
            grouped_per_duration[class_name].append(vals_to_summarize)

        avg_per_duration = {}
        for key, raw_values in grouped_per_duration.items():
            # check
            nr_items = len(raw_values)
            if nr_items != 100:
                print("Warning! ({2}, {3}) Number of items grouped per {0} is {1}, not 100! Is this okay?".format(key, nr_items, self.mut, self.cross))
            # process
            # avg_per_duration[key] = np.mean(raw_values, axis=0)
            # process
            if nr_items >= 10:
                avg_per_duration[key] = np.mean(raw_values, axis=0)
            else:
                print("GridCellData: Ignoring values with length < 10")
                avg_per_duration[key] = [np.nan] * len(raw_values[0])
        return avg_per_duration

    def get_value_string(self, value_to_get, summary_name, value_only=False):
        # Determine origin of value
        val_dict = {}
        if summary_name == "map":
            val_dict = self.per_map_summary
        elif summary_name == "duration":
            val_dict = self.per_duration_summary
        # Determine value to get
        val = 0  # the default value is fitness
        if "delta" in value_to_get:
            name = value_to_get.split("_")[1]
            val = 1 + self.metric_names.index(name)
        elif "dist_to_original" in value_to_get:
            if "sum" in value_to_get:
                val = 3
            elif "avg" in value_to_get:
                val = 4
            else:
                val = 5

        # Format value string
        if val != 5:   # val 5 is a tuple
            if value_only:
                data_str = ",".join(["{0:.6f}".format(val_dict[key][val]) for key in val_dict.keys()])
                return data_str
            else:
                data_list = ["{0:.2f},{1:.2f},{2},{3:.6f}".format(self.mut, self.cross, key, val_dict[key][val]) for key in val_dict.keys()]
                return "\n".join(data_list)
        else:
            if value_only:
                data_str = ",".join(["{0:.6f},{1:.6f}".format(val_dict[key][val], val_dict[key][val + 1]) for key in val_dict.keys()])
                return data_str
            else:
                data_list = ["{0:.2f},{1:.2f},{2},{3:.6f},{4:.6f}".format(self.mut, self.cross, key, val_dict[key][val],
                                                                          val_dict[key][val + 1]) for key in val_dict.keys()]
                return "\n".join(data_list)
