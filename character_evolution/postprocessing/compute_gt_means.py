import os
import numpy as np
import pickle
from collections import defaultdict


class GTObject:
    def __init__(self, measurements):
        self.values = measurements
        self.means = np.mean(measurements, axis=0)
        self.stds = np.std(measurements, axis=0)
        self.intervals = self.compute_ci()

    # this computes the 95% confidence interval
    def compute_ci(self):
        z_factor = 1.96  # change this if you want another conf.interval
        cis = []
        sq_sample_size = np.sqrt(self.values.shape[0])
        for dim in range(self.values.shape[1]):
            margin = z_factor * self.stds[dim] / sq_sample_size
            cis.append([self.means[dim] - margin, self.means[dim] + margin])
        return cis


def parse_gt_file(gt_file, dims):
    num_to_gt_dict = defaultdict(list)
    with open(gt_file, 'rb') as inpt:
        df = pickle.load(inpt)
        gt_df = df.loc[:, dims]
        gt_df['time'] = (gt_df['time'] - 150) / 450

        for line in gt_df.values:
            fi = line[0].rsplit("\\", 1)[1]
            num = int(fi.split("_")[1])
            num_to_gt_dict[num].append(line[1:])
    return num_to_gt_dict


def compute_means(num_to_gt_dict):
    num_to_mean_dict = {}
    for k, v in num_to_gt_dict.items():
        varr = np.asarray(v, dtype=np.float32)
        num_to_mean_dict[k] = GTObject(varr)
    return num_to_mean_dict


def write_output(gt_dict, dims, output_file):
    game_dims = dims[1:]
    with open(output_file, 'w') as out:
        dim_text = ["{0} mean, {0} std, {0} CI low, {0} CI high".format(d) for d in game_dims]
        header = "num, " + ",".join(dim_text)
        out.write(header + "\n")
        for num, obj in gt_dict.items():
            values = ["{0:.4f},{1:.4f},{2:.4f},{3:.4f}".format(obj.means[i], obj.stds[i], *obj.intervals[i]) for i in range(len(game_dims))]
            line = "{0},{1}\n".format(num, ",".join(values))
            out.write(line)


def main():
    gt_file = os.getcwd() + "/character_evolution/Results/character_evolution/Scout_Heavy2/GT_summary_ScoutHeavy2.pkl"
    output_file = os.getcwd() + "/character_evolution/Results/character_evolution/Scout_Heavy2/results/Scout_Heavy-gt_means.csv"
    dims = ('file', 'time', 'team0ratio')
    num_to_gt_dict = parse_gt_file(gt_file, dims)
    num_to_mean_dict = compute_means(num_to_gt_dict)
    write_output(num_to_mean_dict, dims, output_file)

if __name__ == "__main__":
    main()