import os
from character_evolution.postprocessing.evaluate_evolved_weapons import evaluate_results
from common.evaluate_tf2_gt import evaluate_original_classes
from character_evolution.postprocessing.ClassComparisonData import ClassComparisonData
from common.TF2MatchData import TF2MatchSummary
time_targets = {"Short": 0.14, "Medium": 0.36, "Long": 0.72}


def compare_evolved_with_tf_per_map_per_duration(evolved_pmpd, tf, out_fi):
    out_fi = out_fi.replace("evaluation", "tf_comparison_per_map_per_duration")
    with open(out_fi, 'w') as out:
        write_header = True
        for data_map, duration_dict in sorted(evolved_pmpd.items(), key=lambda x: x[0]):
            data_map_els = data_map.split("/")
            if len(data_map_els) > 2:
                data_map = data_map_els[-2] + "/" + data_map_els[-1]

            tf_data = tf[data_map]
            for duration, summary in duration_dict.items():
                comparison = ClassComparisonData(summary, tf_data)
                if write_header:
                    header = "map, duration," + comparison.get_header()
                    out.write(header + "\n")
                    write_header = False
                print("comparing... {0} : {1}".format(data_map, duration))
                line = ",".join([data_map, duration, comparison.to_string()])
                out.write(line + "\n")


def compare_evolved_with_tf_per_duration(evolved_pd, tf, out_fi):
    out_fi = out_fi.replace("evaluation", "tf_comparison_per_duration")
    with open(out_fi, 'w') as out:
        # out.write("duration, total improvement, score improvement, time improvement, "
        #           "total match dist, distance class1 to Scout, distance class2 to Heavy\n")
        write_header = True
        tf2_datalist = []
        for dp in tf.values():
            tf2_datalist.extend(dp.data)
        tf_data = TF2MatchSummary(tf2_datalist)
        for duration, summary in evolved_pd.items():
            comparison = ClassComparisonData(summary, tf_data)
            if write_header:
                header = "duration," + comparison.get_header()
                out.write(header + "\n")
                write_header = False

            print("comparing... {0}".format(duration))
            line = ",".join([duration, comparison.to_string()])
            out.write(line + "\n")


def main():
    result_dir = os.getcwd() + "/character_evolution/Results/character_evolution/"

    tf_gt_file = result_dir + "Original_TF2/GT_summary-OriginalTF2.pkl"
    tf_game_input_file = result_dir + "Original_TF2/tf2_class_scout_vs_heavy_AllMaps-simple.csv"
    tf_out_file = result_dir + "Original_TF2/OriginalTF2-evaluation.csv"

    # ea_result_file = result_dir+ "Scout_Heavy_MO/MO2-best_ind-result2.csv"
    # ea_gt_file = result_dir + "Scout_Heavy_MO/GT_summary-ScoutHeavy_MO2.pkl"
    # ea_out_file = result_dir + "Scout_Heavy_MO/Scout_Heavy_MO2-evaluation.csv"

    # ea_result_file = result_dir + "Scout_Heavy_SO/ScoutHeavy_BestParams-results.csv"
    # ea_gt_file = result_dir + "Scout_Heavy_SO/GT_summary-ScoutHeavy_BestParams.pkl"
    # ea_out_file = result_dir + "Scout_Heavy_SO/SO-evaluation.csv"

    # minor revisions
    # ea_result_file = result_dir + "ScoutHeavy_SOEA_MR/results/summary-SO-result.csv"
    # ea_gt_file = result_dir + "GT_summary-character_evolution-SOEA-GT.pkl"
    # ea_out_file = result_dir + "ScoutHeavy_SOEA_MR/SO-evaluation.csv"

    # ea_result_file = result_dir + "ScoutHeavy_MOEA_MR/summary-MO-result.csv"
    # ea_gt_file = result_dir + "GT_summary-character_evolution-MOEA-GT.pkl"
    # ea_out_file = result_dir + "ScoutHeavy_MOEA_MR/MO-evaluation.csv"

    ea_result_file = result_dir + "ScoutHeavy-MOEA_replicate/summary-best_ind-result.csv"
    ea_gt_file = result_dir + "ScoutHeavy-MOEA_replicate/GT_summary-GT-runs.pkl"
    ea_out_file = result_dir + "ScoutHeavy-MOEA_replicate/MOEA-replicate-evaluation.csv"

    from_class = False

    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    dims = ('file', 'team0ratio', 'time')
    ea_data, pds, pms, pdpms = evaluate_results(ea_result_file, ea_gt_file, ea_out_file, from_class, 2, initial_class1, initial_class2)
    tf_per_map = evaluate_original_classes(tf_game_input_file, tf_gt_file, tf_out_file, dims)

    compare_evolved_with_tf_per_map_per_duration(pdpms, tf_per_map, ea_out_file)
    compare_evolved_with_tf_per_duration(pds, tf_per_map, ea_out_file)
    print("done.")


if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main()
