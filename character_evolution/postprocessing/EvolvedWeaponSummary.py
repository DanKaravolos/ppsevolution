import numpy as np
from character_evolution.evolution import weapon_parameters as wp
from common.EvolvedIndividualSummary import EvolvedIndividualSummary


class EvolvedWeaponSummary(EvolvedIndividualSummary):
    def __init__(self, evolved_weapon_data_list):
        super().__init__(evolved_weapon_data_list)

        self.changed_params = [dp.changed_params for dp in self.data]
        self.dist_to_original_match = [dp.match_dist_to_original for dp in self.data]
        self.dist_to_originals = [dp.dist_to_originals for dp in self.data]

        self.tf2_classes = sorted(self.data[0].tf2_classes)
        self.c1_tf2_dist, self.c2_tf2_dist = self.compute_mean_dist_to_all_tf2()
        self.threshold = 1.5
        self.nearest_c1 = min(self.c1_tf2_dist, key=self.c1_tf2_dist.get) if min(self.c1_tf2_dist.values()) < self.threshold else "Other"
        self.nearest_c2 = min(self.c2_tf2_dist, key=self.c2_tf2_dist.get) if min(self.c2_tf2_dist.values()) < self.threshold else "Other"

        self.mean_chp = np.mean(self.changed_params, axis=0)
        self.mean_dtom = np.mean(self.dist_to_original_match, axis=0)

    def get_header(self):
        line = super().get_header()
        changed_params = ["C1 {0} change".format(c) for c in wp.all_params] + \
                         ["C2 {0} change".format(c) for c in wp.all_params]
        class_distances = ["C1 dist to {0}".format(k) for k in self.tf2_classes] + ["C2 dist to {0}".format(k) for k in self.tf2_classes]
        nearest_classes = ["C1 Nearest TF Class", "C2 Nearest TF Class"]
        str_line = ",".join(nearest_classes + class_distances + changed_params)
        header = line + "," + str_line
        return header

    def to_string(self):
        line = super().to_string()
        class_distances1 = ["{0:4f}".format(self.c1_tf2_dist[k]) for k in self.tf2_classes]
        class_distances2 = ["{0:4f}".format(self.c2_tf2_dist[k]) for k in self.tf2_classes]
        changed_params = ["{0:4f}".format(t) for t in self.mean_chp]
        nearest = [self.nearest_c1, self.nearest_c2]
        str_line = ",".join(nearest + class_distances1 + class_distances2 + changed_params)
        line = line + "," + str_line
        return line

    def compute_mean_dist_to_all_tf2(self):

        def get_mean_values(all_dist_dicts):
            p_dist_dict = {}
            for key in self.tf2_classes:
                all_key_distances = [dist[key] for dist in all_dist_dicts]
                p_dist_dict[key] = np.mean(all_key_distances)
            return p_dist_dict

        distance_dict_p1 = get_mean_values([dp.distances[0] for dp in self.data])
        distance_dict_p2 = get_mean_values([dp.distances[1] for dp in self.data])
        return distance_dict_p1, distance_dict_p2





