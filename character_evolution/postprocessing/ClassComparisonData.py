import numpy as np
from common.ComparisonData import ComparisonData


class ClassComparisonData(ComparisonData):
    def __init__(self, evolved_data, tf_data):
        super().__init__(evolved_data, tf_data)

        # Values for parameter changes
        self.dist_to_orig_classes = np.mean(self.ea_data.dist_to_originals, axis=0)
        self.dist_to_orig_match = self.ea_data.mean_dtom
        self.breakpoint = ""
        self.oc1 = self.ea_data.data[0].original_class1
        self.oc2 = self.ea_data.data[0].original_class2

    def get_header(self):
        header = super().get_header()
        header = header + "," + "dist to original match, dist C1 to {0}, dist C2 to {1}".format(self.oc1, self.oc2)
        return header

    def to_string(self):
        line = super().to_string()
        nums = [self.dist_to_orig_match, *self.dist_to_orig_classes]
        str_nums = ["{0:.2f}".format(n) for n in nums]
        line = line + "," + ",".join(str_nums)
        return line
