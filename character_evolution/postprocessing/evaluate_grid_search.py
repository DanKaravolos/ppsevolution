import os
import numpy as np
from tqdm import tqdm
from collections import defaultdict
from character_evolution.postprocessing.EvolvedWeaponData import EvolvedClass
from character_evolution.postprocessing.GridCellData import GridCellData


def parse_result_files(folder,  nr_ea_targets, initial_class1, initial_class2, from_classification):
    keyword = "result"
    print("Searching for {0} in folder: {1} ".format(keyword, folder))
    files = [folder + f for f in os.listdir(folder) if keyword in f and not "summary" in f]
    grid_cell_data = []
    for fi in tqdm(sorted(files)):
        ea_data = parse_result_file(fi, nr_ea_targets, initial_class1, initial_class2, from_classification)
        fi_name_elems = fi.split("-")
        mut = 0
        crs = 0
        for el in fi_name_elems:
            if "mut" in el:
                mut = float(el.split("_")[1])
            if "crs" in el:
                crs = float(el.split("_")[1])
        grid_cell_data.append(GridCellData(mut, crs, ea_data))
    return grid_cell_data


def parse_result_file(gi_file, nr_ea_targets=2, initial_class1="", initial_class2="", from_classification=False):
    ea_classes = []
    with open(gi_file, 'r') as inpt:
        lines = inpt.readlines()
        header_elems = lines[0].rstrip().split(",")
        # I am assuming the following layout: map, fitness, fitness value, prediction for each game metric,
        # normalized class1 params, normalized class 2 params, stuff that we don't need now
        class_start_finder = (i for i, e in enumerate(header_elems) if e == "HP")  # based on https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-given-a-list-containing-it-in-python
        c1_index = next(class_start_finder)
        c2_index = next(class_start_finder)
        for line in lines[1:]:
            if line.startswith("map"):  # these are header lines. This might introduce new targets, so we copy it.
                header_elems = line.rstrip().split(",")
                continue
            elems = line.rstrip().split(",")
            game_metric_names = header_elems[3:3 + nr_ea_targets]
            game_metrics = elems[3:3 + nr_ea_targets]
            class1 = [float(el) for el in elems[c1_index:c1_index + 8]]
            class2 = [float(el) for el in elems[c2_index:c2_index + 8]]
            targets = header_elems[-nr_ea_targets:]
            map_name = elems[0].replace("/home/daniel/Projects/ppsEvolution/data/maps/","").replace("_Run_0_Map.csv","")
            ea_classes.append(EvolvedClass(map_name, class1, class2, elems[2], game_metric_names, game_metrics, targets,
                                           orig_c1=initial_class1, orig_c2=initial_class2,
                                           from_classification=from_classification))
    return ea_classes


def reshape_grid_data(grid_data):
    grid_dict = defaultdict(lambda: defaultdict(dict))
    for dp in grid_data:
        m = "{:.2f}".format(dp.mut)
        c = "{:.2f}".format(dp.cross)
        time = list(dp.per_duration_summary.keys())[0]  # assuming there is only one target per data point
        grid_dict[m][time][c] = dp
        # grid_dict[m][c].append(dp)
    return grid_dict


def write_value_list(grid_data, out_file, value):
    with open(out_file, 'w') as out:
        header = "mut, crs, key, {0}".format(value)
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.get_value_string(value, "duration")
            out.write(lines + "\n")


def write_value_grid(grid_dict, out_file, value):
    with open(out_file, 'w') as out:
        crsprob = ["{:.2f}".format(i) for i in np.arange(0, 1.1, 0.1)]
        header = "mut, time, {0}, <= crs".format(",".join(crsprob))
        out.write(header + "\n")
        for mut, time_dict in grid_dict.items():
            line = [mut]
            for time, crs_dict in time_dict.items():
                line.append(time)
                for crs, cell in sorted(crs_dict.items(), key=lambda x: x[0]):
                    line.extend([cell.get_value_string(value, "duration", value_only=True)])
                # line.append("\n")
                out.write(",".join(line) + "\n")
                line = [mut]
            out.write("\n")


def write_output(grid_data, out_file):
    with open(out_file, 'w') as out:
        header = grid_data[0].get_summary_header()
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.get_summary_string()
            out.write(lines + "\n")


def write_all_values(grid_data, out_file):
    with open(out_file, 'w') as out:
        # count = 1
        header = grid_data[0].get_header()
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.to_string()
            out.write(lines)
            # out.write("{0},{1}".format(count, line))
            # count += 1


def main():
    # result_folder = os.getcwd() + "/Results/character_evolution/Scout_Heavy_GridSearch/Scout_Heavy_GridSearchData/"
    # output_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_GridSearch/Scout_Heavy_GridSearch-evaluation.csv"
    result_folder = os.getcwd() + "/Results/character_evolution/Scout_Heavy_MOTest/"
    output_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_MOTest-evaluation.csv"
    from_classification = False

    nr_ea_targets = 2
    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    grid_data = parse_result_files(result_folder, nr_ea_targets, initial_class1, initial_class2,
                                          from_classification)
    grid_dict = reshape_grid_data(grid_data)

    write_output(grid_data, output_file)
    # write_value_grid(grid_dict, output_file.replace("evaluation", "fitness_grid"), "fitness")
    # write_value_grid(grid_dict, output_file.replace("evaluation", "delta_team0ratio_grid"), "delta_team0ratio")
    # write_value_grid(grid_dict, output_file.replace("evaluation", "delta_time_grid"), "delta_time")
    write_value_grid(grid_dict, output_file.replace("evaluation", "dist_to_originals_sum_grid"), "dist_to_originals_sum")
    write_value_grid(grid_dict, output_file.replace("evaluation", "dist_to_originals_avg_grid"), "dist_to_originals_avg")
    write_value_grid(grid_dict, output_file.replace("evaluation", "dist_to_originals_grid"), "dist_to_originals")
    #
    # write_value_list(grid_data, output_file.replace("evaluation", "fitness"), "fitness")
    # write_value_list(grid_data, output_file.replace("evaluation", "delta_team0ratio"), "delta_team0ratio")
    # write_value_list(grid_data, output_file.replace("evaluation", "delta_time"), "delta_time")


if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main()