import os
import numpy as np
from character_evolution.evolution import weapon_parameters as wp


class EvolvedClass:
    def __init__(self, param_list1, param_list2, for_map, damage_class_only=True):
        self.params1 = param_list1
        self.params2 = param_list2
        self.map = for_map
        self.dmg_only = damage_class_only
        self.tf2_classes = wp.tf2_dmg_classes if damage_class_only else sorted(wp.tf2_weapon_param_dict.keys())
        self.distances = (self.compute_tf2_distances(self.params1), self.compute_tf2_distances(self.params2))
        self.differences = (self.compute_tf2_differences(self.params1), self.compute_tf2_differences(self.params2))
        self.nearest = self.compute_nearest_tf2(self.distances)

    def compute_tf2_distances(self, param_list):
        distances = {}
        for cls in self.tf2_classes:
            pair_wise = [(tf2 - ea)**2 for tf2, ea in zip(wp.tf2_weapon_param_dict[cls], param_list)]
            distances[cls] = np.sqrt(sum(pair_wise))
        return distances

    def compute_tf2_differences(self, param_list):
        distances = {}
        for cls in self.tf2_classes:
            pair_wise = [(ea - tf2) for tf2, ea in zip(wp.tf2_weapon_param_dict[cls], param_list)]
            distances[cls] = np.mean(pair_wise)
        return distances

    def compute_nearest_tf2(self, distances, threshold=1.5):
        result = []
        for ea_cls in distances:
            nearest = min(ea_cls, key=ea_cls.get)
            result.append(nearest if ea_cls[nearest] < threshold else "Other")
        return result


def parse_game_input_file(gi_file):
    ea_classes = []
    with open(gi_file, 'r') as inpt:
        lines = inpt.readlines()
        # each line looks like this: map, 8 player1 params, 8 player2 params.
        for line in lines:
            elems = line.rstrip().split(",")
            class1 = [float(el) for el in elems[1:9]]
            class2 = [float(el) for el in elems[9:]]
            ea_classes.append(EvolvedClass(class1, class2, elems[0]))
    return ea_classes


def write_output(ea_classes, out_file, use_difference):
    print("Use difference: ", use_difference)
    with open(out_file, 'w') as out:
        count = 1
        if use_difference:
            tf2_classes1 = ",".join("diff P1 {0}".format(t) for t in ea_classes[0].tf2_classes)
            tf2_classes2 = ",".join("diff P2 {0}".format(t) for t in ea_classes[0].tf2_classes)
        else:
            tf2_classes1 = ",".join("dist P1 {0}".format(t) for t in ea_classes[0].tf2_classes)
            tf2_classes2 = ",".join("dist P2 {0}".format(t) for t in ea_classes[0].tf2_classes)
        header = "num, p1 nearest, p2 nearest, {0}, {1}, p1 params, p2 params, map\n".format(tf2_classes1, tf2_classes2)
        out.write(header)
        for ea_cls in ea_classes:
            if use_difference:
                d1 = ",".join(["{:.2f}".format(ea_cls.differences[0][d]) for d in ea_cls.tf2_classes])
                d2 = ",".join(["{:.2f}".format(ea_cls.differences[1][d]) for d in ea_cls.tf2_classes])
            else:
                d1 = ",".join(["{:.2f}".format(ea_cls.distances[0][d]) for d in ea_cls.tf2_classes])
                d2 = ",".join(["{:.2f}".format(ea_cls.distances[1][d]) for d in ea_cls.tf2_classes])
            line = [str(count), *ea_cls.nearest, d1, d2, "_".join(str(p) for p in ea_cls.params1),
                    "_".join(str(p) for p in ea_cls.params2), ea_cls.map]
            line = ",".join([e for e in line])
            out.write(line + "\n")
            count += 1


def main():
    game_input_file = os.getcwd() + "/character_evolution/Results/character_evolution/Scout_Heavy_SO/SO-game_input.csv"
    output_file = os.getcwd() + "/character_evolution/Results/character_evolution/Scout_Heavy_SO/Scout_Heavy-distances.csv"
    # output_file = os.getcwd() + "/character_evolution/Results/character_evolution/Scout_Heavy_SO/Scout_Heavy-differences.csv"
    parsed_ea_classes = parse_game_input_file(game_input_file)
    write_output(parsed_ea_classes, output_file, use_difference="difference" in output_file)


if __name__ == "__main__":
    main()