import numpy as np
from character_evolution.evolution import weapon_parameters as wp
from common.EvolvedIndividualData import EvolvedIndividual


class EvolvedClass(EvolvedIndividual):
    def __init__(self, for_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets,
                 damage_class_only=True, orig_c1="", orig_c2="", from_classification=False):
        # setup values via EvolvedIndividual
        super().__init__(for_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets,
                         damage_class_only, orig_c1, orig_c2, from_classification)
        # inferred values
        self.tf2_classes = wp.tf2_dmg_classes if damage_class_only else sorted(wp.tf2_weapon_param_dict.keys())
        self.distances = None
        self.dist_to_originals = ()
        self.match_dist_to_original = -1
        # self.dist_to_originals_sum = -1
        # self.dist_to_originals_avg = -1
        self.nearest = ""
        self.changed_params = []
        self.p_is_d = []
        self.infer_parameter_metrics()

    def infer_parameter_metrics(self):
        self.distances = (compute_tf2_distances(self.params1, self.tf2_classes),
                          compute_tf2_distances(self.params2, self.tf2_classes))
        self.nearest = compute_nearest_tf2(self.distances)
        self.p_is_d = [self.desired_classes[self.gmn[idx]] == self.prediction_classes[idx] for idx in range(len(self.gmn))]

        if self.original_class1 and self.original_class2:
            self.dist_to_originals = (self.distances[0][self.original_class1], self.distances[1][self.original_class2])
            self.match_dist_to_original = compute_tf2_match_dist(self.params1 + self.params2, self.original_class1, self.original_class2)
            # self.dist_to_originals_sum = self.dist_to_originals[0] + self.dist_to_originals[1]
            # self.dist_to_originals_avg = self.dist_to_originals_sum / 2.0
            self.changed_params = self.compute_changed_params()

    def compute_changed_params(self):
        c1_changes = compute_changes_between(wp.tf2_weapon_param_dict[self.original_class1], self.params1)
        c2_changes = compute_changes_between(wp.tf2_weapon_param_dict[self.original_class2], self.params2)
        return c1_changes + c2_changes

    def get_header(self, external_line_number=True):
        header = super().get_header(external_line_number)
        tf2_classes1 = ",".join("dist P1 {0}".format(t) for t in self.tf2_classes)
        tf2_classes2 = ",".join("dist P2 {0}".format(t) for t in self.tf2_classes)
        header += ",p1 nearest, p2 nearest, {0}, {1}, p1 params, p2 params, ".format(tf2_classes1, tf2_classes2)
        if self.changed_params:
            changed_params = ["C1 {0} change".format(c) for c in wp.all_params] +\
                             ["C2 {0} change".format(c) for c in wp.all_params]
        else:
            changed_params = []

        header = header + ",".join(changed_params)
        return header

    def to_string(self, add_new_line=True):
        line = super().to_string(add_new_line=False)
        d1 = ",".join(["{:.2f}".format(self.distances[0][d]) for d in self.tf2_classes])
        d2 = ",".join(["{:.2f}".format(self.distances[1][d]) for d in self.tf2_classes])
        param_vals = [*self.nearest, d1, d2,
                      "_".join(str(p) for p in self.params1),
                      "_".join(str(p) for p in self.params2)]
        line = line + "," + ",".join(param_vals)

        if self.changed_params:
            changed_params = ["{0:.2f}".format(p) for p in self.changed_params]
        else:
            changed_params = []
        line = line + "," + ",".join(changed_params)
        if add_new_line:
            line += "\n"
        return line


def compute_tf2_distances(param_list, tf2_classes):
    distances = {}
    for cls in tf2_classes:
        pair_wise = [(tf2 - ea)**2 for tf2, ea in zip(wp.tf2_weapon_param_dict[cls], param_list)]
        distances[cls] = np.sqrt(sum(pair_wise))
    return distances


def compute_tf2_match_dist(match_param_list, original_class1, original_class2):
    tf2_match = wp.tf2_weapon_param_dict[original_class1] + wp.tf2_weapon_param_dict[original_class2]
    dist = np.sqrt(sum([(tf2 - ea)**2 for tf2, ea in zip(tf2_match, match_param_list)]))
    return dist


def compute_nearest_tf2(distances, threshold=1.5):
    result = []
    for ea_cls in distances:
        nearest = min(ea_cls, key=ea_cls.get)
        result.append(nearest if ea_cls[nearest] < threshold else "Other")
    return result


def compute_changes_between(old_params, new_params):
    assert len(old_params) == len(new_params), "Cannot compare parameters of different lenghts!"
    changes = [new_params[idx] - old_params[idx] for idx in range(len(old_params))]
    print("compute_changes_between: hardcoded division by two for Range")
    changes[-1] = changes[-1] / 2.0
    return changes

