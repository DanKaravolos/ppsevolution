import cPickle as pickle
import os


def compute_aggregated(pred_time, pred_score, target_time, target_score=0.5):
    return abs(pred_time - target_time) / target_time + abs(pred_score - target_score) / target_score


def compute_squared_error(pred_time, pred_score, target_time, target_score=0.5):
    return (pred_time - target_time) ** 2 + (pred_score - target_score) ** 2


def parse_fitnesses(data_file):
    data = []
    time_index = -1
    score_index = -1
    with open(data_file, 'r') as infile:
        lines = infile.readlines()
        for line in lines:
            if line.startswith(","):
                continue
            if line.startswith("map,"):
                if time_index == -1:
                    elems = line.rstrip().split(",")
                    for i in range(len(elems)):
                        if elems[i] == "mean time":
                            time_index = i
                        elif elems[i] == "mean score":
                            score_index = i
                else:
                    continue
            else:
                elems = line.rstrip().split(",")
                # keep only the "means"  (default: 2 and 4)
                data.append([elems[0], elems[1], float(elems[time_index]), float(elems[score_index])])
    return data


def write_fitness_to_file(data):
    target_time = [0.085, 0.315, 1]
    index = 0
    counter = 0
    for d in data:
        if "maximize_score" in d[1] or "minimize_score" in d[1] or "target_score" in d[1]:
            d.append(d[3])
        elif "aggregated" in d[1]:
            aggr = compute_aggregated(d[2], d[2], target_time[index])
            d.append(aggr)
            counter += 1
            if counter % 20 == 0:
                index += 1
                index = index % 3
        elif "squared_error" in d[1]:
            sq = compute_squared_error(d[2], d[2], target_time[index])
            d.append(sq)
            counter += 1
            if counter % 20 == 0:
                index += 1
                index = index % 3

    counter = 0
    with open(data_f.replace(".csv", "-inside_pkl.csv"), "w") as out:
        for d in data:
            line = ",".join([str(elem) for elem in d])
            out.write(line + "\n")
            counter += 1
            if counter % 20 == 0:
                out.write("\n")


def main(data_file, result_file):
    data = parse_fitnesses(data_file)
    with open(result_file, 'wb') as out:
        pickle.dump(data, out)

    write_fitness_to_file(data)


if __name__ == "__main__":
    data_name = "ant_mse2-P_100-gen_100-mut_0.10-crs_0.20"
    data_f = os.getcwd() + "/results/{}-fitnesses.csv".format(data_name)
    result_f = data_f.replace("csv", "pkl")

    main(data_f, result_f)
