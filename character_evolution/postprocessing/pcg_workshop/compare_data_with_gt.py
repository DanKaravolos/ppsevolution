from __future__ import print_function
import numpy as np
from scipy.stats import normaltest, ttest_ind, mannwhitneyu
import scipy.stats as st
import os
import pickle as pickle


def ttest(map_name, fitness, data_time, data_score, gt_time, gt_score):
    dt_isnormal = normaltest(data_time)
    gt_isnormal = normaltest(gt_time)
    time_val = ttest_ind(data_time, gt_time, equal_var=False)
    ds_isnormal = normaltest(data_score)
    gs_isnormal = normaltest(gt_score)
    score_val = ttest_ind(data_score, gt_score, equal_var=False)

    time_line = "{0},{1} : time is normal? d {2:.2f}, g {3:.2f}. ttest val: {4:.2f}, {5:.4f}".format(map_name, fitness,
                  dt_isnormal.pvalue, gt_isnormal.pvalue,time_val.statistic, time_val.pvalue)
    score_line = "{0},{1} : score is normal? d {2:.2f}, g {3:.2f}. ttest val: {4:.2f}, {5:.4f}".format(map_name,
                  fitness, ds_isnormal.pvalue, gs_isnormal.pvalue, score_val.statistic, score_val.pvalue)
    print(time_line)
    print(score_line)
    return time_val.pvalue, score_val.pvalue


def test_for_normal_gt_data(gt_time, gt_score):
    # Test only for normal distribution
    gt_isnormal = normaltest(gt_time)
    gs_isnormal = normaltest(gt_score)
    print("Ground truth is normal? Time:  {0:.2f}  , Score :  {1:.2f}".format(gt_isnormal.pvalue, gs_isnormal.pvalue))


def mannwitneyutest(map_name, fitness, data_time, data_score, gt_time, gt_score):
    time_val = mannwhitneyu(data_time, gt_time)
    score_val = mannwhitneyu(data_score, gt_score)

    line = "{0},{1}: MWU test, p time : {2:.2f}, p score : {3:.2f}".format(map_name, fitness,
                                                                                time_val.pvalue, score_val.pvalue)
    print(line)
    return time_val.pvalue, score_val.pvalue


def ci1(gt, confidence):
    conf_dict = {
        0.99: 2.576,
        0.98: 2.326,
        0.95: 1.95,
        0.90: 1.645
    }
    mn = np.mean(gt)
    std = np.std(gt)
    z = conf_dict[confidence]
    n = len(gt)
    iv = z * (std / np.sqrt(n))
    ci_low = mn - iv
    ci_high = mn + iv
    return ci_low, ci_high, mn


def within_confidence_interval(map_name, fitness, data_time, data_score, gt_time, gt_score, confidence=0.95):
    ci_bounds_time = ci1(gt_time, confidence)
    ci_bounds_score = ci1(gt_score, confidence)

    # if num_runs > 1:
    data_time = np.mean(data_time)
    data_score = np.mean(data_score)

    t_in_ci = ci_bounds_time[0] <= data_time <= ci_bounds_time[1]
    s_in_ci = ci_bounds_score[0] <= data_score <= ci_bounds_score[1]
    s_low1 = ci_bounds_score[0] == 1
    print("{0},{1}, time in CI, {2}, score in CI, {3}, score low bound = 1?, {4} ".format(map_name, fitness,
                                                                                          t_in_ci, s_in_ci, s_low1))

    out_file = os.getcwd() + "/groundtruths/double_{0}-confidence-p_{1}_runs.csv".format(data_name, num_runs)
    write_header = not os.path.exists(out_file)
    with open(out_file, 'a') as out:
        if write_header:
            out.write("map, fitness, time mean, time ci low, time ci high, time pred, time pred within ci, "
                      "score mean, score ci low, score ci high, score pred, score pred within ci\n")

        items = [map_name, fitness,
                 str(ci_bounds_time[2]), str(ci_bounds_time[0]), str(ci_bounds_time[1]), str(data_time), str(t_in_ci),
                 str(ci_bounds_score[2]), str(ci_bounds_score[0]), str(ci_bounds_score[1]), str(data_score), str(s_in_ci)]
        line = ",".join(items)
        out.write(line + "\n")
    return t_in_ci, s_in_ci


def compare_values(data, gt):
    map_name = data[0][0]
    map_name = map_name.replace(os.getcwd(), "")
    fitness = data[0][1]
    data_time = [d[2] for d in data]
    data_score = [d[3] for d in data]
    gt_time = np.asarray([d[0] for d in gt]).flatten()
    gt_score = np.asarray([d[1] for d in gt]).flatten()

    p_score = p_time = -1

    # test_for_normal_gt_data(gt_time, gt_score)
    # ttest(map_name, fitness, data_time, data_score, gt_time, gt_score)
    # p_time, p_score = mannwitneyutest(map_name, fitness, data_time, data_score, gt_time, gt_score)

    within_confidence_interval(map_name, fitness, data_time, data_score, gt_time, gt_score, confidence=0.95)
    return map_name, fitness, p_time, p_score


def main(data_file, gt_file, result_file, nr_runs_per_setting):
    with open(data_file, 'rb') as data_in:
        data = pickle.load(data_in)
    gt = np.load(gt_file)

    for index in range(0, len(data), nr_runs_per_setting):
        d_slice = data[index:index+nr_runs_per_setting]
        gt_slice = gt[index:index+nr_runs_per_setting]
        result = compare_values(d_slice, gt_slice)


if __name__ == "__main__":
    gt_f = os.getcwd() + "/character_evolution/groundtruths/mse_exp-gt_data2_parsed.npy"
    data_name = "ant_mse2-P_100-gen_100-mut_0.10-crs_0.20"
    data_f = os.getcwd() + "/character_evolution/results/{}-fitnesses.pkl".format(data_name)
    result_f = os.getcwd() + "/character_evolution/results/double_{}-gt_comp.csv".format(data_name)

    num_runs = 1  # set 1 for per class evaluation or to 20 for per time target/map group
    main(data_f, gt_f, result_f, num_runs)
