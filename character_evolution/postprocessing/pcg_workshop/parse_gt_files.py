from __future__ import print_function, division
import os
from os import path
import numpy as np
from common.common import list_to_string
from common.common import normalize_time
from tqdm import tqdm


def get_num_from_filename(filename):
    elems = filename.split("_", 2)[1]
    return int(elems)


def parse_folder(folder):
    f_tag = "/Level1/"
    game_folders = [folder + fold + f_tag for fold in os.listdir(folder) if path.isdir(folder + fold)]
    game_folders = sorted(game_folders)
    sum_files = []
    for f in game_folders:
        g_files = [[f + name, get_num_from_filename(name) ] for name in os.listdir(f) if "Summary" in name and path.isfile(f + name)]
        g_files = sorted(g_files, key=lambda x: x[1])
        sum_files.append(g_files)
    return sum_files


def parse_files(files_per_run):
    # desired data format: data_points x value (time, score) x run
    data = np.zeros((len(files_per_run[0]), 2, len(files_per_run)))
    for run in range(len(files_per_run)):
        for data_point in range(len(files_per_run[run])):
            filename = files_per_run[run][data_point][0]
            time, score = extract_time_and_score(filename)
            data[data_point, 0, run] = time
            data[data_point, 1, run] = score
    return data


def extract_time_and_score(filename):
    time = -1.0
    score = -1.0
    with open(filename, 'r') as infile:
        lines = infile.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            key = elems[0]
            if key.startswith("TotalTime"):
                time = normalize_time(float(elems[1]))
            elif key.startswith("Team0Kills"):
                if elems[2] == "NaN":
                    score = 0.5
                else:
                    score = float(elems[2])
    if time == -1:
        print("Error while reading : {0}. No time value found.".format(filename))
    if score == -1:
        print("Error while reading : {0}. No score value found.".format(filename))
    return time, score


def write_csv_result(data_array, out_file, fit_with_ea_output=False):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    with open(out_file, "w") as out:
        header = ["time"] * data_array.shape[-1]
        header += ["score"] * data_array.shape[-1]
        header = ",".join(header)
        top_header = header + ",GT time mean,time std,time CI 95% low,time CI 95% high, GT score mean, score std,score CI 95% low,score CI 95% high"
        out.write("{0}\n".format(top_header))
        for row in data_array:
            out.write("{0},{1}".format(list_to_string(row[0]), list_to_string(row[1])))
            for dim in row:
                mean = np.mean(dim)
                std = np.std(dim)
                bound = 1.96 * std / np.sqrt(len(dim))
                lower = mean - bound
                upper = mean + bound
                out.write(",{0:.6f}, {1:.6f},{2:.6f}, {3:.6f}".format(mean, std, lower, upper))
            out.write("\n")
            counter += 1
            if fit_with_ea_output and counter % 20 == 0:
                out.write("\n\n{0}\n".format(header))


def main(folder, output_file):
    print("parsing...")
    list_of_files_per_run = parse_folder(folder)
    data_array = parse_files(list_of_files_per_run)
    print("saving...")
    write_csv_result(data_array, output_file + ".csv")
    #
    # print("Writing to numpy : {}.npy".format(output_file))
    # np.save(output_file + ".npy", data_array)


def single():
    # gt_path = os.getcwd() + "/groundtruths/mse_exp-gt_data/"
    gt_path = os.getcwd() + "/character_evolution/TF2Mut_1GT/"
    result_file = gt_path[:-1] + "_parsed"
    main(gt_path, result_file)


def all_in_folder():
    gt_folder = os.getcwd() + "/character_evolution/Groundtruths/"
    gt_paths = [gt_folder + fo +'/' for fo in os.listdir(gt_folder) if os.path.isdir(gt_folder + fo)]
    for gt_path in tqdm(gt_paths):
        result_file = gt_path[:-1] + "_parsed"
        main(gt_path, result_file)

if __name__ == "__main__":
    # single()
    all_in_folder()
