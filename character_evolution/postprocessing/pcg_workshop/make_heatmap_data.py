from __future__ import print_function
import os
from os import path
import numpy as np
from tqdm import *
from parse_gt_playtraces_for_height import parse_folder, parse_pos_height


def parse_files(team0files, team1files, heatmap_res):
    # desired data format: data_points x value (time, score) x run
    nr_runs = len(team0files)
    if len(team1files) != nr_runs:
        print("Error! Nr of files team0 and team1 are not equal!")
        return None

    data = np.zeros((len(team0files[0]), 2, 3, nr_runs, heatmap_res, heatmap_res))  # map/weapons x teams x death, pos, shot, x runs x positions
    for run in range(nr_runs):
        for setting in tqdm(range(len(team0files[run]))):

            team0file = team0files[run][setting][0]
            t0death, t0pos, t0shots = extract_death_and_pos(team0file, heatmap_res)
            data[setting, 0, 0, run] = t0death
            data[setting, 0, 1, run] = t0pos
            data[setting, 0, 2, run] = t0shots

            team1file = team1files[run][setting][0]
            t1death, t1pos, t1shots = extract_death_and_pos(team1file, heatmap_res)
            data[setting, 1, 0, run] = t1death
            data[setting, 1, 1, run] = t1pos
            data[setting, 1, 2, run] = t1shots

    return data


# map width = 4 * 5 * 4 = 80
# lower left -40, -40 is translated to 0,0
def parse_pos(pos_string, heatmap_res=4):
    p_elems = pos_string.split(",")
    int_elems = [int(p_elems[1]), int(p_elems[3])]
    map_width = 80.0
    row0 = (int_elems[0] + (0.5 * map_width))
    row1 = row0 / map_width
    row2 = row1 * heatmap_res
    row = int(row2)
    alt_row = int((int_elems[0] + (0.5 * map_width)) / map_width * heatmap_res)
    col = int((int_elems[1] + (0.5 * map_width)) / map_width * heatmap_res)

    # if row > 3:
    #     row = 3
    # if col > 3:
    #     col = 3
    hm_elems = [min(row, 3), min(col, 3)]
    return hm_elems


def get_hist(pos_xzs, hm_res):
    hm = np.zeros((hm_res, hm_res))
    for pos in pos_xzs:
        hm[hm_res - 1 - pos[0], pos[1]] += 1
    return hm


def extract_death_and_pos(team_file, heatmap_res):
    with open(team_file, 'r') as inpt:
        lines = inpt.readlines()
        pos_xzs = []
        shot_xzs = []
        death_xzs = []
        previous_time_stamp = ""
        for line in lines[4:-1]:
            elems = line.rstrip().split("|")
            pos_height = parse_pos_height(elems[1])
            # maybe skip this row
            if pos_height < 0:
                continue

            # get data
            specifier = elems[-1]
            xz_pos = parse_pos(elems[1], heatmap_res)
            if specifier == "Shooting":
                shot_xzs.append(xz_pos)
            elif specifier == "Agent Dead":
                death_xzs.append(xz_pos)
            else:
                if elems[0] != previous_time_stamp:
                    pos_xzs.append(xz_pos)

            previous_time_stamp = elems[0]

            # print("element extracted")
        # print("parsing done")
        # store count and ratio.
        death_vals = get_hist(death_xzs, heatmap_res)
        pos_vals = get_hist(pos_xzs, heatmap_res)
        shot_vals = get_hist(shot_xzs, heatmap_res)

    return death_vals, pos_vals, shot_vals


def main(folder, output_file, heatmap_res):
    print("parsing...")
    team0_files_per_run = parse_folder(folder, "Team_0")
    team1_files_per_run = parse_folder(folder, "Team_1")

    data_array = parse_files(team0_files_per_run, team1_files_per_run, heatmap_res)

    print("Writing to numpy : {}.npy".format(output_file))
    np.save(output_file + ".npy", data_array)

if __name__ == "__main__":
    gt_path = os.getcwd() + "/groundtruths/mse_exp-gt_data/"
    result_file = gt_path[:-1] + "2_heightmaps"
    cells_per_axis = 4
    main(gt_path, result_file, cells_per_axis)


