from __future__ import print_function
import os
from os import path
import numpy as np
from tqdm import *
from common.common import list_to_string


def get_num_from_filename(filename):
    elems = filename.split("_", 2)[1]
    return int(elems)


def parse_folder(folder, keyword):
    f_tag = "/Level1/"
    game_folders = [folder + fold + f_tag for fold in os.listdir(folder) if path.isdir(folder + fold)]
    game_folders = sorted(game_folders)
    sum_files = []
    for f in game_folders:
        g_files = [[f + name, get_num_from_filename(name) ] for name in os.listdir(f) if keyword in name and path.isfile(f + name)]
        g_files = sorted(g_files, key=lambda x: x[1])
        sum_files.append(g_files)
    return sum_files


def parse_files(team0files, team1files):
    # desired data format: data_points x value (time, score) x run
    nr_runs = len(team0files)
    if len(team1files) != nr_runs:
        print("Error! Nr of files team0 and team1 are not equal!")
        return None

    data = np.zeros((len(team0files[0]), 2, 3, nr_runs, 3))  #map/weapons x teams x death, pos, shot, x runs x count , ratio
    for run in range(nr_runs):
        for setting in tqdm(range(len(team0files[run]))):

            team0file = team0files[run][setting][0]
            t0death, t0pos, t0shots = extract_death_and_pos(team0file)
            data[setting, 0, 0, run] = t0death
            data[setting, 0, 1, run] = t0pos
            data[setting, 0, 2, run] = t0shots

            team1file = team1files[run][setting][0]
            t1death, t1pos, t1shots = extract_death_and_pos(team1file)
            data[setting, 1, 0, run] = t1death
            data[setting, 1, 1, run] = t1pos
            data[setting, 1, 2, run] = t1shots

    return data


def parse_pos_height(pos_string):
    p_elems = pos_string.split(",")
    height = int(p_elems[2])
    return height


def get_count_and_ratio(array):
    nr_items = len(array)
    if nr_items == 0:
        return 0, 0, 0

    count = sum([1 for d in array if d > 0])
    ratio = count / float(nr_items)
    return nr_items, count, ratio


def extract_death_and_pos(team_file):
    with open(team_file, 'r') as inpt:
        lines = inpt.readlines()
        pos_heights = []
        shot_heights = []
        death_heights = []
        previous_time_stamp = ""
        for line in lines[4:-1]:
            elems = line.rstrip().split("|")
            pos_height = parse_pos_height(elems[1])

            # maybe skip this row
            if pos_height < 0:
                continue

            # get data
            specifier = elems[-1]
            if specifier == "Shooting":
                shot_heights.append(pos_height)
            elif specifier == "Agent Dead":
                death_heights.append(pos_height)
            else:
                if elems[0] != previous_time_stamp:
                    pos_heights.append(pos_height)

            previous_time_stamp = elems[0]

            # print("element extracted")
        # print("parsing done")
        # store count and ratio.
        death_vals = np.asarray(get_count_and_ratio(death_heights))
        pos_vals = np.asarray(get_count_and_ratio(pos_heights))
        shot_vals = np.asarray(get_count_and_ratio(shot_heights))

    return death_vals, pos_vals, shot_vals


def write_csv_result(data_array, out_file, fit_with_ea_output=True):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    with open(out_file, "w") as out:

        top_header = ",".join(["team1 (orange)"] * 9 + ["team2 (purple)"] * 9)
        header = "death total, floor 1 deaths, floor 1 death ratio," +\
                "pos total, floor 1 pos, floor 1 pos ratio," +\
                "shot total, floor 1 shots, floor 1 shot ratio,"
        out.write("{0}\n{1}\n".format(top_header, header + header))
        for row in data_array:
            team0 = row[0]
            team1 = row[1]
            summary = []
            for attr in team0:
                summary.extend(np.mean(attr, axis=0))
            for attr in team1:
                summary.extend(np.mean(attr, axis=0))
            out.write("{0}\n".format(list_to_string(summary)))
            counter += 1
            if fit_with_ea_output and counter % 20 == 0:
                out.write("\n\n{0}\n".format(header))


def main(folder, output_file):
    print("parsing...")
    team0_files_per_run = parse_folder(folder, "Team_0")
    team1_files_per_run = parse_folder(folder, "Team_1")

    data_array = parse_files(team0_files_per_run, team1_files_per_run)

    print("saving...")
    write_csv_result(data_array, output_file + ".csv")
    print("Writing to numpy : {}.npy".format(output_file))
    np.save(output_file + ".npy", data_array)

if __name__ == "__main__":
    gt_path = os.getcwd() + "/groundtruths/mse_exp-gt_data/"
    result_file = gt_path[:-1] + "2_heights"
    main(gt_path, result_file)


