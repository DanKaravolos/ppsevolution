import os
from character_evolution.evolution.weapon_parameters import convert_weapon_back


def parse_game_input_file(result_file):
    params = []
    with open(result_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in sorted(lines):
            elems = line.rstrip().split(",")
            match = [float(f) for f in elems[1:]]
            params.append(match)
    print("parsing done.")
    return params


def write_convert_file(parameters, out_file):
    with open(out_file, 'w') as out:
        for match in parameters:
            match1 = convert_weapon_back(match)
            string_match = ",".join(["{0}".format(f) for f in match1])
            out.write("{0}\n".format(string_match))


def main():
    result_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_BestParams/single_obj-mut_0.20-crs_1.00-Scout_Heavy-game_input.csv"
    out_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_BestParams/single_obj-mut_0.20-crs_1.00-Scout_Heavy-unNormalized.csv"
    parameters = parse_game_input_file(result_file)
    write_convert_file(parameters, out_file)


if __name__ == "__main__":
    main()