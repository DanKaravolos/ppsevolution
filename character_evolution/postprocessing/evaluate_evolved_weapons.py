'''
This file parses results files and groundtruth summaries into a datastructure that contains all information necessary
for evaluating the results of evolution. Note that I am assuming that the results are presented in the same order as
the game_input file that was used to create the GT values. This means that I assume that the level numbers in the GT 
file correspond to the order of the results in order to link the data in the two files. So: level_num = result_nr + 1.
If you get weird results, you might be violating that assumption.

More info about the pipeline:
After running evaluation, use common.concat_files_in_folder to concatenate the game_input and result files.
the resulting game_input file are used to get GroundTruth values from simulations, while the resulting result-file is 
used here. After you have obtained the GT values, put the LogFiles folders in new folders in
the data folder of PyProcShooter (on windows) and run parse_summary_files. 
Use the resulting summary PKL-file as the GT file here.

'''

from sys import exit
import os
import numpy as np
from collections import defaultdict
import pickle
from character_evolution.postprocessing.EvolvedWeaponData import EvolvedClass
from character_evolution.postprocessing.EvolvedWeaponSummary import EvolvedWeaponSummary
from common.evaluate_individual import *


def parse_result_file(gi_file, nr_ea_targets=2, initial_class1="", initial_class2="", from_classification=False):
    ea_classes = []
    with open(gi_file, 'r') as inpt:
        lines = inpt.readlines()
        header_elems = lines[0].rstrip().split(",")
        # I am assuming the following layout: map, fitness, fitness value, prediction for each game metric,
        # normalized class1 params, normalized class 2 params, stuff that we don't need now
        class_start_finder = (i for i, e in enumerate(header_elems) if e == "HP")  #based on https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-given-a-list-containing-it-in-python
        c1_index = next(class_start_finder)
        c2_index = next(class_start_finder)
        for line in lines[1:]:
            if line.startswith("map"):  # these are header lines. This might introduce new targets, so we copy it.
                header_elems = line.rstrip().split(",")
                continue
            elems = line.rstrip().split(",")
            game_metric_names = header_elems[3:3 + nr_ea_targets] if "multi" not in elems[1] else header_elems[3 + nr_ea_targets:3 + 2 * nr_ea_targets]
            game_metrics = elems[3:3 + nr_ea_targets] if "multi" not in elems[1] else elems[3 + nr_ea_targets:3 + 2 * nr_ea_targets]
            # N.B. I AM NOW ASSUMING MOEA will have output with MSE fitness before individual targets, so:
            #  map, fitness, MSE value, fitness for each game metric, prediction for each game metric,
            class1 = [float(el) for el in elems[c1_index:c1_index + 8]]
            class2 = [float(el) for el in elems[c2_index:c2_index + 8]]
            targets = header_elems[-nr_ea_targets:]
            map_name = elems[0].replace("/home/daniel/Projects/ppsevolution/data/maps/", "").replace("_Run_0_Map.csv", "")
            ea_classes.append(EvolvedClass(map_name, class1, class2, elems[2], game_metric_names, game_metrics, targets,
                                           orig_c1=initial_class1, orig_c2=initial_class2,
                                           from_classification=from_classification))
    return ea_classes


def parse_gt_file(gt_file, dims):
    num_to_gt_dict = defaultdict(list)
    try:
        with open(gt_file, 'rb') as inpt:
            df = pickle.load(inpt)
            gt_df = df.loc[:, dims]
            gt_df['time'] = (gt_df['time'] - 150) / 450

            for line in gt_df.values:
                try:
                    fi = line[0].rsplit("\\", 1)[1]
                except IndexError as err:
                    # print(err)
                    # print("It seems we are using Linux")
                    fi = line[0].rsplit("/", 1)[1]

                num = int(fi.split("_")[1])
                num_to_gt_dict[num].append(line[1:])
    except FileNotFoundError:
        exit("No GT file found in parse_gt_file: " + gt_file)
    return num_to_gt_dict


def concat_classes_and_gt(parsed_ea_classes, num_to_gt_dict):
    dp_w_no_gt = []
    for idx in range(len(parsed_ea_classes)):
        varr = np.asarray(num_to_gt_dict[idx + 1], dtype=np.float32)
        parsed_ea_classes[idx].add_gt_values(varr)
        print("Data point: {0}. nr GT values: {1}".format(idx + 1, len(varr)))
        if len(varr) == 0:
            dp_w_no_gt.append("Data point: {0}".format(idx + 1))
    return dp_w_no_gt


def evaluate_results(result_file,gt_file, output_file, from_classification, nr_ea_targets, initial_class1, initial_class2, write=True):
    parsed_ea_classes = parse_result_file(result_file, nr_ea_targets, initial_class1, initial_class2,
                                          from_classification)
    dims = ('file', 'team0ratio', 'time')
    try:
        num_to_gt_dict = parse_gt_file(gt_file, dims)
        empty_gt = concat_classes_and_gt(parsed_ea_classes, num_to_gt_dict)
        print("These data points have no GT:")
        for dp in empty_gt:
            print(dp)
    except FileNotFoundError:
        print("No GT File found.")

    pds = create_per_duration_summary(parsed_ea_classes, output_file, EvolvedWeaponSummary, write)
    pms = create_per_map_summary(parsed_ea_classes, output_file, EvolvedWeaponSummary, write)
    pdpms = create_per_duration_per_map_summary(parsed_ea_classes, output_file, EvolvedWeaponSummary, write)
    return parsed_ea_classes, pds, pms, pdpms


def main(moea_data, minor_revisions=True):
    if minor_revisions:
        if moea_data:
            # result_file = os.getcwd() + "/Results/character_evolution-MR/ScoutHeavy_MOEA_MR/summary-MO-result.csv"
            # gt_file = os.getcwd() + "/Results/character_evolution-MR/GT_summary-character_evolution-MOEA-GT.pkl"
            # output_file = os.getcwd() + "/Results/character_evolution-MR/ScoutHeavy_MOEA_MR/MO-evaluation.csv"
            # from_classification = False

            result_file = os.getcwd() + "/Results/character_evolution/ScoutHeavy-MOEA_replicate/summary-best_ind-result.csv"
            gt_file = os.getcwd() + "/Results/character_evolution/ScoutHeavy-MOEA_replicate/GT_summary-GT-runs.pkl"
            output_file = os.getcwd() + "/Results/character_evolution/ScoutHeavy-MOEA_replicate/MO-evaluation.csv"
            from_classification = False
        else:
            result_file = os.getcwd() + "/Results/character_evolution-MR/ScoutHeavy_SOEA_MR/results/summary-SO-result.csv"
            gt_file = os.getcwd() + "/Results/character_evolution-MR/GT_summary-character_evolution-SOEA-GT.pkl"
            output_file = os.getcwd() + "/Results/character_evolution-MR/ScoutHeavy_SOEA_MR/SO-evaluation.csv"
            from_classification = False
    else:
        if moea_data:
            result_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_MO/MO2-best_ind-result2.csv"
            gt_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_MO/GT_summary-ScoutHeavy_MO2.pkl"
            output_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_MO/Scout_Heavy_MO2-evaluation.csv"
            from_classification = False
        else:
            result_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_SO/ScoutHeavy_BestParams-results.csv"
            gt_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_SO/GT_summary-ScoutHeavy_BestParams.pkl"
            output_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_SO/SO-evaluation.csv"
            from_classification = False

    # result_file = os.getcwd() + "/Results/character_evolution/Original_TF2/tf2_class_scout_vs_heavy_AllMaps-simple.csv"
    # gt_file = os.getcwd() + "/Results/character_evolution/Original_TF2/GT_summary-OriginalTF2.pkl"
    # output_file = os.getcwd() + "/Results/character_evolution/Original_TF2/TF2-evaluation.csv"
    # from_classification = False

    # result_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_AllMaps_Cls/results/Scout_Heavy_AllMaps_Cls-result.csv"
    # output_file = os.getcwd() + "/Results/character_evolution/Scout_Heavy_AllMaps_Cls/Scout_Heavy_AllMaps_Cls-evaluation.csv"
    # from_classification = True

    nr_ea_targets = 2
    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    parsed_ea_classes, pds, pms, pdpms = evaluate_results(result_file,gt_file, output_file, from_classification, nr_ea_targets,
                                                          initial_class1, initial_class2)
    write_output(parsed_ea_classes, output_file)

if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main(moea_data=True)
