from __future__ import print_function
# from deap import tools
import numpy as np
tf2_all_classes = \
    {
        0: [0.0, 0.29, 1.0, 0.14, 0.43],
        1: [1.0, 0.41, 0.29, 0.0, 0.54, 0.05],
        2: [0.21, 0.82, 0.56, 0, -0.23, 1.0, 0.04, 0.47],
        3: [0.80, 0.30, 0.0, 0.60, 0.80, 0.95, 1.0],
        4: [0.03, 1.0, 0.02, 0.20, 0.12],
        5: [0.03, 0.04, 1.0, 0.24, 0.0, 0.02],
        6: [1, 0],
        7: [0, 1, 2]
    }

tf2_damage_classes = \
    {
        0: [0.0, 0.285714285714286, 1.0, 0.428571428571429],
        1: [1.0, 0.410714285714286, 0.0, 0.0535714286],
        2: [0.210526315789474, 0.8245614035, 0, 1.0, 0.4736842105],
        3: [0.80, 0.30, 0.60, 1.0],
        4: [0.0251256281407035, 1.0, 0.0150753768844221, 0.120603015075377],
        5: [0.0278481012658228, 0.04487917146145, 1.0, 0.00421940928270042, 0.0189873418],
        6: [1, 0],
        7: [0, 1, 2]
    }


def mutate(two_class_ind, mu=0.0, sigma=0.1, mutprob=0.1):
    for gene in range(len(two_class_ind)):
        if gene == 7 or gene == 15:
            if np.random.rand() <= mutprob:
                # sample from 0,1,2, not allowing current gene.
                new_val = np.random.randint(0, 3)
                while new_val == two_class_ind[gene]:
                    new_val = np.random.randint(0, 3)
                two_class_ind[gene] = new_val
        else:
            if np.random.rand() <= mutprob:
                two_class_ind[gene] += np.random.normal(mu, sigma)
                if two_class_ind[gene] > 1:
                    two_class_ind[gene] = 1
                elif two_class_ind[gene] < 0:
                    two_class_ind[gene] = 0
    return two_class_ind,


def tf2_mutate(two_class_ind, mutprob=0.1):
    for gene in range(len(two_class_ind)):
            # for all other genes, pick one of the options from TF2.
            if np.random.rand() <= mutprob:
                param_index = gene if gene < 8 else gene - 8
                new_val = np.random.choice(tf2_damage_classes[param_index])
                repeat_count = 0
                while new_val == two_class_ind[gene] and repeat_count < 100:
                    new_val = np.random.choice(tf2_damage_classes[param_index])
                    repeat_count += 1
                two_class_ind[gene] = new_val
    return two_class_ind,


def apply_initial_mutation(two_class_ind, nr_mutations, mu=0.0, sigma=0.1):
    for mutation in range(nr_mutations):
        # select a random gene and mutate it.
        gene = np.random.randint(len(two_class_ind))
        if gene == 7 or gene == 15:
            # sample from 0,1,2, not allowing current gene.
            new_val = np.random.randint(0, 3)
            while new_val == two_class_ind[gene]:
                new_val = np.random.randint(0, 3)
            two_class_ind[gene] = new_val
        else:
            previous_value = two_class_ind[gene]
            two_class_ind[gene] += np.random.normal(mu, sigma)
            if two_class_ind[gene] > 1:
                two_class_ind[gene] = 1
                if previous_value == 1 and two_class_ind[gene] == 1:
                    apply_initial_mutation(two_class_ind, 1)  # if the value has not changed, perform another mutation
            elif two_class_ind[gene] < 0:
                two_class_ind[gene] = 0
                if previous_value == 0 and two_class_ind[gene] == 0:
                    apply_initial_mutation(two_class_ind, 1)  # if the value has not changed, perform another mutation
    return two_class_ind

# def check_bounds(min_val, max_val):
#     def decorator(func):
#         def wrapper(*args, **kargs):
#             offspring = func(*args, **kargs)
#             for child in offspring:
#                 # if deviation bounds: check deviations
#                 # if bind_gene_deviations > 0:
#                 #     enforce_deviation_bounds(child, original_genes, bind_gene_deviations)
#                 for i in xrange(len(child)):
#                     # Then, check min/max bounds
#                     if child[i] > max_val:
#                         child[i] = max_val
#                     elif child[i] < min_val:
#                         child[i] = min_val
#             return offspring
#         return wrapper
#     return decorator

# compute difference wrt original weapon.
# def enforce_deviation_bounds(child, orig, dev):
#     if dev == 0:
#         print "ERROR: Enforcing bound of zero. Child will not deviate from original. This is not what you want."
#         return orig
#     for i in xrange(len(child)):
#         diff = child[i] - orig[i]
#         if abs(diff) > dev:
#             if diff < 0:
#                 child[i] = orig[i] - dev
#             elif diff > 0:
#                 child[i] = orig[i] + dev


