# '''
# This program crawls through a folder, finding all pareto front or final population files and computes all
# fitness-related metrics for each individual. This is written to a file in the same folder.
# Additionally, a summary fil is written with the averages per experiment.
# NOTE: THE POPULATIONS ARE SORTED AND CONCATENATED INTO ONE BATCH MANUALLY BASED ON THE NUMBER OF RUNS,
# THERE IS NO NAME-CHECKING!!!
# BOTH single-objective and multi-objective is supported
# '''
#
# import fire
# import numpy as np
# import os
# from math import sqrt
#
# import fitness_functions as ff
# import genotype as gt
# from common.utilities import get_files, get_experiment_populations
# from evaluate_best_individuals import write_individuals
#
# weapons = ['Rifle', 'Rocket', 'Shotgun', 'SMG', 'Sniper Rifle']
# original_weapon = 'Shotgun'
# metrics = ["balance_pen", "big_losses", "balance", "big_wins", "diff_t1t2", "changes", "gene distances",
#            "weapon distances"]
#
#
# def process_pop(pop, summary_name):
#     print "Computing fitnesses..."
#     data = []
#     for ind in pop:
#         genes = ind.genes
#         weapon = gt.convert_to_normalized_weapon(original_weapon, ind.genes)
#         bal_pen, losses, balance, wins, diff_t1t2, changes = ff.compute_metrics(genes, as_string=False)[0:6]
#         # gene_distances = gt.compute_all_gene_euclids(genes)
#         # distances = gt.compute_all_euclids(weapon)
#         gene_distances = gt.compute_gene_euclidean(genes, original_weapon)
#         distances = gt.compute_euclidean(weapon, original_weapon)
#         data.append([summary_name, ind.genes, bal_pen, losses, balance, wins,
#                      diff_t1t2, changes, gene_distances, distances, ",".join(["{:.2f}".format(f) for f in ind.fitnesses])])
#     return data
#
#
# def set_original_weapon_stuff(folder):
#     global original_weapon
#     if "Sniper" in folder:
#         original_weapon = 'Sniper Rifle'
#     else:
#         for w in weapons[0:-1]:
#             if w in folder:
#                 original_weapon = w
#     gt.original_genes = gt.weapon_to_orig_genes(original_weapon)
#     gt.original_wparam = gt.weapon_to_loc_norm_parameters(original_weapon)
#     ff.original_weapon = original_weapon
#     ff.penalty = "diff_t1t2"
#     ann_data = os.getcwd() + "/data/" + nn_data
#     ann_model = os.getcwd() + "/model/" + nn_model
#     ff.prepare_nn_and_data(ann_model, ann_data, original_weapon, nn_opponents)
#
#
# def summarize_experiment(evaluated_pops):
#     print "Summarizing experiment"
#     exp_summary = [["name", "mean", "std dev", "std err"]]
#     nr_metrics = len(metrics)
#     nr_runs = len(evaluated_pops)
#     pop_size = len(evaluated_pops[0])
#     for m in range(nr_metrics):
#         m_vals = []
#         for run in range(nr_runs):
#             metric_vals = [ind[2+m] for ind in evaluated_pops[run]]
#             m_vals.extend(metric_vals)
#         mean_val = np.mean(m_vals)
#         std_dev = np.std(m_vals)
#         std_err = std_dev / sqrt(pop_size)
#         exp_summary.append([metrics[m], mean_val, std_dev, std_err])
#
#     # for average of averages:
#     # for run in range(len(evaluated_pops)):
#     #     # do something for a run?
#     #     for m in range(nr_metrics):
#     #         metric_vals = [ind[m] for ind in evaluated_pops[run]]
#     return exp_summary
#
#
# def write_summary(filename, data_per_experiment):
#     print "Writing summary to", filename
#     with open(filename, 'w') as out:
#         for experiment in data_per_experiment:
#             out.write(experiment[0] + "\n")
#             for m in range(1, len(experiment) - 1):
#                 line = ",".join(["{0}".format(f) for f in experiment[m]])
#                 out.write(line + "\n")
#
#
# def write_summarized_run(folder, file_id, constraint2=""):
#     # manual
#     data_folder = "/home/daniel/Projects/WeaponEvolution/{0}/".format(folder)
#     nr_of_runs = 20
#
#     # automatic
#     set_original_weapon_stuff(folder)
#     pop_files, l_pop = get_files(data_folder, file_id, nr_of_runs)
#     if constraint2:
#         pop_files = [pf for pf in pop_files if constraint2 in pf]
#         l_pop = len(pop_files)
#
#     # loop through all different experiments
#     nr_of_experiments = l_pop
#     data_per_exp = []
#     all_individuals = []
#     for i in range(0, nr_of_experiments, nr_of_runs):
#         print "experiment {0} / {1}".format((i / nr_of_runs) + 1, l_pop / nr_of_runs)
#         # loop through the runs of 1 type
#         # get_individuals_from_file
#         experiment_pops, obj1, _, summary_name = get_experiment_populations(i, pop_files, nr_of_runs, multi_obj)
#         exp_id = summary_name.rsplit("/", 1)[1].replace("P_100-gen_50-mut_0.15-crs_0.50-", "")
#         evaluated_pops = [process_pop(exp_pop, exp_id) for exp_pop in experiment_pops]
#         experiment_summary = [exp_id]
#         experiment_summary += summarize_experiment(evaluated_pops)
#         for pop in evaluated_pops:
#             all_individuals.extend(pop)
#
#         data_per_exp.append(experiment_summary)
#     if constraint2:
#         summ_file = data_folder + "population-{1}-{2}".format(original_weapon, folder.replace("/", "_"),
#                                                                              constraint2)
#     else:
#         summ_file = data_folder + "population-{1}".format(original_weapon, folder.replace("/", "_"))
#
#     if not pop_files:
#         print "Did not find files with ", file_id
#     else:
#         write_summary(summ_file + "-summary.csv", data_per_exp)
#         write_individuals(summ_file + "-individuals.csv", all_individuals, add_changes=True)
#
#
# if __name__ == "__main__":
#     # folder = "mo_Shotgun"
#     folder = "small_changes_Shotgun"
#     identifier = "all_other"
#     nn_model = "best-cnn-fold_6.hdf5"
#     nn_data = "1_per_gen-maps.npy"
#     nn_opponents = "all_other"
#
#     # file_id = "final_pop.csv" # or single objective
#     file_id = "pfront.csv"  # for multi-objective.
#     multi_obj = True
#
#     write_summarized_run(folder, file_id, identifier)
#     # fire.Fire(write_summarized_run)
