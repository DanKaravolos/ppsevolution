import os
import argparse
# import genotype as geno
import numpy as np
from character_evolution.evolution.mutation import tf2_damage_classes, mutate, tf2_mutate, apply_initial_mutation
from character_evolution.evolution.weapon_parameters import tf2_dmg_classes, tf2_weapon_param_dict
# from common.common import parse_nn_settings, parse_targets
initial_match_was_added = False

INITIAL_DEVIATION = 0.1

# def initialize_from_weapon(icls):
#     # print "Initializing from {0}".format(args.initial_weapon)
#     # genes = list(geno.weapon_to_loc_norm_parameters(args.initial_weapon))
#     genes = geno.original_genes
#     return icls(genes)


# def init_rand_from_weapon_params(icls):
#     # print "Initializing from {0} with deviations.".format(args.initial_weapon)
#     original = geno.original_genes
#     deviations = list(np.random.normal(0, INITIAL_DEVIATION, len(original)))
#     deviated = [np.clip(original[i] + deviations[i], 0, 1) for i in xrange(len(deviations))]
#     return icls(deviated)

def init_two_classes(icls, individual_size=16):
    clipped_values = list(np.clip(np.random.uniform(0, 1, individual_size), 0, 1))
    clipped_values[int(individual_size / 2.0) - 1] = np.random.randint(0, 3)
    clipped_values[-1] = np.random.randint(0, 3)
    return icls(clipped_values)


# def init_ind_random(icls, individual_size, mu=0):
#     # print "Random initialization"
#     clipped_values = list(np.clip(np.random.normal(mu, INITIAL_DEVIATION, individual_size), 0, 1))
#     return icls(clipped_values)


def init_ind_from_tf2_params(icls):
    def create_ind():
        ind = []
        for i in range(0, 8):
            param_val = np.random.choice(tf2_damage_classes[i])
            ind.append(param_val)
        return ind
    individual = create_ind()
    individual.extend(create_ind())
    return icls(individual)


def init_ind_from_rnd_tf2_class(icls):
    def create_ind():
        str_class = np.random.choice(tf2_dmg_classes)
        param_class = [i for i in tf2_weapon_param_dict[str_class]]  # to make sure that we are not using a reference.
        return param_class
    individual = create_ind()
    individual.extend(create_ind())
    return icls(individual)


def init_ind_from_tf2_class(icls, class1, class2):
    match = [i for i in tf2_weapon_param_dict[class1]]
    match.extend([i for i in tf2_weapon_param_dict[class2]])
    return icls(match)


def init_ind_from_tf2_class_with_mutations(icls, class1, class2, nr_mutations):
    global initial_match_was_added
    match = [i for i in tf2_weapon_param_dict[class1]]
    match.extend([i for i in tf2_weapon_param_dict[class2]])
    if not initial_match_was_added or nr_mutations == 0:
        initial_match_was_added = True
    else:
        match = apply_initial_mutation(match, nr_mutations)
    return icls(match)


def parse_args(result_folder="test", result_file="test_file", cross_prob=1.0, mut_prob=0.2, initial_match="Scout_Heavy"):
    parser = argparse.ArgumentParser(description="Do an evolutionary search for better weapon")
    parser.add_argument("--cross_prob", type=float, default=cross_prob)
    parser.add_argument("--elites", type=int, default=0.1, help="Number of elites in the population")
    parser.add_argument("--hall_size", type=int, default=3)
    # parser.add_argument("--nn", default="example_cnn-fold_0.h5")
    # parser.add_argument("--nn", default="c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_3.h5")
    # parser.add_argument("--nn", default="c_sizes_[16, 32]-weapon_d_sizes_[8]-d_sizes_[128]-h_act_elu-out_act_sigmoid-tag_elu-drop_0.20-BN-Pool_max-100ep-5_200K_singular_pickups.h5")
    # parser.add_argument("--nn", default="df5_200K_singular-elu_mlp-layers_[16]-in_both-out_time_team0ratio.hdf5")
    parser.add_argument("--nn_data", default="/data/maps/pcg_workshop/Level_1_Run_0_Map.csv")
    parser.add_argument("--initial_mutations", type=int, default=4)
    parser.add_argument("--mut_gene_prob", type=float, default=mut_prob)
    parser.add_argument("--mut_individual_prob", type=float, default=1)
    parser.add_argument("--generations", type=int, default=100)
    parser.add_argument("--pop_size", type=int, default=100)
    parser.add_argument("--selection", choices=["tournament", "roulette"], default="tournament")
    parser.add_argument("--sigma", type=float, default=0.1)
    parser.add_argument("--tourn_size", type=int, default=5)
    parser.add_argument("--runs", type=int, default=1)

    # fitness related stuff
    parser.add_argument("--fitness", choices=["maximize", "multi_maximize", "target", "multi_target",
                                              "minimize", "multi_minimize", "squared_error",
                                              "multi_squared_error"],
                        default="multi_target")
    parser.add_argument("--nn_settings_file", default="regression_model_settings.txt")
    parser.add_argument("--target_file", default="target_values_regression.txt",
                        help="A file that determines the target values per objective, used in target and sq.error")
    parser.add_argument("--match", default=initial_match, help="Define the initial values when initializing from TF2 class")

    # file i/o stuff
    parser.add_argument("--result_file", default=result_file)
    parser.add_argument("--folder", default=result_folder)

    args = parser.parse_args()
    
    
    # SETTING THE MUTATION FUNCTION
    args.mutation_func = mutate

    # more file i/o stuff
    args.nn_data = os.getcwd() + "/data/" + args.nn_data

    args.result_file += "-P_{0}".format(args.pop_size)
    args.result_file += "-gen_{0}".format(args.generations)
    # if args.gene_bound < 999:
    #     args.result_file += "-bound_{0:.1f}".format(args.gene_bound)
    args.result_file += "-mut_{0:.2f}".format(args.mut_gene_prob)
    args.result_file += "-crs_{0:.2f}".format(args.cross_prob)
    # if args.gene_bound > 0:
    #     geno.bind_gene_deviations = args.gene_bound
    #     args.result_file += "-max_dev_{0:.2f}".format(args.gene_bound)

    storage_dir = os.getcwd() + "/character_evolution/Results/"
    if not os.path.exists(storage_dir):
        os.mkdir(storage_dir)
    if not os.path.exists(storage_dir + args.folder):
        current_dir = storage_dir
        for fol in args.folder.split("/"):
            current_dir += "/" + fol
            try:
                os.mkdir(current_dir)
            except OSError:
                continue

    if not os.path.exists(storage_dir + "{0}/results/".format(args.folder)):
        os.mkdir(storage_dir + "{0}/results/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/final_pop/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/best_per_gen/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/initial_pop/".format(args.folder))

    args.result_file = storage_dir + "{0}/results/{1}".format(args.folder, args.result_file)
    args.final_pop_file = args.result_file.replace("results", "results/final_pop")
    args.bpg_file = args.result_file.replace("results", "results/best_per_gen")

    global INITIAL_DEVIATION
    INITIAL_DEVIATION = args.sigma
    return args
