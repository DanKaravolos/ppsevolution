import numpy as np
import character_evolution.evolution.archive.old_fitness_functions as fit_functions
from character_evolution.evolution.weapon_parameters import convert_weapon_back, all_params
from common.common import list_to_string


def write_results(args, output_file, best_per_generation, final_best_predictions, run=None, write_header=True):
    target_keys = sorted(final_best_predictions.keys())
    map_name = args.nn_data
    last_best = best_per_generation[-1][0]

    with open(output_file + "-result.csv", 'a') as out_file:
        last_best_str = ",".join(["{:.6f}".format(w) for w in last_best])
        last_best_orig_str = ",".join(["{:.6f}".format(w) for w in convert_weapon_back(last_best)])
        header = ["map", "fitness", "fitness value"] + target_keys
        ind_params = all_params * 4
        header.extend(ind_params)

        preds = ["{:.4f}".format(final_best_predictions[t]) for t in target_keys]
        out_items = [map_name, args.fitness, last_best.fitness.values[0],
                     *preds,
                     last_best_str,
                     last_best_orig_str]
        # last_best_pretty = pretty_string(last_best)
        # last_best_orig_pretty = pretty_string(convert_weapon_back(last_best))
        # header.append("NN name")
        # out_items.append(args.nn.replace(",", "_"))  # do I want to print all the NNs?
        if run is not None:
            header.append("Run")
            out_items.append(run)
        if "maximize" not in args.fitness:
            targets = ["target {0}: {1}".format(t, args.target_values[t]) for t in target_keys]
            header.extend(targets)
        else:
            header.extend(["maximize {0}".format(t) for t in target_keys])

        out_str = ",".join([str(i) for i in out_items]) + "\n"
        if write_header:
            out_file.write(",".join(header) + "\n")
            out_file.write(out_str)
        else:
            out_file.write(out_str)
        # out_file.write("\n\n")  # add blank lines for stats


def write_best_per_generation(args, out_file, generations):
    with open(out_file + "-bpg-fit_{0}.csv".format(args.fitness), 'a') as out:
        out.write("gen, fitness\n")
        for g in range(len(generations)):
            fitness = list_to_string(generations[g][0].fitness.values)
            ind = list_to_string(generations[g][0])
            out.write("{0},{1},{2}\n".format(g, fitness, ind))


def write_final_pop(args, out_file, final_pop):
    with open(out_file + "-final_pop-fit_{0}.csv".format(args.fitness), 'a') as out:
        out.write(",".join(all_params * 2))
        out.write(",fitness,\n")
        for ind in final_pop:
            str_ind = list_to_string(ind)
            str_fit = list_to_string(ind.fitness.values)
            out.write("{0},{1}\n".format(str_ind, str_fit))


