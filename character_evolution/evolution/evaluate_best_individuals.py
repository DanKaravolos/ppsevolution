# '''
# This program crawls through a folder, finding all ind-files (with info on best individual)
# and computes all fitness-related metrics for each individual. This is written to a file in the same folder.
# Additionally, a summary fil is written with the averages per experiment.
# NOTE: THE BEST INDIVIDUAL FILES ARE SORTED AND CONCATENATED INTO ONE BATCH HARDCODED BASED ON THE NUMBER OF RUNS,
# THERE IS NO NAME-CHECKING!!!
# This works only for single-objective
# '''
# import os
# from math import sqrt
#
# import numpy as np
#
# import fitness_functions as ff
# import genotype as gt
# from common.utilities import items_from_line, Individual, float_list_to_st, weapon_from_folder
# from visualization.plot_pareto_pareto import get_files
#
# weapons = ['Rifle', 'Rocket', 'Shotgun', 'SMG', 'Sniper Rifle']
# original_weapon = 'Shotgun'
# metrics = ["balance_pen", "big_losses", "balance", "big_wins", "diff_t1t2", "changes", "gene distances", "weapon distances"]
#
#
# def get_individual_from_file(file_name):
#     with open(file_name, "r") as inpt:
#         lines = inpt.readlines()
#         for line in lines:
#             if "changes" in line:
#                 genes = items_from_line(line)[1:]
#             if "ratios" in line:
#                 ratios = [float(num) for num in items_from_line(line)[1:]]
#             if "fitness" in line:
#                 fitnesses= items_from_line(line)[1:]
#         ind = Individual(genes, fitnesses)
#
#     return ind, ratios
#
#
# def get_experiment_individuals(exp_index, files, nr_runs):
#     populations = []
#     for run in range(0, nr_runs):
#         ind_file = files[exp_index + run]
#         individual, match_ratios = get_individual_from_file(ind_file)
#         populations.append([individual, match_ratios])
#     plot_name = ind_file.rsplit("-", 2)[0]
#     return populations, plot_name
#
#
# def process_pop(pop, summary_name):
#     print "Computing fitnesses..."
#     ind = pop[0]
#     match_ratios = pop[1]  # ff.compute_match_ratios(gt.convert_to_normalized_weapon(original_weapon, ind.genes), "w1")
#     genes = ind.genes
#     weapon = gt.convert_to_normalized_weapon(original_weapon, ind.genes)
#     balance = match_ratios[1]
#     diff_t1t2 = abs(match_ratios[0] - match_ratios[2])
#     bal_pen = 1 + balance - diff_t1t2
#     changes = ff.compute_fitness(genes, weapon, "changed_genes")
#     gene_distances = gt.compute_gene_euclidean(genes, original_weapon)
#     distances = gt.compute_euclidean(weapon,original_weapon)
#     data = [[summary_name, ind.genes, bal_pen, match_ratios[0], balance, match_ratios[2], diff_t1t2, changes,
#             gene_distances, distances, ind.fitnesses[0]]]
#     return data
#
#
# def set_original_weapon_stuff(folder):
#     global original_weapon
#     original_weapon = weapon_from_folder(folder)
#     gt.original_genes = gt.weapon_to_orig_genes(original_weapon)
#     gt.original_wparam = gt.weapon_to_loc_norm_parameters(original_weapon)
#     ff.original_weapon = original_weapon
#     ff.penalty = "diff_t1t2"
#     ann_data = os.getcwd() + "/data/" + nn_data
#     ann_model = os.getcwd() + "/model/" + nn_model
#     ff.prepare_nn_and_data(ann_model, ann_data, original_weapon, nn_opponents)
#
#
# def summarize_experiment(evaluated_pops):
#     print "Summarizing experiment"
#     exp_summary = [["name", "mean", "std dev", "std err"]]
#     nr_metrics = len(metrics)
#     nr_runs = len(evaluated_pops)
#     pop_size = len(evaluated_pops[0])
#     for m in range(nr_metrics):
#         m_vals = []
#         for run in range(nr_runs):
#             metric_vals = [ind[2+m] for ind in evaluated_pops[run]]
#             m_vals.extend(metric_vals)
#         mean_val = np.mean(m_vals)
#         std_dev = np.std(m_vals)
#         std_err = std_dev / sqrt(pop_size)
#         exp_summary.append([metrics[m], mean_val, std_dev, std_err])
#     return exp_summary
#
#
# def write_summary(filename, data_per_experiment):
#     print "Writing summary to", filename
#     with open(filename, 'w') as out:
#         for experiment in data_per_experiment:
#             out.write(experiment[0] + "\n")
#             for m in range(1, len(experiment) - 1):  # use -1 to ignore weapon distance
#                 line = ",".join(["{0}".format(f) for f in experiment[m]])
#                 out.write(line + "\n")
#
#
# def write_individuals(filename, all_individuals, incl_weapon_dist=False, add_changes=False, one_hot_changes=True):
#     print "Writing individuals to ", filename
#     with open(filename, 'w') as out:
#         header = ["fit_id"] + gt.genes + metrics
#         if incl_weapon_dist:
#             header += weapons
#         header += "Fitness"
#         if add_changes:
#             if one_hot_changes:
#                 header += gt.genes
#             else:
#                 header += ["profoundly changed genes"]
#         out.write(",".join(header) + "\n")
#         for ind in all_individuals:
#             fit_id = ind[0]
#             genes = float_list_to_st(ind[1], end_line=False)
#             ind_metrics = float_list_to_st(ind[2:], end_line=False)
#             gene_distances = gt.compute_all_gene_euclids(ind[1])
#             info3 = []
#             if incl_weapon_dist:
#                 dist = float_list_to_st(gene_distances, end_line=False)
#                 info3.append(dist)
#             if add_changes:
#                 count, changes = gt.count_significant_changes(ind[1], original_weapon, mode="genes", threshold=0.1)
#                 if one_hot_changes:
#                     changes = ['1' if g in changes else '0' for g in gt.genes]
#
#                 info3.append(",".join(changes))
#             if incl_weapon_dist or add_changes:
#                 out.write("{0},{1},{2},{3}\n".format(fit_id, genes, ind_metrics, ",".join(info3)))
#             else:
#                 out.write("{0},{1},{2}\n".format(fit_id, genes, ind_metrics))
#
#
# def write_summarized_run(folder, constraint2=""):
#     # manual
#     data_folder = "/home/daniel/Projects/WeaponEvolution/{0}/".format(folder)
#     nr_of_runs = 20
#
#     # automatic
#     set_original_weapon_stuff(folder)
#     ind_files, l_pop = get_files(data_folder, "ind.csv", nr_of_runs)
#     if constraint2:
#         ind_files = [pf for pf in ind_files if constraint2 in pf]
#         l_pop = len(ind_files)
#
#     # loop through all different experiments
#     nr_of_experiments = l_pop
#     data_per_exp = []
#     all_individuals = []
#     for i in range(0, nr_of_experiments, nr_of_runs):
#         print "experiment {0} / {1}".format((i / nr_of_runs) + 1, l_pop / nr_of_runs)
#         # loop through the runs of 1 type
#         # get_individuals_from_file
#         experiment_pop, summary_name = get_experiment_individuals(i, ind_files, nr_of_runs)
#         exp_id = summary_name.rsplit("/", 1)[1].replace("P_100-gen_50-mut_0.15-crs_0.50-", "")
#         evaluated_pop = [process_pop(exp_pop, exp_id) for exp_pop in experiment_pop]
#         experiment_summary = [exp_id]
#         experiment_summary += summarize_experiment(evaluated_pop)
#         all_individuals.extend([p[0] for p in evaluated_pop])
#
#         data_per_exp.append(experiment_summary)
#     if constraint2:
#         summ_file = data_folder + "best_inds-{1}-{2}".format(original_weapon, folder.replace("/", "_"),
#                                                                              constraint2)
#     else:
#         summ_file = data_folder + "best_inds-{1}".format(original_weapon, folder.replace("/", "_"))
#
#     if not ind_files:
#         print "Did not find indfiles..."
#     else:
#         write_summary(summ_file + "-summary.csv", data_per_exp)
#         write_individuals(summ_file + "-individuals.csv", all_individuals, incl_weapon_dist=True, add_changes=True)
#
#
# if __name__ == "__main__":
#     folder = "shotgun_results/so_Shotgun"
#     identifier = "all_other"
#     nn_model = "best-cnn-fold_6.hdf5"
#     nn_data = "1_per_gen-maps.npy"
#     nn_opponents = "all_other"
#
#     write_summarized_run(folder, identifier)
#     # fire.Fire(write_summarized_run)
