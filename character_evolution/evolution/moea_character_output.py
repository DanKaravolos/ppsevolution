# import os
import numpy as np
from common.common import list_to_string
from character_evolution.evolution.weapon_parameters import convert_weapon_back, all_params
import warnings
EPSILON = 0.0001


def process_multi_results(log, pareto, final_pop, eval_func, args, run, best_hv, output_file):
    map_name = args.nn_data
    # collect more information
    print("Only processing 2 dimensions right now!")
    non_pareto_inds = []
    for ind in final_pop:
        is_not_pareto = True
        for fitness in pareto.keys:
            val = fitness.values
            if abs(ind.fitness.values[0] - val[0]) < EPSILON and abs(ind.fitness.values[1] - val[1]) < EPSILON:
                is_not_pareto = False
                break
        if is_not_pareto:
            non_pareto_inds.append(ind)
    print("Len of non_pareto: {0}, len of pareto: {1}".format(len(non_pareto_inds), len(pareto.items)))

    fit_sum = [np.sqrt(sum(p.fitness.values)) for p in pareto.items]
    best_ind_num = np.argmin(fit_sum)
    best_ind = pareto.items[best_ind_num]

    # Output functions
    write_best_ind_results(best_ind, eval_func, args, run, output_file, run == 0)
    write_results(pareto, eval_func, args, run, output_file)
    write_fitnesses(pareto, non_pareto_inds, eval_func, args.target_values, best_ind_num, output_file)
    # write_pareto(pareto, output_file)
    print("Only writing best ind to game input!")
    # write_game_input(pareto, map_name, output_file)
    write_best_to_game_input(best_ind, map_name, output_file)
    write_log(log, output_file)

    # Output checks
    nr_objectives = len(pareto.items[0].fitness.values)
    if nr_objectives > 2:
        warnings.warn("You have {0} fitnesses, but write_pareto and write_log only write two values to file!".format(nr_objectives))
    print("Just so you know best_hypervolume index is not used!")


def write_fitnesses(pareto, non_pareto, eval_func, targets, best_hv, output_file):
    # warnings.warn("in write_fitnesses: hardcoded mention of predicted time and score.")
    pred_string = ",".join(["predicted {0}".format(t) for t in sorted(targets.keys())])  # + "obj.{0}, pred {1}".format(t) for t in sorted(targets.keys())])
    fit_string = ",".join(["obj {0}".format(c) for c in range(len(targets.keys()))])
    with open(output_file + "-fitnesses.csv", "a") as out:
        header = "num,type ,{0},{1}, average fitness, sqrt of sum of fitnesses, individual\n".format(fit_string, pred_string)
        out.write(header)
        ind_count = 0
        for p in pareto.items:
            fitnesses = ",".join(["{:.6f}".format(f) for f in p.fitness.values])
            fit_sum = np.sqrt(sum(p.fitness.values))
            fit_avg = np.mean(p.fitness.values)
            pred = eval_func(p, separator=",", with_name=False)
            p_ind_str = list_to_string(p)
            if ind_count == best_hv:
                line = "{0},pareto_best,{1},{2},{3},{4},{5}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum, p_ind_str)
            else:
                line = "{0},pareto,{1},{2},{3},{4},{5}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum, p_ind_str)
            out.write(line)
            ind_count += 1

        for ind in non_pareto:
            fitnesses = ",".join(["{:.6f}".format(f) for f in p.fitness.values])
            pred = eval_func(ind, separator=",", with_name=False)
            fit_sum = np.sqrt(sum(p.fitness.values))
            fit_avg = np.mean(p.fitness.values)
            line = "{0},dominated,{1},{2},{3},{4}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum)
            out.write(line)
            ind_count += 1


def write_pareto(pareto, output_file):
    cnt = 0
    for p_ind in pareto.items:
        class1_ind = list_to_string(p_ind[0:8])
        class2_ind = list_to_string(p_ind[8:])
        fitness = p_ind.fitness.values
        tp = output_file + "-pareto_{0}-f1_{1:.2f}-f2_{2:.2f}.csv".format(cnt, fitness[0], fitness[1])
        with open(tp, "w") as out:
            out.write(class1_ind + "," + class2_ind + "\n")
        cnt += 1


def write_game_input(pareto, map_name, output_file):
    cnt = 0
    tp = output_file + "-pareto-game_input.csv".format(cnt)
    with open(tp, "w") as out:
        for p_ind in pareto.items:
            class1_ind = list_to_string(p_ind[0:8])
            class2_ind = list_to_string(p_ind[8:])
            out.write("{0},{1},{2}\n".format(map_name, class1_ind, class2_ind))


def write_best_to_game_input(best, map_name, output_file):
    tp = output_file + "-best-game_input.csv"
    with open(tp, "a") as out:
        class1_ind = list_to_string(best[0:8])
        class2_ind = list_to_string(best[8:])
        out.write("{0},{1},{2}\n".format(map_name, class1_ind, class2_ind))


def write_log(log, output_file):
    # warnings.warn("In write_log: Only writing two objectives to file!")
    with open(output_file + "-log.csv", 'w') as out:
        out.write("gen,obj1 min,obj1 max,obj1 avg,obj1 std,obj2 min,obj2 max,obj2 avg,obj2 std\n")
        obj1 = log.chapters['obj1']
        obj2 = log.chapters['obj2']
        for line in range(len(obj1)):
            obj1_vals = [obj1[line]["min"], obj1[line]["max"], obj1[line]["avg"], obj1[line]["std"]]
            obj2_vals = [obj2[line]["min"], obj2[line]["max"], obj2[line]["avg"], obj2[line]["std"]]
            values = [log[line]["gen"]] + obj1_vals + obj2_vals
            gen_string = list_to_string(values)
            out.write(gen_string + "\n")


def write_best_ind_results(best_ind, eval_func, args, run, output_file, write_header=True):
    with open(output_file + "-best_ind-result.csv", 'a') as out_file:
        write_ind(best_ind, eval_func, args, run, out_file, write_header)


def write_results(pareto, eval_func, args, run, output_file, write_header=True):
    with open(output_file + "-result.csv", 'a') as out_file:
        for pareto_ind in pareto.items:
            write_ind(pareto_ind, eval_func, args, run, out_file, write_header)
            write_header = False


def write_ind(pareto_ind, eval_func, args, run, out_file, write_header):
    target_keys = sorted(args.target_values.keys())
    map_name = args.nn_data.replace("/home/daniel/Projects/ppsEvolution/data/maps/", "")
    last_best_str = ",".join(["{:.6f}".format(w) for w in pareto_ind])
    last_best_orig_str = ",".join(["{:.6f}".format(w) for w in convert_weapon_back(pareto_ind)])
    fitnesses = ",".join(["{:.6f}".format(f) for f in pareto_ind.fitness.values])
    fit_sum = np.sqrt(sum(pareto_ind.fitness.values))
    fit_avg = np.mean(pareto_ind.fitness.values)
    preds = eval_func(pareto_ind, separator=",", with_name=False)
    header = ["map", "fitness", "avg. fitness"] + ["{0} {1}".format(args.fitness, i) for i in range(len(target_keys))] + target_keys + \
             ["Sqrt. sum of fitnesses"]
    ind_params = all_params * 4
    header.extend(ind_params)

    out_items = [map_name, args.fitness, fit_avg, fitnesses, preds, fit_sum,
                 last_best_str,
                 last_best_orig_str]
    if run is not None:
        header.append("Run")
        out_items.append(run)
    if "maximize" not in args.fitness:
        targets = ["target {0}: {1}".format(t, args.target_values[t]) for t in target_keys]
        header.extend(targets)
    else:
        header.extend(["maximize {0}".format(t) for t in target_keys])

    out_str = ",".join([str(i) for i in out_items]) + "\n"
    if write_header:
        out_file.write(",".join(header) + "\n")
        out_file.write(out_str)
    else:
        out_file.write(out_str)
    # out_file.write("\n\n")  # add blank lines for stats


def write_final_pop(args, out_file, final_pop):
    with open(out_file + "-final_pop-fit_{0}.csv".format(args.fitness), 'a') as out:
        out.write(",".join(all_params * 2))
        out.write(",fitness,\n")
        for ind in final_pop:
            str_ind = list_to_string(ind)
            str_fit = list_to_string(ind.fitness.values)
            out.write("{0},{1}\n".format(str_ind, str_fit))