from __future__ import print_function
from character_evolution.evolution.so_evolution_main import main
from character_evolution.evolution.init_and_select import parse_args
from character_evolution.evolution.mutation import tf2_mutate
import os
import time
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # use a specific GPU
# os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # use a specific GPU
# os.environ["CUDA_VISIBLE_DEVICES"] = "2"  # use a specific GPU
# os.environ["CUDA_VISIBLE_DEVICES"] = "3"  # use a specific GPU
print ("Start time:", time.strftime("%Y-%m-%d %H:%M"))
print ("Using GPU ", os.environ["CUDA_VISIBLE_DEVICES"])
t0 = time.time()

# fitnesses = ["maximize_score", "target_score", "minimize_score", "aggregated"]  #
# maps = ["Level_1_Run_0_Map_3tile.npy", "Level_2_Run_0_Map_3tile.npy", "Level_3_Run_0_Map_3tile.npy",
#         "Level_4_Run_0_Map_3tile.npy", "map_antonios_valid_empty_bases_3tile.npy"]
# maps = ["Level_1_Run_0_Map.npy", "Level_2_Run_0_Map.npy", "Level_3_Run_0_Map.npy", "Level_4_Run_0_Map.npy"]
# maps = ["map_antonios_valid_empty_bases_3tile.npy"]
# maps = ["Level_{0}_Run_0_Map_3tile.npy".format(i + 1) for i in range(5)]
# maps = ["Level_{0}_Run_0_Map_3tile.npy".format(i + 6) for i in range(5)]
maps = ["Level_{0}_Run_0_Map_3tile.npy".format(i + 1) for i in range(10)]
# maps = ["Custom_{0}_Run_0_Map_3tile.npy".format(i + 1) for i in range(5)]
# maps = ["Custom_{0}_Run_0_Map_3tile.npy".format(i + 6) for i in range(5)]
# maps = ["Custom_{0}_Run_0_Map_3tile.npy".format(i + 1) for i in range(10)]


fitnesses = ["squared_error"]  #
nr_runs = 1

for m in maps:
    m_name = m.rsplit("_", 4)[0] if "ant" not in m else "Level_Ant"
    # folder_name = "CIGNET_ICCC_" + m_name + "_TF2_{0}".format(nr_runs
    # folder_name = "CIGNET_" + m_name + "_TF2_{0}".format(nr_runs)
    folder_name = "CIGNET_" + m_name + "_Gauss_{0}".format(nr_runs)
    print("current folder name: ", folder_name)
    for f in fitnesses:
        if "aggregated" in f or "squared_error" in f:
            times = [0.085, 0.315, 1]  # 200, 300 and 600 seconds in the ICCC/CIG dataset df4
            # times = [0.11, 0.333, 1]  # 200, 300 and 600 seconds in df5 
            for t in times:
                if "TF2" in folder_name:
                    args = parse_args(result_folder=folder_name, result_file="{0}-tf2-t_{1}".format(m_name, t))
                    args.mutation_func = tf2_mutate
                else:
                    args = parse_args(result_folder=folder_name, result_file="{0}-t_{1}".format(m_name, t))
                args.runs = nr_runs
                args.nn = "c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_3.h5"  # setting CNN to CIGNET
                args.nn_data = os.getcwd() + "/data/maps/pcg_workshop/" + m
                # args.nn_data = os.getcwd() + "/data/maps/iccc/" + m
                # args.nn_data = os.getcwd() + "/data/maps/custom_maps/" + m
                args.target_time = t
                args.fitness = f
                main(args)
        else:
            main(args)

t1 = time.time()
print("Total time taken: {0} min".format((t1 - t0) / 60.0))
