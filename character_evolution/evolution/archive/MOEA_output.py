import os
from common.common import list_to_string
from map_evolution.visualization.map_to_image import image_from_asci_map


def process_multi_results(log, pareto, final_pop, eval_func, args, output_file):
    non_pareto_maps = []
    for ind in final_pop:
        is_not_pareto = True
        for fitness in pareto.keys:
            val = fitness.values
            if abs(ind.fitness.values[0] - val[0]) < 0.0001 and abs(ind.fitness.values[1] - val[1]) < 0.0001:
                is_not_pareto = False
                break
        if is_not_pareto:
            non_pareto_maps.append(ind)
    print("Len of non_pareto: {0}, len of pareto: {1}".format(len(non_pareto_maps), len(pareto.items)))
    print("Writing to", output_file)
    write_fitnesses(pareto, non_pareto_maps, eval_func, output_file)
    write_pareto(pareto, output_file)
    write_log(log, output_file)


def write_fitnesses(pareto, non_pareto, eval_func, output_file):
    with open(output_file + "-fitnesses.csv", "a") as out:
        header = "num,type,obj1,obj2, pred_time, pred_score\n"
        out.write(header)
        ind_count = 0
        for p in pareto.items:
            fitness = p.fitness.values
            pred = eval_func(p)[0]
            line = "{0},pareto,{1},{2},{3},{4}\n".format(ind_count, fitness[0], fitness[1], pred[0], pred[1])
            out.write(line)
            ind_count += 1

        for ind in non_pareto:
            fitness = ind.fitness.values
            pred = eval_func(ind)[0]
            line = "{0},dominated,{1},{2},{3},{4}\n".format(ind_count, fitness[0], fitness[1], pred[0], pred[1])
            out.write(line)
            ind_count += 1


def write_pareto(pareto, output_file):
    cnt = 0
    for p_ind in pareto.items:
        class1_ind = list_to_string(p_ind[0:8])
        class2_ind = list_to_string(p_ind[8:])
        fitness = p_ind.fitness.values
        tp = output_file + "-pareto_{0}-f1_{1:.2f}-f2_{2:.2f}.csv".format(cnt, fitness[0], fitness[1])
        with open(tp, "w") as out:
            out.write(class1_ind + "\n")
            out.write(class2_ind + "\n")


def write_log(log, output_file):
    with open(output_file + "-log.csv", 'w') as out:
        out.write("gen,obj1 min,obj1 max,obj1 avg,obj1 std,obj2 min,obj2 max,obj2 avg,obj2 std\n")

        obj1 = log.chapters['obj1']
        obj2 = log.chapters['obj2']
        for line in range(len(obj1)):
            obj1_vals = [obj1[line]["min"], obj1[line]["max"], obj1[line]["avg"], obj1[line]["std"]]
            obj2_vals = [obj2[line]["min"], obj2[line]["max"], obj2[line]["avg"], obj2[line]["std"]]
            values = [log[line]["gen"]] + obj1_vals + obj2_vals
            gen_string = list_to_string(values)
            out.write(gen_string + "\n")
