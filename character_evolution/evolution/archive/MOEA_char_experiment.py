from __future__ import print_function

import os
from time import time

from character_evolution.evolution.archive.multi_objective_main import main
from character_evolution.evolution.init_and_select import parse_args

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # use a specific GPU
t0 = time()
# args = parse_args()

fitnesses = ["squared_error_time", "squared_error_score"]
maps = ["/data/map_antonios_valid_empty_bases.npy"]  # or map_antonios_valid_empty_bases?
# maps = ["/data/Level_3_Run_0_Map.npy"]
# maps = ["/data/maps/Level_1_Run_0_Map.npy", "/data/maps/Level_2_Run_0_Map.npy", "/data/maps/Level_3_Run_0_Map.npy",
#         "/data/maps/Level_4_Run_0_Map.npy", "/data/maps/map_antonios_valid_empty_bases.npy"]

for map_index in range(len(maps)):
    tag = "Ant" if "antonios" in maps[map_index] else map_index + 1
    args = parse_args("character_evolution/MOEA_Levels_1-5", "Level_{0}".format(tag))
    init_map = maps[map_index]
    args.nn_data = os.getcwd() + init_map
    args.fitness = fitnesses[0]
    args.second_fit = fitnesses[1]
    times = [0.085, 0.315, 1]  # 200, 300 and 600 seconds
    # times = [0.085]
    # times = [0.315]
    # times = [1]
    for t in times:
        args.target_time = t
        main(args)
t1 = time()

print("Total time taken: {0} min".format((t1 - t0) / 60.0))
