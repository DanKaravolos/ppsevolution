from __future__ import print_function

import time

# import random
import numpy as np
from MOEA_output import process_multi_results
from deap import algorithms
from deap import base
from deap import creator
from deap import tools

import character_evolution.evolution.archive.old_fitness_functions as fit_functions
import character_evolution.evolution.mutation as mutation
from character_evolution.evolution.init_and_select import parse_args, init_two_class

args = None


def init_parameters(run_args):
    global args
    args = run_args
    print("loading network and data ...")
    fit_functions.fitness = args.fitness
    fit_functions.secondary = args.second_fit
    fit_functions.target_score = args.target_score
    fit_functions.target_time = args.target_time
    print("confirming fitness 1: {0}".format(fit_functions.fitness))
    print("confirming fitness 2: {0}".format(fit_functions.secondary))

    if ("target" in fit_functions.fitness) or ("aggregated" in fit_functions.fitness) or\
       ("error" in fit_functions.fitness):
        print("target time: {0}, target score: {1}".format(fit_functions.target_time, fit_functions.target_score))
    fit_functions.prepare_nn_and_data(args.model, args.nn_data)


def define_fitness_weight(fit_string):
    if "maximize" in fit_string:
        print("maximizing '{0}'".format(fit_string))
        return 1
    else:
        print("minimizing '{0}'".format(fit_string))
        return -1


def setup_multi_ea():
    w1 = define_fitness_weight(args.fitness)
    w2 = define_fitness_weight(args.second_fit)
    print("First obj {0}, weight {1}".format(args.fitness, w1))
    print("Second obj {0}, weight {1}".format(args.second_fit, w2))
    creator.create("FitnessMax", base.Fitness, weights=(w1, w2))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    toolbox = base.Toolbox()

    # Structure initializers
    toolbox.register("init_individual", init_two_class, creator.Individual, 16)
    toolbox.register("population", tools.initRepeat, list, toolbox.init_individual)
    toolbox.register("evaluate", fit_functions.multi_evaluate)
    toolbox.register("mate", tools.cxOnePoint)  # could be cxTwoPoint
    # toolbox.register("mutate", tools.mutGaussian,  mu=0.0, sigma=args.sigma, indpb=args.mut_gene_prob)
    toolbox.register("mutate", mutation.mutate, mu=0.0, sigma=args.sigma, mutprob=args.mut_gene_prob)
    # toolbox.decorate("mutate", mutation.check_bounds(0, 1))  # decorate mutation function
    toolbox.register("select", tools.selNSGA2)
    population = toolbox.population(n=args.pop_size)
    return toolbox, population


def do_multi_evolution(population, toolbox, cxpb=0.5, mutpb=1, ngen=1, stats=None, halloffame=None, verbose=True):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # This is just to assign the crowding distance to the individuals
    # no actual selection is done
    population = toolbox.select(population, len(population))

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(1, ngen + 1):
        # Select the next generation individuals
        offspring = tools.selTournamentDCD(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population = toolbox.select(population + offspring, len(population))

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)
    print("population size: ", len(population))
    return population, logbook


def evolution(toolbox, population):
    # random.seed(64)
    stats1 = tools.Statistics(key=lambda ind: ind.fitness.values[0])

    stats1.register("avg", np.mean)
    stats1.register("std", np.std)
    stats1.register("min", np.min)
    stats1.register("max", np.max)

    stats2 = tools.Statistics(key=lambda ind: ind.fitness.values[1])

    stats2.register("avg", np.mean)
    stats2.register("std", np.std)
    stats2.register("min", np.min)
    stats2.register("max", np.max)

    m_stats = tools.MultiStatistics(obj1=stats1, obj2=stats2)
    # hof = tools.ParetoFront()
    pop, log = do_multi_evolution(population, toolbox, cxpb=args.cross_prob,
                                  mutpb=args.mut_individual_prob, ngen=args.generations, stats=m_stats,
                                  halloffame=None, verbose=True)
    return pop, log  # , hof


def main(run_args):
    init_parameters(run_args)
    print("Running EA ...")
    print("Setting up EA ...")
    for run in xrange(args.runs):
        print("run ", run)
        t0 = time.time()
        output_file = args.result_file + "-tt_{0:.3f}".format(fit_functions.target_time)
        # output_file += "-run_{0}".format(run)
        tb, pop = setup_multi_ea()
        final_pop, log = evolution(tb, pop)  # difference between main and fire_main
        pareto = tools.ParetoFront()
        pareto.update(final_pop)

        t_diff = time.time() - t0
        print("Time taken: {0} min".format(t_diff / 60))

        # do something with final_population, hall of fame and log.
        process_multi_results(log, pareto, final_pop, fit_functions.nn_evaluate_individual, args, output_file)
        # process_multi_results(log, pareto, final_pop, args, output_file)
        # plot_multi_obj_population(final_pop, pareto, output_file + "-" + args.initial_weapon, args.fitness,
        #                           args.second_obj)
        # plot_multi_fitness(log, output_file, obj1_name=args.fitness, obj2_name=args.second_obj)

if __name__ == "__main__":
    my_run_args = parse_args()
    main(my_run_args)
