from __future__ import division
import numpy as np
from keras.models import load_model
import os
from deap import tools

# Internal variables
neural_net = None
input_maps = []
other_weapons = []
fitness = "squared_error"
secondary = ""
# penalty = ""
target_score = 0.5
target_time = 0.31  # = 300 MEAN TIME: 325 (or 0.37)


# WRAPPER FUNCTION
def evaluate(individual):
    if secondary:
        return multi_evaluate(individual)
    return compute_fitness(individual, fitness),


def multi_evaluate(individual):
    obj1 = compute_fitness(individual, fitness)
    obj2 = compute_fitness(individual, secondary)
    return obj1, obj2


def compute_fitness(individual, fit_func):
    if "maximize" in fit_func or "minimize" in fit_func:
        return regression_maximize(individual, fit_func)
    elif "target" in fit_func:
        return regression_target(individual, fit_func)
    elif "aggregated" in fit_func:
        return regression_aggregated(individual, fit_func)
    elif "squared_error" in fit_func:
        return regression_mse(individual, fit_func)
    else:
        print("What is your fitness function???")
        return -999


# initialization functions   ############################################################
def prepare_nn_and_data(nn, nn_data):
    # set global neural network
    global neural_net, input_maps, other_weapons
    print("Getting map data from: {0}".format(nn_data))
    neural_net = load_model(nn)
    neural_net.compile(optimizer='adam', loss='mse')  # necessary for usage
    neural_net.summary()
    # connect maps from file with weapon params.
    input_maps = np.load(nn_data)
    if len(input_maps.shape) == 3:
        input_maps = np.expand_dims(input_maps, axis=0)

    if "mlp" in nn or "c_sizes" not in nn:
        print("loading an MLP. Input will be flattened")
        input_maps = np.reshape(input_maps, (input_maps.shape[0], -1))
        input_maps = np.concatenate((np.zeros((input_maps.shape[0], 16)), input_maps), axis=1)
        print(input_maps.shape)


# UTILITY FUNCTIONS   ############################################################

def get_weight(fit_func):
    if "maximize" in fit_func or "target" in fit_func:
        return 1
    if "minimize" in fit_func or "aggregated" in fit_func:
        return -1


# # ARE YOU SURE THIS IS CORRECT????? to be sure: I added or "maximize" in fitness at July 10.
# def get_best_individual(population):
#     sorted_pop = sorted(population, key=lambda x: x.fitness)
#     if "regression" in fitness or "maximize" in fitness:
#         best = sorted_pop[-1]
#     else:
#         best = sorted_pop[0]
#     return best

# def get_best_individual(population):
#     best = max(population, key=lambda ind: ind.fitness)
#     return best

def get_best(population, num=1):
    best_all = tools.selBest(population, num)
    return best_all[0]


def nn_evaluate_individual(individual):
    if len(input_maps.shape) == 2:
        # MLP evaluation
        for level_num in range(len(input_maps)):
            input_maps[level_num, 0:16] = individual
            predictions = neural_net.predict(input_maps)
            if type(predictions) != np.ndarray:
                predictions = np.concatenate((predictions[0], predictions[1]), axis=1)
    else:
        # CNN evaluation
        input_weapons = np.asarray([individual] * len(input_maps))
        predictions = neural_net.predict([input_weapons, input_maps])
        if type(predictions) != np.ndarray:
            predictions = np.concatenate((predictions[0], predictions[1]), axis=1)
    return predictions


def denormalize_time(time):
    time_min = 163.0
    time_max = 600.0
    orig = time * (time_max - time_min) + time_min
    return orig


def normalize_time(orig):
    time_min = 163.0
    time_max = 600.0
    time = (orig - time_min) / (time_max - time_min)
    return time


# FITNESS FUNCTIONS   ############################################################

def regression_maximize(individual, fit_func):
    input_weapons = np.asarray([individual] * len(input_maps))
    predictions = neural_net.predict([input_weapons, input_maps])
    if "time" in fit_func:
        times = predictions[:, 0]
        return np.mean(times)
    elif "score" in fit_func:
        scores = predictions[:, 1]
        return np.mean(scores)
    else:
        print("ERROR: fitness function undefined.")
        return -1


def regression_target(individual, fit_func):
    input_weapons = np.asarray([individual] * len(input_maps))
    predictions = neural_net.predict([input_weapons, input_maps])
    if "time" in fit_func:
        target = target_time
        pred = predictions[:, 0]
    elif "score" in fit_func:
        target = target_score
        pred = predictions[:, 1]
    else:
        print("ERROR: fitness function undefined.")
        return -1
    deviations = -np.absolute(pred - target)  # negative absolute target in order to maximize
    return np.mean(deviations)


def regression_aggregated(individual, fit_func):
    input_weapons = np.asarray([individual] * len(input_maps))
    predictions = neural_net.predict([input_weapons, input_maps])
    agg = [abs(p[0]-target_time)/target_time + abs(p[1]-target_score)/target_score for p in predictions]
    return np.mean(agg)


# def regression_mse(individual, fit_func):
#     input_weapons = np.asarray([individual] * len(input_maps))
#     predictions = neural_net.predict([input_weapons, input_maps])
#     agg = [(abs(p[0]-target_time) + abs(p[1]-target_score))**2 for p in predictions]
#     return np.mean(agg)


def regression_mse(individual, fit_func):
    if len(input_maps.shape) == 2:
        # MLP evaluation
        for level_num in range(len(input_maps)):
            input_maps[level_num, 0:16] = individual
            predictions = neural_net.predict(input_maps)
    else:
        # CNN evaluation
        input_weapons = np.asarray([individual] * len(input_maps))

        predictions = neural_net.predict([input_weapons, input_maps])
    if type(predictions) == np.ndarray:
        # in this case the network gives a two dimensional output per input
        agg = [(p[0]-target_time)**2 + (p[1]-target_score)**2 for p in predictions]
    else:
        # in this case the network gives a list with predictions per input for each output node
        p = predictions
        agg = [(p[0][i] - target_time) ** 2 + (p[1][i] - target_score) ** 2 for i in range(len(p[0]))]
    return np.mean(agg)


if __name__ == "__main__":
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # use a specific GPU

    # nn_loc = "/home/daniel/Projects/ppsEvolution/models/c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_3.h5"
    nn_loc = "/home/daniel/Projects/ppsEvolution/models/c_sizes_[16, 32]-weapon_d_sizes_[8]-d_sizes_[128]-h_act_elu-out_act_sigmoid-tag_elu-drop_0.20-BN-Pool_max-100ep-5_200K_singular_pickups.h5"
    nn_data = "/home/daniel/Projects/ppsEvolution/data/maps/map_antonios_valid_empty_bases.npy"
    prepare_nn_and_data(nn_loc, nn_data)
