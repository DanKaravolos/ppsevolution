# import fire
from __future__ import print_function
import time
from warnings import warn
import numpy as np
import gc
from deap import algorithms, base, creator, tools
import common.fitness_functions as ff
import character_evolution.evolution.init_and_select as init
# from character_evolution.evolution.init_and_select import init_ind_from_tf2_params, parse_args, init_ind_from_tf2_class
# from character_evolution.evolution.init_and_select import init_ind_from_tf2_class_with_mutations
from character_evolution.evolution.output import write_results, write_best_per_generation, write_final_pop
from character_evolution.evolution.weapon_parameters import convert_weapon_back, pretty_string
from common.common import define_fitness_weight, save_initial_classes
from common.selection import elite_roulette_selection, elite_tour_selection

args = None
fit_functions = None

BETWEEN_EPOCHS_EPSILON = 1e-5
CLOSE_TO_ZERO_EPSILON = 1e-5
MAX_EPOCHS_WITHOUT_PROGRESS = 10
warn("Note: MAX_EPOCHS_WITHOUT_PROGRESS = {0}".format(MAX_EPOCHS_WITHOUT_PROGRESS))


def setup_ea():
    weight = define_fitness_weight(args.fitness)
    print("weight: {0}".format(weight))
    creator.create("FitnessMax", base.Fitness, weights=(weight,))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    toolbox = base.Toolbox()

    # Structure initializers
    # toolbox.register("init_individual", init_two_classes, creator.Individual, 16)
    classes = args.match.split("_")
    print("Initializing from {0} vs {1}, with {2} mutations".format(classes[0], classes[1], args.initial_mutations))
    print("init.initial_match_was_added before override: ", init.initial_match_was_added)
    init.initial_match_was_added = False
    print("init.initial_match_was_added after override: ", init.initial_match_was_added)
    toolbox.register("init_individual", init.init_ind_from_tf2_class_with_mutations, creator.Individual,
                     classes[0], classes[1], args.initial_mutations)
    toolbox.register("population", tools.initRepeat, list, toolbox.init_individual)
    toolbox.register("evaluate", fit_functions.evaluate)
    toolbox.register("mate", tools.cxOnePoint)  # could be cxTwoPoint
    # toolbox.register("mutate", mutation.mutate, mu=0.0, sigma=args.sigma, mutprob=args.mut_gene_prob)
    # toolbox.register("mutate", mutation.tf2_mutate, mutprob=args.mut_gene_prob)
    toolbox.register("mutate", args.mutation_func, mutprob=args.mut_gene_prob)
    n_elites = int(args.elites * args.pop_size)
    if args.selection == "tournament":
        toolbox.register("select", elite_tour_selection, tournsize=args.tourn_size, n_elites=n_elites)
    else:
        toolbox.register("select", elite_roulette_selection, n_elites=n_elites)
    population = toolbox.population(n=args.pop_size)
    return toolbox, population


def do_evolution(population, toolbox, cxpb=0, mutpb=1, ngen=1, stats=None, halloffame=None, verbose=True):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    best_per_generation = [tools.selBest(population, 1)]
    # Begin the generational process
    prev_best_fitness = -1
    epochs_without_progress = 0

    for gen in range(1, ngen):
        # Select the next generation individuals
        elites, plebs = toolbox.select(population, len(population))
        if args.elites > 0:
            best_per_generation.append(tools.selBest(elites, 1))
        else:
            best_per_generation.append(tools.selBest(population, 1))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(plebs, toolbox, cxpb, mutpb)
        offspring.extend(elites)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

        # compute stop conditions
        best_fitness = logbook[-1]["max"] if "maximize" in args.fitness else logbook[-1]["min"]
        if best_fitness < CLOSE_TO_ZERO_EPSILON:
            break
        if abs(best_fitness - prev_best_fitness) < BETWEEN_EPOCHS_EPSILON:
            epochs_without_progress += 1
        else:
            epochs_without_progress = 0
        if epochs_without_progress == MAX_EPOCHS_WITHOUT_PROGRESS:
            break
        prev_best_fitness = best_fitness

    best_per_generation.append(tools.selBest(population, 1))
    print("population size: ", len(population))
    return population, logbook, best_per_generation


def evolution(toolbox, population):
    # random.seed(64)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    if args.hall_size > 0:
        hof = tools.HallOfFame(args.hall_size)
    else:
        hof = None
    pop, log, best_per_gen = do_evolution(population, toolbox, cxpb=args.cross_prob, mutpb=args.mut_individual_prob,
                                          ngen=args.generations, stats=stats, halloffame=hof, verbose=True)
    return pop, log, hof, best_per_gen


def init_parameters(run_args):
    global args
    global fit_functions
    args = run_args
    print("loading network and data ...")
    fit_functions = ff.FitnessFunction("character", args.fitness, args.target_values)
    fit_functions.prepare_nn_and_data(args.models, args.nn_data)
    print("Confirming fitness: {0}".format(fit_functions.fitness))


def main(run_args):
    print("Setting up EA ...")
    init_parameters(run_args)
    print("Running EA ...")
    write_header = True
    for run in list(range(args.runs)):
        print("run ", run)
        t0 = time.time()
        output_file = args.result_file
        tb, pop = setup_ea()
        save_initial_classes(args, pop, run, is_dual=False)

        final_pop, log, hall_of_fame, best_per_generation = evolution(tb, pop)  # difference between main and fire_main
        last_best = best_per_generation[-1][0]
        lb_string = ",".join(["{:.6f}".format(w) for w in last_best])
        print("last gen best: {0}".format(lb_string))
        print("last gen best ind: {0}".format(pretty_string(last_best)))
        print("original dim: {0}".format(pretty_string(convert_weapon_back(last_best))))
        predictions = fit_functions.nn_evaluate_individual(last_best)
        # pred_list = []
        for target in sorted(fit_functions.target_values.keys()):
            pred = predictions[target]
            # pred_list.append(pred)
            t = fit_functions.target_values[target]
            print("Target {0}: desired val {1}, prediction: {2}".format(target, t, pred))
        fit = last_best.fitness.values[0]
        print("Best individual fitness: ", fit)
        t_diff = time.time() - t0
        print("Time taken: {0} min".format(t_diff / 60))

        # do something with final_population, hall of fame and log.
        # warn("character.evolution_main: Output to text has not been tested.")
        write_results(args, output_file, best_per_generation, predictions, run=run, write_header=write_header)
        write_header = False
        with open(output_file + "-game_input.csv", 'a') as out:
            out.write(args.nn_data + ",")
            out.write(lb_string + "\n")

        write_best_per_generation(args, args.bpg_file + "-{0}".format(run), best_per_generation)
        write_final_pop(args, args.final_pop_file + "-{0}".format(run), final_pop)
        print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        # cleaning up...
        tb = None
        pop = None
        gc.collect()


if __name__ == "__main__":
    my_run_args = init.parse_args()
    main(my_run_args)
