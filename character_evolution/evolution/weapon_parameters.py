# import numpy as np
# from os import getcwd
all_params = ["HP", "Speed", "Damage", "Acc", "ClipSize", "RateOfFire", "NrBullets", "Range"]
min_array = [125, 0.77, 36, 0, 1, 0.5, 1, 0, 125, 0.77, 36, 0, 1, 0.5, 1, 0]
max_array = [300, 1.33, 150, 1, 200, 10, 10, 2, 300, 1.33, 150, 1, 200, 10, 10, 2]

tf2_weapon_param_dict = {
    "Scout": [0.00,1.00,0.21,0.80,0.03,0.03,1,0],
    "Pyro": [0.29,0.41,0.82,0.30,1.00,0.04,1,0],
    "Heavy": [1.00,0.00,0.00,0.60,1.00,1.00,0,0],
    "Engineer": [0.00,0.41,0.21,0.80,0.03,0.03,1,0],
    "Medic": [0.14,0.54,-0.23,0.95,0.20,0.24,0,1],
    "Sniper": [0.00,0.41,1.00,1.00,0.12,0.00,0,2],
    "Spy": [0.00,0.54,0.04,0.95,0.03,0.03,0,1],
    "Soldier": [0.43,0.05,0.47,1.00,0.02,0.02,0,2]
    }

tf2_dmg_classes = ["Heavy", "Pyro", "Scout", "Sniper", "Soldier"]


def minmax_normalization(weapon, minarr=min_array, maxarr=max_array):
    norm_weapon = []
    for i in range(len(weapon)):
        if weapon[i] == 0:  # 0 is always zero
            norm_weapon.append(0)
        elif weapon[i] > 0 and minarr[i] == maxarr[i]:  # we cannot do a division, but also: all values are the same
            norm_weapon.append(1)
        else:
            norm_val = (weapon[i] - minarr[i]) / (maxarr[i] - minarr[i])
            # print "min: {0}, max: {1}, weap: {2}. normalised:{3}".format(minarr[i], maxarr[i], weapon[i], norm_val)
            norm_weapon.append(norm_val)
    return norm_weapon


def convert_weapon_back(parameter_array, minarr=min_array, maxarr=max_array):
    original_range_parameters = []
    for i in range(len(parameter_array)):
        denormalized_param = parameter_array[i] * (maxarr[i] - minarr[i]) + minarr[i]
        original_range_parameters.append(denormalized_param)
    original_range_parameters[7] = parameter_array[7]
    original_range_parameters[15] = parameter_array[15]
    return original_range_parameters


def float_list_to_st(flist, end_line=True):
    if end_line:
        return ",".join(["{:.2f}".format(f) for f in flist]) + "\n"
    else:
        return ",".join(["{:.2f}".format(f) for f in flist])


def pretty_string(individual):
    combined = zip(all_params * 2, individual)
    pretty = ["{0}: {1:.2f}".format(h, g) for h, g in combined]
    return ",".join(pretty)


# if __name__ == "__main__":
    # with open(getcwd() + "/weapon_info/loc_norm.csv", "w") as out:
    #     out.write("normalized\n")
    #     out.write(",".join(all_params) + "\n")
    #     i = 0
    #     for weapon in weapons:
    #         wp_params = minmax_normalization(weapon, mins, maxs)
    #         out.write(weapon_names[i] + ",")
    #         out.write(float_list_to_st(wp_params))
    #         i += 1
    #     i = 0
    #     out.write("original\n")
    #     for weapon in weapons:
    #         out.write(weapon_names[i] + ",")
    #         out.write(float_list_to_st(weapon))
    #         i += 1


