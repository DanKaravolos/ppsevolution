import os
from time import time
from datetime import datetime

from character_evolution.evolution.so_evolution_main import main
from character_evolution.evolution.init_and_select import parse_args
from common.common import parse_nn_settings, parse_targets
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # use a specific GPU
t0 = time()

mut = 0.2
crs = 0.8

args = parse_args(result_folder="character_evolution/ScoutHeavy_Test", result_file="SO_MR",
                  cross_prob=crs, mut_prob=mut)
args.runs = 3

# maps = ["/data/maps/custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3,4,5,6,7,8,9,10]]
# maps = ["/data/maps/custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3]]
maps = ["/data/maps/custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1]]

# target_settings = ["target_values_regression_short.txt", "target_values_regression_med.txt", "target_values_regression_long.txt"]
target_settings = ["target_values_regression_short.txt"]
# target_settings = ["target_values_regression_med.txt"]
# target_settings = ["target_values_regression_long.txt"]
model_settings = ["regression_model_settings.txt"] * len(target_settings)
fitnesses = ["squared_error"]

orig_rf = args.result_file
orig_bpg = args.bpg_file
orig_fp = args.final_pop_file
print("Your  code is becoming messy. Fix the init_and_select")

for model_set, target_set in zip(model_settings, target_settings):
    args.nn_settings_file = model_set
    args.target_file = target_set
    args.models = parse_nn_settings(os.getcwd() + "/character_evolution/" + model_set)
    args.target_values = parse_targets(os.getcwd() + "/character_evolution/" + target_set)
    str_tar = "-".join(["{0}_{1:.2f}".format(k, v) for k, v in sorted(args.target_values.items())])
    args.match = "Scout_Heavy"
    args.result_file = orig_rf + "-{0}-{1}".format(str_tar, args.match)
    args.bpg_file = orig_bpg + "-{0}-{1}".format(str_tar, args.match)
    args.final_pop_file = orig_fp + "-{0}-{1}".format(str_tar, args.match)
    for fitness in fitnesses:
        args.fitness = fitness
        for evaluated_map in maps:
            args.nn_data = os.getcwd() + evaluated_map
            print("\nCurrent time:", datetime.now().strftime("%Y-%m-%d %H:%M"))
            print("Starting run with these arguments:")
            for key, value in iter(args.__dict__.items()):
                print("{0}: {1}".format(key, value))
            main(args)

t1 = time()

print("Total time taken: {0} min".format((t1 - t0) / 60.0))
