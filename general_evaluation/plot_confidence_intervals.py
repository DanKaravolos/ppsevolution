import matplotlib.pyplot as plt
import os
import numpy as np
from collections import defaultdict
from common.plot_vars import *


def parse_ci_input(input_fi):
    print("Reading file :", input_fi)
    ci_dict = defaultdict(lambda : defaultdict(list))
    with open(input_fi, 'r') as inpt:
        lines = inpt.readlines()
        header = lines[0]
        header_elems = header.rstrip().split(",")
        header_elems = [h.lstrip() for h in header_elems]
        d_time = header_elems.index("d class time")
        score_low = header_elems.index("team0ratio CI low")
        score_high = header_elems.index("team0ratio CI high")
        time_low = header_elems.index("time CI low")
        time_high = header_elems.index("time CI high")
        for line in lines[1:]:
            elems = line.rstrip().split(",")
            map_name = elems[1].replace("custom_maps/", "")
            ci_items = [elems[score_low], elems[score_high], elems[time_low], elems[time_high]]
            if "" not in ci_items:
                ci_dict[map_name][elems[d_time]].append([float(it) for it in ci_items])
            else:
                print("empty gt found at line ", elems[0])
    return ci_dict


def parse_tf_input(tf_inpt):
    tf_dict = defaultdict(dict)
    with open(tf_inpt, 'r') as inpt:
        lines = inpt.readlines()
        header = lines[0]
        header_elems = header.rstrip().split(",")
        header_elems = [h.lstrip() for h in header_elems]
        cmap = header_elems.index("map")
        score = header_elems.index("team0ratio")
        time = header_elems.index("time")
        # indices should probably be 0,1,2.
        for line in lines[1:]:
            elems = line.rstrip().split(",")
            map_name = elems[cmap].replace("custom_maps/", "")
            tf_dict[map_name]["time"] = float(elems[time])
            tf_dict[map_name]["score"] = float(elems[score])
    return tf_dict


def process_ci_data(datadict):
    for cmap, time_dict in datadict.items():
        for time, metrics in time_dict.items():
            means = np.mean(metrics, axis=0)
            datadict[cmap][time] = means
    return datadict


def make_ci_plot(ci_means, tf_data, plot_title):
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), sharey=True)
    plt.title(plot_title)
    maps = sorted(ci_means.keys(), key=lambda x: int(x.replace("Custom_", "")))
    print("hard coding map order")

    idx = 1
    for cmap in maps:
        axes[0].scatter(idx, tf_data[cmap]["time"], color="k", zorder=4)
        axes[1].scatter(idx, tf_data[cmap]["time"], color="k", zorder=4)
        box_width= 0.1
        idx -= box_width
        for ti in times:
            # plot duration
            interval = ci_means[cmap][ti][2:4]
            # axes[0].plot(idx, time_goals[ti], c='#000000', zorder=7, alpha=0.6)

            bar_size = interval[1] - interval[0]
            bottom = interval[0]

            axes[0].bar(idx, bar_size, box_width, bottom=bottom, edgecolor='k', fc=box_colors[ti], zorder=5)
            # plot score
            interval = ci_means[cmap][ti][0:2]
            # axes[1].plot(idx, 0.5, c='#000000', zorder=7, alpha=0.6)
            bar_size = interval[1] - interval[0]
            bottom = interval[0]

            axes[1].bar(idx, bar_size, box_width, bottom=bottom, edgecolor='k', fc=box_colors[ti], zorder=5)

            idx += box_width

        idx += 1
    axes[1].axhline(0.5, c='#000000', zorder=3)

    for ti in times:
        axes[0].axhline(time_goals[ti], color=box_colors[ti], zorder=6)

    axes[0].set_ylabel("Duration")
    axes[1].set_ylabel("Score")
    for ax in axes.flatten():
        ax.set_ylim([0, 1])
        # ax.set_xlim([0, indices[-1][-1] + 1])
        ax.set_xticklabels([])
        # ax.set_xticks(np.arange(0, indices[-1][-1] + 1, 0.25 if whiskerplot else 0.5))
        ax.set_xticks([])
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.set_yticklabels([0, "", "", "", "", 0.5, "", "", "", "", 1])
        ax.grid(True)


def main(ci_inpt, tf_input, plot_title, plot_file, show=True):
    print("parsing files...")
    ci_data = parse_ci_input(ci_inpt)
    tf_data = parse_tf_input(tf_input)
    print("processing data...")
    ci_means = process_ci_data(ci_data)

    print("plotting...")
    make_ci_plot(ci_means, tf_data, plot_title)

    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def init(plt_title):
    print("Plotting", plt_title)
    # input:
    tf_input_file = os.getcwd() + "/Original_TF_Matches/OriginalTF2_ScoutHeavy/OriginalTF2_ScoutHeavy-per_map_summary.csv"

    # maps:
    if plt_title == "Map MO":
        folder = os.getcwd() + "/map_evolution/Results/"
        ci_input_file = folder + "Scout_Heavy_MO/results/MOEA-evaluation.csv"

    if plt_title == "Map SO":
        folder = os.getcwd() + "/map_evolution/Results/"
        ci_input_file = folder + "Scout_Heavy_Best/p100/tog_p100-evaluation.csv"


    # classes:
    if plt_title == "Class MO":
        folder = os.getcwd() + "/character_evolution/Results/character_evolution/"
        ci_input_file = folder + "Scout_Heavy_MO/Scout_Heavy_MO2-evaluation.csv"
    if plt_title == "Class SO":
        folder = os.getcwd() + "/character_evolution/Results/character_evolution/"
        ci_input_file = folder + "Scout_Heavy_SO/SO-evaluation.csv"

    # classes:
    if plt_title == "SIM MO":
        folder = os.getcwd() + "/dual_evolution/Results/"
        ci_input_file = folder + "Scout_Heavy_MO/results//MOEA_orchestration-evaluation.csv"
    if plt_title == "SIM SO":
        folder = os.getcwd() + "/dual_evolution/Results/"
        ci_input_file = folder + "Scout_Heavy/results/orchestration-evaluation.csv"

    # output:
    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    plot_file = result_folder + "{0}-GT_intervals.png".format(plt_title)
    main(ci_input_file, tf_input_file, plt_title, plot_file, show=True)


if __name__ == "__main__":
    change_font_settings()
    print("The functions in this file are hardcoded for team0ratio and time.")
    titles = ["SIM SO", "SIM MO"]
    for title in titles:
        init(title)
