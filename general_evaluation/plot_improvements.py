import os
import numpy as np
from general_evaluation.compute_most_improved import compute_most_improved
import matplotlib.pyplot as plt
from common.plot_vars import *
from general_evaluation.summaries import *


def plot_improvements(all_imp, plot_title, plot_file, mode, show=True):
    print("plotting improvements")
    # fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), sharey=True)
    fig = plt.figure(figsize=(12, 4))
    ax = plt.gca()
    # plt.title(plot_title)
    maps = sorted(all_imp.keys(), key=lambda x: int(x.replace("Custom_", "")))
    print("hard coding map order")
    idx = 0
    box_width = 0.75
    labels = []
    for cmap in maps:
        for ti in times:
            idata = all_imp[cmap][ti]
            vals = [run[0] for run in idata]
            mean = np.mean(vals)
            conf95 = 1.96 * np.std(vals) / np.sqrt(len(vals))
            print("{2}\t {3}\tMean impr.: {0:.2f}, 95% conf: {1:.2f}".format(mean, conf95, cmap, ti))
            ax.bar(idx * box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

            if ti == "Medium":
                labels.append(cmap)
            else:
                labels.append("")
            idx += 1
        idx += 1
        labels.append("")

    for ti in times:
        vals = []
        for cmap in maps:
            vals.extend([run[0] for run in all_imp[cmap][ti]])
        mean = np.mean(vals)
        conf95 = 1.96 * np.std(vals) / np.sqrt(len(vals))
        ax.bar(idx*box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

        if ti == "Medium":
            labels.append("Overall")
        else:
            labels.append("")
        idx += 1

    # plot settings
    last_index = len(labels)
    ticks = [i*box_width for i in range(last_index)]
    ax.axhline(y=0, color='k', linewidth=1)
    ax.set_xticks(ticks)
    ax.set_xticklabels([l.replace("Custom_", "D") for l in labels])
    ax.set_xlim([0-0.5*box_width, (last_index - 1)*box_width + 0.5*box_width])
    ymax = 1
    if mode == "map":
        if "MO" in plot_file:
            ymin = -0.2
        else:
            ymin = -0.7
        ymax = 0.9
    elif mode == "class":
        if "MO" in plot_file:
            ymin = -0.7
        else:
            ymin = -0.3
    elif mode == "duo":
        ymin = 0
    ax.set_ylim([ymin, ymax])
    ax.set_yticks([i for i in np.arange(ymin, 1.01, 0.1)])
    # Adjust Map MO labels
    if mode == "map" and "MO" in plot_file:
            # labels = zip(["{:.0f}%".format(i * 100) for i in np.arange(ymin, 1.01, 0.2)], ["" for i in np.arange(ymin, 1.01, 0.2)])
            ymin = -0.1
            labels = zip(["" for i in np.arange(ymin, 1.01, 0.2)], ["{:.0f}%".format(i * 100) for i in np.arange(ymin, 1.01, 0.2)])
    else:
        labels = zip(["{:.0f}%".format(i*100) for i in np.arange(ymin, 1.01, 0.2)], ["" for i in np.arange(ymin, 1.01, 0.2)])
    ylabels = [[num, empty] for num, empty in labels]
    ylabels = np.ravel(ylabels)
    ax.set_yticklabels(ylabels)


    ax.grid(axis="y")
    plt.axis(pad=0)
    # Turn off the display of all ticks.
    ax.tick_params(which='both',  # Options for both major and minor ticks
                   top='off',  # turn off top ticks
                   left='on',  # turn off left ticks
                   right='off',  # turn off right ticks
                   bottom='off')  # turn off bottom ticks

    plt.tight_layout()

    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def main(mode, moea):
    if mode == "map":
        ea_data, pds, pms, pdpms, ea_out_file = get_map_summaries(moea)
        plt_title = "Map MO" if moea else "Map SO"
    elif mode == "class":
        ea_data, pds, pms, pdpms, ea_out_file = get_class_summaries(moea)
        plt_title = "Class MO" if moea else "Class SO"
    elif mode == "duo":
        ea_data, pds, pms, pdpms, ea_out_file = get_duo_summaries(moea)
        plt_title = "Orchestration MO" if moea else "Orchestration SO"

    tf_per_map = get_tf_summaries()
    _, all_improvements = compute_most_improved(ea_data, tf_per_map, ea_out_file)

    # output:
    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    if not os.path.exists(result_folder):
        os.mkdir(result_folder)
    plot_file = result_folder + "improvements-{0}.png".format(plt_title.replace(" ", "_"))
    plot_improvements(all_improvements, plt_title, plot_file, mode, show=False)


if __name__ == "__main__":
    change_font_settings()
    # Running the code from ppsEvolution/
    # parameters: map_data, moea

    # # # map generation:
    # main("map", True)
    # main("map", False)

    # class generation
    # main("class", True)
    # main("class", False)

    # # # orchestration
    # main("duo", True)
    main("duo", False)


    # debug note:
    # if this happens: ValueError: operands could not be broadcast together with shapes (2,) (0,)
    # do:  Activate evolved_map_data_list = [dp for dp in evolved_map_data_list if dp.has_gt] (line 13) of EvolvedMapSummary
    # or similar classes
