from collections import defaultdict
from common.ComparisonData import ComparisonData
from common.EvolvedIndividualSummary import EvolvedIndividualSummary
from map_evolution.postprocessing.MostImprovedMapData import MostImprovedMapData
from general_evaluation.summaries import *


def compute_most_improved(ea_data, tf, out_fi, write_output=False):
    improvement_data = defaultdict(lambda: defaultdict(list))
    for e_dp in ea_data:
        if e_dp.has_gt:
            # print("compute_improvement")
            if e_dp.initial_map.startswith("/home/"):
                initial_map_name = e_dp.initial_map.rsplit("/", 1)[1]
            else:
                initial_map_name = e_dp.initial_map.replace("custom_maps/", "")
            tf_map_name = "custom_maps/" + initial_map_name
            single_point_summary = EvolvedIndividualSummary([e_dp])
            comp_data = ComparisonData(single_point_summary, tf[tf_map_name])
            improvement_data[initial_map_name][e_dp.desired_classes['time']].append([comp_data.improvement, comp_data])
        else:
            print("dp has no GT")
            # improvement_data[initial_map_name][e_dp.desired_classes['time']].append([-999, comp_data])

        #     tf_map_name = e_dp.initial_map if e_dp.initial_map.startswith("custom_maps/") else "custom_maps/" + e_dp.initial_map  # STUPID fix. but I feel lazy
        #     single_point_summary = EvolvedIndividualSummary([e_dp])
        #     comp_data = ComparisonData(single_point_summary, tf[tf_map_name])
        #     improvement_data[e_dp.initial_map][e_dp.desired_classes['time']].append([comp_data.improvement, comp_data])
        # else:
        #     print("dp has no GT")
        #     improvement_data[e_dp.initial_map][e_dp.desired_classes['time']].append([-999, comp_data])

    print("computing most improved run per map per duration")
    most_improved_dict = defaultdict(lambda: defaultdict(list))
    for key, time_dict in improvement_data.items():
        for time, data in time_dict.items():
            idx = data.index(max(data, key=lambda x: x[0]))
            most_improved_dict[key][time] = [idx] + data[idx]
    if out_fi != "" or not write_output:
        write_result(improvement_data, most_improved_dict, out_fi)
    return most_improved_dict, improvement_data


def write_result(improvement_data, most_improved_dict, out_fi):
    print("writing output")
    out_fi = out_fi.replace("evaluation", "most_improved")
    with open(out_fi, 'w') as out:
        write_header = True
        # header = ["map", "desired time", "run", "is best", "improvement"]
        # out.write(",".join(header) + "\n")
        for key, time_dict in improvement_data.items():
            for time, data in time_dict.items():
                if write_header:
                    header = ["map", "desired time", "run", "is best", "improvement"]
                    data_header = [improvement_data[key][time][0][1].get_header()]
                    out.write(",".join(header + data_header) + "\n")
                    write_header = False

                for idx in range(len(data)):
                    improvement = improvement_data[key][time][idx][0]
                    is_best = most_improved_dict[key][time][0] == idx
                    line = [key, time, str(idx), str(is_best), "{0:.4f}".format(improvement)]

                    if is_best:
                        line.append(most_improved_dict[key][time][2].to_string())

                    out.write(",".join(line) + "\n")


def compute_map_properties(most_improved, out_fi):
    print("computing comparison of most improved")
    map_data = MostImprovedMapData(most_improved)

    print("writing comparison output")
    mi_fi = out_fi.replace("evaluation", "most_improved_comparison")
    with open(mi_fi, 'w') as out:
        out.write(map_data.get_header(True))
        out.write(map_data.to_string())


def main(mode, moea):
    if mode == "map":
        ea_data, pds, pms, pdpms, ea_out_file = get_map_summaries(moea)
    elif mode == "class":
        ea_data, pds, pms, pdpms, ea_out_file = get_class_summaries(moea)
    elif mode == "duo":
        ea_data, pds, pms, pdpms, ea_out_file = get_duo_summaries(moea)
    tf_per_map = get_tf_summaries()
    # output:
    most_improved_dict, _ = compute_most_improved(ea_data, tf_per_map, ea_out_file)
    if mode == "map":
        compute_map_properties(most_improved_dict, ea_out_file)


if __name__ == "__main__":
    # Running the code from ppsEvolution/
    # main("map", True)
    # main("map", False)
    # main("class", True)
    # main("class", False)
    main("duo", False)
    # main("duo", True)
