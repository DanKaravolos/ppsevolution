import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from common.plot_vars import *
from general_evaluation.summaries import get_map_summaries, get_tf_summaries
from general_evaluation.compute_most_improved import compute_most_improved
from general_evaluation.summaries import get_map_summaries, get_tf_summaries, get_duo_summaries
from common.ParetoData import ParetoSummary

desired = {"Short": (0.5, 0.14), "Medium": (0.5, 0.36), "Long": (0.5, 0.72)}
times = ["Short", "Medium", "Long"]


def plot_pareto_front_gameplay(run_data, plot_file, time, xlims, ylims, show=True):
    print("plot gameplay")
    fig = plt.figure(figsize=(4, 4), dpi=400)
    ax = plt.gca()
    for point in run_data:
        # x = point.fit_score
        # y = point.fit_duration
        x = point.score
        y = point.duration
        marker = "d" if point.is_best else "o"
        color = [0, 0.85, 0.1, 1] if point.is_best else [0, 0, 0, 0.6]
        z = 9 if point.is_best else 5
        ax.scatter(x, y, c=color, marker=marker, zorder=z)

    # axis settings:
    # ax.set_xlim([0.4, 0.6])
    # # dtime = desired[time][1]
    # if time == "Short":
    #     ax.set_ylim([0.135, 0.16])
    # elif time == "Medium":
    #     ax.set_ylim([0.27, 0.37])
    # elif time == "Long":
    #     ax.set_ylim([0.45, 0.75])
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)

    ax.set_xlabel("Kill Ratio (KR)")
    ax.set_ylabel("Duration (t)")
    plt.grid(True)
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.01)
    if show:
        plt.show()
    # plt.close()


def plot_pareto_front_fitness(run_data, plot_file, xlims, ylims, show=True):
    print("plot fitness")
    fig = plt.figure(figsize=(6, 5), dpi=400)
    # fig = plt.figure(dpi=400)
    plt.grid(True)
    ax = plt.gca()
    for point in run_data:
        x = point.fit_score
        y = point.fit_duration
        # x = point.score
        # y = point.duration
        marker = "d" if point.is_best else "o"
        z = 9 if point.is_best else 5
        color = [0, 0.85, 0.1, 1] if point.is_best else [0, 0, 0, 0.5]
        ax.scatter(x, y, c=color, marker=marker, zorder=z)

    # axis settings:
    # xlims = [max(xlims[0], 0), xlims[1]]
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    # ax.xaxis.set_major_formatter(FormatStrFormatter('%.3f'))
    # ax.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
    # ax.set_xticks(np.arange(xlims[0] + 0.001, xlims[1] + 0.001, 0.005))
    ax.set_xticks([0, 0.01, 0.02], minor=False)
    ax.set_xticks([0.005, 0.015, 0.025], minor=True)
    ax.set_yticks(np.arange(ylims[0], ylims[1] + 0.001, 0.01))
    plt.grid(b=False, which='minor', color=[0.7, 0.7, 0.7], linestyle='--')
    # ax.set_xlabel("Kill Ratio Fitness")
    # ax.set_ylabel("Duration Fitness")
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.01)
    if show:
        plt.show()
    # plt.close()

def get_map_pareto_data():
    fo = "/home/daniel/Projects/ppsEvolution/map_evolution/Results/Scout_Heavy_MO/results/"
    fi_base = fo + "MOEA_BP-P_100-gen_100-mut_0.20-crs_0.80-Scout_Heavy-"
    pareto_files = [fi_base + "team0ratio_{0:.2f}-time_{1:.2f}-Scout_Heavy-fitnesses".format(desired[d][0], desired[d][1]) for d in times]
    return pareto_files


def get_duo_pareto_data():
    fo = "/home/daniel/Projects/ppsEvolution/dual_evolution/Results/Scout_Heavy_MO/results/"
    fi_base = fo + "SIM_MO-P_100-gen_100-mut_0.20-crs_0.80-Scout_Heavy-"
    pareto_files = [fi_base + "team0ratio_{0:.2f}-time_{1:.2f}-fitnesses.csv".format(desired[d][0], desired[d][1]) for d in times]
    return pareto_files


def parse_pareto(fitness_file, mode):
    data = []
    run_count = 0
    with open(fitness_file, "r") as inpt:
        lines = inpt.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            if elems[0] == "num":
                run_count += 1
            data.append(elems)
    summary = ParetoSummary(data, mode=mode, runs_per_map=10)
    return summary


def pareto_main(mode, map_to_plot):
    change_font_settings()
    if mode == "map":
        pareto_files = get_map_pareto_data()
    elif mode == "duo":
        pareto_files = get_duo_pareto_data()
        print("Parsing data...")
    pareto_data = [parse_pareto(p, mode) for p in pareto_files]

    # for t in range(len(times)):
    # # for t in range(1):
    #     run = 0
    #     for run_data in pareto_data[t].per_map_data[map_to_plot]:
    #         print("time {0}, run {1}".format(t, run))
    #         plot_file = os.getcwd() + "/general_evaluation/pareto/pareto-fitness-{0}-{1}-run_{2}.png".format(map_to_plot, times[t], run)
    #         xlims = pareto_data[t].fit_score_minmax[map_to_plot][run] + [-0.005, 0.005]
    #         ylims = pareto_data[t].fit_duration_minmax[map_to_plot][run] + [-0.005, 0.005]
    #         plot_pareto_front_fitness(run_data, plot_file, xlims, ylims, show=False)
    #
    #         # plot_file = os.getcwd() + "/general_evaluation/pareto/pareto-gameplay-{0}-{1}-run_{2}.png".format(map_to_plot, times[t], run)
    #         # xlims = pareto_data[t].score_minmax[map_to_plot][run] + [-0.01, 0.01]
    #         # ylims = pareto_data[t].duration_minmax[map_to_plot][run] + [-0.01, 0.01]
    #         # plot_pareto_front_gameplay(run_data, plot_file, times[t], xlims, ylims, show=False)
    #         run += 1
    #         plt.close()
    # ToG plot:
    t = 2
    run = 7
    run_data = pareto_data[t].per_map_data[map_to_plot][run]
    plot_file = os.getcwd() + "/general_evaluation/pareto/pareto-{0}-{1}-r{2}.png".format(map_to_plot.replace("Custom_", "D"), times[t], run)
    xlims = np.asarray([-0.001, 0.025])
    ylims = np.asarray([0.01, 0.05])
    plot_pareto_front_fitness(run_data, plot_file, xlims, ylims, show=False)
    print("done")


if __name__ == "__main__":
    # Running the code from ppsEvolution/
    pareto_main("duo", "Custom_1")
