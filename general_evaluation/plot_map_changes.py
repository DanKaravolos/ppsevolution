import os
import numpy as np
import matplotlib.pyplot as plt
from common.plot_vars import *
from general_evaluation.summaries import get_map_summaries, get_duo_summaries
# from map_evolution.postprocessing.EvolvedMapData import sorted_tiles
graph_sorted_files = ["0", "1", "2", "S", "A", "D", "H"]


def plot_changes(pds, plot_title, plot_file, mode, show=True):
    print("plotting map changes")
    # fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), sharey=True)
    fig = plt.figure() #figsize=(12, 4)
    ax = plt.gca()
    # plt.title(plot_title)

    box_width = 0.3
    labels = []
    bar_idx = 0
    max_vals = []
    tiles = [tile for tile in graph_sorted_files if len(tile) == 1]
    for tile in tiles:
        for ti in times:
            tiles = [d.tc_diff_w_initial[tile] for d in pds[ti].data]
            max_vals.append(max(tiles))
            # tiles = [change[tile] for change in all_changes]
            mean = np.mean(tiles, axis=0)
            conf95 = 1.96 * np.std(tiles) / np.sqrt(len(tiles))
            ax.bar(bar_idx*box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

            if ti == "Medium":
                labels.append(tile.replace("2", "W"))
            else:
                labels.append("")
            bar_idx += 1
        labels.append("")
        bar_idx += 1
    # plot settings
    last_index = len(labels) - 1
    ticks = [i*box_width for i in range(last_index)]
    ax.axhline(y=0, color='k', linewidth=1)
    ax.set_xticks(ticks)
    ax.set_xticklabels(labels)
    ax.set_xlim([0-0.5*box_width, (last_index - 1) * box_width + 0.5*box_width])

    if mode == "map":
        # maxy = 20  # for thesis
        maxy = 40
        ax.set_yticks([i for i in np.arange(-maxy, maxy + 1, 4)])
    elif mode == "duo":
        maxy = 10
        ax.set_yticks([i for i in np.arange(-maxy, maxy + 1, 2)])

    ax.set_ylim([-maxy, maxy])

    # ax.set_yticklabels(["{:.2f}%".format(i/4) for i in np.arange(-25, 26, 5)])

    ax.grid(axis="y")
    # Turn off the display of all ticks.
    ax.tick_params(which='both',  # Options for both major and minor ticks
                   top='off',  # turn off top ticks
                   left='on',  # turn off left ticks
                   right='off',  # turn off right ticks
                   bottom='off')  # turn off bottom ticks
    # plt.axis(pad=0)
    # plt.tight_layout()

    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def main(mode, moea):
    if mode == "map":
        ea_data, pds, pms, pdpms, ea_out_file = get_map_summaries(moea)
        plt_title = "Map MO" if moea else "Map SO"
    elif mode == "duo":
        ea_data, pds, pms, pdpms, ea_out_file = get_duo_summaries(moea)
        plt_title = "Orchestration MO" if moea else "Orchestration SO"

    # output:
    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    plot_file = result_folder + "map_changes-{0}.png".format(plt_title.replace(" ", "_"))
    plot_changes(pds, plt_title, plot_file, mode, show=True)


if __name__ == "__main__":
    change_font_settings()
    # plt.rc('xtick', labelsize=18)
    # plt.rc('ytick', labelsize=22)
    # plt.rc('figure', titlesize=22)
    # Running the code from ppsEvolution/
    # main("map", True)
    # main("map", False)
    main("duo", False)
    # main("duo", True)
