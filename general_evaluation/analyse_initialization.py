import numpy as np
import os
import re
# import tqdm
from tqdm.auto import tqdm, trange
from tqdm import tqdm_notebook as tqdmn
from os.path import join
import glob
from general_evaluation.analyse_initial_population import analyse_single_map_pop, compute_aggregate

data_folder = "/home/daniel/Projects/ppsEvolution/data/"
random_map_pop_path = data_folder + "df8_6K_maps.npy"
random_class_pop_path = data_folder + "df8_200K_weapons.npy"
mutated_folder = data_folder + "initial_populations/"
rng = np.random.default_rng(seed=137)


def encoding_to_string(eight_layers):
    return ",".join(["{:.0f}".format(layer) for layer in eight_layers])


def get_pop_from(pop_path, pop_size=100, nr_pops=10):
    print("Getting population from ", pop_path)
    population_data = np.load(pop_path)

    pop_mask = rng.choice(np.arange(0, len(population_data)), size=(nr_pops, pop_size), replace=False)
    random_pops = population_data[pop_mask]
    print("Population shape: ", random_pops.shape)
    return random_pops


def compute_randpop_distance_to_map(pop, original_np_map):
    differences = []
    rows, cols, dims = original_np_map.shape
    nr_inds = len(pop) - 1
    for i in range(nr_inds):
        if i == 0:
            print("Analyzing pop size {0}. To account for original map being in the same population".format(nr_inds))
        individual = pop[i]
        nr_differences = 0
        for row in np.arange(rows):
            for col in np.arange(cols):
                ind_tile = encoding_to_string(individual[row][col])
                original_tile = encoding_to_string(original_np_map[row][col])
                if ind_tile != original_tile:
                    nr_differences += 1
        differences.append(nr_differences)
    return np.mean(differences), np.std(differences)


def compute_pairwise_distance_randpop(pop):
    differences = []
    nr_inds, rows, cols, dims = pop.shape
    for i in range(nr_inds):
        for j in range(nr_inds):
            if i != j:
                individual1 = pop[i]
                individual2 = pop[j]
                nr_differences = 0
                for row in np.arange(rows):
                    for col in np.arange(cols):
                        ind_tile = individual1[row][col]
                        original_tile = individual2[row][col]
                        # ind_tile = encoding_to_string(individual1[row][col])
                        # original_tile = encoding_to_string(individual2[row][col])
                        # if ind_tile != original_tile:
                        #     nr_differences += 1
                        if not (ind_tile == original_tile).all():
                            nr_differences += 1
                differences.append(nr_differences)
    return np.mean(differences), np.std(differences)


def compute_pairwise_distance_mutpop(file):
    differences = []
    with open(file, 'r') as inpt:
        lines = inpt.readlines()
        nr_lines = len(lines)
        for i in range(nr_lines):
            for j in range(nr_lines):
                if i != j:
                    original_map = re.split("_|;", lines[i].rstrip())
                    mutated = re.split("_|;", lines[j].rstrip())
                    map_differences = 0
                    for char in range(len(original_map)):
                        mut = mutated[char]
                        orig = original_map[char]
                        if mut != orig:
                            map_differences += 1
                    differences.append(map_differences)
    return np.mean(differences), np.std(differences)


def avg_pop_distance_to_initial_map(map_name, folder):
    print("Computing population distance to initial of mutated maps")
    files = glob.glob(join(folder, "*maps.txt"))
    files = [fi for fi in files if map_name in fi]
    all_distances = []
    for fi in tqdm(files):
        pop_distance_info = analyse_single_map_pop(fi)
        all_distances.append(pop_distance_info)

    print("Map aggregate info:")
    # do something with all_distances to get aggregate. compute mean and 95%interval
    aggregate_info = compute_aggregate(all_distances)
    return all_distances, aggregate_info


def avg_randpop_distance_to_initial_map(random_map_pop, initial_np_map):
    print("Computing population distance to initial of random maps")
    all_distances = []
    for pop in tqdm(random_map_pop):
        pop_distance_info = compute_randpop_distance_to_map(pop, initial_np_map)
        all_distances.append(pop_distance_info)

    print("Map aggregate info:")
    aggregate_info = compute_aggregate(all_distances)
    return all_distances, aggregate_info


def avg_pairwise_dist_randpop(random_map_pop):
    print("Computing pairwise distances of random populations")
    all_distances = []
    for i, pop in tqdm(enumerate(random_map_pop)):
        print("Population ", i + 1)
        pop_distance_info = compute_pairwise_distance_randpop(pop)
        all_distances.append(pop_distance_info)
    print("Random population pairwise aggregate info:")
    aggregate_info = compute_aggregate(all_distances, nr_of_measurements=9900)
    return all_distances, aggregate_info


def avg_pairwise_dist_mutpop(map_name, folder):
    print("Computing pairwise distances of mutated populations")
    files = glob.glob(join(folder, "*maps.txt"))
    files = [fi for fi in files if map_name in fi]
    all_distances = []
    for i, fi in tqdm(enumerate(files)):
        print("Population ", i + 1)
        pop_distance_info = compute_pairwise_distance_mutpop(fi)
        all_distances.append(pop_distance_info)
    print("Mutated population pairwise aggregate info:")
    aggregate_info = compute_aggregate(all_distances, nr_of_measurements=9900)
    return all_distances, aggregate_info


def analyse_map_init(map_name, random_maps_path):
    print("Analysing maps...")
    # random population:
    random_map_pop = get_pop_from(random_maps_path)
    original_np_map = np.load(data_folder + "/maps/custom_maps/{0}_Run_0_Map_1tile.npy".format(map_name))
    print("Original map shape: ", original_np_map.shape)
    avg_randpop_distance_to_initial_map(random_map_pop, original_np_map)
    print("\n")
    avg_pairwise_dist_randpop(random_map_pop)

    # mutated initial population:
    print("\n")
    avg_pop_distance_to_initial_map(map_name, mutated_folder)
    print("\n")
    avg_pairwise_dist_mutpop(map_name, mutated_folder)


def main():
    initial_map_name = "Custom_9"
    initial_classes = " Scout_Heavy"
    analyse_map_init(initial_map_name, random_map_pop_path)
    # analyse_class_init(initial_classes, random_class_pop_path)


if __name__ == "__main__":
    main()
