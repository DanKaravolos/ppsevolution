import os
import numpy as np
# import matplotlib.pyplot as plt
# from collections import defaultdict
from common.plot_vars import *
from general_evaluation.summaries import get_map_summaries, get_tf_summaries, get_duo_summaries, get_class_summaries
from general_evaluation.compute_most_improved import compute_most_improved
from general_evaluation.plot_pareto_front import pareto_main
desired = {"Short": (0.5, 0.14), "Medium": (0.5, 0.36), "Long": (0.5, 0.72)}

#params for label only
# x_offset = 0.01
# y_offset = 0.015
# x_left = -0.06

x_offset = 0.09
y_offset = 0.02
x_left = -0.15
y_left = 0.02
label_location_initial = {
    "D2": (x_left, y_left),
    "D4": (x_left, y_left),
    "D5": (x_left, y_left),
    "D6": (x_left, y_left),
    "D9": (x_left, y_left + 0.04),
    "D10": (x_offset, y_offset + 0.01),
}

a_mod_x = 0
a_mod_y = 0
arrow_mods = {
    "D1": (a_mod_x, a_mod_y),
    "D2": (-a_mod_x, a_mod_y),
    "D3": (a_mod_x, a_mod_y),
    "D4": (-a_mod_x, a_mod_y),
    "D5": (-a_mod_x, a_mod_y),
    "D6": (-a_mod_x, a_mod_y),
    "D7": (a_mod_x, a_mod_y),
    "D8": (a_mod_x, a_mod_y),
    "D9": (-a_mod_x, a_mod_y),
    "D10": (a_mod_x, a_mod_y),
}

def plot_initial(tf_per_map, plot_file, show=True):
    fig = plt.figure(figsize=(4, 4))
    ax = plt.gca()
    for cmap, vals in tf_per_map.items():
        xval = vals.game_metric_data[0]
        yval = vals.game_metric_data[1]
        x_error = 1.96 * np.std([d.game_metrics[0] for d in vals.data]) / len(vals.data)
        y_error = 1.96 * np.std([d.game_metrics[1] for d in vals.data]) / len(vals.data)

        # ax.scatter(xval, yval, c="k", zorder=5)
        ax.errorbar(xval, yval, c="k", zorder=10, xerr=x_error, yerr=y_error, fmt='o')
        name = cmap.replace("custom_maps/Custom_", "D")
        # xytext = (xval+0.01, yval + 0.01) if int(name[-1]) % 2 == 0 else (xval+0.01, yval + 0.01)
        xy_mod = label_location_initial.get(name, (x_offset, y_offset))
        xy_loc = (xval + xy_mod[0], yval + xy_mod[1])

        ax.annotate(name, xy=(xval, yval), xytext=xy_loc, zorder=9,
                    arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    print("Plotting initial state based on {0} runs".format(len(vals.data)))
    for d, vals in desired.items():
        ax.scatter(vals[0], vals[1], c=box_colors[d], marker='d', zorder=5)

    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    # ax.set_xticks(np.arange(0.1, 1, 0.2), minor=True)
    # ax.set_yticks(np.arange(0.1, 1, 0.2), minor=True)

    ax.set_xticks(np.arange(0, 1, 0.2), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.2), minor=True)
    labeled_ticks = [0] + list(np.arange(0.1, 1, 0.2)) + [1]
    ax.set_xticks(labeled_ticks, minor=False)
    ax.set_yticks(labeled_ticks, minor=False)
    ax.set_xlabel("Kill Ratio (KR)")
    ax.set_ylabel("Duration (t)")
    plt.grid()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()

def plot_tog_initial(tf_per_map, plot_file, show=True):
    # plt.rcParams.update({'font.size': '18'})
    fig = plt.figure(figsize=(5, 5))
    ax = plt.gca()
    for cmap, vals in tf_per_map.items():
        xval = vals.game_metric_data[0]
        yval = vals.game_metric_data[1]
        x_error = 1.96 * np.std([d.game_metrics[0] for d in vals.data]) / len(vals.data)
        y_error = 1.96 * np.std([d.game_metrics[1] for d in vals.data]) / len(vals.data)

        # ax.scatter(xval, yval, c="k", zorder=5)
        ax.errorbar(xval, yval, c="k", zorder=10, xerr=x_error, yerr=y_error, fmt='o')
        name = cmap.replace("custom_maps/Custom_", "D")
        # xytext = (xval+0.01, yval + 0.01) if int(name[-1]) % 2 == 0 else (xval+0.01, yval + 0.01)
        xy_mod = label_location_initial.get(name, (x_offset, y_offset))
        xy_loc = (xval + xy_mod[0], yval + xy_mod[1])
        a_mod = arrow_mods.get(name)
        arrow_loc = (xval + a_mod[0], yval + a_mod[1])

        ax.annotate(name, xy=arrow_loc, xytext=xy_loc, zorder=9, fontsize=18,
                    arrowprops=dict(arrowstyle='->,head_length=0.3', connectionstyle='arc3,rad=0'))
    print("Plotting initial state based on {0} runs".format(len(vals.data)))
    for d, vals in desired.items():
        ax.scatter(vals[0], vals[1], c=box_colors[d], marker='d', zorder=5)

    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    # ax.set_xticks(np.arange(0.1, 1, 0.2), minor=True)
    # ax.set_yticks(np.arange(0.1, 1, 0.2), minor=True)
    ax.set_xticks(np.arange(0, 1, 0.2), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.2), minor=True)
    labeled_ticks = [0] + list(np.arange(0.1, 1, 0.2)) + [1]

    ax.set_xticks(labeled_ticks, minor=False)
    ax.set_yticks(labeled_ticks, minor=False)
    tick_labels = ["{:.0f}".format(t) if t == 0 or t == 1 else "{:.1f}".format(t) for t in labeled_ticks]
    ax.set_xticklabels(tick_labels)
    ax.set_yticklabels(tick_labels)

    ax.set_xlabel("Kill Ratio (KR)")
    ax.set_ylabel("Duration (t)")
    plt.grid()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.01)
    if show:
        plt.show()


def initial_main(tf_per_map):
    plt.rcParams.update({'font.size': '20'})
    print("plot initial main")
    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    plot_file = result_folder + "initial_state.png"
    plot_tog_initial(tf_per_map, plot_file, show=True)
    print("saved to", plot_file)


def plot_obj_search(improvement_dict, plot_file, plot_arrows, show=True):
    fig = plt.figure(figsize=(8, 8), dpi=400)
    ax = plt.gca()
    hwidth = 0.01
    for time, data in improvement_dict.items():
        actual = data[2].ea_actual
        target = np.asarray(data[2].desired)  # could also be the desired at the top of this file
        predicted = np.asarray(data[2].ea_data.data[0].predictions)
        initial = data[2].tf_actual
        markersize=8
        ax.scatter(initial[0], initial[1], c='k', s=markersize**2, zorder=3)
        ax.scatter(actual[0], actual[1], c=box_colors[time], s=markersize**2, zorder=5)
        ax.scatter(target[0], target[1], c=box_colors[time], s=markersize**2, marker='d', zorder=4)
        ax.scatter(predicted[0], predicted[1], c=box_colors[time], s=markersize**2, marker='x', zorder=6)

        if plot_arrows:
            d_pi = predicted - initial
            if sum(d_pi) != 0:
                ax.arrow(initial[0], initial[1], d_pi[0], d_pi[1], linestyle=":", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_di = target-initial
            if sum(d_di) != 0:
                ax.arrow(initial[0], initial[1], d_di[0], d_di[1], linestyle="-", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_dp = target - predicted
            if sum(d_dp) != 0:
                ax.arrow(predicted[0], predicted[1], d_dp[0], d_dp[1], linestyle="-.", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_ad = actual - target
            if sum(d_ad) != 0:
                ax.arrow(target[0], target[1], d_ad[0], d_ad[1], linestyle="--", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)

    # ax.set_xlim([0, 1])
    # ax.set_ylim([0, 1])

    # ax.set_xticks(np.arange(0.1, 1, 0.2), minor=True)
    # ax.set_yticks(np.arange(0.1, 1, 0.2), minor=True)

    ax.set_xticks(np.arange(0, 1, 0.2), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.2), minor=True)
    labeled_ticks = [0] + list(np.arange(0.1, 1, 0.2)) + [1]
    ax.set_xticks(labeled_ticks, minor=False)
    ax.set_yticks(labeled_ticks, minor=False)

    # ax.set_xticks(np.arange(0, 1, 0.2), minor=True)
    # ax.set_yticks(np.arange(0, 1, 0.2), minor=True)
    # ax.set_xticks(np.arange(0.1, 1, 0.2), minor=False)
    # ax.set_yticks(np.arange(0.1, 1, 0.2), minor=False)
    # ax.set_xlabel("Kill Ratio (KR)")
    # ax.set_ylabel("Duration (t)")
    # ax.set_xlim([0, 0.8])
    # ax.set_ylim([0, 0.8])
    plt.grid()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.1)
    if show:
        plt.show()


def plot_tiny_obj_search(improvement_dict, plot_file, plot_arrows, show=True):
    fig = plt.figure(figsize=(6, 5), dpi=400)
    # fig = plt.figure(figsize=(4,4), dpi=400)
    ax = plt.gca()
    hwidth = 0.01
    for time, data in improvement_dict.items():
        actual = data[2].ea_actual
        target = np.asarray(data[2].desired)  # could also be the desired at the top of this file
        predicted = np.asarray(data[2].ea_data.data[0].predictions)
        initial = data[2].tf_actual
        markersize=8
        ax.scatter(initial[0], initial[1], c='k', s=markersize**2, zorder=3)
        ax.scatter(actual[0], actual[1], c=box_colors[time], s=markersize**2, zorder=5)
        ax.scatter(target[0], target[1], c=box_colors[time], s=markersize**2, marker='d', zorder=4)
        ax.scatter(predicted[0], predicted[1], c=box_colors[time], s=markersize**2, marker='x', zorder=6)

        if plot_arrows:
            d_pi = predicted - initial
            if sum(d_pi) != 0:
                ax.arrow(initial[0], initial[1], d_pi[0], d_pi[1], linestyle=":", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_di = target-initial
            if sum(d_di) != 0:
                ax.arrow(initial[0], initial[1], d_di[0], d_di[1], linestyle="-", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_dp = target - predicted
            if sum(d_dp) != 0:
                ax.arrow(predicted[0], predicted[1], d_dp[0], d_dp[1], linestyle="-.", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)
            d_ad = actual - target
            if sum(d_ad) != 0:
                ax.arrow(target[0], target[1], d_ad[0], d_ad[1], linestyle="--", color=box_colors[time], length_includes_head=True, head_width=hwidth, width=0)

    labeled_ticks = list(np.arange(0.1, 1, 0.2)) + [0.8]  # [0] +
    # # 0.6 dashed:
    # ax.set_xticks([0.5, 0.7], minor=False)
    # ax.set_xticks([0.6], minor=True)
    # ax.set_xticklabels([0.6], minor=True)
    # ax.axvline(0.6, linestyle="--", linewidth=1, c=[0.7,0.7,0.7])
    # 0.6 line
    # ax.set_xticks([0.5,0.6, 0.7], minor=False)

    # all minor ticks grid
    ax.set_xticks([0.5, 0.7], minor=False)
    ax.set_xticks([0.6], minor=True)
    plt.grid(b=False, which='minor', color=[0.7, 0.7, 0.7], linestyle='--')

    ax.set_yticks(np.arange(0, 1, 0.2), minor=True)
    ax.set_yticks(labeled_ticks, minor=False)
    ax.set_xlim([0.45, 0.75])
    ax.set_ylim([0.1, 0.8])
    plt.grid()
    plt.tight_layout()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0.01)
    if show:
        plt.show()


def single_obj_search_main(tf_per_map, plot_map, plot_arrows, mode="duo"):
    print("plot single objective main")
    # Map generation:
    if mode == "map":
        ea_data, _, _, _, ea_out_file = get_map_summaries(False)
    # Orchestration:
    elif mode == "duo":
        ea_data, _, _, _, ea_out_file = get_duo_summaries(False)
    elif mode == "class":
        ea_data, _, _, _, ea_out_file = get_class_summaries(False)

    most_improved_dict, _ = compute_most_improved(ea_data, tf_per_map, ea_out_file)

    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    # plot_file = result_folder + "search_process-{1}-{0}.png".format(plot_map.replace("Custom_", "D"), mode.replace("duo", "orch"))
    # plot_obj_search(most_improved_dict[plot_map], plot_file, plot_arrows, show=False)
    plot_file = result_folder + "tiny-search_process-{1}-{0}.png".format(plot_map.replace("Custom_", "D"), mode.replace("duo", "orch"))
    plot_tiny_obj_search(most_improved_dict[plot_map], plot_file, plot_arrows, show=False)
    return most_improved_dict


def plot_dot_legend():
    plt.figure()
    # time color dots
    colors = [plt.Line2D([0, 0], [0, 0], color="k", marker='o', markersize=24, linestyle='')]
    colors += [plt.Line2D([0, 0], [0, 0], color=box_colors[color], marker='o', markersize=24, linestyle='') for color in times]
    plt.legend(colors, ["Initial"] + times, numpoints=1, loc=9, ncol=4)
    plt.figure()
    colors = [plt.Line2D([0, 0], [0, 0], color=box_colors[color], marker='o', markersize=24, linestyle='') for color in times]
    plt.legend(colors, times, numpoints=1, loc=9, ncol=4)


    # marker shapes black
    plt.figure()
    marker_types = ["o", "d", "x"]
    # names = ["actual", "predicted", "desired"]
    names = ["Actual", "Predicted", "Desired"]
    markers = [plt.Line2D([0, 0], [0, 0], color="k", marker=m, markersize=23, linestyle='') for m in marker_types]
    plt.legend(markers, names, numpoints=1, loc=9, ncol=3)
    plt.show()


def plot_box_legend():
    plt.figure()
    markers = [plt.Rectangle([0, 0], 1, 1, facecolor=box_colors[color], edgecolor="k") for color in times]
    plt.legend(markers, times, numpoints=1, loc=9, ncol=3)
    plt.show()


def main():
    change_font_settings()
    # plot initial state
    tf_per_map = get_tf_summaries()
    initial_main(tf_per_map)

    # plot search process of most improved:
    # most_improved = single_obj_search_main(tf_per_map, "Custom_4", plot_arrows=False, mode="map")
    # most_improved = single_obj_search_main(tf_per_map, "Custom_5", plot_arrows=False, mode="map")
    # most_improved = single_obj_search_main(tf_per_map, "Custom_1", plot_arrows=False, mode="duo")
    # most_improved = single_obj_search_main(tf_per_map, "Custom_3", plot_arrows=False, mode="duo")
    # plot_dot_legend()
    # plot_box_legend()


if __name__ == "__main__":
    # Running the code from ppsEvolution/
    main()