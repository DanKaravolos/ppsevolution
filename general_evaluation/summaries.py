import os
from map_evolution.postprocessing.evaluate_evolved_maps import evaluate_results as evaluate_maps
from character_evolution.postprocessing.evaluate_evolved_weapons import evaluate_results as evaluate_classes
from dual_evolution.postprocessing.evaluate_evolved_duo import evaluate_results as evaluate_orchestration
from common.evaluate_tf2_gt import evaluate_original_classes

minor_revisions = True

if minor_revisions:
    from_class = False

    initial_class1 = "Scout"
    initial_class2 = "Heavy"


    # def get_duo_summaries(moea):
    #     result_dir = os.getcwd() + "/MutationInitTest/"
    #
    #     if moea:
    #         print("ERROR NO MOEA PATH DEFINED! YOU ARE USING THE MUT INIT TEST PATH!")
    #     else:
    #         print("MutInitTest paths")
    #         num = 4
    #         ea_result_file = result_dir + "ScoutHeavy_SOEA-{0}Mut/results/summary-result-Mut{0}.csv".format(num)
    #         ea_gt_file = result_dir + "GT_summary-Mut{0}-GT.pkl".format(num)
    #         ea_out_file = result_dir + "SO-evaluation-Mut-{0}.csv".format(num)
    #
    #     ea_data, pds, pms, pdpms = evaluate_orchestration(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
    #                                                       initial_class1, initial_class2, write=False)
    #
    #     return ea_data, pds, pms, pdpms, ea_out_file

    def get_duo_summaries(moea):
        result_dir = os.getcwd() + "/MR_Results/dual_evolution-MR/"

        if moea:
            ea_result_file = result_dir + "ScoutHeavy-MOEA_MR/results/summary-MO-result.csv"
            ea_gt_file = result_dir + "ScoutHeavy-MOEA_MR/results/GT_summary-dual_evolution-MOEA-GT.pkl"
            ea_out_file = result_dir + "MO-evaluation.csv"
        else:
            ea_result_file = result_dir + "ScoutHeavy-SOEA_MR/results/summary-SO-result.csv"
            ea_gt_file = result_dir + "ScoutHeavy-SOEA_MR/results/GT_summary-dual_evolution-SOEA-GT.pkl"
            ea_out_file = result_dir + "SO-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_orchestration(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                          initial_class1, initial_class2, write=False)

        return ea_data, pds, pms, pdpms, ea_out_file


    def get_class_summaries(moea):
        result_dir = os.getcwd() + "/MR_Results/character_evolution-MR/"

        if moea:
            ea_result_file = result_dir + "summary-MO-result.csv"
            ea_gt_file = result_dir + "GT_summary-character_evolution-MOEA-GT.pkl"
            ea_out_file = result_dir + "MO-evaluation.csv"
        else:
            ea_result_file = result_dir + "summary-SO-result.csv"
            ea_gt_file = result_dir + "GT_summary-character_evolution-SOEA-GT.pkl"
            ea_out_file = result_dir + "SO-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_classes(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                    initial_class1, initial_class2, write=False)

        return ea_data, pds, pms, pdpms, ea_out_file


    def get_map_summaries(moea):
        result_dir = os.getcwd() + "/MR_Results/map_evolution-MR/"

        if moea:
            ea_result_file = result_dir + "ScoutHeavy-MOEA_MR/results/summary-MO-result.csv"
            ea_gt_file = result_dir + "GT_summary-map_evolution-MOEA-GT.pkl"
            ea_out_file = result_dir + "MO-evaluation.csv"
        else:
            ea_result_file = result_dir + "ScoutHeavy-SOEA_MR/results/summary-SO-result.csv"
            ea_gt_file = result_dir + "GT_summary-map_evolution-SOEA-GT.pkl"
            ea_out_file = result_dir + "SO-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_maps(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                 initial_class1, initial_class2, write=False)
        return ea_data, pds, pms, pdpms, ea_out_file

else:
    from_class = False

    initial_class1 = "Scout"
    initial_class2 = "Heavy"

    def get_duo_summaries(moea):
        result_dir = os.getcwd() + "/dual_evolution/Results/"

        if moea:
            ea_result_file = result_dir+ "Scout_Heavy_MO/results/SIM-MOEA-best_ind-result.csv"
            ea_gt_file = result_dir + "Scout_Heavy_MO/results/GT_summary-Orchestration-MOEA.pkl"
            ea_out_file = result_dir + "Scout_Heavy_MO/results/Scout_Heavy_MO-evaluation.csv"
        else:
            ea_result_file = result_dir + "Scout_Heavy/results/SIM-result.csv"
            ea_gt_file = result_dir + "Scout_Heavy/results/GT_summary-Orchestration.pkl"
            ea_out_file = result_dir + "Scout_Heavy/results/orchestration-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_orchestration(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                          initial_class1, initial_class2, write=False)

        return ea_data, pds, pms, pdpms, ea_out_file


    def get_class_summaries(moea):
        result_dir = os.getcwd() + "/character_evolution/Results/character_evolution/"

        if moea:
            # thesis D4 and D7 replication:
            # ea_result_file = result_dir+ "ScoutHeavy-MOEA_replicate/summary-best_ind-result.csv"
            # ea_gt_file = result_dir + "ScoutHeavy-MOEA_replicate/GT_summary-GT-runs.pkl"
            # ea_out_file = result_dir + "ScoutHeavy-MOEA_replicate/replicate-evaluation.csv"
            ea_result_file = result_dir+ "Scout_Heavy_MO/MO2-best_ind-result2.csv"
            ea_gt_file = result_dir + "Scout_Heavy_MO/GT_summary-ScoutHeavy_MO2.pkl"
            ea_out_file = result_dir + "Scout_Heavy_MO/Scout_Heavy_MO2-evaluation.csv"
        else:
            ea_result_file = result_dir + "Scout_Heavy_SO/ScoutHeavy_BestParams-results.csv"
            ea_gt_file = result_dir + "Scout_Heavy_SO/GT_summary-ScoutHeavy_BestParams.pkl"
            ea_out_file = result_dir + "Scout_Heavy_SO/SO-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_classes(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                    initial_class1, initial_class2, write=False)

        return ea_data, pds, pms, pdpms, ea_out_file


    def get_map_summaries(moea):
        result_folder = os.getcwd() + "/map_evolution/Results/"

        if moea:
            ea_result_file = result_folder + "Scout_Heavy_MO/results/MOEA-raw-results.csv"
            ea_gt_file = result_folder + "Scout_Heavy_MO/results/GT_summary-Tog-P100-MO-MapGen.pkl"
            ea_out_file = result_folder + "Scout_Heavy_MO/results/MOEA-evaluation.csv"
        else:
            ea_result_file = result_folder + "Scout_Heavy_Best/p100/summary-tog_p100-result.csv"
            ea_gt_file = result_folder + "Scout_Heavy_Best/p100/tog_p100-GT_summary.pkl"  # 10% unfinished matches
            ea_out_file = result_folder + "Scout_Heavy_Best/p100/tog_p100-evaluation.csv"

        ea_data, pds, pms, pdpms = evaluate_maps(ea_result_file, ea_gt_file, ea_out_file, from_class, 2,
                                                 initial_class1, initial_class2, write=False)
        return ea_data, pds, pms, pdpms, ea_out_file


def get_tf_summaries():
    result_dir = os.getcwd() + "/Original_TF_Matches/"
    tf_gt_file = result_dir + "/OriginalTF2_ScoutHeavy/GT_summary-OriginalTF2.pkl"
    tf_game_input_file = result_dir + "/OriginalTF2_ScoutHeavy/tf2_class_scout_vs_heavy-game_input.csv"
    tf_out_file = result_dir + "/OriginalTF2_ScoutHeavy/OriginalTF2-evaluation.csv"

    dims = ('file', 'team0ratio', 'time')
    tf_per_map = evaluate_original_classes(tf_game_input_file, tf_gt_file, tf_out_file, dims, write=False)
    return tf_per_map
