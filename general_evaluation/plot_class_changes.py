import os
import numpy as np
# import matplotlib.pyplot as plt
from common.plot_vars import *
from general_evaluation.summaries import get_class_summaries, get_duo_summaries
all_params = ["HP", "Speed", "Damage", "Acc", "ClipSize", "R.o.F", "Bullets", "Range"]
single_letters = ["H", "S", "D", "A", "C", "RF", "B", "R"]


def plot_changes(pds, plot_title, plot_file, mode, show=True):
    print("plotting class changes")
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 6),sharey=True) #figsize=(8, 8),
    fig.subplots_adjust(hspace=.45)
    # fig = plt.figure(figsize=(12, 4))
    # ax = plt.gca()
    plt.title(plot_title)

    box_width = 1
    labels = []
    for param_idx in range(len(all_params)):
        bar_idx = param_idx * (len(times) + 1)
        for ti in times:
            all_changes = pds[ti].changed_params
            class1 = [run[0:8][param_idx] for run in all_changes]
            class2 = [run[8:][param_idx] for run in all_changes]
            classes = [class1, class2]
            for c_id in range(2):
                mean = np.mean(classes[c_id], axis=0)
                conf95 = 1.96 * np.std(classes[c_id]) / np.sqrt(len(classes[c_id]))
                axes[c_id].bar(bar_idx*box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

            if ti == "Medium":
                labels.append(all_params[param_idx])
            else:
                labels.append("")
            bar_idx += 1
        labels.append("")
    # plot settings
    last_index = len(labels)
    ticks = [i*box_width for i in range(last_index)]
    axes[0].set_title("Class 1")
    axes[1].set_title("Class 2")

    for ax in axes:
        ax.axhline(y=0, color='k', linewidth=1)
        ax.set_xticks(ticks)
        ax.set_xticklabels(labels)
        ax.set_xlim([0-0.5*box_width, (last_index - 1) * box_width - 0.5*box_width])

        # adjusted for class only:
        if mode == "class":
            # if "MO" in plot_title:
            #     ymin = -0.6
            # else:
            ymin = -0.8
        # # adjusted for orchestration:
        if mode == "duo":
            ymin = -0.2
        # if mode == "class" and "MO" in plot_title:
        #     axes[1].set_ylim([-0.65, 1.1])
        # else:
        ax.set_ylim([ymin, 1])
        ax.set_yticks([i for i in np.arange(ymin + 0.1, 1, 0.2)], minor=True)
        ax.set_yticks([i for i in np.arange(ymin, 1.01, 0.2)], minor=False)

        # if mode == "class" and not "MO" in plot_title:
        #     labels = zip(["" for i in np.arange(ymin, 1.01, 0.4)], ["{:.2f}".format(i) for i in np.arange(-0.6, 1.01, 0.4)])
        # else:
        labels = zip(["{:.2f}".format(i) for i in np.arange(ymin, 1.01, 0.4)], ["" for i in np.arange(ymin, 1.01, 0.4)])
        ylabels = [[num, empty] for num, empty in labels]
        ylabels = np.ravel(ylabels)
        ax.set_yticklabels(ylabels)

        ax.grid(axis="y")
        # Turn off the display of all ticks.
        ax.tick_params(which='both',  # Options for both major and minor ticks
                       top='off',  # turn off top ticks
                       # left='off',  # turn off left ticks
                       right='off',  # turn off right ticks
                       bottom='off' # turn off bottom ticks
                       )
    # plt.axis(pad=0)
    # plt.tight_layout()

    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0)
    if show:
        plt.show()


def plot_changes_single_letter(pds, plot_title, plot_file, mode, show=True, label_at_bottom=True):
    print("plotting class changes")
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(5, 6),sharey=True) #figsize=(8, 8),
    # fig = plt.figure(figsize=(12, 4))
    # ax = plt.gca()
    plt.title(plot_title)

    box_width = 1
    labels = []
    for param_idx in range(len(all_params)):
        bar_idx = param_idx * (len(times) + 1)
        for ti in times:
            all_changes = pds[ti].changed_params
            class1 = [run[0:8][param_idx] for run in all_changes]
            class2 = [run[8:][param_idx] for run in all_changes]
            classes = [class1, class2]
            for c_id in range(2):
                mean = np.mean(classes[c_id], axis=0)
                conf95 = 1.96 * np.std(classes[c_id]) / np.sqrt(len(classes[c_id]))
                axes[c_id].bar(bar_idx*box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

            if ti == "Medium":
                labels.append(single_letters[param_idx])
            else:
                labels.append("")
            bar_idx += 1
        labels.append("")
    # plot settings
    last_index = len(labels)
    ticks = [i*box_width for i in range(last_index)]
    axes[0].set_title("Class 1")
    axes[1].set_title("Class 2")

    if label_at_bottom:
        fig.subplots_adjust(hspace=.25)
        axes[0].set_xticklabels([])
        axes[1].set_xticklabels(labels)
    else:
        fig.subplots_adjust(hspace=.45)
        axes[0].set_xticklabels(labels)
        axes[1].set_xticklabels([])

    for ax in axes:
        ax.axhline(y=0, color='k', linewidth=1)
        ax.set_xticks(ticks)
        # if not label_at_bottom:
        #     ax.set_xticklabels(labels)
        ax.set_xlim([0-0.5*box_width, (last_index - 1) * box_width - 0.5*box_width])

        # adjusted for class only:
        if mode == "class":
            # if "MO" in plot_title:
            #     ymin = -0.6
            # else:
            ymin = -0.8
        # # adjusted for orchestration:
        if mode == "duo":
            ymin = -0.2
        # if mode == "class" and "MO" in plot_title:
        #     axes[1].set_ylim([-0.65, 1.1])
        # else:
        ax.set_ylim([ymin, 1])
        ax.set_yticks([i for i in np.arange(ymin + 0.1, 1, 0.2)], minor=True)
        ax.set_yticks([i for i in np.arange(ymin, 1.01, 0.2)], minor=False)

        if mode == "class":
            labels = zip(["" for i in np.arange(ymin, 1.01, 0.4)], ["{:.2f}".format(i) for i in np.arange(-0.6, 1.01, 0.4)])
        else:
            labels = zip(["{:.2f}".format(i) for i in np.arange(ymin, 1.01, 0.4)], ["" for i in np.arange(ymin, 1.01, 0.4)])
        ylabels = [[num, empty] for num, empty in labels]
        ylabels = np.ravel(ylabels)
        ax.set_yticklabels(ylabels)

        ax.grid(axis="y")
        # Turn off the display of all ticks.
        ax.tick_params(which='both',  # Options for both major and minor ticks
                       top='off',  # turn off top ticks
                       # left='off',  # turn off left ticks
                       right='off',  # turn off right ticks
                       bottom='off' # turn off bottom ticks
                       )
    # plt.axis(pad=0)
    # plt.tight_layout()

    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0)
    if show:
        plt.show()


def plot_changes_narrow(pds, plot_title, plot_file, mode, show=True):
    print("plotting class changes")
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(5, 6),sharey=True) #figsize=(8, 8),
    fig.subplots_adjust(hspace=0.27)
    fig.subplots_adjust(bottom=0.16)
    # fig = plt.figure(figsize=(12, 4))
    # ax = plt.gca()
    plt.title(plot_title)

    box_width = 0.75
    labels = []
    for param_idx in range(len(all_params)):
        bar_idx = param_idx * (len(times) + 1)
        for ti in times:
            all_changes = pds[ti].changed_params
            class1 = [run[0:8][param_idx] for run in all_changes]
            class2 = [run[8:][param_idx] for run in all_changes]
            classes = [class1, class2]
            for c_id in range(2):
                mean = np.mean(classes[c_id], axis=0)
                conf95 = 1.96 * np.std(classes[c_id]) / np.sqrt(len(classes[c_id]))
                axes[c_id].bar(bar_idx*box_width, mean, box_width, edgecolor='k', fc=box_colors[ti], zorder=5, yerr=conf95)

            if ti == "Medium":
                labels.append(all_params[param_idx])
            else:
                labels.append("")
            bar_idx += 1
        labels.append("")
    # plot settings
    last_index = len(labels)
    ticks = [i*box_width for i in range(last_index)]
    axes[0].set_title("Class 1")
    axes[1].set_title("Class 2")
    axes[0].set_xticklabels([])
    axes[1].set_xticklabels(labels, rotation=60, ha='center')
    for ax in axes:
        ax.axhline(y=0, color='k', linewidth=1)
        ax.set_xticks(ticks)
        ax.set_xlim([0-0.5*box_width, (last_index - 1) * box_width - 0.5*box_width])

        # adjusted for class only:
        if mode == "class":
            if "MO" in plot_title:
                ymin = -0.6
            else:
                ymin = -0.8
        # # adjusted for orchestration:
        if mode == "duo":
            ymin = -0.2
        # if mode == "class" and "MO" in plot_title:
        #     axes[1].set_ylim([-0.65, 1.1])
        # else:
        ax.set_ylim([ymin, 1])
        ax.set_yticks([i for i in np.arange(ymin + 0.1, 1, 0.2)], minor=True)
        ax.set_yticks([i for i in np.arange(ymin, 1.01, 0.2)], minor=False)

        if mode == "class" and not "MO" in plot_title:
            labels = zip(["" for i in np.arange(ymin, 1.01, 0.4)], ["{:.2f}".format(i) for i in np.arange(-0.6, 1.01, 0.4)])
        else:
            labels = zip(["{:.2f}".format(i) for i in np.arange(ymin, 1.01, 0.4)], ["" for i in np.arange(ymin, 1.01, 0.4)])
        ylabels = [[num, empty] for num, empty in labels]
        ylabels = np.ravel(ylabels)
        ax.set_yticklabels(ylabels)

        ax.grid(axis="y")
        # Turn off the display of all ticks.
        ax.tick_params(which='both',  # Options for both major and minor ticks
                       top='off',  # turn off top ticks
                       # left='off',  # turn off left ticks
                       right='off',  # turn off right ticks
                       bottom='off' # turn off bottom ticks
                       )
    # plt.tight_layout()
    plt.savefig(plot_file, bbox_inches='tight', pad_inches=0)
    if show:
        plt.show()


def main(mode, moea):
    if mode == "class":
        ea_data, pds, pms, pdpms, ea_out_file = get_class_summaries(moea)
        plot_title = "Class MO" if moea else "Class SO"
    elif mode == "duo":
        ea_data, pds, pms, pdpms, ea_out_file = get_duo_summaries(moea)
        plot_title = "Orchestration MO" if moea else "Orchestration SO"

    # output:
    result_folder = os.getcwd() + "/general_evaluation/graphs/"
    plot_file = result_folder + "class_changes-{0}.png".format(plot_title.replace(" ", "_"))
    # plot_changes_narrow(pds, plot_title, plot_file, mode, show=True)
    plot_changes_single_letter(pds, plot_title, plot_file, mode, show=True)


if __name__ == "__main__":
    change_font_settings()
    plt.rc('xtick', labelsize=18)
    plt.rc('ytick', labelsize=22)
    # plt.rc('figure', titlesize=22)
    # Running the code from ppsEvolution/
    # main("class", True)
    # main("class", False)
    main("duo", False)
    # main("duo", True)
