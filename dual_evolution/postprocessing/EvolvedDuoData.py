from character_evolution.postprocessing.EvolvedWeaponData import EvolvedClass
from map_evolution.postprocessing.EvolvedMapData import EvolvedMap, sorted_tiles
from common.EvolvedIndividualData import EvolvedIndividual
from character_evolution.evolution.weapon_parameters import all_params
from warnings import warn


class EvolvedDuo(EvolvedIndividual):
    def __init__(self, initial_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets, evolved_map_loc,
                 orig_c1="", orig_c2="", infeasible=False, frm_cls=False):
        # warn("Diamond of death warning")
        super().__init__(initial_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets, evolved_map_loc,
                         orig_c1=orig_c1, orig_c2=orig_c2, from_classification=frm_cls)
        self.evolved_class = EvolvedClass(initial_map, param_list1, param_list2, fitness, game_metric_names, game_metrics, targets,
                             orig_c1=orig_c1, orig_c2=orig_c2, from_classification=frm_cls)
        self.evolved_map = EvolvedMap(initial_map, orig_c1, orig_c2, fitness, game_metric_names, game_metrics, targets,
                                      infeasible, evolved_map_loc, from_classification=frm_cls)
        # inherited values from class
        self.tf2_classes = self.evolved_class.tf2_classes
        self.distances = self.evolved_class.distances
        self.dist_to_originals = self.evolved_class.dist_to_originals
        self.match_dist_to_original = self.evolved_class.match_dist_to_original
        self.nearest = self.evolved_class.nearest
        self.changed_params = self.evolved_class.changed_params
        self.p_is_d = self.evolved_class.p_is_d

        # inherited values from map
        self.infeasible = infeasible
        self.evolved_map_loc = self.evolved_map.evolved_map_loc
        self.evolved_map_arr = self.evolved_map.evolved_map_arr
        self.initial_map_loc = self.evolved_map.initial_map_loc

        self.initial_map_arr = self.evolved_map.initial_map_arr
        self.tile_diff_w_initial = self.evolved_map.tile_diff_w_initial
        self.tile_counts = self.evolved_map.tile_counts
        self.initial_tile_counts = self.evolved_map.initial_tile_counts
        self.tc_diff_w_initial = self.evolved_map.tc_diff_w_initial

    def get_header(self, external_line_number=True):
        header = super().get_header(external_line_number)
        # class specific elems
        tf2_classes1 = ",".join("dist P1 {0}".format(t) for t in self.tf2_classes)
        tf2_classes2 = ",".join("dist P2 {0}".format(t) for t in self.tf2_classes)
        header += ",p1 nearest, p2 nearest, {0}, {1}, p1 params, p2 params, ".format(tf2_classes1, tf2_classes2)
        if self.changed_params:
            changed_params = ["C1 {0} change".format(c) for c in all_params] +\
                             ["C2 {0} change".format(c) for c in all_params]
        else:
            changed_params = []
        # map specific elems
        map_elems = ["tile diff to initial", ",".join([key for key in sorted_tiles])
                     , ",".join(["diff {0}".format(key) for key in sorted_tiles])]
        header = header + "," + ",".join(changed_params + map_elems)

        return header

    def to_string(self, add_new_line=True):
        line = super().to_string(add_new_line=False)
        # class specific elems
        d1 = ",".join(["{:.2f}".format(self.distances[0][d]) for d in self.tf2_classes])
        d2 = ",".join(["{:.2f}".format(self.distances[1][d]) for d in self.tf2_classes])
        param_vals = [*self.nearest, d1, d2,
                      "_".join(str(p) for p in self.params1),
                      "_".join(str(p) for p in self.params2)]
        line = line + "," + ",".join(param_vals)

        if self.changed_params:
            changed_params = ["{0:.2f}".format(p) for p in self.changed_params]
        else:
            changed_params = []
        line = line + "," + ",".join(changed_params)
        # map specific elems
        map_elems = [self.tile_diff_w_initial] + [self.tile_counts[key] for key in sorted_tiles]\
                    + [self.tc_diff_w_initial[key] for key in sorted_tiles]
                    # + [self.initial_tile_counts[key] for key in sorted_tiles]
        line = line + "," + ",".join(["{0:.1f}".format(m) for m in map_elems])
        if add_new_line:
            line += "\n"
        return line



