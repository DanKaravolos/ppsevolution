'''
This file parses results files and groundtruth summaries into a datastructure that contains all information necessary
for evaluating the results of evolution. Note that I am assuming that the results are presented in the same order as
the game_input file that was used to create the GT values. This means that I assume that the level numbers in the GT
file correspond to the order of the results in order to link the data in the two files. So: level_num = result_nr + 1.
If you get weird results, you might be violating that assumption.

More info about the pipeline:
After running evolution, use common.concat_files_in_folder to concatenate the game_input and result files.
the resulting game_input file are used to get GroundTruth values from simulations, while the resulting result-file is
used here. After you have obtained the GT values, put the LogFiles folders in new folders in
the data folder of PyProcShooter (on windows) and run parse_summary_files.
Use the resulting summary PKL-file as the GT file here.
'''

import os
import numpy as np
from collections import defaultdict
from dual_evolution.postprocessing.EvolvedDuoData import EvolvedDuo
from dual_evolution.postprocessing.EvolvedDuoSummary import EvolvedDuoSummary
from character_evolution.postprocessing.evaluate_evolved_weapons import parse_gt_file, concat_classes_and_gt
from common.evaluate_individual import *


def extract_targets_from_header(header_elems, nr_ea_targets, game_metric_names):
    targets_elems = header_elems[-nr_ea_targets:]
    targets = {}
    for el in targets_elems:
        target_els = el.split(":")
        target_name = target_els[0].replace("target ", "")
        if target_name in game_metric_names:
            targets[target_name] = float(target_els[1].strip())
    return targets


def parse_result_file(gi_file, nr_ea_targets=2, from_classification=False):
    ea_classes = []
    with open(gi_file, 'r') as inpt:
        lines = inpt.readlines()
        header_elems = lines[0].rstrip().split(",")
        class_start_finder = (i for i, e in enumerate(header_elems) if e == "HP")  #based on https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-given-a-list-containing-it-in-python
        c1_index = next(class_start_finder)
        c2_index = next(class_start_finder)
        # I am assuming the following layout:
        # initial map,match,fitness,team0ratio,time,is_invalid,nr_generations,ind_name,ind_map, c1 params, c2 params,
        # c1 game params, c2 game params ,Run,target team0ratio: 0.5,target time: 0.14
        #
        # MOEA: initial map,match,fitness,avg. fitness,multi_squared_error 0,multi_squared_error 1,team0ratio,time,Sqrt. sum of fitnesses,
        # nr_generations,ind_name,ind_map, c1 params, c2 params, c1 game params, c2 game params ,Run,target team0ratio: 0.5,target time: 0.14
        for line in lines[1:]:
            if line.startswith("initial map"):  # these are header lines. This might introduce new targets, so we copy it.
                header_elems = line.rstrip().split(",")
                continue
            elems = line.rstrip().split(",")
            map_name = elems[0]

            # stuff for classes:
            match = elems[1]
            o_class1, o_class2 = match.split("_")
            class1 = [float(el) for el in elems[c1_index:c1_index + 8]]
            class2 = [float(el) for el in elems[c2_index:c2_index + 8]]

            # stuff for targets
            game_metric_names = header_elems[3:3 + nr_ea_targets] if "multi" not in elems[2] else header_elems[4 + nr_ea_targets:4 + 2 * nr_ea_targets]
            game_metrics = elems[3:3 + nr_ea_targets] if "multi" not in elems[2] else elems[4 + nr_ea_targets:4 + 2 * nr_ea_targets]
            # targets = extract_targets_from_filename(gi_file, game_metric_names)
            targets = extract_targets_from_header(header_elems, nr_ea_targets, game_metric_names)
            # infeasible = elems[header_elems.index("is_invalid")] == "True" if "multi" not in elems[2] else False

            # stuff for evolved map:
            ind_name = elems[header_elems.index("ind_name")] if "multi" not in elems[2] else elems[header_elems.index("ind_name")].rsplit("/", 1)[1]
            evolved_map = gi_file.rsplit("/", 1)[0] + "/best_ind/" + ind_name
            if ".csv" not in evolved_map:
                evolved_map += ".csv"
            ea_classes.append(EvolvedDuo(map_name, class1, class2, elems[3], game_metric_names, game_metrics, targets, evolved_map,
                                         orig_c1=o_class1, orig_c2=o_class2, frm_cls=from_classification))
    return ea_classes


def evaluate_results(result_file,gt_file, output_file, from_classification, nr_ea_targets, initial_class1, initial_class2, write=True):
    print("parsing result file")
    parsed_ea_maps = parse_result_file(result_file, nr_ea_targets, from_classification)
    dims = ('file', 'team0ratio', 'time')
    print("parsing and concatenating GT")
    num_to_gt_dict = parse_gt_file(gt_file, dims)
    empty_gt = concat_classes_and_gt(parsed_ea_maps, num_to_gt_dict)
    print("These data points have no GT:")
    for dp in empty_gt:
        print(dp)

    print("Creating summary")
    pds = create_per_duration_summary(parsed_ea_maps, output_file, EvolvedDuoSummary, write)
    pms = create_per_map_summary(parsed_ea_maps, output_file, EvolvedDuoSummary, write)
    pdpms = create_per_duration_per_map_summary(parsed_ea_maps, output_file, EvolvedDuoSummary, write)
    return parsed_ea_maps, pds, pms, pdpms


def main():
    # result_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy/results/SIM-result.csv"
    # gt_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy/results/GT_summary-Orchestration.pkl"
    # output_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy/results/orchestration-evaluation.csv"

    # result_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy_MO/results/SIM-MOEA-best_ind-result.csv"
    # gt_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy_MO/results/GT_summary-Orchestration-MOEA.pkl"
    # output_file = os.getcwd() + "/dual_evolution/Results/Scout_Heavy_MO/results/MOEA_orchestration-evaluation.csv"

    # result_file = os.getcwd() + "/MR_Results/dual_evolution-MR/ScoutHeavy-MOEA_MR/results/summary-best_ind-result.csv"
    # gt_file = os.getcwd() + "/MR_Results/dual_evolution-MR/dual_evolution-MOEA-GT/GT_summary-dual_evolution-MOEA-GT.pkl"
    # output_file = os.getcwd() + "/MR_Results/dual_evolution-MR/ScoutHeavy-MOEA_MR/results/MOEA_orchestration-evaluation.csv"

    # result_file = os.getcwd() + "/MR_Results/dual_evolution-MR/ScoutHeavy-SOEA_MR/results/summary-result.csv"
    # gt_file = os.getcwd() + "/MR_Results/dual_evolution-MR/dual_evolution-SOEA-GT/GT_summary-dual_evolution-SOEA-GT.pkl"
    # output_file = os.getcwd() + "/MR_Results/dual_evolution-MR/ScoutHeavy-SOEA_MR/results/SOEA_orchestration-evaluation.csv"

    num = 0
    result_dir = os.getcwd() + "/MutationInitTest/"
    result_file = result_dir + "/ScoutHeavy_SOEA-{0}Mut/results/summary-result-Mut{0}.csv".format(num)
    gt_file = result_dir + "/GT_summary-Mut{0}-GT.pkl".format(num)
    output_file = result_dir + "/ScoutHeavy_SOEA-{0}Mut/results/SOEA_orchestration-Mut{0}-evaluation.csv".format(num)

    from_classification = False

    nr_ea_targets = 2
    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    parsed_ea_duo, pds, pms, pdpms = evaluate_results(result_file, gt_file, output_file, from_classification, nr_ea_targets,
                                                       initial_class1, initial_class2)
    write_output(parsed_ea_duo, output_file)


if __name__ == "__main__":
    # Running the code from ppsEvolution/
    main()
