from common.EvolvedIndividualSummary import EvolvedIndividualSummary
from character_evolution.postprocessing.EvolvedWeaponSummary import EvolvedWeaponSummary
from map_evolution.postprocessing.EvolvedMapSummary import EvolvedMapSummary, possible_tiles
from character_evolution.evolution.weapon_parameters import all_params


class EvolvedDuoSummary(EvolvedIndividualSummary):
    def __init__(self, evolved_data_list):
        initial_len = len(evolved_data_list)
        evolved_data_list = [dp for dp in evolved_data_list if dp.has_gt]
        if len(evolved_data_list) < initial_len:
            print("Ignoring Duos without GT runs...")
        super().__init__(evolved_data_list)
        self.evolved_class = EvolvedWeaponSummary(evolved_data_list)
        self.evolved_map = EvolvedMapSummary(evolved_data_list)

        # inherited values from class
        self.changed_params = self.evolved_class.changed_params
        self.dist_to_original_match = self.evolved_class.dist_to_original_match
        self.dist_to_originals = self.evolved_class.dist_to_originals

        self.tf2_classes = self.evolved_class.tf2_classes
        self.c1_tf2_dist = self.evolved_class.c1_tf2_dist
        self.c2_tf2_dist = self.evolved_class.c2_tf2_dist
        self.threshold = self.evolved_class.threshold
        self.nearest_c1 = self.evolved_class.nearest_c1
        self.nearest_c2 = self.evolved_class.nearest_c2

        self.mean_chp = self.evolved_class.mean_chp
        self.mean_dtom = self.evolved_class.mean_dtom

        # inherited from map
        self.infeasible_count = self.evolved_map.infeasible_count
        self.mean_tile_count = self.evolved_map.mean_tile_count
        self.mean_tc_diff = self.evolved_map.mean_tc_diff

        self.bdi = self.evolved_map.bdi
        self.mean_bin_dist_to_initial = self.evolved_map.mean_bin_dist_to_initial

    def get_header(self):
        line = super().get_header()
        changed_params = ["C1 {0} change".format(c) for c in all_params] + \
                         ["C2 {0} change".format(c) for c in all_params]
        class_distances = ["C1 dist to {0}".format(k) for k in self.tf2_classes] + ["C2 dist to {0}".format(k) for k in self.tf2_classes]
        nearest_classes = ["C1 Nearest TF Class", "C2 Nearest TF Class"]

        map_elems = ["mean tile diff to initial"] + \
                    ["mean " + tile for tile in possible_tiles] + \
                    ["mean {0} diff".format(tile) for tile in possible_tiles]
        str_line = ",".join(nearest_classes + class_distances + changed_params + map_elems)
        header = line + "," + str_line
        return header

    def to_string(self):
        line = super().to_string()
        class_distances1 = ["{0:4f}".format(self.c1_tf2_dist[k]) for k in self.tf2_classes]
        class_distances2 = ["{0:4f}".format(self.c2_tf2_dist[k]) for k in self.tf2_classes]
        changed_params = ["{0:4f}".format(t) for t in self.mean_chp]
        nearest = [self.nearest_c1, self.nearest_c2]
        map_elems = ["{0:.1f}".format(self.mean_bin_dist_to_initial)] + \
                    ["{0:.1f}".format(self.mean_tile_count[tile]) for tile in possible_tiles] + \
                    ["{0:.1f}".format(self.mean_tc_diff[tile]) for tile in possible_tiles]
        str_line = ",".join(nearest + class_distances1 + class_distances2 + changed_params + map_elems)
        line = line + "," + str_line
        return line
