from __future__ import print_function
import random
import numpy as np
# possible_mutations = ["add_pickup", "move_pickup", "remove_pickup", "grow_cell", "erode_cell", "place_stairs", "place_block"]
possible_mutations = ["move_pickup", "grow_cell", "erode_cell", "place_stairs", "place_block"]
overall_mutations = ["place_block", "place_stairs"]
DEBUG = False


def mutate(dual_ind, mutprob=0.1):
    gene_mut_prob = mutprob

    occur = np.random.uniform()
    if occur < 0.5:
        return gene_prob_map_mutation(dual_ind, gene_mut_prob)
    else:
        return class_mutation(dual_ind, gene_mut_prob)


# probabilisic mutation per gene
def gene_prob_map_mutation(dual_ind, mut_prob=0.1):
    applied = False
    cells = [col for row in dual_ind[1].sketch for col in row]
    for cell in cells:
        occur = np.random.uniform()
        if occur < mut_prob:
            # if mutation occurs, try a random mutation on a gene
            random.shuffle(possible_mutations)  # note that we are shuffling the global possible mutations!
            count = 0
            while not applied and count < len(possible_mutations):
                rand_mut = possible_mutations[count]
                applied = dual_ind[1].apply_op_on_cell(cell, rand_mut, debug=DEBUG)
                count += 1
            # print(rand_mut)
    dual_ind[1].analyze_map(fix=True)
    dual_ind[1].try_fix_map()
    return dual_ind,


def class_mutation(dual_ind, mu=0.0, sigma=0.1, mutprob=0.1):
    min_val = 0
    max_val = 1
    two_class_ind = dual_ind[0]
    for gene in range(len(two_class_ind)):
        # maybe perform mutation
        if gene == 7 or gene == 15:
            if np.random.rand() <= mutprob:
                # sample from 0,1,2, not allowing current gene.
                new_val = np.random.randint(0, 3)
                while new_val == two_class_ind[gene]:
                    new_val = np.random.randint(0, 3)
                two_class_ind[gene] = new_val
        else:
            if np.random.rand() <= mutprob:
                two_class_ind[gene] += np.random.normal(mu, sigma)
            # check whether the value is still within bounds
            if two_class_ind[gene] > max_val:
                two_class_ind[gene] = max_val
            elif two_class_ind[gene] < min_val:
                two_class_ind[gene] = min_val
    return dual_ind,

