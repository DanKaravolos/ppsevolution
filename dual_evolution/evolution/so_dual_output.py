from character_evolution.evolution.weapon_parameters import convert_weapon_back, pretty_string, all_params
from common.common import list_to_string
from map_evolution.visualization.map_to_image import image_from_asci_map


def write_results(args, output_file, best_per_generation, final_pop, log, last_best, final_best_predictions, run=None, write_header=True):
    target_keys = sorted(final_best_predictions.keys())
    ind_name = output_file + "-{0}-run_{1}".format(args.map_name, run)
    csv_ind = ind_name.replace("results/", "results/best_ind/") + ".csv"
    png_ind = ind_name.replace("results/", "results/pngs/") + ".png"

    with open(output_file + "-result.csv", "a") as out:
        header = ["initial map", "match", "fitness", *target_keys, "is_invalid", "nr_generations", "ind_name", "ind_map"]
        ind_params = all_params * 4
        header.extend(ind_params)

        class_str = list_to_string(last_best[0])
        class_orig_str = list_to_string(convert_weapon_back(last_best[0]))
        last_best[1].visualize_bases()
        map_str = last_best[1].to_string(inline=True)
        is_invalid = last_best[1].invalid
        preds = ["{:.4f}".format(final_best_predictions[t]) for t in target_keys]

        out_items = [args.map_name, args.match, last_best.fitness.values[0], *preds, is_invalid, len(log),
                     ind_name.rsplit("/", 1)[1], map_str, class_str, class_orig_str]
        if run is not None:
            header.append("Run")
            out_items.append(run)

        if "maximize" not in args.fitness:
            targets = ["target {0}: {1}".format(t, args.target_values[t]) for t in target_keys]
            header.extend(targets)
        else:
            header.extend(["maximize {0}".format(t) for t in target_keys])

        if write_header:
            header_str = ",".join(header)
            out.write(header_str + "\n")
        str_values = ",".join([str(v) for v in out_items]) + "\n"
        out.write(str_values)

    # save game input for GT runs
    with open(args.game_input_file, 'a') as out:
        out.write(ind_name + ".csv," + class_str + "\n")

    # save fitness progression for fitness graph
    write_fitness_history(log, best_per_generation, args.fitlog_file)

    # save final best individual for GT runs and visual inspection
    write_map_individual(last_best, csv_ind)
    image_from_asci_map(last_best[1].to_matrix(), show_now=False, save=True, target_path=png_ind)


def write_map_individual(deap_ind, target_file):
    str_ind = deap_ind[1].to_string()
    with open(target_file, 'w')as out:
        out.write(str_ind)


def write_fitness_history(logbook, best_per_generation, target_file):
    with open(target_file, 'w') as out:
        out.write("gen,min,max,avg,std\n")

        for line in logbook:
            values = [line["gen"], line["min"], line["max"], line["avg"], line["std"]]
            gen_string = list_to_string(values)
            out.write(gen_string + "\n")

