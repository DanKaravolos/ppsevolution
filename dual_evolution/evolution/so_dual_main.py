from __future__ import print_function

import gc
import time
import numpy as np
from deap import algorithms, base, creator, tools
from dual_evolution.evolution.crossover import crossover
import dual_evolution.evolution.mutation as mutation
import dual_evolution.evolution.init_and_select as init
from dual_evolution.evolution.so_dual_output import write_results
from common.selection import elite_tour_selection, elite_roulette_selection
from common.common import define_fitness_weight
from common.common import save_initial_classes, save_initial_maps, save_initial_dual_pop
import common.fitness_functions as ff
from warnings import warn

args = None
fit_functions = None

BETWEEN_EPOCHS_EPSILON = 1e-5
CLOSE_TO_ZERO_EPSILON = 1e-5
MAX_EPOCHS_WITHOUT_PROGRESS = 10
warn("Note: MAX_EPOCHS_WITHOUT_PROGRESS = {0}".format(MAX_EPOCHS_WITHOUT_PROGRESS))


def setup_ea():
    weight = define_fitness_weight(args.fitness)
    print("weight: {0}".format(weight))
    creator.create("FitnessMax", base.Fitness, weights=(weight,))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    toolbox = base.Toolbox()

    # Structure initializers
    classes = args.match.split("_")
    print("Initializing from {0} vs {1}".format(classes[0], classes[1]))
    init.initial_match_was_added = False
    toolbox.register("init_individual", init.init_ind_from_existing, creator.Individual, args.initial_map, args.match,
                     args.initial_mutations)
    toolbox.register("population", tools.initRepeat, list, toolbox.init_individual)
    toolbox.register("evaluate", fit_functions.evaluate)
    toolbox.register("mate", crossover)
    toolbox.register("mutate", mutation.mutate, mutprob=args.mut_gene_prob)

    n_elites = int(args.elites * args.pop_size)
    if args.selection == "tournament":
        toolbox.register("select", elite_tour_selection, tournsize=args.tourn_size, n_elites=n_elites)
    else:
        toolbox.register("select", elite_roulette_selection, n_elites=n_elites)

    print("Creating initial population...")
    population = toolbox.population(n=args.pop_size)
    return toolbox, population


def do_evolution(population, toolbox, cxpb=0, mutpb=1, ngen=1, stats=None, halloffame=None, verbose=True):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    ind_to_reevaluate = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
    for ind, fit in zip(ind_to_reevaluate, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(ind_to_reevaluate), **record)
    if verbose:
        print(logbook.stream)

    best_per_generation = [tools.selBest(population, 1)]
    # Begin the generational process
    prev_best_fitness = -1
    epochs_without_progress = 0

    for gen in range(1, ngen):
        # Select the next generation individuals
        elites, plebs = toolbox.select(population, len(population))
        if args.elites > 0:
            best_per_generation.append(fit_functions.get_best_individual(elites, check_validity=True))
        else:
            best_per_generation.append(fit_functions.get_best_individual(elites, check_validity=True))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(plebs, toolbox, cxpb, mutpb)
        offspring.extend(elites)

        # Evaluate the individuals with an invalid fitness
        ind_to_reevaluate = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
        for ind, fit in zip(ind_to_reevaluate, fitnesses):
            ind.fitness.values = fit

        # print info about infeasibility:
        nr_infeasibles = [ind[1].invalid for ind in offspring].count(True)
        print("nr of infeasible solutions in offspring: ", nr_infeasibles)

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(ind_to_reevaluate), **record)
        if verbose:
            print(logbook.stream)

        # compute stop conditions
        best_fitness = logbook[-1]["max"] if "maximize" in args.fitness else logbook[-1]["min"]
        if best_fitness < CLOSE_TO_ZERO_EPSILON:
            break
        if abs(best_fitness - prev_best_fitness) < BETWEEN_EPOCHS_EPSILON:
            epochs_without_progress += 1
        else:
            epochs_without_progress = 0
        if epochs_without_progress == MAX_EPOCHS_WITHOUT_PROGRESS:
            break
        prev_best_fitness = best_fitness

    best_per_generation.append(tools.selBest(population, 1))
    print("population size: ", len(population))
    return population, logbook, best_per_generation


def evolution(toolbox, population):
    # random.seed(64)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    if args.hall_size > 0:
        hof = tools.HallOfFame(args.hall_size)  # this stores the best individual EVER. across all generations
    else:
        hof = None
    pop, log, best_per_gen = do_evolution(population, toolbox, cxpb=args.cross_prob, mutpb=args.mut_individual_prob,
                                          ngen=args.generations, stats=stats, halloffame=hof, verbose=True)
    return pop, log, hof, best_per_gen


def init_parameters(run_args):
    global args
    global fit_functions
    args = run_args
    print("loading network and data ...")
    fit_functions = ff.FitnessFunction("dual", args.fitness, args.target_values)
    fit_functions.prepare_nn_and_data(args.models)
    print("Confirming fitness: {0}".format(fit_functions.fitness))


def main(run_args):
    print("Setting up EA ...")
    init_parameters(run_args)
    # print("Result file: ", args.result_file)
    print("Running EA ...")
    write_header = True
    for run in list(range(args.runs)):
        print("run ", run)
        t0 = time.time()
        output_file = args.result_file
        tb, pop = setup_ea()
        # save initial population to demonstrate diversity
        save_initial_classes(args, pop, run, is_dual=True)
        save_initial_maps(args, pop, run, is_dual=True)
        save_initial_dual_pop(args, pop, run)

        final_pop, log, hall_of_fame, best_per_generation = evolution(tb, pop)  # difference between main and fire_main
        last_best = best_per_generation[-1][0]
        print("Fitness function: {0}".format(fit_functions.fitness))

        print("Best individual evaluation:")
        # str_predictions = fit_functions.nn_str_prediction(last_best)
        predictions = fit_functions.nn_evaluate_individual(last_best)
        pred_list = []
        for target in sorted(fit_functions.target_values.keys()):
            pred = predictions[target]
            pred_list.append(pred)
            t = fit_functions.target_values[target]
            print("Target {0}: desired val {1}, prediction: {2}".format(target, t, pred))
        fit = last_best.fitness.values[0]
        print("Best individual fitness: ", fit)
        print("Best individual is infeasible? ", last_best[1].invalid)
        nr_infeasibles = [ind[1].invalid for ind in final_pop].count(True)
        print("nr of infeasible solutions in final pop: ", nr_infeasibles)

        # do something with final_population, hall of fame and log.
        write_results(args, output_file, best_per_generation, final_pop, log, last_best, predictions, run=run, write_header=write_header)
        write_header = False

        t_diff = time.time() - t0
        print("Time taken: {0:.2f} min".format(t_diff / 60))
        print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        # cleaning up...
        tb = None
        pop = None
        gc.collect()

if __name__ == "__main__":
    my_run_args = init.parse_args()
    main(my_run_args)
