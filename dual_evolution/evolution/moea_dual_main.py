from __future__ import print_function
import time
import gc
import numpy as np
from deap import algorithms, base, creator, tools
import dual_evolution.evolution.mutation as mutation
from dual_evolution.evolution.crossover import crossover
import dual_evolution.evolution.init_and_select as init
from common.common import is_equal_dual, define_fitness_weight
from common.common import save_initial_classes, save_initial_maps, save_initial_dual_pop
import common.fitness_functions as ff

from dual_evolution.evolution.moea_dual_output import process_multi_results
import warnings
args = None
fit_functions = None

# EPSILON = 0.0001
# MAX_EPOCHS_WITHOUT_PROGRESS = 20


def setup_moea():
    print("First fitness:")
    w1 = define_fitness_weight(args.fitness)
    weights = [w1] * len(fit_functions.target_values.keys())
    print("weights: {0}".format(weights))
    creator.create("FitnessMax", base.Fitness, weights=weights)
    creator.create("Individual", list, fitness=creator.FitnessMax)

    # Structure initializers
    toolbox = base.Toolbox()
    classes = args.match.split("_")
    print("Initializing from {0} vs {1}".format(classes[0], classes[1]))
    init.initial_match_was_added = False
    toolbox.register("init_individual", init.init_ind_from_existing, creator.Individual, args.initial_map, args.match,
                     args.initial_mutations)
    toolbox.register("population", tools.initRepeat, list, toolbox.init_individual)
    toolbox.register("evaluate", fit_functions.evaluate)
    toolbox.register("mate", crossover)
    toolbox.register("mutate", mutation.mutate, mutprob=args.mut_gene_prob)
    toolbox.register("select", tools.selNSGA2)

    print("Creating initial population...")
    population = toolbox.population(n=args.pop_size)
    return toolbox, population


def do_mo_evolution(population, toolbox, cxpb=0, mutpb=1, ngen=1, stats=None, halloffame=None, verbose=True):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    ind_to_reevaluate = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
    for ind, fit in zip(ind_to_reevaluate, fitnesses):
        ind.fitness.values = fit

    # This is just to assign the crowding distance to the individuals
    # no actual selection is done
    population = toolbox.select(population, len(population))

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(ind_to_reevaluate), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    # prev_best_fitness = -1
    # epochs_without_progress = 0
    # tmp_pareto = tools.ParetoFront()
    for gen in range(1, ngen):
        # Select the next generation individuals
        offspring = tools.selTournamentDCD(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
        # Evaluate the individuals with an invalid fitness
        ind_to_reevaluate = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
        for ind, fit in zip(ind_to_reevaluate, fitnesses):
            ind.fitness.values = fit

        # print info about infeasibility:
        nr_infeasibles = [ind[1].invalid for ind in offspring].count(True)
        print("nr of infeasible solutions in offspring: ", nr_infeasibles)

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population = toolbox.select(population + offspring, len(population))

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(ind_to_reevaluate), **record)
        if verbose:
            print(logbook.stream)

        # tmp_pareto.update(population)
        # print("Size of tmp pareto front. keys {0}, items {1}".format(len(tmp_pareto.keys), len(tmp_pareto.items)))

        # # compute stop conditions
        # best_fitness = logbook[-1]["max"] if "maximize" in args.fitness or "target" in args.fitness else logbook[-1]["min"]
        # if best_fitness < EPSILON:
        #     print("Stopping EA: fitness < {0}".format(EPSILON))
        #     break
        # if abs(best_fitness - prev_best_fitness) < EPSILON:
        #     epochs_without_progress += 1
        # else:
        #     epochs_without_progress = 0
        # if epochs_without_progress == MAX_EPOCHS_WITHOUT_PROGRESS:
        #     print("Stopping EA: fitness has not improved for {0} generations".format(MAX_EPOCHS_WITHOUT_PROGRESS))
        #     break
    print("population size: ", len(population))
    return population, logbook


def evolution(toolbox, population):
    if len(fit_functions.target_values.keys()) > 2:
        warnings.warn("Stats are only showing two objectives.")
    # keys = sorted(fit_functions.target_values.keys())
    # random.seed(64)
    stats1 = tools.Statistics(key=lambda ind: ind.fitness.values[0])

    stats1.register("avg", np.mean)
    stats1.register("std", np.std)
    stats1.register("min", np.min)
    stats1.register("max", np.max)

    stats2 = tools.Statistics(key=lambda ind: ind.fitness.values[1])

    stats2.register("avg", np.mean)
    stats2.register("std", np.std)
    stats2.register("min", np.min)
    stats2.register("max", np.max)

    m_stats = tools.MultiStatistics(obj1=stats1, obj2=stats2)
    pop, log = do_mo_evolution(population, toolbox, cxpb=args.cross_prob,
                                  mutpb=args.mut_individual_prob, ngen=args.generations, stats=m_stats,
                                  halloffame=None, verbose=True)
    return pop, log


def init_parameters(run_args):
    global args
    global fit_functions
    args = run_args
    print("loading network and data ...")
    fit_functions = ff.FitnessFunction("dual", args.fitness, args.target_values)
    fit_functions.prepare_nn_and_data(args.models)
    print("Confirming fitness: {0}".format(fit_functions.fitness))


def main(run_args):
    init_parameters(run_args)
    print("Running EA ...")
    print("Setting up EA ...")

    print("Result file: ", args.result_file)
    for run in list(range(args.runs)):
        print("run ", run)
        t0 = time.time()
        # output_file = args.result_file + "-" + "-".join(["{0}_{1:.2f}".format(k, v) for k, v in sorted(fit_functions.target_values.items())]) #  + "-run_{0}".format(run)
        tb, pop = setup_moea()

        # save initial population to demonstrate diversity
        save_initial_classes(args, pop, run, is_dual=True)
        save_initial_maps(args, pop, run, is_dual=True)
        save_initial_dual_pop(args, pop, run)

        final_pop, log = evolution(tb, pop)  # difference between main and fire_main

        warnings.warn("ParetoFront does not compute a good pareto front. Sometimes still contains dominated solutions")
        pareto = tools.ParetoFront(similar=is_equal_dual)
        pareto.update(final_pop)
        print("Size of pareto front. keys {0}, items {1}".format(len(pareto.keys), len(pareto.items)))

        # do something with final_population, hall of fame and log.
        process_multi_results(log, pareto, final_pop, fit_functions.nn_str_prediction, args, run, args.result_file)
        t1 = time.time()
        print("Run time: {0} min".format((t1-t0) / 60.0))
        print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        # cleaning up...
        tb = None
        pop = None
        gc.collect()


if __name__ == "__main__":
    my_run_args = init.parse_args()
    main(my_run_args)
