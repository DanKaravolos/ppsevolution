from __future__ import print_function
from copy import deepcopy


# Cross-over will swap maps
def crossover(ind1, ind2):
    child1 = deepcopy(ind1)
    child2 = deepcopy(ind2)

    t1_map = child1[1]
    child1[1] = child2[1]
    child2[1] = t1_map

    return child1, child2
