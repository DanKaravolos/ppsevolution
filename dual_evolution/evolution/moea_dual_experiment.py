import os
from time import time
from datetime import datetime

from dual_evolution.evolution.moea_dual_main import main
from dual_evolution.evolution.init_and_select import parse_args
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # use a specific GPU
t0 = time()

mutations = [0.2]
crossover = [0.8]
for m in mutations:
    for c in crossover:
        # maps = ["pcg_workshop/Level_1_Run_0_Map.csv"]
        maps = ["custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3,4,5,6,7,8,9,10]]
        # maps = ["custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3,4,5]]
        # maps = ["custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [6,7,8,9,10]]

        target_settings = ["target_values_regression_short.txt", "target_values_regression_med.txt", "target_values_regression_long.txt"]
        # target_settings = ["target_values_regression_med.txt", "target_values_regression_long.txt"]
        # target_settings = ["target_values_regression_long.txt"]
        # target_settings = ["target_values_regression_med.txt"]
        # target_settings = ["target_values_regression_short.txt"]
        model_settings = ["regression_model_settings.txt"] * len(target_settings)

        for model_set, target_set in zip(model_settings, target_settings):
            for initial_map in maps:
                args = parse_args(result_folder="ScoutHeavy_MOEA_MR", result_file="MO_MR", fitness="multi_squared_error",
                                  cross_prob=c, mut_prob=m, model_settings=model_set, target_settings=target_set, initial_map=initial_map)
                # , initial_match="Scout_Heavy", fitness="squared_error"
                args.runs = 10
                print("\nCurrent time:", datetime.now().strftime("%Y-%m-%d %H:%M"))
                print("Starting run with these arguments:")
                print("Using GPU: ", os.environ["CUDA_VISIBLE_DEVICES"])
                for key, value in iter(args.__dict__.items()):
                    print("{0}: {1}".format(key, value))
                main(args)

t1 = time()
t_diff = (t1 - t0)
print("Total time taken: {0} min, which is {1} hours.".format(t_diff / 60.0, t_diff / 3600.0))

