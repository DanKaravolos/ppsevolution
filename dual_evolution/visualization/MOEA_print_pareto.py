import os
from map_evolution.visualization.MOEA_print_pareto import multi_main, single_main


if __name__ == "__main__":
    par_file = os.getcwd() + "/map_evolution/moea_test/moea_test-P_20-gen_100-mut_0.10-crs_0.00-fitnesses.csv"
    par_dir = os.getcwd() + "/dual_evolution/moea_test/"
    # single_main(par_file, use_game_dims=False, show=False)
    multi_main(par_dir, use_game_dims=True)  # game_dims = normalized duration and score, not game_dims = fitness