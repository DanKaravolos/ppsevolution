from map_evolution.evolution.so_map_evolution_main import main
from map_evolution.evolution.init_and_select import parse_args, parse_match_settings
import os
import time
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # use a specific GPU
print("Start time:", time.strftime("%Y-%m-%d %H:%M"))
print("Using GPU ", os.environ["CUDA_VISIBLE_DEVICES"])
t0 = time.time()
fake_args = parse_args()
# match_file = os.getcwd() + "/data/5_class_matches_1.csv"
# match_dict = parse_match_settings(match_file)
match_dict = fake_args.matches

maps = ["/data/maps/pcg_workshop/Level_{0}_Run_0_Map.csv".format(i + 1) for i in range(2, 3)]
# maps = ["/data/maps/pcg_workshop/Level_{0}_Run_0_Map.csv".format(i + 1) for i in range(5, 10)]
# maps = ["/data/maps/custom_maps/Custom_{0}_Run_0_Map.csv".format(i + 1) for i in range(0, 5)]
# maps = ["/data/maps/custom_maps/Custom_{0}_Run_0_Map.csv".format(i + 1) for i in range(5, 10)]
# maps = ["/data/maps/Level_1_Run_0_Map.csv"] # "/data/Level_2_Run_0_Map.csv", "/data/Level_3_Run_0_Map.csv", "/data/Level_4_Run_0_Map.csv"
# maps = ["/data/Level_3_Run_0_Map.csv"]
# maps = ["/data/map_antonios_valid_empty_bases.csv", "/data/Level_3_Run_0_Map.csv"]

all_matches = True
if all_matches:
    # all matches in file
    matches = [match_dict[m] for m in sorted(match_dict)]  # just to make sure that we run experiments in the same order
    match_names = [m for m in sorted(match_dict)]
else:
    # specific match:
    match_names = ["Scout_Scout"]
    matches = [match_dict[m] for m in match_names]  # just to make sure that we run experiments in the same order


for map_index in range(len(maps)):
    num = maps[map_index].rsplit("_", 4)[1]
    map_tag = "Generated_{0}".format(int(num))  # map_index + 1
    # map_tag = "Custom_{0}".format(int(num))

    args = parse_args(result_folder="{0}_1runs".format(map_tag), result_file=map_tag)
    args.runs = 1
    # args = parse_args(result_folder="map_evolution/Levels_4_19runs", result_file="Level_{0}".format(map_index + 1))
    init_map = maps[map_index]
    for match in range(17, len(matches)):
        args.matches = matches[match]
        args.match_name = match_names[match]
        args.initial_map = os.getcwd() + init_map
        # times = [0.11, 0.33, 1]  # 200, 300 and 600 seconds
        times = [0.085, 0.315, 1]  # 200, 300 and 600 seconds
        for t in times:
            args.target_time = t
            main(args)
t1 = time.time()

print("Total time taken: {0} min".format((t1 - t0) / 60.0))
