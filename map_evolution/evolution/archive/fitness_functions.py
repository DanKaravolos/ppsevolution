from __future__ import division
import numpy as np
from keras.models import load_model
import os
from common.parse_csv_maps import parse_array

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # use a specific GPU

# Internal variables
neural_net = None
pickup_neighborhood = 1.0
print("pickup neighborhood = 1")
matches = np.zeros((1, 16))
fitness = "target_score"
secondary = ""
penalty = ""
target_score = 0.5
target_time = 0.31  # = 300 MEAN TIME: 325 (or 0.37)


# WRAPPER FUNCTION
def evaluate(individual):
    if secondary:
        if individual[0].invalid:
            return max(individual[0].unreachable_tiles, 1), max(individual[0].unreachable_tiles, 1)
        np_individual = parse_array(individual[0].to_matrix(), pickup_neighborhood)
        return multi_evaluate(np_individual)
    else:
        if individual[0].invalid:
            return max(individual[0].unreachable_tiles, 1),
        np_individual = parse_array(individual[0].to_matrix(), pickup_neighborhood)
        return compute_fitness(np_individual, fitness),


def multi_evaluate(individual):
    obj1 = compute_fitness(individual, fitness)
    obj2 = compute_fitness(individual, secondary)
    return obj1, obj2


def compute_fitness(individual, fit_func):
    if "maximize" in fit_func or "minimize" in fit_func:
        return regression_maximize(individual, fit_func)
    elif "target" in fit_func:
        return regression_target(individual, fit_func)
    elif "aggregated" in fit_func:
        return regression_aggregated(individual, fit_func)
    elif "squared_error" in fit_func:
        return regression_mse(individual, fit_func)
    else:
        print("What is your fitness function???")
        return -999


# initialization functions   ############################################################
def prepare_nn_and_data(nn, np_weapons):
    # set global neural network
    global neural_net, matches
    neural_net = load_model(nn)
    neural_net.compile(optimizer='adam', loss='mse')  # necessary for usage
    neural_net.summary()
    # connect maps from file with weapon params.
    matches = np_weapons


# UTILITY FUNCTIONS   ############################################################

def get_weight(fit_func):
    if "maximize" in fit_func or "target" in fit_func:
        return 1
    if "minimize" in fit_func or "aggregated" in fit_func:
        return -1


# def get_best_individual(population):
#     best = max(population, key=lambda x: x.fitness)
#     return best


def get_best_individual(population, check_validity=False, return_predictions=False):
    if check_validity:
        population = [ind for ind in population if not ind[0].invalid]
    best_valid = max(population, key=lambda ind: ind.fitness)  # or: tools.selBest()
    if return_predictions:
        pred = nn_evaluate_individual(best_valid)
        means = np.mean(pred, axis=0)
        return best_valid, means
    else:
        return best_valid


def nn_evaluate_individual(deap_individual):
    np_individual = parse_array(deap_individual[0].to_matrix(), pickup_neighborhood)
    input_maps = np.asarray([np_individual] * matches.shape[0])
    predictions = neural_net.predict([matches, input_maps])
    return predictions


def denormalize_time(time):
    time_min = 150.0  # 163.0
    time_max = 600.0
    orig = time * (time_max - time_min) + time_min
    return orig


def normalize_time(orig):
    time_min = 150.0  # 163.0
    time_max = 600.0
    time = (orig - time_min) / (time_max - time_min)
    return time


# FITNESS FUNCTIONS   ############################################################

def regression_maximize(individual, fit_func):
    input_maps = np.asarray([individual] * matches.shape[0])
    predictions = neural_net.predict([matches, input_maps])
    if "time" in fit_func:
        times = predictions[:, 0]
        return np.mean(times)
    elif "score" in fit_func:
        scores = predictions[:, 1]
        return np.mean(scores)
    else:
        print("ERROR: fitness function undefined.")
        return -1


def regression_target(individual, fit_func):
    input_maps = np.asarray([individual] * matches.shape[0])
    predictions = neural_net.predict([matches, input_maps])
    if "time" in fit_func:
        target = target_time
        pred = predictions[:, 0]
    elif "score" in fit_func:
        target = target_score
        pred = predictions[:, 1]
    else:
        print("ERROR: fitness function undefined.")
        return -1
    deviations = np.absolute(pred - target)  # absolute difference w target. need to minimize
    return np.mean(deviations)


def regression_aggregated(individual, fit_func):
    input_maps = np.asarray([individual] * matches.shape[0])
    predictions = neural_net.predict([matches, input_maps])
    agg = [abs(p[0]-target_time)/target_time + abs(p[1]-target_score)/target_score for p in predictions]
    return np.mean(agg)


# def regression_mse(individual, fit_func):
#     input_weapons = np.asarray([individual] * len(input_maps))
#     predictions = neural_net.predict([input_weapons, input_maps])
#     agg = [(abs(p[0]-target_time) + abs(p[1]-target_score))**2 for p in predictions]
#     return np.mean(agg)


def regression_mse(individual, fit_func):
    input_maps = np.asarray([individual] * matches.shape[0])
    predictions = neural_net.predict([matches, input_maps])

    if "time" in fit_func:
        agg = [(p[0] - target_time) ** 2 for p in predictions]
    elif "score" in fit_func:
        agg = [(p[1] - target_score) ** 2 for p in predictions]
    else:
        if type(predictions) == np.ndarray:
            # in this case the network gives a two dimensional output per input
            agg = [(p[0] - target_time) ** 2 + (p[1] - target_score) ** 2 for p in predictions]
        else:
            # in this case the network gives a list with predictions per input for each output node
            p = predictions
            agg = [(p[0][i] - target_time) ** 2 + (p[1][i] - target_score) ** 2 for i in range(len(p[0]))]
    return np.mean(agg)
