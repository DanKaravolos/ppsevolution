import argparse
import os
from map_evolution.evolution.individual.MapIndividual import MapIndividual
from map_evolution.evolution.mutation import apply_x_mutations
from common.common import parse_nn_settings, parse_targets, csv_to_array
from character_evolution.evolution.weapon_parameters import tf2_weapon_param_dict
initial_map_was_added = False


def init_match_from_tf2(match_name):
    classes = match_name.split("_")
    match = [i for i in tf2_weapon_param_dict[classes[0]]]
    match.extend([i for i in tf2_weapon_param_dict[classes[1]]])
    return match


def init_ind_from_existing(creator, original_file):
    ind_map = MapIndividual(csv_to_array(original_file), 4, 4)
    # apply_one_random_mutation(ind_map)  # initialize individual with one mutation
    deap_ind = creator([ind_map])
    return deap_ind


def init_ind_from_existing_with_mutations(creator, original_file, nr_mutations):
    global initial_map_was_added
    ind_map = MapIndividual(csv_to_array(original_file), 4, 4)
    if not initial_map_was_added or nr_mutations == 0:
        initial_map_was_added = True
    else:
        apply_x_mutations(ind_map, nr_mutations)
    deap_ind = creator([ind_map])
    return deap_ind


def parse_args(result_folder="test", result_file="test_file", cross_prob=1.0, mut_prob=0.2, fitness="squared_error",
               initial_map="maps/custom_maps/Custom_1_Run_0_Map.csv", initial_match="Scout_Heavy",
               model_settings="target_values_regression_short.txt", target_settings="regression_model_settings.txt"):

    parser = argparse.ArgumentParser(description="Do an evolutionary search for better weapon")
    parser.add_argument("--cross_prob", type=float, default=cross_prob)
    parser.add_argument("--elites", type=int, default=0.1, help="Elitism percentage")
    parser.add_argument("--hall_size", type=int, default=0)
    parser.add_argument("--nn", default="c_sizes_[8, 16]-weapon_d_sizes_[8]-d_sizes_[32]-h_act_relu-fold_3.h5")
    # parser.add_argument("--nn", default="c_sizes_[16, 32]-weapon_d_sizes_[8]-d_sizes_[128]-h_act_elu-out_act_sigmoid-tag_elu-drop_0.20-BN-Pool_max-100ep-5_200K_singular_pickups.h5")
    parser.add_argument("--match_file", default="all_5_class_matches.csv")
    parser.add_argument("--initial_map", default=initial_map)
    # parser.add_argument("--initial_map", default="map_antonios_valid_empty_bases.csv")
    parser.add_argument("--initial_map_mutations", type=int, default=4)
    parser.add_argument("--mut_gene_prob", type=float, default=mut_prob)
    parser.add_argument("--mut_individual_prob", type=float, default=1)
    parser.add_argument("--generations", type=int, default=100)
    parser.add_argument("--pop_size", type=int, default=100)
    parser.add_argument("--selection", choices=["tournament", "roulette"], default="tournament")
    parser.add_argument("--tourn_size", type=int, default=5)
    parser.add_argument("--runs", type=int, default=1)

    # fitness related stuff
    parser.add_argument("--fitness", choices=["maximize", "multi_maximize", "target", "multi_target",
                                              "minimize", "multi_minimize", "squared_error",
                                              "multi_squared_error"],
                        default=fitness)
    parser.add_argument("--nn_settings_file", default=model_settings)
    parser.add_argument("--target_file", default=target_settings,
                        help="A file that determines the target values per objective, used in target and sq.error")
    parser.add_argument("--match", default=initial_match, help="Define the initial values when initializing from TF2 class")


    # file i/o stuff
    parser.add_argument("--result_file", default=result_file)
    parser.add_argument("--folder", default=result_folder)

    args = parser.parse_args()
    args.initial_map = os.getcwd() + "/data/maps/" + args.initial_map
    args.map_name = args.initial_map.rsplit("/", 1)[1].replace("_Run_0_Map.csv", "")
    args.models = parse_nn_settings(os.getcwd() + "/map_evolution/" + args.nn_settings_file)
    args.target_values = parse_targets(os.getcwd() + "/map_evolution/" + args.target_file)

    args.result_file += "-P_{0}".format(args.pop_size)
    args.result_file += "-gen_{0}".format(args.generations)
    args.result_file += "-mut_{0:.2f}".format(args.mut_gene_prob)
    args.result_file += "-crs_{0:.2f}".format(args.cross_prob)

    storage_dir = os.getcwd() + "/map_evolution/Results/"
    if not os.path.exists(storage_dir):
        os.mkdir(storage_dir)
    if not os.path.exists(storage_dir + args.folder):
        current_dir = storage_dir
        for fol in args.folder.split("/"):
            current_dir += "/" + fol
            try:
                os.mkdir(current_dir)
            except OSError:
                continue

    if not os.path.exists(storage_dir + "{0}/results/".format(args.folder)):
        os.mkdir(storage_dir + "{0}/results/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/fitlog/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/game_input/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/pngs/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/best_ind/".format(args.folder))
        os.mkdir(storage_dir + "{0}/results/initial_pop/".format(args.folder))

    str_tar = "-".join(["{0}_{1:.2f}".format(k, v) for k, v in sorted(args.target_values.items())])
    args.result_file = storage_dir + "{0}/results/{1}{2}-{3}-{4}".format(args.folder, args.result_file,
                                                                         "-{0}".format(args.match) if args.match else "", str_tar, args.match)
    args.fitlog_file = args.result_file.replace("results", "results/fitlog") + "-{0}-fitlog.csv".format(args.map_name)
    args.game_input_file = args.result_file.replace("results", "results/game_input") + "-game_input.csv" #+ "-{0}".format(args.map_name)
    return args


if __name__ == "__main__":
    args1 = parse_args(result_file="init_and_select_test")
    # csv_ind = csv_to_array(args.initial_map)
    # map_ind = MapIndividual(csv_ind)
    print("blub")
