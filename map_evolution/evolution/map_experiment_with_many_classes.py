import os
from time import time
from datetime import datetime

from map_evolution.evolution.so_map_evolution_main import main
from map_evolution.evolution.init_and_select import parse_args
# from common.common import parse_nn_settings, parse_targets
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # use a specific GPU
t0 = time()
mutation = 0.2
crossover = 0.8

###
# TO DO: Get the right model.
# Maybe: get the right mutation and crossover.
###

classes = ["Heavy", "Pyro", "Scout", "Spy", "Sniper"]
matches = ["{0}_{1}".format(i, j) for i in classes for j in classes]

for initial_match in matches:
    # maps = ["pcg_workshop/Level_1_Run_0_Map.csv"]
    # maps = ["custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3,4,5,6,7,8,9,10]]
    # maps = ["custom_maps/Custom_{0}_Run_0_Map.csv".format(i) for i in [1,2,3]]
    maps = ["iccc/map_antonios_valid_empty_bases.csv"]
    target_settings = [
                       "target_values_OldMapGen_regression_short.txt",
                       "target_values_OldMapGen_regression_med.txt",
                       "target_values_OldMapGen_regression_long.txt"
                       ]
    model_settings = ["regression_model_settings.txt"] * len(target_settings)

    for model_set, target_set in zip(model_settings, target_settings):
        for initial_map in maps:
            args = parse_args(result_folder="MultiClass_SOEA_MR", result_file="MultiClass",
                              cross_prob=crossover, mut_prob=mutation, model_settings=model_set, target_settings=target_set,
                              initial_map=initial_map, match=initial_match)
            #  , fitness="squared_error"
            args.runs = 10
            args.pop_size = 20
            print("\nCurrent time:", datetime.now().strftime("%Y-%m-%d %H:%M"))
            print("Starting run with these arguments:")
            for key, value in iter(args.__dict__.items()):
                print("{0}: {1}".format(key, value))
            main(args)

t1 = time()
print("Total time taken: {0} min or {1} hours.".format((t1 - t0) / 60.0, (t1 - t0) / 3600.0))
