from __future__ import print_function
from collections import defaultdict
import random
from copy import deepcopy
from map_evolution.evolution.individual.Grid import Grid


class Cell(Grid):
    nr_rows = 5
    nr_columns = 5

    def __init__(self, row, col, np_array_2d):
        Grid.__init__(self, np_array_2d)
        self.coords = (row, col)
        self.name = "C({0},{1})".format(row, col)

    def count_tiles(self, tile_type):
        return len(self.tile_dict[tile_type])

    def tile_type_in_neighbors(self, tile_type, coord, moore=False):
        for n in self.get_neighbors(coord, moore):
            if tile_type in self.grid[n]:
                return True
        return False

    # returns whether a pickup can be placed and if so, where
    def can_add_pickup(self, return_where=False):
        # get all valid tiles for placement, i.e. ground and first floor that do not have any additional tile_type
        valid_coords = self.tile_dict['0'] + self.tile_dict['1']

        new_valid_coords = [c for c in valid_coords if not len(self.grid[c[0], c[1]]) > 1
                            and not self.tile_type_in_neighbors('S', c)]
        # if there are no valid tiles left, we cannot add a pickup
        if not return_where:
            return len(new_valid_coords) != 0
        else:
            return len(new_valid_coords) != 0, new_valid_coords

    def can_remove_pickup(self, tile_type):
        return len(self.tile_dict[tile_type]) > 0

    # returns whether placement of pickup was successful
    def add_pickup(self, tile_type):
        # get all valid tiles for placement, i.e. ground and first floor that do not have any additional tile_type
        is_valid, valid_coords = self.can_add_pickup(return_where=True)
        if not is_valid:
            return False

        # add tiletype to both the grid and the lookup table
        rand_coord = random.choice(valid_coords)
        self.grid[rand_coord[0], rand_coord[1]] += tile_type
        self.tile_dict[tile_type].append(rand_coord)
        return True

    # returns whether removement of pickup was successful
    def remove_pickup(self, tile_type):
        # we cannot remove something that does not exist
        if not self.can_remove_pickup(tile_type):
            return False

        pickup_coords = self.tile_dict[tile_type]
        rand_coord = random.choice(pickup_coords)
        # self.grid[rand_coord] = self.grid[rand_coord][0]  # remove second char  will remove the tiletype
        self.grid[rand_coord] = self.grid[rand_coord].replace(tile_type, "")    # alternate way of removing the tiletype
        self.tile_dict[tile_type].remove(rand_coord)
        return True

    def place_staircase(self):
        unacceptable_nbs = ["1", "2", "0S"]
        placed = False
        floor1_tiles = [t for t in self.tile_dict['1']]
        random.shuffle(floor1_tiles)
        new_stair_loc = None
        for tile in floor1_tiles:
            # for each floor 1 tile, find a neighbor that is 0 and that has 0, as neighbors
            nbrs = self.get_neighbors(tile)
            block_coords = [tile]  # + nbrs
            for nb in nbrs:
                # check if neighbor itself is acceptable
                if self.grid[nb] != "0":
                    continue
                else:
                    # check if neighbor's neighbors are acceptable
                    new_nbrs = [self.grid[nwnb] for nwnb in self.get_neighbors(nb) if nwnb not in block_coords]
                    if any([True for x in new_nbrs if x in unacceptable_nbs]) or len(new_nbrs) < 3:
                        continue
                    else:
                        new_stair_loc = nb
                        break
            # if both neighbor and its neighbors have been accepted, then the new_stair_loc is assigned.
            # so break out of the loop and make that neighbor a stair
            if new_stair_loc is not None:
                break
        if new_stair_loc is not None:
            self.grid[new_stair_loc] = "0S"
            self.tile_dict["S"].append(new_stair_loc)
            placed = True
        return placed

    def tiles_in_neighbors(self, coord, tile_types):
        nbs = self.get_neighbors(coord, moore=False)
        next_to_higher = False
        for nb in nbs:
            nb_val = self.grid[nb]
            for t in tile_types:
                if t in nb_val:
                    next_to_higher = True
                    break
        return next_to_higher

    def grow(self, tile_type):
        next_type = "1" if tile_type == "0" else "2"
        if tile_type == "0":
            higher_types = ["1", "2"]
        elif tile_type == "1":
            higher_types = ["2"]
        else:
            print("Growing of type {0} is not allowed.".format(tile_type))
            return False
        return self.convert(tile_type, next_type, higher_types)

    def erode(self, tile_type):
        next_type = "1" if tile_type == "2" else "0"
        if tile_type == "2":
            lower_types = ["1", "0"]
        elif tile_type == "1":
            lower_types = ["0"]
        else:
            print("Eroding of type {0} is not allowed.".format(tile_type))
            return False
        return self.convert(tile_type, next_type, lower_types)

        # check for stairs in neighborhood as well. (and move it to current cell)

    def convert(self, tile_type, new_type, neighbor_types):
        new_grid = deepcopy(self.grid)
        new_dict = deepcopy(self.tile_dict)

        tiles = self.tile_dict[tile_type]
        success = False
        if len(tiles) == 0:
            # print("No {0} tiles found to convert to {1}".format(tile_type, new_type))
            return False
        num_neighbors = sum([len(self.tile_dict[nt]) for nt in neighbor_types])
        if num_neighbors == 0:
            # print("No {0} neighbors found to convert from {1} to {2}".format(neighbor_types, tile_type, new_type))
            return False

        num_converted = 0
        for coord in tiles:
            next_to_nb_type = self.tiles_in_neighbors(coord, neighbor_types)
            next_to_stairs = self.tiles_in_neighbors(coord, ["0S"])
            # print("is {0} next to {1}? ".format(coord, new_type), next_to_lower)

            if next_to_nb_type and not next_to_stairs:
                if new_type == "2":
                    new_grid[coord] = "2"  # SHOULD I DO SOMETHING ABOUT POWERUPS? Currently removing it.
                if "S" in self.grid[coord]:
                    self.move_stairs(coord, new_grid)
                    new_grid[coord] = "1"
                else:
                    new_grid[coord] = new_grid[coord].replace(tile_type, new_type)
                new_dict[tile_type].remove(coord)
                new_dict[new_type].append(coord)
                success = True
                num_converted += 1
        self.grid = new_grid
        self.tile_dict = new_dict
        return success and num_converted > 0

    def move_stairs(self, coord, grid):
        north = (coord[0] + 1, coord[1])
        east = (coord[0], coord[1] + 1)
        south = (coord[0] - 1, coord[1])
        west = (coord[0], coord[1] - 1)

        def try_move(c1, c2):
            if self.contains_coord(c1) and self.contains_coord(c2):
                for t in ["1", "2"]:
                    if t in grid[c1] and grid[c2] == "0":  # only move south if it is empty
                        grid[c2] = "0S"
                        return True
                    elif t in grid[c2] and grid[c1] == "0":
                        grid[c1] = "0S"
                        return True
            return False
        moved = try_move(north, south)
        if not moved:
            moved = try_move(east, west)
        return moved

    def place_block(self, ground_type):
        success = False
        if ground_type == "0":
            tiles = self.tile_dict["0"]
            for t in tiles:
                success = self.place_block_on_ground(t)
                if success:
                    break
        elif ground_type == "1":
            tiles = self.tile_dict["1"]
            for t in tiles:
                success = self.dig_hole_in_floor(t)
                if success:
                    break
        else:
            print("Cannot place block on type ", ground_type)
        return success

    def place_block_on_ground(self, center):
        nbrs = self.get_neighbors(center, moore=True)
        block_coords = nbrs + [center]
        around_block = []
        no_stairs = True
        for nb in nbrs:
            if 'S' in self.grid[nb]:
                no_stairs = False
                break
            else:
                new_nbrs = [nwb for nwb in self.get_neighbors(nb) if nwb not in block_coords]
                for nb_nb in new_nbrs:
                    if 'S' in self.grid[nb_nb]:
                        no_stairs = False
                        break
                around_block.extend(new_nbrs)

        if no_stairs:
            # potential stairs are 0 and have ground floors in their neighbors
            potential_stairs = [s for s in around_block if self.grid[s] == "0" and
                                all([True if "0" in nb else False for nb in self.get_neighbors(s)])]
            if potential_stairs:
                stair = random.choice(potential_stairs)
                self.grid[stair] = "0S"
                self.tile_dict["S"].append(stair)
                # place block
                for tile in block_coords:
                    self.grid[tile] = self.grid[tile].replace("0", "1")
                    self.tile_dict["0"].remove(tile)
                    self.tile_dict["1"].append(tile)
                    return True
        return False

    def dig_hole_in_floor(self, center):
        # print("dig center: ", center)
        nbrs = self.get_neighbors(center, moore=True)
        block_coords = nbrs + [center]
        around_block = []
        first_floor = True
        for nb in nbrs:
            if '1' not in self.grid[nb]:
                first_floor = False
                break
            else:
                new_nbrs = [nwb for nwb in self.get_neighbors(nb) if nwb not in block_coords]
                for nb_nb in new_nbrs:
                    if '1' not in self.grid[nb_nb]:
                        first_floor = False
                        break
                around_block.extend(new_nbrs)

        if first_floor:
            # place block
            for tile in block_coords:
                self.grid[tile] = self.grid[tile].replace("1", "0")
                self.tile_dict["1"].remove(tile)
                self.tile_dict["0"].append(tile)

            # # potential stairs are 0 and within the hole
            stair = random.choice(block_coords)
            self.grid[stair] = "0S"
            self.tile_dict["S"].append(stair)
            return True
        else:
            return False


def cell_from_grid(np_grid, nr_rows=5, nr_cols=5):
    max_rows, max_cols = np_grid.shape
    if max_rows < nr_rows or max_cols < nr_cols:
        print("grid size smaller than cell size")
        return None

    top_left_r = random.randint(0, max_rows - nr_rows)
    top_left_c = random.randint(0, max_cols - nr_cols)

    mini_grid = np_grid[top_left_r:top_left_r+nr_rows, top_left_c: top_left_c + nr_cols]
    new_cell = Cell(top_left_r, top_left_c, mini_grid)
    return new_cell


def apply_cell_to_grid(np_grid, cell):
    mini_grid = cell.grid
    tlr = cell.coords[0]
    tlc = cell.coords[1]
    cell_rows, cell_cols = mini_grid.shape
    np_grid[tlr:tlr+cell_rows, tlc:tlc+cell_cols] = mini_grid
    return np_grid
