from __future__ import print_function
from map_evolution.evolution.individual.MapIndividual import MapIndividual


def add_remove_stress_test(map_ind):
    # # can_add_pickup test
    # place_test = map_ind.sketch[0][0].can_add_pickup()
    # print("can place pickup at 0,0? ", place_test)
    map_ind.move_pickup('A')
    map_ind.move_pickup('H')
    # map_ind.add_pickup('D')
    # add & remove test:
    rm = 0
    while map_ind.remove_pickup('H'):
        rm += 1
        print("removing HP ", rm)
    while map_ind.remove_pickup('A'):
        rm += 1
    while map_ind.remove_pickup('D'):
        rm += 1
    map_ind.print_stats()

    add = 0
    while map_ind.add_pickup('H'):
        add += 1
        # print("adding HP ", add)
    map_ind.print_stats()
    ground_tiles = map_ind.count_tiles('0')
    print(" ground tiles: ", ground_tiles)
    ft = map_ind.count_tiles('1')
    print(" 1st floor tiles: ", ft)


def regular_test(map_ind):
    map_ind.print_map()
    map_ind.move_pickup('A')
    map_ind.move_pickup('H')
    map_ind.move_pickup('D')
    map_ind.move_pickup('H')
    # map_ind.add_pickup('A')
    map_ind.remove_pickup('H')
    map_ind.print_map()


def grow_erode_test00(map_ind):
    cell0 = map_ind.sketch[0][0]
    cell0.grow("0")
    map_ind.print_map()
    cell0.erode("1")
    map_ind.print_map()
    cell0.grow("0")
    map_ind.print_map()

    cell0.erode("2")
    map_ind.print_map()
    cell0.erode("2")
    map_ind.print_map()
    cell0.erode("0")


def grow_erode_test13(map_ind):
    map_ind.print_map()
    cell1 = map_ind.sketch[1][3]
    cell1.grow("0")
    map_ind.print_map()
    cell1.grow("1")
    map_ind.print_map()
    cell1.erode("2")
    map_ind.print_map()
    cell1.grow("0")
    map_ind.print_map()


def test_fix_function(map_ind):
    map_ind.print_map()
    map_ind.analyze_map()


def print_analysis(map_ind):
    map_ind.analyze_map()
    print("Map is invalid? {0}".format(map_ind.invalid))
    print("Nr unreachable tiles:", map_ind.unreachable_tiles)
    if map_ind.invalid:
        map_ind.analyze_map(fix=True)
    image_from_asci_map(map_ind.to_matrix(), show_now=True)


def test_mutations(map_ind):
    # pm = ["add_pickup", "move_pickup", "remove_pickup", "grow_cell", "erode_cell", "place_stairs", "place_block"]
    sm = ["grow_cell", "erode_cell", "place_block", "place_stairs"]
    print_analysis(map_ind)
    for m in sm:
        map_ind.apply_operation(m, debug=True, cannot_use_bases=True)
        print_analysis(map_ind)
    for m in sm:
        map_ind.apply_operation(m, debug=True, cannot_use_bases=True)
        print_analysis(map_ind)
    print("Done mutating")


def test_random_area(map_ind):
    for i in range(10):
        map_ind.apply_operation_anywhere("grow_cell", debug=True)
        map_ind.analyze_map(fix=True)
    for i in range(10):
        map_ind.apply_operation_anywhere("erode_cell", debug=True)
        map_ind.analyze_map(fix=True)
    for i in range(10):
        map_ind.apply_operation_anywhere("grow_cell", debug=True)
        map_ind.apply_operation_anywhere("erode_cell", debug=True)
        # map_ind.apply_operation_anywhere("place_block", debug=True)
        map_ind.apply_operation_anywhere("place_stairs", debug=True)
        map_ind.analyze_map(fix=True)

    # map_ind.apply_operation_anywhere("place_block", debug=True)
    # print_analysis(map_ind)
    print("Trying to fix now...")
    map_ind.try_fix_map()
    print_analysis(map_ind)


def test_fixed_area(map_ind):
    for i in range(10):
        map_ind.apply_operation("grow_cell", debug=True)
        map_ind.analyze_map(fix=True)
    for i in range(10):
        map_ind.apply_operation("erode_cell", debug=True)
        map_ind.analyze_map(fix=True)
    for i in range(10):
        map_ind.apply_operation("grow_cell", debug=True)
        map_ind.apply_operation("erode_cell", debug=True)
        map_ind.apply_operation_anywhere("place_block", debug=True)
        map_ind.apply_operation_anywhere("place_stairs", debug=True)
        map_ind.analyze_map(fix=True)

    # map_ind.apply_operation_anywhere("place_block", debug=True)
    # print_analysis(map_ind)
    print("Trying to fix now...")
    map_ind.try_fix_map()
    print_analysis(map_ind)

if __name__ == "__main__":
    from map_evolution.evolution.init_and_select import parse_args
    from common.common import csv_to_array
    from map_evolution.visualization.map_to_image import image_from_asci_map
    import map_evolution.evolution.mutation as mutation
    import random
    # random.seed(66)

    # args = parse_args(result_file="init_and_select_test")
    ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv")
    map_ind = MapIndividual(ind, 4, 4)

    # add_remove_stress_test(map_ind)
    # regular_test(map_ind)
    # grow_erode_test13(map_ind)
    # test_fix_function(map_ind)
    # test_random_area(map_ind)
    test_fixed_area(map_ind)
    # test_mutations(map_ind)
    print("blub")
