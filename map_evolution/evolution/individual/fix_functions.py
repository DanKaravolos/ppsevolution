from map_evolution.evolution.individual.grid_functions import *
'''
Below are listed the functions for the complete 20x20 map. Which is again assumed to be a 2D numpy array.
'''


def check_spawn_connection(grid_map):
    t1_base = (grid_map.shape[0] - 1, 0)
    # t2_base = (0, grid_map.shape[1] - 1)
    return check_connection_between(grid_map, t1_base, "0Z", traversables=["0", "0H", "0D", "0A", "0Y"])


# def check_spawn_connection(grid_map):
#     t1_base = (grid_map.shape[0] - 1, 0)
#     t2_base = (0, grid_map.shape[1] - 1)
#     _, cost1 = astar(grid_map, t1_base, "0Z", traversables=["0", "0H", "0D", "0A", "1"])
#     _, cost2 = astar(grid_map, t2_base, "0Y", traversables=["0", "0H", "0D", "0A", "1"])
#     return cost1 < 500 and cost2 < 500


def split_per_floor(pickup_list, grid_map):
    ground_p = [p for p in pickup_list if "0" in grid_map[p]]
    floor1_p = [p for p in pickup_list if "1" in grid_map[p]]
    return ground_p, floor1_p


def check_all_pickup_connections(gm_dict, grid_map):
    all_connected = []
    # first check whether all pickups are connected either to a base or to a stair, depending on the floor
    for pickup_tile in ["H", "A", "D"]:
        ground_p, floor1_p = split_per_floor(gm_dict[pickup_tile], grid_map)
        c_ground = [check_connection_between(grid_map, pickup, ["0Y", "0Z"], ["0"]) for pickup in ground_p]
        c_floor1 = [check_connection_between(grid_map, pickup, "0S", ["1"]) for pickup in floor1_p]
        all_connected.append(all(c_ground))
        all_connected.append(all(c_floor1))

    # then check whether all stairs are connected to a base
    all_connected.append(check_stair_connections(gm_dict, grid_map))
    return all(all_connected)


def check_stair_connections(gm_dict, grid_map):
    all_connected = [check_connection_between(grid_map, stair, ["0Y", "0Z"], ["0"]) for stair in gm_dict["S"]]
    return all(all_connected)


def fix_stairs(gm_dict, grid_map):
    stair_list = [s for s in gm_dict["S"]]
    nr_removed = 0
    for stair in stair_list:
        nbrs = get_neighbors(stair, grid_map)
        # if all neighbors are zero, the stair is maybe moved, otherwise removed from map
        zero_per_nbr = [grid_map[n][0] == "0" for n in nbrs]
        all_zero = all(zero_per_nbr)
        if all_zero or len(nbrs) < 4:
            new_loc = try_move_stair(stair, grid_map)
            if new_loc is None:
                gm_dict["S"].remove(stair)
                grid_map[stair] = "0"  # removing "S" from "0S".
                nr_removed += 1
            else:
                # print("moved a stair!")
                gm_dict["S"].remove(stair)
                grid_map[stair] = "0"  # removing "S" from "0S".
                grid_map[new_loc] = "0S"
                gm_dict["S"].append(new_loc)
        # if two or more neighbors are not zero or if there is a stair neighbor, then GameEngine will be confused.
        # or if there are no ones to connect to.
        elif zero_per_nbr.count(False) >= 2 or any(["S" in grid_map[n] for n in nbrs]) or\
                not any(["1" in grid_map[n] for n in nbrs]):
            gm_dict["S"].remove(stair)
            grid_map[stair] = "0"  # removing "S" from "0S".
            nr_removed += 1
    # print("fixed it")
    return nr_removed


def try_move_stair(center, grid):
    nbrs = get_neighbors(center, grid)
    block_coords = [center]  # + nbrs
    new_stair_loc = None
    # first_floor = False
    for nb in nbrs:
        if len(nbrs) == 4 and '1' in grid[nb]:
            print("Error: trying to move stair while it is already next to first floor. coord: {}".format(center))
            return None
        else:
            new_nbrs = [nwb for nwb in get_neighbors(nb, grid) if nwb not in block_coords]
            ones = [grid[nb_nb] == "1" for nb_nb in new_nbrs]
            zeros = [grid[nb_nb] == "0" for nb_nb in new_nbrs]
            if ones.count(True) == 1 and zeros.count(True) == 2:
                new_stair_loc = nb
                break
    return new_stair_loc


def try_place_stairs(gm_dict, grid_map, grow_unreachable_ones=True):
    unacceptable_nbs = ["1", "2", "0S"]
    placed_stairs = 0
    ones = gm_dict["1"]
    for one_coord in ones:
        conn_to_stair = check_connection_between(grid_map, one_coord, "0S", ["1"])
        if not conn_to_stair:
            # # check neighbors to see if we can place a stair next to this elevation
            # nbrs = get_neighbors(one_coord, grid_map)
            #
            # # find empty neighbors on the ground:
            # # ground_nbrs = [idx for idx in range(len(nbrs)) if grid_map[nbrs[idx]] == "0"] #this gets indices..?
            # ground_nbrs = [n for n in nbrs if grid_map[n][0] == "0"]
            # for ground_n in ground_nbrs:
            #     nbrs_ground_n = get_neighbors(ground_n, grid_map)
            #     zero_per_nbr = [grid_map[n][0] == "0" for n in nbrs_ground_n]
            #     if zero_per_nbr.count(False) == 1:
            #         # if that ground neighbor only has this 1 as non-ground neighbor, we can place a stair!
            #         grid_map[ground_n] = "0S"
            #         gm_dict["S"].append(ground_n)
            #         placed_stairs += 1
            #         break  # let's not place more than one stair
            # for each floor 1 tile, find a neighbor that is 0 and that has 0, as neighbors
            nbrs = get_neighbors(one_coord, grid_map)
            block_coords = [one_coord]  # + nbrs
            new_stair_loc = None
            for nb in nbrs:
                # check if neighbor itself is acceptable
                if grid_map[nb] != "0":
                    continue
                else:
                    # check if neighbor's neighbors are acceptable
                    new_nbrs = [grid_map[nwnb] for nwnb in get_neighbors(nb, grid_map) if nwnb not in block_coords]

                    if any([True for x in new_nbrs if x in unacceptable_nbs]) or len(new_nbrs) < 3:
                        continue
                    else:
                        new_stair_loc = nb
                        break
            if new_stair_loc is not None:
                grid_map[new_stair_loc] = "0S"
                gm_dict["S"].append(new_stair_loc)
                placed_stairs += 1

    ones = gm_dict["1"]
    unreachable_ones = [one_coord for one_coord in ones if not check_connection_between(grid_map, one_coord, "0S", ["1"])]
    unreachable_1_count = len(unreachable_ones)

    if grow_unreachable_ones:
        for unreachable in unreachable_ones:
            grid_map[unreachable] = "2"
            gm_dict["1"].remove(unreachable)
            gm_dict["2"].append(unreachable)

    is_s_connected = [check_connection_between(grid_map, stair, ["0Y", "0Z"], ["0"]) for stair in gm_dict["S"]]
    unreachable_stair_count = is_s_connected.count(False)
    # print("placed stairs: {}".format(placed_stairs))
    return unreachable_1_count, unreachable_stair_count


def fix_holes(gm_dict, grid_map):
    zeros = [z for z in gm_dict['0']]
    for z in zeros:
        nbrs = get_neighbors(z, grid_map)
        if not any([grid_map[nb][0] == "0" for nb in nbrs]):
            grid_map[z] = "1"
            gm_dict['0'].remove(z)
            gm_dict['1'].append(z)

    ones = [o for o in gm_dict['1']]
    for o in ones:
        nbrs = get_neighbors(o, grid_map)
        if all([grid_map[nb][0] == "2" for nb in nbrs]):
            grid_map[o] = "2"
            gm_dict['1'].remove(o)
            gm_dict['2'].append(o)


def check_for_unreachable_areas(grid_map, base1, base2):
    # flood ground
    traverse_1 = ["0", "0H", "0A", "0D", "0S", "0Y", "0Z"]
    flood_grid, v_stairs_t1 = floodfill(grid_map, base1, traverse_1, True)
    flood_grid, v_stairs_t2 = floodfill(flood_grid, base2, traverse_1, True)
    v_stairs = v_stairs_t1 + v_stairs_t2
    # flood first floors from each visited stair
    v_stairs_2 = []
    traverse_2 = ["1", "0S", "1H", "1A", "1D"]
    for stair in v_stairs:
        flood_grid, new_stairs = floodfill(flood_grid, stair, traverse_2, True)
        v_stairs_2.extend(new_stairs)

    # flood ground again from each visited stair
    v_stairs_3 = []
    for stair in v_stairs_2:
        flood_grid, new_stairs = floodfill(flood_grid, stair, traverse_1, True)
        v_stairs_3.extend(new_stairs)

    # flood first_floor again from each visited stair
    for stair in v_stairs_3:
        flood_grid, new_stairs = floodfill(flood_grid, stair, traverse_2, True)

    non_wall = traverse_1 + traverse_2
    unreachable_areas = any([tile in flood_grid for tile in non_wall])
    return unreachable_areas, flood_grid


def check_stair_validity(gm_dict, grid_map):
    stair_info = []
    for s in gm_dict['S']:
        nbrs = get_neighbors(s, grid_map)
        not_enough = len(nbrs) < 3
        has_wall = any(["2" in grid_map[n] for n in nbrs])
        not_has_one = not any(["1" in grid_map[n] for n in nbrs])
        stair_info.extend([not_enough, has_wall, not_has_one])

    invalid_stairs = any(stair_info)
    return invalid_stairs
