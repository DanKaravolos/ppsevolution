from __future__ import print_function
import random
from collections import defaultdict
import numpy as np
from map_evolution.evolution.individual.Cell import Cell, cell_from_grid, apply_cell_to_grid
from map_evolution.evolution.individual import grid_functions as grif
from map_evolution.evolution.individual import fix_functions as fixf
from map_evolution.visualization.map_to_image import image_from_asci_map

NR_LOOP_ATTEMPTS = 100


class MapIndividual:
    tile_types = ['0', '1', '2', 'S', 'H', 'A', 'D']

    def __init__(self, asci_array, nr_rows, nr_cols):
        # Grid.__init__(self)
        self.nr_rows = nr_rows
        self.nr_columns = nr_cols
        self.sketch = self.build_grid(asci_array)
        self.cell_dict = self.build_dict()

        self.t1_base = (nr_rows - 1, 0)
        self.t2_base = (0, nr_cols - 1)

        self.function_dict = {
            "add_pickup": self.manipulate_pickup,
            "move_pickup": self.manipulate_pickup,
            "remove_pickup": self.manipulate_pickup,
            "grow_cell": self.grow_cell,
            "erode_cell": self.erode_cell,
            "place_block": self.place_block,
            "place_stairs": self.place_staircase,
        }

        # Statistics for fitness
        self.bases_connected = None
        self.all_pickups_connected = None
        # self.unreachable_ones = None
        # self.unreachable_stairs = None
        self.unreachable_tiles = None
        self.invalid = None

        # debug stuff:
        # self.print_stats()

    def build_grid(self, asci_array):
        sketch = []
        cell_vals = []
        r_count = 0

        for r in range(0, len(asci_array), Cell.nr_rows):
            c_count = 0
            for c in range(0, len(asci_array[0]), Cell.nr_columns):
                cell_vals.append(Cell(r_count, c_count, asci_array[r:r + Cell.nr_rows, c:c + Cell.nr_columns]))
                c_count += 1
            r_count += 1

        for row in range(self.nr_rows):
            sketch_row = []
            for col in range(self.nr_columns):
                idx = row * self.nr_columns + col
                sketch_row.append(cell_vals[idx])
            sketch.append(sketch_row)
        return sketch

    def build_dict(self):
        tile_dict = defaultdict(list)
        for r in range(self.nr_rows):
            for c in range(self.nr_columns):
                for t in self.tile_types:
                    if self.sketch[r][c].count_tiles(t) > 0:
                        tile_dict[t].append((r, c))
        return tile_dict

    def contains_coord(self, coord):
        return 0 <= coord[0] < self.nr_rows and 0 <= coord[1] < self.nr_columns

    def get_neighbors(self, coord, moore=False):
        nbrs = grif.get_neighbors(coord, None, moore)
        return [n for n in nbrs if self.contains_coord(n)]

    def count_tiles(self, tile_type):
        count = 0
        for row in self.sketch:
            for cell in row:
                count += cell.count_tiles(tile_type)
        return count

    def print_stats(self):
        counts = ["{0}: {1}".format(t, self.count_tiles(t)) for t in self.tile_types]
        counts = ", ".join(counts)
        print("The map contains: ", counts)
        # connectedness_string = "Bases connected: {0}, all pickups connected: {1}, " \
        #                        "nr of unreachable 1s: {2}, nr unreachable stairs: {3}. " \
        #                        "Invalid: {4}".format(self.bases_connected,
        #                        self.all_pickups_connected, self.unreachable_ones, self.unreachable_stairs,
        #                        self.invalid)
        connectedness_string = "Bases connected: {0}, all pickups connected: {1}, "\
                               "Invalid: {2}".format(self.bases_connected, self.all_pickups_connected, self.invalid)
        print(connectedness_string)

    def to_matrix(self):
        def concat_row(row):
            return np.concatenate([col.grid for col in row], axis=1)
        return np.concatenate([concat_row(row) for row in self.sketch], axis=0)

    def to_string(self, inline=False):
        if inline:
            return ";".join(["_".join(line) for line in self.to_matrix()])
        else:
            return "\n".join([",".join(line) for line in self.to_matrix()])

    def print_map(self):
        print(self.to_string())
        print("\n")

    def manipulate_pickup(self, sketch_cell, manipulation):
        pickups = ["A", "D", "H"]
        random.shuffle(pickups)
        success = False
        for p in pickups:
            if manipulation == "add_pickup":
                success = self.add_pickup_in_cell(sketch_cell, p)
            elif manipulation == "move_pickup":
                success = self.move_pickup_in_cell(sketch_cell, p)
            elif manipulation == "remove_pickup":
                success = self.remove_pickup_in_cell(sketch_cell, p)
            else:
                print("Error: pickup manipulation {0} undefined.".format(manipulation))
                break
            if success:
                break
        return success

    def move_pickup_in_cell(self, cell_orig, tile_type):
        # check if there is something to move
        can_remove = cell_orig.can_remove_pickup(tile_type)
        if not can_remove:
            return False

        coord_orig = cell_orig.coords
        # find a place to move the pickup to
        can_place = False
        cp = 0  # for debug
        while not can_place and cp < NR_LOOP_ATTEMPTS:
            cp += 1
            rand_dest = random.choice(self.get_neighbors(coord_orig, moore=True))  # moving diagonal as well
            cell_dest = self.sketch[rand_dest[0]][rand_dest[1]]
            # cannot place if cell cannot have pickup and also if cell is one of the spawns
            can_place = cell_dest.can_add_pickup() and rand_dest != self.t1_base and rand_dest != self.t2_base
            # print("finding cell to add {0} to. {1}".format(tile_type, cr))

        # print("orig cell {0} contains {1} {2}.".format(rand_orig, cell_orig.count_tiles(tile_type), tile_type))
        # print("dest cell {0} contains {1} {2}.".format(rand_dest, cell_dest.count_tiles(tile_type), tile_type))
        # perform the actual movement of pickup
        if can_place and can_remove:
            cell_orig.remove_pickup(tile_type)
            cell_dest.add_pickup(tile_type)
            if cell_orig.count_tiles(tile_type) == 0:
                self.cell_dict[tile_type].remove(coord_orig)
            if rand_dest not in self.cell_dict[tile_type]:
                self.cell_dict[tile_type].append(rand_dest)
        else:
            print("Did not move pickup. Why? Can place: {0}, Can remove: {1}".format(can_place, can_remove))
        # print("orig cell {0} contains {1} {2}.".format(rand_orig, cell_orig.count_tiles(tile_type), tile_type))
        # print("dest cell {0} contains {1} {2}.".format(rand_dest, cell_dest.count_tiles(tile_type), tile_type))
        return can_place and can_remove

    def add_pickup_in_cell(self, cell_orig, tile_type):
        can_add = cell_orig.can_add_pickup()
        # print("cell {0} contains {1} {2}.".format(cell_orig.name, cell_orig.count_tiles(tile_type), tile_type))
        if can_add:
            cell_orig.add_pickup(tile_type)
            # update dictionary
            if cell_orig.coords not in self.cell_dict[tile_type]:
                self.cell_dict[tile_type].append(cell_orig.coords)
        # print("rand cell {0} contains {1} {2}.".format(rand_cell.name, rand_cell.count_tiles(tile_type), tile_type))
        return can_add

    def remove_pickup_in_cell(self, cell_orig, tile_type):
        can_remove = cell_orig.can_remove_pickup(tile_type)
        # print("finding cell to remove {0} from. {1}".format(tile_type, cr))
        if can_remove:
            cell_orig.remove_pickup(tile_type)
            # update dictionary
            if cell_orig.count_tiles(tile_type) == 0:
                self.cell_dict[tile_type].remove(cell_orig.coords)
        return can_remove

    def analyze_map(self, fix=False):
        grid_map = self.to_matrix()  # Grid.Grid(self.to_matrix())
        gm_dict = grif.build_dict(grid_map)
        # mark bases with special character
        t1_base = (grid_map.shape[0] - 1, 0)
        t2_base = (0, grid_map.shape[1] - 1)
        grid_map[t1_base] = '0Y'
        grid_map[t2_base] = '0Z'

        # left local variables in here for debugging
        if fix:
            # fixf.fix_holes(gm_dict, grid_map)
            stairs_removed = fixf.fix_stairs(gm_dict, grid_map)
            # unreachable_ones, unreachable_stairs = fixf.try_place_stairs(gm_dict, grid_map)
            # print("Stairs removed: ", stairs_removed)

        self.bases_connected = fixf.check_spawn_connection(grid_map)
        # self.all_pickups_connected = fixf.check_all_pickup_connections(gm_dict, grid_map)
        has_invalid_stairs = fixf.check_stair_validity(gm_dict, grid_map)

        invalid_map, flooded_map = fixf.check_for_unreachable_areas(grid_map, t1_base, t2_base)
        # if fix:
        #     for coords, t in np.ndenumerate(flooded_map):
        #         if t != "2":
        #             grid_map[coords] = "2"

        self.unreachable_tiles = sum([1 for _, t in np.ndenumerate(flooded_map) if t != "2"])

        self.invalid = invalid_map or has_invalid_stairs or not self.bases_connected # or not self.all_pickups_connected
        # image_from_asci_map(flooded_map, show=True, show_now=True)

        self.sketch = self.build_grid(grid_map)
        self.cell_dict = self.build_dict()

    def try_fix_map(self):
        if not self.invalid:
            return
        grid_map = self.to_matrix()  # Grid.Grid(self.to_matrix())
        gm_dict = grif.build_dict(grid_map)

        fixf.fix_holes(gm_dict, grid_map)
        stairs_removed = fixf.fix_stairs(gm_dict, grid_map)
        unreachable_ones, unreachable_stairs = fixf.try_place_stairs(gm_dict, grid_map)
        # print("Stairs removed: ", stairs_removed)

        # has_invalid_stairs = fixf.check_stair_validity(gm_dict, grid_map)
        t1_base = (grid_map.shape[0] - 1, 0)
        t2_base = (0, grid_map.shape[1] - 1)
        invalid_map, flooded_map = fixf.check_for_unreachable_areas(grid_map, t1_base, t2_base)
        # image_from_asci_map(flooded_map, show=True, show_now=True)
        for coords, t in np.ndenumerate(flooded_map):
            if t != "2":
                grid_map[coords] = "2"
        # self.invalid = invalid_map or has_invalid_stairs
        # # or not self.bases_connected or not self.all_pickups_connected

        self.sketch = self.build_grid(grid_map)
        self.cell_dict = self.build_dict()

    def visualize_bases(self):
        cell_p1 = self.sketch[self.nr_rows - 1][0]
        cell_p2 = self.sketch[0][self.nr_columns - 1]

        def assign_char_to_bases(cell, new_tile):
            for tile in cell.tile_dict["0"]:
                if cell.grid[tile] == "0":
                    cell.grid[tile] = new_tile
        assign_char_to_bases(cell_p1, "0Y")
        assign_char_to_bases(cell_p2, "0Z")

    def undo_visualize_bases(self):
        cell_p1 = self.sketch[self.nr_rows - 1][0]
        cell_p2 = self.sketch[0][self.nr_columns - 1]

        def assign_char_to_bases(cell, new_tile):
            for tile in cell.tile_dict["0"]:
                if cell.grid[tile] == "0Y" or cell.grid[tile] == "0Z":
                    cell.grid[tile] = new_tile
        assign_char_to_bases(cell_p1, "0")
        assign_char_to_bases(cell_p2, "0")

    @staticmethod
    def grow_cell(sketch_cell):
        # cell = self.sketch[sketch_cell[0]][sketch_cell[1]]
        applied = False
        floors = ["0", "1"]
        random.shuffle(floors)
        for tt in floors:
            applied = sketch_cell.grow(tt)
            if applied:
                break
        return applied

    @staticmethod
    def erode_cell(sketch_cell):
        # cell = self.sketch[sketch_cell[0]][sketch_cell[1]]
        applied = False
        floors = ["2", "1"]
        random.shuffle(floors)
        for tt in floors:
            applied = sketch_cell.erode(tt)
            if applied:
                break
        return applied

    @staticmethod
    def place_block(sketch_cell):
        # cell = self.sketch[sketch_cell[0]][sketch_cell[1]]
        applied = False
        floors = ["0", "1"]
        random.shuffle(floors)
        for tt in floors:
            applied = sketch_cell.place_block(tt)
            if applied:
                break
        return applied

    @staticmethod
    def place_staircase(sketch_cell):
        # cell = self.sketch[sketch_cell[0]][sketch_cell[1]]
        applied = sketch_cell.place_staircase()
        return applied

    def apply_operation(self, op_name, debug=False, cannot_use_bases=True):
        shuffled_cells = [col for row in self.sketch for col in row]
        if cannot_use_bases:
            shuffled_cells.remove(self.sketch[self.nr_rows - 1][0])
            shuffled_cells.remove(self.sketch[0][self.nr_columns - 1])

        random.shuffle(shuffled_cells)
        applied = apply_function(shuffled_cells, self.function_dict[op_name], op_name, debug)
        if debug and not applied:
            print("Operation {} could not be applied".format(op_name))
        return applied

    def apply_operation_anywhere(self, op_name, debug=False):
        grid_map = self.to_matrix()
        adhoc_cell = cell_from_grid(grid_map)

        applied = apply_function(adhoc_cell, self.function_dict[op_name], op_name, debug)
        if debug and not applied:
            print("Operation {} could not be applied".format(op_name))

        apply_cell_to_grid(grid_map, adhoc_cell)
        self.sketch = self.build_grid(grid_map)
        self.cell_dict = self.build_dict()
        return applied

    def apply_op_on_cell(self, cell, op_name, debug=False):
        applied = apply_function([cell], self.function_dict[op_name], op_name, debug)
        if debug and not applied:
            print("Operation {} could not be applied".format(op_name))
        return applied


def apply_function(shuffled_cells, function_name, op_string, debug):
    if type(shuffled_cells) != list:
        shuffled_cells = [shuffled_cells]

    f_args = []
    if "pickup" in op_string:
        f_args.append(op_string)

    for cell in shuffled_cells:
        applied = function_name(cell, *f_args)
        if cell.coords == (4, 4):
            print("trying to apply to right bottom corner")
        if applied:
            if debug:
                print("applied {0} to {1}".format(op_string, cell.name))
            break
    return applied

if __name__ == "__main__":
    from map_evolution.evolution.init_and_select import parse_args
    from common.common import csv_to_array
    from map_evolution.visualization.map_to_image import image_from_asci_map
    # args = parse_args(result_file="init_and_select_test")
    # ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/fix_test_map.csv")
    ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv")
    map_ind = MapIndividual(ind, 4, 4)

    map_ind.print_map()
    image_from_asci_map(map_ind.to_matrix(), show=True)
    map_ind.analyze_map()
    map_ind.visualize_bases()
    map_ind.print_stats()

    rgb_map = image_from_asci_map(map_ind.to_matrix(), show=True, show_now=True)
    print("blub")
