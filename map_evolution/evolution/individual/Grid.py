import map_evolution.evolution.individual.grid_functions as gf


class Grid:
    nr_rows = 0
    nr_columns = 0

    def __init__(self, np_grid):
        self.grid = np_grid
        self.nr_rows, self.nr_columns = np_grid.shape
        self.size = self.nr_columns * self.nr_rows
        self.tile_dict = gf.build_dict(np_grid)

    def contains_coord(self, coord):
        return 0 <= coord[0] < self.nr_rows and 0 <= coord[1] < self.nr_columns

    def get_neighbors(self, coord, moore=False):
        return gf.get_neighbors(coord, self.grid, moore)


