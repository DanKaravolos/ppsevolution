# import os
import numpy as np
from common.common import list_to_string, inline_map_to_array
from map_evolution.visualization.map_to_image import image_from_asci_map
from character_evolution.evolution.weapon_parameters import all_params
import warnings
EPSILON = 0.0001


def process_multi_results(log, pareto, final_pop, eval_func, initial_preds, args, run, output_file):
    map_name = args.initial_map.rsplit("maps/", 1)[1].replace("_Run_0_Map.csv", "")
    ind_name = output_file + "-{0}-run_{1}".format(args.map_name, run)
    csv_ind = ind_name.replace("results/", "results/best_ind/") + ".csv"
    png_ind = ind_name.replace("results/", "results/pngs/") + ".png"
    # collect more information
    print("Only processing 2 fitness dimensions right now!")
    non_pareto_inds = []
    for ind in final_pop:
        is_not_pareto = True
        for fitness in pareto.keys:
            val = fitness.values
            if abs(ind.fitness.values[0] - val[0]) < EPSILON and abs(ind.fitness.values[1] - val[1]) < EPSILON:
                is_not_pareto = False
                break
        if is_not_pareto:
            non_pareto_inds.append(ind)
    print("Len of non_pareto: {0}, len of pareto: {1}".format(len(non_pareto_inds), len(pareto.items)))

    fit_sum = [np.sqrt(sum(p.fitness.values)) for p in pareto.items]
    best_ind_num = np.argmin(fit_sum)
    best_ind = pareto.items[best_ind_num]

    # Output functions
    write_best_ind_results(map_name, ind_name, best_ind, eval_func, initial_preds, args, run, output_file, run == 0)
    write_results(map_name, pareto, eval_func, initial_preds, args, run, output_file)
    write_fitnesses(pareto, non_pareto_inds, eval_func, args.target_values, best_ind_num, output_file)
    # write_pareto(pareto, output_file)
    print("Only writing best ind to game input!")
    write_best_to_game_input(args, args.game_input_file, ind_name)
    write_best_ind(best_ind, csv_ind, png_ind)
    write_log(log, args.fitlog_file)

    # Output checks
    nr_objectives = len(pareto.items[0].fitness.values)
    if nr_objectives > 2:
        warnings.warn("You have {0} fitnesses, but write_pareto and write_log only write two values to file!".format(nr_objectives))
    print("Just so you know best_hypervolume index is not used!")


def write_fitnesses(pareto, non_pareto, eval_func, targets, best_hv, output_file):
    warnings.warn("Writing pareto individuals to fitness file (per line), but this is untested.")
    # warnings.warn("in write_fitnesses: hardcoded mention of predicted time and score.")
    pred_string = ",".join(["predicted {0}".format(t) for t in sorted(targets.keys())])  # + "obj.{0}, pred {1}".format(t) for t in sorted(targets.keys())])
    fit_string = ",".join(["obj {0}".format(c) for c in range(len(targets.keys()))])
    with open(output_file + "-fitnesses.csv", "a") as out:
        header = "num,type,{0},{1}, sqrt of sum of fitnesses, individual\n".format(fit_string, pred_string)
        out.write(header)
        ind_count = 0
        for p in pareto.items:
            fitnesses = ",".join(["{:.6f}".format(f) for f in p.fitness.values])
            fit_sum = np.sqrt(sum(p.fitness.values))
            fit_avg = np.mean(p.fitness.values)
            pred = eval_func(p, separator=",", with_name=False)
            p[0].visualize_bases()
            pareto_ind_str = p[0].to_string(inline=True)
            if ind_count == best_hv:
                line = "{0},pareto_best,{1},{2},{3},{4},{5}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum, pareto_ind_str)
            else:

                line = "{0},pareto,{1},{2},{3},{4},{5}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum, pareto_ind_str)

            out.write(line)
            ind_count += 1

        for ind in non_pareto:
            fitnesses = ",".join(["{:.6f}".format(f) for f in p.fitness.values])
            pred = eval_func(ind, separator=",", with_name=False)
            fit_sum = np.sqrt(sum(p.fitness.values))
            fit_avg = np.mean(p.fitness.values)
            line = "{0},dominated,{1},{2}\n".format(ind_count, fitnesses, pred, fit_avg, fit_sum)
            out.write(line)
            ind_count += 1


def write_pareto(pareto, output_file):
    cnt = 0
    for p_ind in pareto.items:
        p_ind[0].visualize_bases()
        map_ind = p_ind[0].to_string()
        fitness = p_ind.fitness.values
        tp = output_file + "-pareto_{0}-f1_{1:.2f}-f2_{2:.2f}.csv".format(cnt, fitness[0], fitness[1])
        with open(tp, "a") as out:
            out.write(map_ind)

        tp = tp.replace(".csv", ".png")
        image_from_asci_map(p_ind[0].to_matrix(), new_size=316, border_size=2, show_now=False, save=True, target_path=tp)
        cnt += 1


def write_best_ind(best_ind, csv_out, png_out):
    best_ind[0].visualize_bases()
    map_ind = best_ind[0].to_string()
    with open(csv_out, "a") as out:
        out.write(map_ind)

    image_from_asci_map(best_ind[0].to_matrix(), new_size=316, border_size=2, show_now=False, save=True, target_path=png_out)


def write_best_to_game_input(args, output_file, ind_name):
    tp = output_file  # + "-best-game_input.csv"
    with open(tp, "a") as out:
        match_params = ",".join(["{0:.6f}".format(p) for p in args.match_params])
        map_name = ind_name + ".csv"
        out.write("{0},{1}\n".format(map_name, match_params))


def write_log(log, fitness_log_file):
    # warnings.warn("In write_log: Only writing two objectives to file!")
    with open(fitness_log_file, 'a') as out:
        out.write("gen,obj1 min,obj1 max,obj1 avg,obj1 std,obj2 min,obj2 max,obj2 avg,obj2 std\n")
        obj1 = log.chapters['obj1']
        obj2 = log.chapters['obj2']
        for line in range(len(obj1)):
            obj1_vals = [obj1[line]["min"], obj1[line]["max"], obj1[line]["avg"], obj1[line]["std"]]
            obj2_vals = [obj2[line]["min"], obj2[line]["max"], obj2[line]["avg"], obj2[line]["std"]]
            values = [log[line]["gen"]] + obj1_vals + obj2_vals
            gen_string = list_to_string(values)
            out.write(gen_string + "\n")


def write_best_ind_results(map_name, ind_name, best_ind, eval_func, initial_preds, args, run, output_file, write_header=True):
    with open(output_file + "-best_ind-result.csv", 'a') as out_file:
        write_ind(map_name, best_ind, ind_name, eval_func, initial_preds, args, run, out_file, write_header)


def write_results(map_name, pareto, eval_func, initial_preds, args, run, output_file, write_header=True):
    with open(output_file + "-result.csv", 'a') as out_file:
        for pareto_ind in pareto.items:
            write_ind(map_name, pareto_ind, output_file, eval_func, initial_preds, args, run, out_file, write_header)
            write_header = False


def write_ind(map_name, pareto_ind, ind_name, eval_func, initial_preds, args, run, out_file, write_header):
    target_keys = sorted(args.target_values.keys())
    # map_name = args.initial_map.replace("/home/daniel/Projects/ppsEvolution/data/maps/", "")
    ind_name = ind_name.rsplit("/", 1)[1]
    pareto_ind[0].visualize_bases()
    match_params = ",".join(["{0:.6f}".format(p) for p in args.match_params])
    fitnesses = ",".join(["{:.6f}".format(f) for f in pareto_ind.fitness.values])
    fit_sum = np.sqrt(sum(pareto_ind.fitness.values))
    fit_avg = np.mean(pareto_ind.fitness.values)
    preds = eval_func(pareto_ind, separator=",", with_name=False)
    header = ["initial map", "ind", "fitness", "avg. fitness"] + ["{0} {1}".format(args.fitness, i) for i in range(len(target_keys))] + \
             target_keys + ["Sqrt. sum of fitnesses"] + ["initial {0} {1}".format(args.fitness, i) for i in range(len(target_keys))] + \
             ["evolved map"]
    ind_params = all_params * 2
    header.extend(ind_params)
    initial_preds = ",".join(["{:.6f}".format(initial_preds[f]) for f in sorted(initial_preds.keys())])
    out_items = [map_name, ind_name, args.fitness, fit_avg, fitnesses, preds, fit_sum, initial_preds, pareto_ind[0].to_string(inline=True),
                 match_params]
    if run is not None:
        header.append("Run")
        out_items.append(run)
    if "maximize" not in args.fitness:
        targets = ["target {0}: {1}".format(t, args.target_values[t]) for t in target_keys]
        header.extend(targets)
    else:
        header.extend(["maximize {0}".format(t) for t in target_keys])

    out_str = ",".join([str(i) for i in out_items]) + "\n"
    if write_header:
        out_file.write(",".join(header) + "\n")
        out_file.write(out_str)
    else:
        out_file.write(out_str)
    # out_file.write("\n\n")  # add blank lines for stats
