from __future__ import print_function
import random
import numpy as np

# possible_mutations = ["add_pickup", "move_pickup", "remove_pickup", "grow_cell", "erode_cell", "place_stairs", "place_block"]
possible_mutations = ["move_pickup", "grow_cell", "erode_cell", "place_stairs", "place_block"]
overall_mutations = ["place_block", "place_stairs"]
DEBUG = False


def mutate(deap_ind, mutprob=0.2):
    mode = "gene_prob_mut"
    gene_mut_prob = mutprob
    if mode == "one_mut":
        return one_mutation(deap_ind)
    elif mode == "prob_mut":
        return prob_mutation(deap_ind, gene_mut_prob)
    elif mode == "gene_prob_mut":
        return gene_prob_mutation(deap_ind, gene_mut_prob)


# one mutation per generation
def old_one_mutation(deap_ind):
    applied = False
    count = 0
    while not applied and count < 20:
        rand_mut = random.choice(possible_mutations)
        if rand_mut in overall_mutations:
            applied = deap_ind[0].apply_operation_anywhere(rand_mut, debug=DEBUG)
        else:
            applied = deap_ind[0].apply_operation(rand_mut, debug=DEBUG)
        count += 1
    # print(rand_mut)
    deap_ind[0].analyze_map(fix=True)
    deap_ind[0].try_fix_map()
    return deap_ind,


# probabilistic mutation
def prob_mutation(deap_ind, mut_prob=0.2):
    for mut in possible_mutations:
        prob = np.random.uniform(0, 1)
        if prob < mut_prob:
            if mut in overall_mutations:
                applied = deap_ind[0].apply_operation_anywhere(mut, debug=DEBUG)
            else:
                applied = deap_ind[0].apply_operation(mut, debug=DEBUG)
            # print(mut)
    # print(rand_mut)
    deap_ind[0].try_fix_map()
    deap_ind[0].analyze_map(fix=True)
    return deap_ind,


# probabilisic mutation per gene
def gene_prob_mutation(deap_ind, mut_prob=0.1):
    cells = [col for row in deap_ind[0].sketch for col in row]
    cell_count = 0
    for cell in cells:
        applied = False
        cell_count += 1
        occur = np.random.uniform()
        if DEBUG:
            print("cell {0}. prob: {1}".format(cell_count, occur))
        if occur < mut_prob:
            # if mutation occurs, try a random mutation on a gene
            random.shuffle(possible_mutations)  # note that we are shuffling the global possible mutations!
            count = 0
            while not applied and count < len(possible_mutations):
                rand_mut = possible_mutations[count]
                if DEBUG:
                    print("considering", rand_mut)
                applied = deap_ind[0].apply_op_on_cell(cell, rand_mut, debug=DEBUG)
                count += 1
            # print(rand_mut)
    deap_ind[0].analyze_map(fix=True)
    deap_ind[0].try_fix_map()
    return deap_ind,


# def mutate(map_ind, mutprob=0.01):
#     for mut in possible_mutations:
#         if random.uniform < mutprob:
#             map_ind[0].apply_operation(mut)
#     map_ind[0].global_analyze_and_fix_map()
#     return map_ind,


# def apply_structure_mutation(map_ind):
#     rand_mut = random.choice(structure_mutations)
#     map_ind[0].apply_operation(rand_mut)
#     map_ind[0].analyze_map()
#     return map_ind,
#
#
def apply_one_random_mutation_to_all_cells(map_ind):
    return mutate(map_ind, 1)
#     rand_mut = random.choice(possible_mutations)
#     map_ind.apply_operation(rand_mut)
#     map_ind.analyze_map()


# one mutation per individual
def one_mutation(deap_ind):
    applied = False
    count = 0
    random.shuffle(possible_mutations)
    while not applied and count < len(possible_mutations):
        rand_mut = possible_mutations[count]
        applied = deap_ind[0].apply_operation(rand_mut, debug=DEBUG)
        count += 1
    if DEBUG:
        print("Last mut: {0}, applied: {1}".format(rand_mut, applied))
    deap_ind[0].analyze_map(fix=True)
    deap_ind[0].try_fix_map()
    return deap_ind,


# x mutations per individual
def apply_x_mutations(map_ind, nr_mutations):

    def do_mutation(ind):
        applied = False
        count = 0
        random.shuffle(possible_mutations)
        while not applied and count < len(possible_mutations):
            rand_mut = possible_mutations[count]
            applied = ind.apply_operation(rand_mut, debug=DEBUG)
            count += 1
        # if DEBUG:
        #     print("Last mut: {0}, applied: {1}".format(rand_mut, applied))
        return ind
    for i in range(nr_mutations):
        if DEBUG:
            print("x mutations: {0}".format(i))
        do_mutation(map_ind)
    map_ind.analyze_map(fix=True)
    map_ind.try_fix_map()
    return map_ind,


# initial map mutation: apply a mutation to a nr_mutations number of cells
# can be the same as apply_x_mutations by using random.choices
def mutate_x_cells(initial_map, nr_mutations):
    cells = [col for row in initial_map.sketch for col in row]
    cells.remove(cells[3])
    cells.remove(cells[11])
    selected_cells = random.sample(cells, nr_mutations)  # select nr_mutations vars from cells
    did_cells_mutate = []
    for cell in selected_cells:
        applied = False
        random.shuffle(possible_mutations)  # note that we are shuffling the global possible mutations!
        count = 0
        while not applied and count < len(possible_mutations):
            rand_mut = possible_mutations[count]
            applied = initial_map.apply_op_on_cell(cell, rand_mut, debug=DEBUG)
            count += 1
        did_cells_mutate.append(applied)
    nr_applied = sum(did_cells_mutate)
    if nr_applied < nr_mutations:
        print("nr applied {0}. that's not enough... recursion time!!".format(nr_applied))
        mutate_x_cells(initial_map, nr_mutations - nr_applied)  # Recursion!! whoop whoop
    initial_map.analyze_map(fix=True)
    initial_map.try_fix_map()
    return initial_map

# def test_initial_map_mutation():
#     # ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv"
#     ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_9_Run_0_Map.csv")
#     map_ind = MapIndividual(ind, 4, 4)
#
#     # map_ind.print_map()
#     # map_ind.visualize_bases()
#     image_from_asci_map(map_ind.to_matrix(), show=False)
#     # random.seed(0)
#     mutate_x_cells(map_ind, 4)
#
#     # map_ind.analyze_map()
#     map_ind.visualize_bases()
#
#     rgb_map = image_from_asci_map(map_ind.to_matrix(), show=True, show_now=True, save=True,
#                                   target_path="/home/daniel/Projects/ppsEvolution/data/maps/mutations/mut.png")
#     print("blub")


# def test_init_time(nr_tests):
#     print("test_init_time")
#     # ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv"
#     ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_9_Run_0_Map.csv")
#
#     maps = [deepcopy(MapIndividual(ind, 4, 4)) for i in range(nr_tests)]
#     # random.seed(0)
#     t0 = time()
#     for map_ind in tqdm(maps):
#         if DEBUG:
#             print("\nNew individual")
#         mutate_initial_map(map_ind, 4)
#     t1 = time()
#     total = t1 - t0
#     print("nr iterations {0}. Time taken: {1}".format(nr_tests, total))
#     maps[-1].visualize_bases()
#     maps[0].visualize_bases()
#
#     rgb_map = image_from_asci_map(maps[-1].to_matrix(), show=True, show_now=False, save=False,
#                                   target_path="/home/daniel/Projects/ppsEvolution/data/maps/mutations/mut.png")
#     rgb_map2 = image_from_asci_map(maps[0].to_matrix(), show=True, show_now=True, save=False,
#                                   target_path="/home/daniel/Projects/ppsEvolution/data/maps/mutations/mut.png")
#
#
def test_initial_map_mutation(nr_inds, nr_mutations):
    ind = "/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_9_Run_0_Map.csv"

    def init_ind_from_existing_with_mutations(original_file, nr_muts):
        ind_map = MapIndividual(csv_to_array(original_file), 4, 4)
        apply_x_mutations(ind_map, nr_muts)  # initialize individual with x mutations
        deap_ind = [ind_map]
        return deap_ind

    # random.seed(0)
    t0 = time()
    for i in tqdm(range(nr_inds)):
        if DEBUG:
            print("\nNew individual ".format(i))
        new_map = init_ind_from_existing_with_mutations(ind, nr_mutations)
        new_map[0].visualize_bases()
        image_from_asci_map(new_map[0].to_matrix(), show=True, show_now=True, save=False,
                            target_path="/home/daniel/Projects/ppsEvolution/data/maps/mutations/mut.png")
    t1 = time()
    total = t1 - t0
    print("nr iterations {0}. Time taken: {1}".format(nr_inds, total))


def test_default():
    # args = parse_args(result_file="init_and_select_test")
    # ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/fix_test_map.csv")
    # ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv"
    ind = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_9_Run_0_Map.csv")
    map_ind = MapIndividual(ind, 4, 4)
    # ind = init_ind_from_existing("/home/daniel/Projects/ppsEvolution/data/tiny_map2.csv")
    # ind[(4, 4)] = "1"
    # map_ind = MapIndividual(ind, 1, 1)

    # map_ind.print_map()
    # map_ind.visualize_bases()
    image_from_asci_map(map_ind.to_matrix(), show=False)
    random.seed(0)
    # for mut in possible_mutations:
    # for mut in ["grow_cell", "erode_cell", "place_stairs"]:
    #     map_ind.apply_operation(mut)
    #     rgb_map = image_from_asci_map(map_ind.to_matrix(), show=True, show_now=False)
    mutate([map_ind])

    # map_ind.analyze_map()
    map_ind.visualize_bases()

    rgb_map = image_from_asci_map(map_ind.to_matrix(), show=True, show_now=True, save=True,
                                  target_path="/home/daniel/Projects/ppsEvolution/data/maps/mutations/initial_mut.png")
    print("blub")


if __name__ == "__main__":
    # from map_evolution.evolution.init_and_select import init_ind_from_existing_with_mutations
    from common.common import csv_to_array
    from map_evolution.visualization.map_to_image import image_from_asci_map
    from map_evolution.evolution.individual.MapIndividual import MapIndividual
    from copy import deepcopy
    from time import time
    from tqdm import tqdm
    DEBUG = False
    # test_default()
    # test_initial_map_mutation()

    # test_init_time(100)
    random.seed(0)
    test_initial_map_mutation(3, 4)


