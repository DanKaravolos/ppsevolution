from map_evolution.evolution.archive.fitness_functions import get_best_individual
from common.common import list_to_string
from map_evolution.visualization.map_to_image import image_from_asci_map


def write_results(args, output_file, best_per_generation, final_pop, log, last_best, final_best_predictions, initial_map_eval,
                  run=None, write_header=True):
    target_keys = sorted(final_best_predictions.keys())
    ind_name = output_file + "-{0}-run_{1}".format(args.map_name, run)
    csv_ind = ind_name.replace("results/", "results/best_ind/") + ".csv"
    png_ind = ind_name.replace("results/", "results/pngs/") + ".png"

    with open(output_file + "-result.csv", "a") as out:
        predicted_h = ["predicted initial {0}".format(t) for t in target_keys]
        header = ["initial map", "match", "fitness", *target_keys, *predicted_h, "is_invalid", "nr_generations", "ind_name", "ind_map"]
        is_invalid = last_best[0].invalid
        last_best[0].visualize_bases()
        last_best_str = last_best[0].to_string(inline=True)
        # if is_invalid:
        #     last_best, best_valid_vals = get_best_individual(final_pop, True, True)
        #     final_best_predictions = best_valid_vals
        # else:
        #     best_valid_ind = last_best
        preds = ["{:.4f}".format(final_best_predictions[t]) for t in target_keys]
        initial_map_preds = ["{:.4f}".format(initial_map_eval[t]) for t in target_keys]

        out_items = [args.map_name, args.match, last_best.fitness.values[0], *preds, *initial_map_preds,
                     is_invalid, len(log), ind_name.rsplit("/", 1)[1], last_best_str]
        if run is not None:
            header.append("Run")
            out_items.append(run)

        if "maximize" not in args.fitness:
            targets = ["target {0}: {1}".format(t, args.target_values[t]) for t in target_keys]
            header.extend(targets)
        else:
            header.extend(["maximize {0}".format(t) for t in target_keys])

        if write_header:
            header_str = ",".join(header)
            out.write(header_str + "\n")
        str_values = ",".join([str(v) for v in out_items]) + "\n"
        out.write(str_values)

    # save game input for GT runs
    with open(args.game_input_file, 'a') as out:
        out.write(ind_name + ".csv,")
        out.write(",".join(["{0:.6f}".format(p) for p in args.match_params]) + "\n")

    # save fitness progression for fitness graph
    write_fitness_history(log, best_per_generation, args.fitlog_file)

    # save final best individual for GT runs and visual inspection
    last_best[0].visualize_bases()
    write_individual(last_best, csv_ind)
    image_from_asci_map(last_best[0].to_matrix(), show_now=False, save=True, target_path=png_ind)


def write_individual(deap_ind, target_file):
    str_ind = deap_ind[0].to_string()
    with open(target_file, 'w')as out:
        out.write(str_ind)


def write_fitness_history(logbook, best_per_generation, target_file):
    with open(target_file, 'w') as out:
        out.write("gen,min,max,avg,std\n")

        for line in logbook:
            values = [line["gen"], line["min"], line["max"], line["avg"], line["std"]]
            gen_string = list_to_string(values)
            out.write(gen_string + "\n")

