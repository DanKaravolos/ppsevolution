from __future__ import print_function
import time
# from warnings import warn
import numpy as np
import gc
from deap import algorithms, base, creator, tools
from common.common import define_fitness_weight, is_equal_map, save_initial_maps
import common.fitness_functions as ff
import map_evolution.evolution.init_and_select as init
# from map_evolution.evolution.init_and_select import parse_args, init_ind_from_existing, init_match_from_tf2
# from map_evolution.evolution.init_and_select import init_ind_from_existing_with_mutations
import map_evolution.evolution.mutation as mutation
from map_evolution.evolution.crossover import crossover
# from map_evolution.evolution.so_output import write_results, write_individual, write_fitness_history
# from map_evolution.visualization.map_to_image import image_from_asci_map
from map_evolution.evolution.moea_map_output import process_multi_results
args = None
fit_functions = None

# BETWEEN_EPOCHS_EPSILON = 1e-5
# CLOSE_TO_ZERO_EPSILON = 1e-6
# MAX_EPOCHS_WITHOUT_PROGRESS = 10
# warn("Note: MAX_EPOCHS_WITHOUT_PROGRESS = {0}".format(MAX_EPOCHS_WITHOUT_PROGRESS))


def setup_moea():
    print("First fitness:")
    w1 = define_fitness_weight(args.fitness)
    weights = [w1] * len(fit_functions.target_values.keys())
    print("weights: {0}".format(weights))
    creator.create("FitnessMax", base.Fitness, weights=weights)
    creator.create("Individual", list, fitness=creator.FitnessMax)

    # Structure initializers
    toolbox = base.Toolbox()
    toolbox.register("init_individual", init.init_ind_from_existing_with_mutations, creator.Individual,
                     args.initial_map, args.initial_map_mutations)
    toolbox.register("population", tools.initRepeat, list, toolbox.init_individual)
    toolbox.register("evaluate", fit_functions.evaluate)
    toolbox.register("mate", crossover)
    toolbox.register("mutate", mutation.mutate, mutprob=args.mut_gene_prob)
    toolbox.register("select", tools.selNSGA2)
    print("Creating initial population...")
    population = toolbox.population(n=args.pop_size)
    return toolbox, population


def do_mo_evolution(population, toolbox, cxpb=0, mutpb=1, ngen=1, stats=None, halloffame=None, verbose=True):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    ind_to_reevaluate = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
    for ind, fit in zip(ind_to_reevaluate, fitnesses):
        ind.fitness.values = fit

    # This is just to assign the crowding distance to the individuals
    # no actual selection is done
    population = toolbox.select(population, len(population))

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(ind_to_reevaluate), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    # prev_best_fitness = -1
    # epochs_without_progress = 0
    # tmp_pareto = tools.ParetoFront()
    for gen in range(1, ngen):
        # Select the next generation individuals
        offspring = tools.selTournamentDCD(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
        # Evaluate the individuals with an invalid fitness
        ind_to_reevaluate = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, ind_to_reevaluate)
        for ind, fit in zip(ind_to_reevaluate, fitnesses):
            ind.fitness.values = fit

        # print info about infeasibility:
        nr_infeasibles = [ind[0].invalid for ind in offspring].count(True)
        print("nr of infeasible solutions in offspring: ", nr_infeasibles)

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population = toolbox.select(population + offspring, len(population))

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(ind_to_reevaluate), **record)
        if verbose:
            print(logbook.stream)

        # tmp_pareto.update(population)
        # print("Size of tmp pareto front. keys {0}, items {1}".format(len(tmp_pareto.keys), len(tmp_pareto.items)))

        # # compute stop conditions
        # best_fitness = logbook[-1]["max"] if "maximize" in args.fitness or "target" in args.fitness else logbook[-1]["min"]
        # if best_fitness < EPSILON:
        #     print("Stopping EA: fitness < {0}".format(EPSILON))
        #     break
        # if abs(best_fitness - prev_best_fitness) < EPSILON:
        #     epochs_without_progress += 1
        # else:
        #     epochs_without_progress = 0
        # if epochs_without_progress == MAX_EPOCHS_WITHOUT_PROGRESS:
        #     print("Stopping EA: fitness has not improved for {0} generations".format(MAX_EPOCHS_WITHOUT_PROGRESS))
        #     break
    print("population size: ", len(population))
    return population, logbook


def evolution(toolbox, population):
    # random.seed(64)
    stats1 = tools.Statistics(key=lambda ind: ind.fitness.values[0])

    stats1.register("avg", np.mean)
    stats1.register("std", np.std)
    stats1.register("min", np.min)
    stats1.register("max", np.max)

    stats2 = tools.Statistics(key=lambda ind: ind.fitness.values[1])

    stats2.register("avg", np.mean)
    stats2.register("std", np.std)
    stats2.register("min", np.min)
    stats2.register("max", np.max)

    m_stats = tools.MultiStatistics(obj1=stats1, obj2=stats2)
    pop, log = do_mo_evolution(population, toolbox, cxpb=args.cross_prob,
                                  mutpb=args.mut_individual_prob, ngen=args.generations, stats=m_stats,
                                  halloffame=None, verbose=True)
    return pop, log


def init_parameters(run_args):
    global args
    global fit_functions
    args = run_args
    print("loading network and data ...")
    match_params = init.init_match_from_tf2(args.match)
    print("Match: {0}. Params: {1}".format(args.match, match_params))
    fit_functions = ff.FitnessFunction("map", args.fitness, args.target_values, match=match_params)
    fit_functions.prepare_nn_and_data(args.models)  # it does not make sense to give args.initial map here, because we will evolve it.
    print("Confirming fitness: {0}".format(fit_functions.fitness))
    return match_params


def main(run_args):
    print("Setting up EA ...")
    args.match_params = init_parameters(run_args)
    print("Running EA ...")
    print("Result file: ", args.result_file)
    for run in list(range(args.runs)):
        print("run ", run)
        print("Match: ", args.match)
        t0 = time.time()
        output_file = args.result_file
        tb, pop = setup_moea()
        # Store initial population
        save_initial_maps(args, pop, run, is_dual=False)

        initial_map = pop[0]
        final_pop, log = evolution(tb, pop)  # difference between main and fire_main

        pareto = tools.ParetoFront(similar=is_equal_map)
        pareto.update(final_pop)
        print("Size of pareto front. keys {0}, items {1}".format(len(pareto.keys), len(pareto.items)))
        initial_preds = fit_functions.nn_evaluate_individual(initial_map)

        # do something with final_population, hall of fame and log.
        process_multi_results(log, pareto, final_pop, fit_functions.nn_str_prediction, initial_preds, args, run, output_file)

        t_diff = time.time() - t0
        print("Time taken: {0} min".format(t_diff / 60))
        print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        # cleaning up...
        tb = None
        pop = None
        gc.collect()

if __name__ == "__main__":
    my_run_args = init.parse_args()
    main(my_run_args)
