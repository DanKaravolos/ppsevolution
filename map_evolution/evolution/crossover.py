from __future__ import print_function
from copy import deepcopy
import numpy as np
from map_evolution.evolution.individual.MapIndividual import MapIndividual


def to_matrix(cell_list):
    def concat_row(row):
        return np.concatenate([col.grid for col in row], axis=1)
    return np.concatenate([concat_row(row) for row in cell_list], axis=0)


def crossover(ind1, ind2):
    copy1 = deepcopy(ind1)
    copy2 = deepcopy(ind2)

    child1 = []
    child2 = []
    for row in range(ind1[0].nr_rows):
        row_child1 = []
        row_child2 = []
        for col in range(ind1[0].nr_columns):
            dice_roll = np.random.uniform()
            if dice_roll < 0.5:
                row_child1.append(ind1[0].sketch[row][col])
                row_child2.append(ind2[0].sketch[row][col])
            else:
                row_child1.append(ind2[0].sketch[row][col])
                row_child2.append(ind1[0].sketch[row][col])
        child1.append(row_child1)
        child2.append(row_child2)

    copy1[0] = MapIndividual(to_matrix(child1), 4, 4)
    copy2[0] = MapIndividual(to_matrix(child2), 4, 4)
    return copy1, copy2


if __name__ == "__main__":
    from common.common import csv_to_array
    from map_evolution.visualization.map_to_image import image_from_asci_map

    # np.random.seed(33)
    ind1 = csv_to_array("/home/daniel/Projects/ppsEvolution/data/map_antonios_valid_empty_bases.csv")
    ind2 = csv_to_array("/home/daniel/Projects/ppsEvolution/data/test_data1/Level_3_Run_0_Map.csv")
    ind1 = MapIndividual(ind1, 4, 4)
    ind2 = MapIndividual(ind2, 4, 4)
    c1, c2 = crossover([ind1], [ind2])
    c1[0].analyze_map(fix=True)
    c1[0].try_fix_map()

    c2[0].analyze_map(fix=True)
    c2[0].try_fix_map()

    for ind in [[ind1], [ind2], c1, c2]:
        ind[0].visualize_bases()

    image_from_asci_map(ind1.to_matrix(), show=True)
    image_from_asci_map(ind2.to_matrix(), show=True)
    image_from_asci_map(c1[0].to_matrix(), show=True)
    image_from_asci_map(c2[0].to_matrix(), show_now=True)
