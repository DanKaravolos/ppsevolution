from shutil import copyfile
import os
root = '/home/daniel/Projects/ppsEvolution/map_evolution/CIG18Extra/'
folders = [root + '{0}/results/'.format(fo) for fo in os.listdir(root)]
target_fo = '/home/daniel/Projects/ppsEvolution/map_evolution/all_cigextra_maps/'

for fo in folders:
    last_bests = [fi for fi in os.listdir(fo) if "best.csv" in fi]
    for fi in last_bests:
        copyfile(fo + fi, target_fo + fi)
