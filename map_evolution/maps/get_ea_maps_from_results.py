from shutil import copy2
import os
from common.common import get_specific_files_in_folder, index_to_target


def make_tag(match, time, run):
    return "{0}-time_{1}-run_{2}".format(match, time, run)


crs_files_to_retrieve = [("crs_best_change", make_tag("Sniper_Sniper", "1", 11)),
                         ("crs_worst_change", make_tag("Sniper_Sniper", "0.085", 17)),

                         ("crs_best_gt1", make_tag("Spy_Spy", "0.085", 12)),
                         ("crs_best_gt2", make_tag("Spy_Spy", "0.085", 13)),
                         ("crs_best_gt3", make_tag("Pyro_Pyro", "0.315", 7)),
                         ("crs_worst_gt", make_tag("Spy_Heavy", "0.085", 18)),

                         ("crs_best_pred1", make_tag("Heavy_Pyro", "0.315", 12)),
                         ("crs_best_pred2", make_tag("Heavy_Heavy", "1", 2)),
                         ("crs_worst_pred", make_tag("Sniper_Sniper", "0.085", 17))
                         ]

mut_files_to_retrieve = [("mut_best_change", make_tag("Scout_Spy", "1", 13)),
                         ("mut_worst_change", make_tag("Spy_Spy", "0.085", 15)),

                         ("mut_best_gt", make_tag("Scout_Spy", "1", 13)),
                         ("mut_worst_gt1", make_tag("Scout_Pyro", "0.085", 14)),
                         ("mut_worst_gt2", make_tag("Scout_Heavy", "0.085", 1)),

                         ("mut_best_pred1", make_tag("Pyro_Pyro", "0.315", 8)),
                         ("mut_best_pred2", make_tag("Sniper_Pyro", "0.085", 8)),
                         ("mut_worst_pred", make_tag("Scout_Pyro", "0.085", 14))
                         ]


def copy_ea_files(file_info, folder):
    for finfo in file_info:
        dest_f = os.getcwd() + "/map_evolution/maps/ea_map_data/{0}/".format(finfo[0])
        if not os.path.isdir(dest_f):
            os.mkdir(dest_f)
        tag = finfo[1]
        files = get_specific_files_in_folder(folder, tag)
        for src in files:
            dest = dest_f
            copy2(src, dest)


if __name__ == "__main__":
    crs_folder = os.getcwd() + "/map_evolution/results/results_20runs_crs0.2/"
    mut_folder = os.getcwd() + "/map_evolution/results/results_20runs_crs0/"

    copy_ea_files(crs_files_to_retrieve, crs_folder)
    copy_ea_files(mut_files_to_retrieve, mut_folder)