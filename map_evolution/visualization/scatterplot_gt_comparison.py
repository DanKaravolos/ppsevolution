from __future__ import print_function
import matplotlib.pyplot as plt
# from pylab import *
# import seaborn
import pandas as pd
import os
import numpy as np
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
interesting_columns = ["match", "target time", "initial time", "pred time", "gt time",
                       "initial score", "pred score", "gt score"]
plot_to_name = ["short", "medium", "long"]
markers = ['o', '>', 'd', 's']
colors = ['xkcd:grey', 'xkcd:royal purple', 'xkcd:orange', 'xkcd:green']


def parse_input(input_file):
    df = pd.read_csv(input_file)
    df.sort_values(by=['match'])
    times = []
    scores = []
    for t in [0.085, 0.315, 1]:
        all_time_data = df.loc[df['target_time'] == t]

        time = all_time_data.loc[:, "match":"gt time"]
        score = all_time_data.loc[:, ["match", "initial score", "pred score", "gt score"]]
        times.append(time)
        scores.append(score)
    return times, scores


def plot_legend(result_folder):
    lfig = plt.figure(figsize=(1, 1))
    lax = plt.gca()
    plt.rcParams['font.size'] = '42'
    plot_names = ["intent", "initial", "predicted", "groundtruth"]
    # legend_handles = [mpatches.Patch(label=plot_names[l], fc=colors[l], edgecolor='k', lw=1) for l in
    #                   range(len(plot_names))]
    # lax.legend(loc=(0, 0.5), handles=legend_handles, ncol=4, fancybox=False, edgecolor='w')

    # intent = plt.Circle((0,0), fc=colors[0], label=plot_names[0])
    # initial = mpatches.RegularPolygon((0,0), 3, fc=colors[1], label=plot_names[1])
    # pred = mpatches.RegularPolygon((0, 0), 4, fc=colors[2], label=plot_names[2])
    # gt = plt.Rectangle((0, 0), 5,5, fc=colors[3], label=plot_names[3])
    legend_shapes = [Line2D(range(1), range(1), color="white", marker=markers[p], markersize=44,
                            markerfacecolor=colors[p]) for p in range(len(plot_names))]

    plt.legend(legend_shapes, plot_names, ncol=4, edgecolor='w')
    lax.axis('off')
    plt.savefig(result_folder + "legend.png", bbox_inches='tight')


def make_scatter(times, scores, result_file, show, save):
    plt.figure()
    plt.subplot(211)
    plt.grid()
    plt.ylim([0, 1])
    ax = plt.gca()
    matches = times.values[:, 0]
    nr_dimensions = len(times.iloc[0]) - 1
    for i in range(0, nr_dimensions):
        # print("times {0}".format(i))
        plt.scatter(np.arange(len(matches)), times.values[:, i + 1],
                    marker=markers[i], zorder=5+i, color=colors[i], s=60)
    ax.set_xticks(range(0, len(matches)))
    ax.set_xticklabels([])
    y_ticks = [0, 0.25, 0.5, 0.75, 1]
    ax.set_yticks(y_ticks)

    plt.subplot(212)
    plt.grid()
    plt.ylim([0, 1])
    ax = plt.gca()
    # matches = scores.values[:, 0]
    plt.scatter(np.arange(len(scores)), [0.5] * len(scores), marker=markers[0], color=colors[0])
    for i in range(0, nr_dimensions - 1):
        plt.scatter(np.arange(len(matches)), scores.values[:, i + 1],
                    marker=markers[i + 1], zorder=10+i, color=colors[i + 1], s=60)
    ax.set_xticks(range(0, len(matches)))
    ax.set_xticklabels([])
    ax.set_yticks(y_ticks)
    # ax.set_xticklabels(matches, rotation=40, ha='right')
    if save:
        plt.savefig(result_file, bbox_inches='tight')
    if show:
        plt.show()


def main(input_file, result_folder, tag, show=False, save=True):
    times, scores = parse_input(input_file)
    change_font_settings()
    for plot_num in range(len(times)):
    # plot_num = 0
        plot_name = result_folder + tag + "-{}.png".format(plot_to_name[plot_num])
        make_scatter(times[plot_num], scores[plot_num], plot_name, show, save)
    plot_legend(result_folder)


def change_font_settings():
    params = {
              # 'axes.axisbelow': 'true',
              # 'font.family': 'sans-serif',  # sans-serif
              # 'font.serif': 'Times New Roman',  # Helvetica Neue
              'font.weight': 'normal',
              # 'axes.labelweight': 'normal',
              'font.size': '16',
              # 'text.usetex': 'false',
              }  # this sets the radial lines
    plt.rcParams.update(params)

    plt.rcParams['font.serif'].insert(0, 'Times New Roman')
    # plt.rcParams['font.sans-serif'].insert(0, 'Arial')
    plt.rcParams['font.family'] = 'serif'

if __name__ == "__main__":
    gt_folder = os.getcwd() + "/map_evolution/gt_comparison/"
    gt_file = gt_folder + "crs0.2-gt_comp_means-merged.csv"
    result_tag = "recomb"
    main(gt_file, gt_folder + "scatterplots/", result_tag)
