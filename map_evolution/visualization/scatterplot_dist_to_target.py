import matplotlib.pyplot as plt
import os
import pandas as pd
import numpy as np
# from matplotlib.colors import ListedColormap
#
# colors = ['xkcd:pumpkin', "xkcd:bright sky blue", 'xkcd:light green',
#           'salmon', 'grey', 'xkcd:pale grey']
# cmap = ListedColormap(colors)


def plot_scatter(match_names, values, target_time, include_initial):
    values = np.asarray(values)
    values = np.nan_to_num(values)

    match_ids = set(match_names)
    colormap = plt.cm.nipy_spectral  #gist_ncar, nipy_spectral, Set1,Paired, tab20, gist_rainbow
    colors = colormap(np.linspace(0, 1, len(match_ids)), alpha=0.9)
    # colors = plt.cm.tab20c(np.arange(0, 1, 1.0/len(match_ids)))
    color_dict = dict(zip(sorted(match_ids), colors))

    plt.figure()
    plt.subplot(121)
    for i in range(len(values)):
        plt.scatter(values[i, 4], values[i,2], c=color_dict[match_names[i]])
    # plt.scatter([0.5,0.5,0.5], [0.085, 0.315, 1], c="#000000")
    plt.scatter(0.5, target_time, c="#000000", marker='x')
    plt.xlim([0, 1])
    plt.ylim([0, 1.1])
    plt.ylabel("Time")
    plt.xlabel("Score")

    plt.subplot(122)
    for i in range(len(values)):
        plt.scatter(values[i, 3], values[i, 1], c=color_dict[match_names[i]])
    # plt.scatter([0.5,0.5,0.5], [0.085, 0.315, 1], c="#000000")
    plt.scatter(0.5, target_time, c="#000000", marker='x')

    plt.xlim([0, 1])
    plt.ylim([0, 1.1])
    plt.ylabel("Time")
    plt.xlabel("Score")


    markers = [plt.Line2D([0, 0], [0, 0], color=color_dict[color], marker='o', linestyle='') for color in sorted(color_dict.keys())]
    plt.legend(markers, sorted(color_dict.keys()), numpoints=1)
    # plt.legend()
    plt.show()


if __name__ == "__main__":
    data_folder = os.getcwd() + "/map_evolution/gt_comparison/"
    data_file = data_folder + "results_20runs_crs0-gt_comp_per_map-euclid.csv"

    df = pd.read_csv(data_file, header=0)
    # match,	 # target_time,		initial time,		gt time,	initial score,		gt score,
    subset1 = df.iloc[:, [0,1,3,5,6,8]]
    target_time = 1
    subset2 = [s[1:] for s in subset1.values if s[1] == target_time]
    matches = [s[0] for s in subset1.values if s[1] == target_time]
    plot_scatter(matches, subset2, target_time, include_initial=False)