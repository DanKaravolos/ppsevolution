import matplotlib.pyplot as plt
import seaborn
import os
import numpy as np
from common.common import get_files_in_folder


def plot_all_in_folder(folder):
    files = get_files_in_folder(folder)
    target_folder = folder + "fitness_graphs/"
    if not os.path.exists(target_folder):
        os.mkdir(target_folder)

    for logfile in files:
        plot_name = logfile.rsplit("/", 1)[1]
        with open(logfile, 'r') as inpt:
            lines = inpt.readlines()
            data = []
            for line in lines[1:]:
                elems = line.rstrip().split(",")
                tup = [float(el) for el in elems]  # gen, min, max, avg, std
                data.append(tup)
            np_data = np.asarray(data)
            plot_min_and_avg(np_data, target_folder, plot_name, save=True)


def plot_min_and_avg(data, target_folder, plot_name, save=True, show=False):
    x = data[:, 0]
    minf = data[:, 1]
    # avgf = data[:, 3]
    plt.figure()
    ax = plt.gca()
    line_min = ax.plot(x, minf)
    # line_avg = ax.plot(x, avgf)
    plt.legend(['best fitness (min)'])
    plt.ylim([0, 1])
    plt.xlabel("Generations")

    if save:
        plt.savefig(target_folder + plot_name + '.png', bbox_inches='tight')
    if show:
        plt.show()


if __name__ == "__main__":
    target_folder = os.getcwd() + "/map_evolution/results/results_20runs_crs0/"
    plot_all_in_folder(target_folder)