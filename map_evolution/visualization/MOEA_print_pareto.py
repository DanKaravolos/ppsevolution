from __future__ import print_function
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from tqdm import tqdm


def parse_pop_file(input_file, use_game_dims):
    par = []
    npar = []

    with open(input_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines:
            if line.startswith("num"):
                continue
            elems = line.rstrip().split(",")
            if use_game_dims:
                coords = [float(elems[4]), float(elems[5])]
            else:
                coords = [float(elems[2]), float(elems[3])]
            if elems[1] == "pareto":
                par.append(coords)
            else:
                npar.append(coords)
    print("parsed file: ", input_file)
    print("Pareto: {0}, Non-Pareto: {1}".format(len(par), len(npar)))
    return par, npar


def plot_pareto(pareto, non_pareto, use_game_dims, output_file=""):
    colors = ['xkcd:green', 'xkcd:blue']

    plt.figure()
    plt.grid()
    ax = plt.gca()
    py = [p[0] for p in pareto] #+ np.random.normal(0, 0.00001, len(pareto))
    px = [p[1] for p in pareto] #+ np.random.normal(0, 0.00001, len(pareto))
    npy = [p[0] for p in non_pareto] #+ np.random.normal(0, 0.00001, len(non_pareto))
    npx = [p[1] for p in non_pareto] #+ np.random.normal(0, 0.00001, len(non_pareto))
    alpha = 0.25  # 1.0 / (len(pareto))
    plt.scatter(px, py, color=colors[0], alpha=alpha, label="Pareto Front", zorder=8)
    plt.scatter(npx, npy, color=colors[1], alpha=alpha, label="Non-Pareto", zorder=4)
    plt.legend()

    if use_game_dims:
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.xlabel("Score")
        plt.ylabel("Time")
    else:
        # plt.xlim([0, 0.1])
        # plt.ylim([0, 0.1])
        plt.xlabel("Sq.error Score")
        plt.ylabel("Sq.error Time")

    if output_file:
        plt.savefig(output_file, bbox_inches='tight')


# game_dims = normalized duration and score, not game_dims = fitness
def single_main(pareto_file, use_game_dims, show=False):
    pare, npare = parse_pop_file(pareto_file, use_game_dims=use_game_dims)
    if use_game_dims:
        png_file = pareto_file.replace(".csv", "_gd.png")
    else:
        png_file = pareto_file.replace(".csv", ".png")
    plot_pareto(pare, npare, use_game_dims=use_game_dims, output_file=png_file)
    if show:
        plt.show()


# game_dims = normalized duration and score, not game_dims = fitness
def multi_main(pareto_dir, use_game_dims, show=False):
    files = [pareto_dir + f for f in os.listdir(pareto_dir) if "fitnesses.csv" in f]
    for pareto_file in tqdm(files):
        single_main(pareto_file, use_game_dims, show)

if __name__ == "__main__":
    par_file = os.getcwd() + "/map_evolution/moea_test/moea_test-P_20-gen_100-mut_0.10-crs_0.00-fitnesses.csv"
    par_dir = os.getcwd() + "/map_evolution/moea_paper_test2/"
    # single_main(par_file)
    multi_main(par_dir, False)  # game_dims = normalized duration and score, not game_dims = fitness

