import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from map_evolution.visualization.map_to_cnn_view import image_from_np_array


# color_dict = {
#     '0': "#FFFFFF",
#     '1': "#808080",
#     '2': "#000000",
#     'base1': "#ff8000",
#     'base2': "#800080",
#     'armor': "#000080",
#     'health': "#ff0000",
#     'damage': "#008080",
# }

old_color_dict = {
    '0': [255, 255, 255],
    '1': [128, 128, 128],
    '2': [0, 0, 0],
    'base1': [255, 128, 0],
    'base2': [128, 0, 128],
    'armor': [0, 0, 128],
    'health': [255, 0, 0],
    'damage': [0, 128, 128],
    'stairs': [192, 192, 192],
    '1armor': [0, 0, 64],
    '1health': [128, 0, 0],
    '1damage': [0, 64, 64],
}

color_dict = {
    '0': [255, 255, 255],
    '1': [128, 128, 128],
    '2': [0, 0, 0],
    'base1': [255, 128, 0],
    'base2': [128, 0, 128],
    'armor': [75, 75, 255],
    'health': [255, 0, 0],
    'damage': [44, 176, 122],
    'stairs': [192, 192, 192],
    '1armor': [23, 23, 166],
    '1health': [145, 30, 30],
    '1damage': [18, 104, 50],
}

tile_dict = {
    '0': color_dict['0'],
    '1': color_dict['1'],
    '2': color_dict['2'],
    '0Y': color_dict['base1'],
    '0Z': color_dict['base2'],
    'Y': color_dict['base1'],
    'Z': color_dict['base2'],
    '0S': color_dict['stairs'],
    '0A': color_dict['armor'],
    '1A': color_dict['1armor'],
    '0H': color_dict['health'],
    '1H': color_dict['1health'],
    '0D': color_dict['damage'],
    '1D': color_dict['1damage']
}


# returns an RGB image from an asci map input. Optionally save or show the results
def image_from_asci_map(asci_map, new_size=316, border_size=2, show=False, show_now=False,
                        save=False, target_path=None, return_resized=False):
    rows, columns = asci_map.shape
    img = np.zeros((rows, columns, 3), dtype=np.uint8)

    # manually set the bases
    img[-5:, 0:5] = color_dict['base1']
    img[0:5, -5:] = color_dict['base2']

    # color pixels based on asci map
    for r in range(rows):
        for c in range(columns):
            img[r, c] = tile_dict[asci_map[r, c]]

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # resize based on user input
    big_img = cv2.resize(img, (new_size, new_size), interpolation=cv2.INTER_NEAREST)

    # add black border
    if border_size > 0:
        b_size = border_size
        big_img = cv2.copyMakeBorder(big_img, b_size, b_size, b_size, b_size, cv2.BORDER_CONSTANT, value=[0, 0, 0])
        # print(big_img.shape)

    # Save the image to disk and or show result
    if save:
        # target_path = image_path.replace(".png", "_{0}b.png".format(new_size + 2 * b_size))
        # target_path = "/home/daniel/Projects/ppsEvolution/map_evolution/maps/test.png"

        cv2.imwrite(target_path, big_img)
    if show or show_now:
        plt.figure(figsize=(4, 4))
        big_img = cv2.cvtColor(big_img, cv2.COLOR_RGB2BGR)
        plt.imshow(big_img, 'gray')
        plt.tight_layout()
        plt.xticks([])
        plt.yticks([])
        if show_now:
            plt.show()
    if return_resized:
        return big_img
    else:
        return img


def single_map():
    #asci_map = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/map_antonios_valid_empty_bases.csv")
    asci_map = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/Level_2_Run_0_Map.txt")
    tp = "/home/daniel/Projects/ppsEvolution/map_evolution/maps/Level_2_Run_0_Map.png"
    ind = MapIndividual(asci_map, 4, 4)
    ind.visualize_bases()
    image_from_asci_map(ind.to_matrix(), new_size=316, border_size=2, show_now=True, save=True, target_path=tp)


def multiple_maps():
    dir = "/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/"
    # dir = "/home/daniel/Projects/ppsEvolution/data/maps/pcg_workshop/"
    maps = [dir + fi for fi in os.listdir(dir) if ".csv" in fi]
    for map_file in maps:
        tp = map_file.replace(".csv", ".png")
        print(tp)
        asci_map = csv_to_array(map_file)
        ind = MapIndividual(asci_map, 4, 4)
        ind.visualize_bases()
        image_from_asci_map(ind.to_matrix(), new_size=316, border_size=2, show_now=False, save=True, target_path=tp)

if __name__ == "__main__":
    from common.common import csv_to_array
    from map_evolution.evolution.individual.MapIndividual import MapIndividual

    single_map()
    # multiple_maps()

