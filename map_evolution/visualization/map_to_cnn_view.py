import cv2
import numpy as np
import matplotlib.pyplot as plt

cnn_view_color_dict = {
    0: [0, 0, 0],
    1: [255, 255, 255],
}

#ground_map, first_floor_map, second_floor_map, armor_map, cover_map, dd_map, hp_map, stair_map
old_color_dict = {
    '1,0,0,0,0,0,0,0': [255, 255, 255],
    '0,1,0,0,0,0,0,0': [128, 128, 128],
    '0,0,1,0,0,0,0,0': [0, 0, 0],
    'base1': [255, 128, 0],
    'base2': [128, 0, 128],
    '1,0,0,1,0,0,0,0': [0, 0, 128],
    '1,0,0,0,0,0,1,0': [255, 0, 0],
    '1,0,0,0,0,1,0,0': [0, 128, 128],
    '1,0,0,0,0,0,0,1': [192, 192, 192],
    '0,1,0,1,0,0,0,0': [0, 0, 64],
    '0,1,0,0,0,0,1,0': [128, 0, 0],
    '0,1,0,0,0,1,0,0': [0, 64, 64],
}

#ground_map, first_floor_map, second_floor_map, armor_map, cover_map, dd_map, hp_map, stair_map
color_dict = {
    '1,0,0,0,0,0,0,0': [255, 255, 255],
    '0,1,0,0,0,0,0,0': [128, 128, 128],
    '0,0,1,0,0,0,0,0': [0, 0, 0],
    'base1': [255, 128, 0],
    'base2': [128, 0, 128],
    '1,0,0,1,0,0,0,0': [75, 75, 255],
    '1,0,0,0,0,0,1,0': [255, 0, 0],
    '1,0,0,0,0,1,0,0': [44, 176, 122],
    '1,0,0,0,0,0,0,1': [192, 192, 192],
    '0,1,0,1,0,0,0,0': [23, 23, 166],
    '0,1,0,0,0,0,1,0': [145, 30, 30],
    '0,1,0,0,0,1,0,0': [18, 104, 50],
}



def encoding_to_string(eight_layers):
    return ",".join(["{:.0f}".format(layer) for layer in eight_layers])


# returns an RGB image from a numpy array input. Optionally save or show the results
def image_from_np_array(np_array, new_size=316, border_size=2, show=False, show_now=False,
                        save=False, target_path=None, visualize_bases=True):
    rows, columns = np_array.shape[0:2]
    img = np.zeros((rows, columns, 3), dtype=np.uint8)

    # manually set the bases
    if visualize_bases:
        img[-5:, 0:5] = color_dict['base1']
        img[0:5, -5:] = color_dict['base2']

    # color pixels based on asci map
    for r in range(rows):
        for c in range(columns):
            # maybe we are in a base position. Check whether we need to change anything.
            if np.all(img[r, c] == color_dict['base1']) or np.all(img[r, c] == color_dict['base2']):
                tile_code = encoding_to_string(np_array[r, c])
                if tile_code =='1,0,0,0,0,0,0,1':  # if we have stairs, override the spawn base colors. if not, continue
                    img[r, c] = color_dict.get(tile_code)
            else:
                # normal tile coloring.
                img[r, c] = color_dict.get(encoding_to_string(np_array[r, c]), [64, 64, 64])

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # resize based on user input
    big_img = cv2.resize(img, (new_size, new_size), interpolation=cv2.INTER_NEAREST)

    # add black border
    if border_size > 0:
        b_size = border_size
        big_img = cv2.copyMakeBorder(big_img, b_size, b_size, b_size, b_size, cv2.BORDER_CONSTANT, value=[0, 0, 0])
        # print(big_img.shape)

    # Save the image to disk and or show result
    if save:
        cv2.imwrite(target_path, big_img)
    if show or show_now:
        plt.figure(figsize=(4, 4))
        big_img = cv2.cvtColor(big_img, cv2.COLOR_RGB2BGR)
        plt.imshow(big_img, 'gray')
        plt.tight_layout()
        plt.xticks([])
        plt.yticks([])
        if show_now:
            plt.show()
    return img


# returns an RGB image of each layer of an X-layer numpy input. Optionally save or show the results
def cnn_view_image_from_np_array(np_array, new_size=316, border_size=2, show=False, show_now=False,
                        save=False, target_path=None):
    for layer in range(np_array.shape[-1]):
        rows, columns = np_array.shape[0:2]
        img = np.zeros((rows, columns, 3), dtype=np.uint8)

        # color pixels based on asci map
        for r in range(rows):
            for c in range(columns):
                img[r, c] = cnn_view_color_dict[np_array[r, c, layer]]

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # resize based on user input
        big_img = cv2.resize(img, (new_size, new_size), interpolation=cv2.INTER_NEAREST)

        # add black border
        if border_size > 0:
            b_size = border_size
            big_img = cv2.copyMakeBorder(big_img, b_size, b_size, b_size, b_size, cv2.BORDER_CONSTANT, value=[0, 0, 0])
            # print(big_img.shape)

        # Save the image to disk and or show result
        if save:
            if "CNN" not in target_path:
                target_path = target_path.replace(".png", "-CNN_{0}.png".format(layer))
            else:
                target_path = target_path.replace("-CNN_{0}.png".format(layer - 1), "-CNN_{0}.png".format(layer))
            # target_path = "/home/daniel/Projects/ppsEvolution/map_evolution/maps/test.png"

            cv2.imwrite(target_path, big_img)
        if show or show_now:
            plt.figure(figsize=(4, 4))
            big_img = cv2.cvtColor(big_img, cv2.COLOR_RGB2BGR)
            plt.imshow(big_img, 'gray')
            plt.tight_layout()
            plt.xticks([])
            plt.yticks([])
            if show_now:
                plt.show()
    return img


if __name__ == "__main__":
    from common.common import csv_to_array
    from map_evolution.evolution.individual.MapIndividual import MapIndividual
    from common.parse_csv_maps import parse_array

    # asci_map = csv_to_array("/home/daniel/Projects/ppsEvolution/maps/data/map_antonios_valid_empty_bases.csv")
    # tp = "/home/daniel/Projects/ppsEvolution/map_evolution/maps/map_antonios_valid_empty_bases.png"

    asci_map = csv_to_array("/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_1_Run_0_Map.csv")
    tp = "/home/daniel/Projects/ppsEvolution/data/maps/custom_maps/Custom_1-CNN_0.png"

    ind = MapIndividual(asci_map, 4, 4)
    cnn_view_image_from_np_array(parse_array(ind.to_matrix(), neighbor_val=0), new_size=316, border_size=2, show_now=True, save=True,
                        target_path=tp)

    image_from_np_array(parse_array(ind.to_matrix(), neighbor_val=0), new_size=316, border_size=2, show_now=True)
