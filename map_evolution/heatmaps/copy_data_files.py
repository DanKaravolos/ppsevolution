from shutil import copy2
import os
from common.common import get_specific_files_in_folder


crs_files_to_retrieve = [("crs_best_change", 38),
                         ("crs_best_pred1", 394),
                         ("crs_best_pred2", 474),
                         ("crs_best_pred3", 774),
                         ("crs_worst_change", 758),
                         ("crs_worst_pred", 546)
                         ]

mut_files_to_retrieve = [("mut_best_change", 288),
                         ("mut_best_pred1", 710),
                         ("mut_best_pred2", 135),
                         ("mut_worst_change", 198),
                         ("mut_worst_change", 148),
                         ("mut_worst_pred", 857)
                         ]


def copy_gt_files(file_info, folder):
    run_folders = [folder + fo for fo in os.listdir(folder) if os.path.isdir(folder + fo)]

    for finfo in file_info:
        dest_f = os.getcwd() + "/map_evolution/heatmaps/gt_map_data/{0}/".format(finfo[0])
        if not os.path.isdir(dest_f):
            os.mkdir(dest_f)
        tag = "Level_{0}_".format(finfo[1])
        for fo in run_folders:
            gt_num = fo[-1]
            fold = fo + "/Level1/"
            files = get_specific_files_in_folder(fold, tag)
            for src in files:
                dest = dest_f + src.rsplit("/",1)[1]
                dest = dest.replace(".txt", "-{0}.txt".format(gt_num))
                copy2(src, dest)


if __name__ == "__main__":
    crs_folder = os.getcwd() + "/map_evolution/groundtruths/GT_crs0.2/"
    mut_folder = os.getcwd() + "/map_evolution/groundtruths/GT_crs0/"

    copy_gt_files(crs_files_to_retrieve, crs_folder)
    copy_gt_files(mut_files_to_retrieve, mut_folder)