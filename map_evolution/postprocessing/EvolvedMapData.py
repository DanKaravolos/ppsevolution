import os
from common.EvolvedIndividualData import EvolvedIndividual
from character_evolution.evolution.weapon_parameters import tf2_weapon_param_dict
from common.parse_csv_maps import csv_to_array
from collections import defaultdict
from warnings import warn
warn("Assuming that the original map is in: project main/data/maps/custom_maps/")

possible_tiles = ["0", "1", "2", "S", "0A", "1A", "0D", "1D", "0H", "1H", "A", "D", "H"]
sorted_tiles = sorted(possible_tiles)


class EvolvedMap(EvolvedIndividual):
    def __init__(self, initial_map, class1, class2, fitness, game_metric_names, game_metrics, targets, infeasible, evolved_map_loc,
                 from_classification=False):
        # set up class/general metrics
        param_list_c1 = tf2_weapon_param_dict[class1]
        param_list_c2 = tf2_weapon_param_dict[class2]

        super().__init__(initial_map, param_list_c1, param_list_c2, fitness, game_metric_names, game_metrics, targets,
                         orig_c1=class1, orig_c2=class2, from_classification=from_classification)

        # set up map specfic metrics
        self.infeasible = infeasible
        self.evolved_map_loc = evolved_map_loc
        self.evolved_map_arr = csv_to_array(self.evolved_map_loc, map_starts_at_first_line=True)

        try:
            self.initial_map_loc = os.path.join(os.getcwd(), "data/maps/custom_maps/") + initial_map + "_Run_0_Map.csv"
        except FileNotFoundError:
            self.initial_map_loc = os.path.join(os.getcwd(), os.pardir, "data/maps/custom_maps/") + initial_map + "_Run_0_Map.csv"

        self.initial_map_arr = csv_to_array(self.initial_map_loc)
        self.tile_diff_w_initial = []
        self.tile_counts = {}
        self.initial_tile_counts = {}
        self.tc_diff_w_initial = {}
        self.compute_map_metrics()
        # print("parse")

    def compute_map_metrics(self):
        self.tile_diff_w_initial = compute_binary_distance(self.initial_map_arr, self.evolved_map_arr)
        self.tile_counts = count_tile_types(self.evolved_map_arr)
        self.initial_tile_counts = count_tile_types(self.initial_map_arr)
        self.tc_diff_w_initial = compute_tc_difference(self.tile_counts, self.initial_tile_counts)

    def get_header(self, external_line_number=True):
        header = super().get_header(external_line_number)
        map_elems = ["tile diff to initial", ",".join([key for key in sorted_tiles])
                     , ",".join(["diff {0}".format(key) for key in sorted_tiles])]
        header = header + "," + ",".join(map_elems)
        return header

    def to_string(self, add_new_line=True):
        line = super().to_string(False)
        map_elems = [self.tile_diff_w_initial] + [self.tile_counts[key] for key in sorted_tiles]\
                    + [self.tc_diff_w_initial[key] for key in sorted_tiles]
                    # + [self.initial_tile_counts[key] for key in sorted_tiles]
        line = line + "," + ",".join(["{0:.1f}".format(m) for m in map_elems])

        if add_new_line:
            line += "\n"
        return line


def compute_binary_distance(initial_map, evolved_map):
    total_rows, total_cols = initial_map.shape
    tile_difference_count = 0
    tile_same_sanity_check = 0
    for row in range(total_rows):
        for col in range(total_cols):
            tile1 = initial_map[row][col]
            tile2 = evolved_map[row][col]
            if tile1 == tile2 or tile1 + "Y" == tile2 or tile1 + "Z" == tile2 or tile2 + "Y" == tile1 or tile2 + "Z" == tile1:
                tile_same_sanity_check += 1
            else:
                tile_difference_count += 1
    if tile_difference_count + tile_same_sanity_check != total_cols * total_rows:
        print("ERROR in difference function!!")
        print("Do stupidity check!")
        return -999
    return tile_difference_count


def count_tile_types(map_arr):
    count_dict = defaultdict(int)
    for row in map_arr:
        for col in row:
            if len(col) == 2:
                if "S" in col:
                    count_dict["S"] += 1
                elif "Y" in col or "Z" in col:
                    count_dict["0"] += 1
                else:
                    count_dict[col[0]] += 1
                    count_dict[col[1]] += 1
                    count_dict[col] += 1
            else:
                count_dict[col] += 1
    return count_dict


def compute_tc_difference(tc_dict1, tc_dict2):
    tc_diff = defaultdict(int)
    for tile in possible_tiles:
        tc_diff[tile] = tc_dict1[tile] - tc_dict2[tile]
    return tc_diff
