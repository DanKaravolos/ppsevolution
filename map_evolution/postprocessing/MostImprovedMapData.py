from collections import defaultdict
from common.MostImprovedData import MostImprovedData
from map_evolution.postprocessing.EvolvedMapData import compute_binary_distance, sorted_tiles


class MostImprovedMapData(MostImprovedData):
    def __init__(self, most_improved_dict):
        super().__init__(most_improved_dict)

        # fields of this class:

        self.map_data = self.get_map_data()
        self.pairwise_dist = self.compute_pairwise_distances()

        # call functions for processing data

    def get_map_data(self):
        map_dict = defaultdict(lambda: defaultdict(dict))
        for m in self.map_names:
            for t in self.times:
                # structure of data: run_nr, improvement, comparison_data
                current = self.data[m][t][2].ea_data.data[0]  # get the data for the evolved map
                map_dict[m][t] = current
        return map_dict

    def compute_pairwise_distances(self):
        dist_dict = defaultdict(lambda: defaultdict(dict))
        # computing for all maps: the pairwise distances from each evolved map for a certain time and the initial.
        for m in self.map_names:
            for t in self.times:
                # structure of data: run_nr, improvement, comparison_data
                current = self.data[m][t][2].ea_data.data[0]  # get the data for the evolved map
                curr_map = current.evolved_map_arr
                # dist to initial:
                dist_to_initial = current.tile_diff_w_initial
                # initial_map = current.initial_map_arr
                # check_init_dist = compute_binary_distance(initial_map, curr_map)
                dist_dict[m][t]["Initial"] = dist_to_initial

                # dist to evolved maps for different durations:
                for time2 in self.times:
                    other_map = self.data[m][time2][2].ea_data.data[0].evolved_map_arr
                    dist_to_other = compute_binary_distance(curr_map, other_map)
                    dist_dict[m][t][time2] = dist_to_other
        return dist_dict

    def get_header(self, newline=False):
        header = ["initial map", "duration", "dist to initial"]
        dist_elems = ["dist to {0}".format(t) for t in self.times]
        map_elems = [",".join(["diff {0}".format(key) for key in sorted_tiles])
                     , ",".join(["total {0}".format(key) for key in sorted_tiles])]
        line = ",".join(header + dist_elems + map_elems)
        if newline:
            line += "\n"
        return line

    def to_string(self):
        others = ["Initial", "Short", "Medium", "Long"]
        lines = []
        for m in self.map_names:
            for t in self.times:
                base = [m, t]
                distances = [str(self.pairwise_dist[m][t][o]) for o in others]
                t_diff = [str(self.map_data[m][t].tc_diff_w_initial[tile]) for tile in sorted_tiles]
                t_counts = [str(self.map_data[m][t].tile_counts[tile]) for tile in sorted_tiles]
                line = ",".join(base + distances + t_diff + t_counts)
                lines.append(line)
        all_lines = "\n".join(lines)
        return all_lines