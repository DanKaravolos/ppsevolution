'''
This file parses results files and groundtruth summaries into a datastructure that contains all information necessary
for evaluating the results of evolution. Note that I am assuming that the results are presented in the same order as
the game_input file that was used to create the GT values. This means that I assume that the level numbers in the GT
file correspond to the order of the results in order to link the data in the two files. So: level_num = result_nr + 1.
If you get weird results, you might be violating that assumption.

More info about the pipeline:
After running evaluation, use common.concat_files_in_folder to concatenate the game_input and result files.
the resulting game_input file are used to get GroundTruth values from simulations, while the resulting result-file is
used here. After you have obtained the GT values, put the LogFiles folders in new folders in
the data folder of PyProcShooter (on windows) and run parse_summary_files.
Use the resulting summary PKL-file as the GT file here.
'''

import os
import numpy as np
from collections import defaultdict
import pickle
from map_evolution.postprocessing.EvolvedMapData import EvolvedMap
from map_evolution.postprocessing.EvolvedMapSummary import EvolvedMapSummary
from map_evolution.postprocessing.evaluate_grid_search import parse_result_file
from character_evolution.postprocessing.evaluate_evolved_weapons import parse_gt_file, concat_classes_and_gt
from common.evaluate_individual import *


def evaluate_results(result_file,gt_file, output_file, from_classification, nr_ea_targets, initial_class1, initial_class2, write=True):
    print("parsing result file")
    parsed_ea_maps = parse_result_file(result_file, nr_ea_targets, from_classification)
    dims = ('file', 'team0ratio', 'time')
    print("parsing and concatenating GT")
    num_to_gt_dict = parse_gt_file(gt_file, dims)
    empty_gt = concat_classes_and_gt(parsed_ea_maps, num_to_gt_dict)
    print("These data points have no GT:")
    for dp in empty_gt:
        print(dp)

    print("Creating summary")
    pds = create_per_duration_summary(parsed_ea_maps, output_file, EvolvedMapSummary, write)
    pms = create_per_map_summary(parsed_ea_maps, output_file, EvolvedMapSummary, write)
    pdpms = create_per_duration_per_map_summary(parsed_ea_maps, output_file, EvolvedMapSummary, write)
    return parsed_ea_maps, pds, pms, pdpms


def main():
    # result_file = os.getcwd() + "/Results/Scout_Heavy_Best/p100/summary-tog_p100-result.csv"
    # gt_file = os.getcwd() + "/Results/Scout_Heavy_Best/p100/tog_p100-GT_summary.pkl"  # 10% unfinished matches
    # output_file = os.getcwd() + "/Results/Scout_Heavy_Best/p100/tog_p100-evaluation.csv"

    # result_file = os.getcwd() + "/Results/Scout_Heavy_Best/p20/summary-tog_p20-result.csv"
    # gt_file = os.getcwd() + "/Results/Scout_Heavy_Best/p20/tog_p20-GT_summary.pkl"  # 10% unfinished matches
    # output_file = os.getcwd() + "/Results/Scout_Heavy_Best/p20/tog_p20-evaluation.csv"

    # result_file = os.getcwd() + "/Results/Scout_Heavy_MO/results/MOEA-raw-results.csv"
    # gt_file = os.getcwd() + "/Results/Scout_Heavy_MO/results/GT_summary-Tog-P100-MO-MapGen.pkl"  # 10% unfinished matches
    # output_file = os.getcwd() + "/Results/Scout_Heavy_MO/results/MOEA-evaluation.csv"
    from_classification = False

    result_file = os.getcwd() + "/map_evolution/Results/ScoutHeavy-SOEA_MR/results/summary-SO-result.csv"
    gt_file = os.getcwd() + "/map_evolution/Results/map_evolution-SOEA-GT/GT_summary-map_evolution-SOEA-GT.pkl"  # 6% unfinished matches
    output_file = os.getcwd() + "/map_evolution/Results/ScoutHeavy-SOEA_MR/gt-evaluation.csv"

    nr_ea_targets = 2
    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    parsed_ea_maps, pds, pms, pdpms = evaluate_results(result_file, gt_file, output_file, from_classification, nr_ea_targets,
                                                       initial_class1, initial_class2)
    write_output(parsed_ea_maps, output_file)


if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main()
