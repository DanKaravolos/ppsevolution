import os
from map_evolution.postprocessing.evaluate_evolved_maps import evaluate_results
from map_evolution.postprocessing.MapComparisonData import MapComparisonData
from common.evaluate_tf2_gt import evaluate_original_classes
from common.TF2MatchData import TF2MatchSummary


time_targets = {"Short":0.14, "Medium": 0.36, "Long": 0.72}


def compare_evolved_with_tf_per_map_per_duration(evolved_pmpd, tf, out_fi):
    out_fi = out_fi.replace("evaluation", "tf_comparison_per_map_per_duration")
    with open(out_fi, 'w') as out:
        write_header = True
        for data_map, duration_dict in evolved_pmpd.items():
            tf_map_name = "custom_maps/" + data_map  # STUPID fix. but I feel lazy
            print("Adding 'custom_maps' to map name, so TF2Data understands")
            tf_data = tf[tf_map_name]
            for duration, summary in duration_dict.items():
                comparison = MapComparisonData(summary, tf_data)
                if write_header:
                    header = "map, duration," + comparison.get_header()
                    out.write(header + "\n")
                    write_header = False
                print("comparing... {0} : {1}".format(data_map, duration))
                line = ",".join([data_map, duration, comparison.to_string()])
                out.write(line + "\n")


def compare_evolved_with_tf_per_duration(evolved_pd, tf, out_fi):
    out_fi = out_fi.replace("evaluation", "tf_comparison_per_duration")
    with open(out_fi, 'w') as out:
        write_header = True
        tf2_datalist = []
        for dp in tf.values():
            tf2_datalist.extend(dp.data)
        tf_data = TF2MatchSummary(tf2_datalist)
        for duration, summary in evolved_pd.items():
            comparison = MapComparisonData(summary, tf_data)
            if write_header:
                header = "duration," + comparison.get_header()
                out.write(header + "\n")
                write_header = False

            print("comparing... {0}".format(duration))
            line = ",".join([duration, comparison.to_string()])
            out.write(line + "\n")


def main():
    result_folder = os.getcwd() + "/Results/"

    tf_gt_file = result_folder + "Original_TF_Matches/OriginalTF2_ScoutHeavy/GT_summary-OriginalTF2.pkl"
    tf_game_input_file = result_folder+ "Original_TF_Matches/OriginalTF2_ScoutHeavy/tf2_class_scout_vs_heavy-game_input.csv"
    tf_out_file = result_folder + "Original_TF_Matches/OriginalTF2_ScoutHeavy/OriginalTF2_ScoutHeavy-evaluation.csv"

    # ea_result_file = result_folder + "Scout_Heavy_Best/p100/summary-tog_p100-result.csv"
    # ea_gt_file = result_folder + "Scout_Heavy_Best/p100/tog_p100-GT_summary.pkl"  # 10% unfinished matches
    # ea_out_file = result_folder + "Scout_Heavy_Best/p100/tog_p100-evaluation.csv"
    #
    ea_result_file = result_folder + "Scout_Heavy_MO/results/MOEA-raw-results.csv"
    ea_gt_file = result_folder + "Scout_Heavy_MO/results/GT_summary-Tog-P100-MO-MapGen.pkl"
    ea_out_file = result_folder + "Scout_Heavy_MO/results/MOEA-evaluation.csv"


    # ea_result_file = result_folder + "Scout_Heavy_Best/p20/summary-tog_p20-result.csv"
    # ea_gt_file = result_folder + "Scout_Heavy_Best/p20/tog_p20-GT_summary.pkl"  # 10% unfinished matches
    # ea_out_file = result_folder + "Scout_Heavy_Best/p20/tog_p20-evaluation.csv"
    from_class = False

    initial_class1 = "Scout"
    initial_class2 = "Heavy"
    dims = ('file', 'team0ratio', 'time')
    ea_data, pds, pms, pdpms = evaluate_results(ea_result_file, ea_gt_file, ea_out_file, from_class, 2, initial_class1, initial_class2)
    tf_per_map = evaluate_original_classes(tf_game_input_file, tf_gt_file, tf_out_file, dims)

    compare_evolved_with_tf_per_map_per_duration(pdpms, tf_per_map, ea_out_file)
    compare_evolved_with_tf_per_duration(pds, tf_per_map, ea_out_file)
    print("done.")


if __name__ == "__main__":
    # Running the code from ppsEvolution/character_evolution/
    main()