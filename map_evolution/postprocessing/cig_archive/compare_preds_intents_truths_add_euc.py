from __future__ import print_function
import os
import numpy as np
import cPickle as pickle
from common.common import *

focus_matches = ["Scout_Scout", "Scout_Pyro", "Sniper_Pyro", "Sniper_Sniper"]


def key_with_max_val(d):
    """ a) create a list of the dict's keys and values; 
        b) return the key with the max value"""
    v = list(d.values())
    k = list(d.keys())
    return k[v.index(max(v))]


def parse_initial_preds(csv_file):
    pred_dict = {}
    with open(csv_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines[1:]:
            elems = line.rstrip().split(",")
            pred_dict[elems[0]] = [float(elems[1]), float(elems[3])]  # stdevs at 2 and 4
    return pred_dict


def compare_values(match_pred, match_gt, match_initial, add_euclidean):
    init_time = match_initial[0]
    init_score = match_initial[1]

    if add_euclidean:
        data_array = np.zeros((3, 20, 25))
    else:
        data_array = np.zeros((3, 20, 18))
    print("comparing...")
    for ttime in range(3):
        for level in range(20):
            intention = float(index_to_target[ttime])
            gt_time = np.mean(match_gt[ttime][level][0])
            gt_score = np.mean(match_gt[ttime][level][1])
            pred_time = match_pred[ttime][0][level]
            pred_score = match_pred[ttime][1][level]

            t_initial_intent = init_time - intention
            t_gt_intent = gt_time - intention
            t_pred_intent = pred_time - intention

            t_gt_initial = gt_time - init_time
            t_pred_initial = pred_time - init_time

            t_gt_pred = gt_time - pred_time

            s_initial_intent = init_score - 0.5
            s_gt_intent = gt_score - 0.5
            s_pred_intent = pred_score - 0.5

            s_gt_initial = gt_score - init_score
            s_pred_initial = pred_score - init_score

            s_gt_pred = gt_score - pred_score

            v_row = [init_time, pred_time, gt_time, init_score, pred_score, gt_score]
            data_row = [t_initial_intent, t_gt_intent, t_pred_intent, t_gt_initial, t_pred_initial, t_gt_pred,
                        s_initial_intent, s_gt_intent, s_pred_intent, s_gt_initial, s_pred_initial, s_gt_pred]

            if add_euclidean:
                euc_initial_intent = np.sqrt(t_initial_intent ** 2 + s_initial_intent ** 2)
                euc_gt_intent = np.sqrt(t_gt_intent ** 2 + s_gt_intent ** 2)
                euc_gt_initial = np.sqrt(t_gt_initial ** 2 + s_gt_initial ** 2)
                euc_gt_pred = np.sqrt(t_gt_pred ** 2 + s_gt_pred ** 2)

                euc_pred_intent = np.sqrt(t_pred_intent ** 2 + s_pred_intent ** 2)
                euc_pred_initial = np.sqrt(t_pred_initial ** 2 + s_pred_initial ** 2)

                euc_improvement = euc_gt_intent - euc_initial_intent

                euc_row = [euc_initial_intent, euc_gt_intent, euc_gt_initial, euc_gt_pred, euc_pred_intent,
                           euc_pred_initial, euc_improvement]
                data_array[ttime, level, :] = np.asarray(v_row + data_row + euc_row)
            else:
                data_array[ttime, level, :] = np.asarray(v_row + data_row)

    means_of_match = np.nanmean(data_array, axis=1)
    return data_array, means_of_match


def make_header(aggregated, add_euclidean):
    if aggregated:
        data_h = ["match,target_time"]
    else:
        data_h = ["match,target_time,run"]

    time_h = ["initial time", "pred time", "gt time"]
    score_h = ["initial score", "pred score", "gt score"]
    diff_h = ["time initial - target", "time gt - target", "time predicted - target", "time gt - initial",
              "time predicted - initial", "time gt - predicted",
               "score initial - target", "score gt - target", "score predicted - target", "score gt - initial",
               "score predicted - initial", "score gt - predicted"]
    if add_euclidean:
        euc_h = ["euc_initial_intent", "euc_gt_intent", "euc_gt_initial", "euc_gt_pred", "euc_pred_intent",
                 "euc_pred_initial", "diff between gt and init w.r.t. intent"]
        header = data_h + time_h + score_h + diff_h + euc_h
    else:
        header = data_h + time_h + score_h + diff_h
    header = ",".join(header)
    return header
#
#
# def get_best_and_worst_maps(metric_dict, header):
#     dimensions = [""]


def main(data_file, gt_file, add_euclidean, means_file="", result_file=""):
    with open(data_file, 'rb') as data_in:
        preds = pickle.load(data_in)
    with open(gt_file, 'rb') as data_in:
        gt_data = pickle.load(data_in)

    baselines = parse_initial_preds(os.getcwd() + "/map_evolution/groundtruths/GT_initial_map-parsed.csv")

    per_map_dict = {}
    mean_dict = {}

    compare_matches = focus_matches if "focus" in data_file else preds
    for match in compare_matches:
        print("Match: ", match)
        per_map, means = compare_values(preds[match], gt_data[match], baselines[match], add_euclidean)
        per_map_dict[match] = per_map
        mean_dict[match] = means

    if result_file:
        aggregated = False
        # header = make_header(aggregated, add_euclidean)
        # baw = get_best_and_worst_maps(per_map_dict, header)
        write_results(per_map_dict, result_file, add_euclidean, aggregated)
    if means_file:
        aggregated = True
        # header = make_header(aggregated, add_euclidean)
        # baw = get_best_and_worst_maps(mean_dict, header)
        write_results(mean_dict, means_file, add_euclidean, aggregated)


def write_results(match_dict, result_file, euclidean, aggregated):
    header = make_header(aggregated, euclidean)
    if euclidean:
        result_file = result_file.replace(".csv", "-euclid.csv")
    print("writing to ", result_file)
    with open(result_file, 'w') as out:
        out.write(header + "\n")
        for match in sorted(match_dict):
            match_data = match_dict[match]
            for ttime in range(match_data.shape[0]):
                if aggregated:
                    str_vals = list_to_string(match_data[ttime])
                    line = ",".join([match, index_to_target[ttime], str_vals])
                    out.write(line + "\n")
                else:
                    for level in range(match_data.shape[1]):
                        str_vals = list_to_string(match_data[ttime, level])
                        line = ",".join([match, index_to_target[ttime], str(level), str_vals])
                        out.write(line + "\n")

if __name__ == "__main__":
    gt_f = os.getcwd() + "/map_evolution/groundtruths/GTRev_crs0.2-parsed-w_nan-per_map-raw.pkl"
    # data_name = "focus_maps"
    # data_name = "results_20runs_crs0"
    data_name = "reversed_crs0.2"

    data_f = os.getcwd() + "/map_evolution/rev_results/{}-parsed.pkl".format(data_name)
    result_f = os.getcwd() + "/map_evolution/gt_comparison/{}-gt_comp_per_map.csv".format(data_name)
    means_f = os.getcwd() + "/map_evolution/gt_comparison/{}-gt_comp_means.csv".format(data_name)

    # main(data_f, gt_f, euclidean=False, means_file=means_f)
    # main(data_f, gt_f, euclidean=True, means_file=means_f)
    # main(data_f, gt_f, add_euclidean=False, result_file=result_f)
    main(data_f, gt_f, add_euclidean=True, result_file=result_f, means_file=means_f)
