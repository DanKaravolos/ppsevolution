from __future__ import print_function
from os import path
import pickle as pickle
from common.common import *
from map_evolution.postprocessing.cig_archive.make_game_input_for_validation import process_folder
from collections import Counter
from itertools import compress
nans = 0
nan_files = []
remove_cnt = 0


def process_game_input(game_input_list):
    game_input_dict = {}
    for line in range(len(game_input_list)):
        name = game_input_list[line][0]
        level = name.split("-")[0]
        for elem in name.split("-"):
            if "match" in elem:
                match_name = elem.split("_", 1)[1]
            elif "time" in elem:
                target = elem.split("_", 1)[1]
            elif "run" in elem:
                run = int(elem.split("_", 1)[1])
        # match_name = [elem.split("_", 1)[1] for elem in name.split("-") if "match" in elem][0]
        # target = [elem.split("_", 1)[1] for elem in name.split("-") if "time" in elem][0]
        game_input_dict[line] = (level, match_name, target, run)
    return game_input_dict


def get_num_from_filename(filename):
    elems = filename.split("_", 2)[1]
    return int(elems)


def parse_folder(folder):
    f_tag = "/Level1/"
    game_folders = [folder + fold + f_tag for fold in os.listdir(folder) if path.isdir(folder + fold)]
    game_folders = sorted(game_folders)
    sum_files = []
    for f in game_folders:
        g_files = [[f + name, get_num_from_filename(name)] for name in os.listdir(f) if "Summary" in name and path.isfile(f + name)]
        g_files = sorted(g_files, key=lambda x: x[1])
        sum_files.append(g_files)
    return sum_files


def make_map_data(match_names):
    # desired data format: match x value (time, score) x run
    data = {}
    for m in match_names:
        data[m] = [[[[], [], []] for i in range(20)] for j in range(3)]
    return data


def parse_files_store_per_map(files_per_run, param_to_match_dict, match_names, game_input_dict):
    global remove_cnt
    all_levels = {}

    for run in range(len(files_per_run)):
            for data_point in range(len(files_per_run[run])):
                filename = files_per_run[run][data_point][0]
                match, time, score = extract_match_info(filename, param_to_match_dict)
                tmp_map_id = filename.rsplit("/", 1)[1]
                if score == -1 or tmp_map_id in nan_files:
                    print("not including {0} because of nan".format(tmp_map_id))
                    remove_cnt += 1
                    # continue
                level_name, gi_name, target_time, map_num = game_input_dict[data_point]
                if match != gi_name:
                    print("Error! found match_name '{0}' does not match game input match_name '{1}!".format(match, gi_name))
                    return None
                # target_time = game_input_dict[data_point][1]
                # run = game_input_dict[data_point][2]
                fitness = (time - float(target_time)) ** 2 + (score - 0.5) ** 2

                # if level has not been encountered yet, make new data structure
                if level_name not in all_levels:
                    all_levels[level_name] = make_map_data(match_names)
                # add match info to datastructe of the level
                data = all_levels[level_name]
                data[match][target_to_index[target_time]][map_num][0].append(time)
                data[match][target_to_index[target_time]][map_num][1].append(score)
                data[match][target_to_index[target_time]][map_num][2].append(fitness)
    return all_levels  # return a list of map data structures


def parse_files_store_per_target(files_per_run, param_to_match_dict, match_names, game_input_dict):

    per_map_data = parse_files_store_per_map(files_per_run, param_to_match_dict, match_names, game_input_dict)

    # desired data format: match x value (time, score) x run
    data = {}
    for m in match_names:
        data[m] = [[[], [], []] for i in range(3)]

        for target_time in range(len(per_map_data[m])):
            tt_data = per_map_data[m][target_time]
            for map_num in tt_data:
                times, scores, fitnesses = map_num
                data[m][target_time][0].extend(times)
                data[m][target_time][1].extend(scores)
                data[m][target_time][2].extend(fitnesses)
    return data


def extract_match_info(filename, params_to_match_dict):
    global nans, nan_files
    time = -1.0
    score = -1.0
    t0 = []
    t1 = []
    with open(filename, 'r') as infile:
        lines = infile.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            key = elems[0]
            if key.startswith("TotalTime"):
                time = normalize_time(float(elems[1]))
                # print("WARNING! Normalizing via minimum value of 163!!!")
            elif key.startswith("Team0Kills"):
                if elems[2] == "NaN":
                    score = np.nan
                    nans += 1
                    nan_files.append(filename.rsplit("/", 1)[1])
                else:
                    score = float(elems[2])
            elif key.startswith("Team0Params"):
                t0 = elems[1:9]
            elif key.startswith("Team1Params"):
                t1 = elems[1:9]

    test_str = list_to_string([float(num) for num in t0 + t1])
    match_name = params_to_match_dict[test_str]
    if time == -1:
        print("Error while reading : {0}. No time value found.".format(filename))
    if score == -1:
        print("Error while reading : {0}. No score value found. Map time: {1}".format(filename, time))
    if not t0:
        print("Error while reading : {0}. No Team0Params value found.".format(filename))
    if not t1:
        print("Error while reading : {0}. No Team1Params found.".format(filename))
    return match_name, time, score


def write_csv_result_per_map(data_dict, match_names, out_file, write_raw=True):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    nr_runs = len(data_dict[match_names[0]][0][0][0])
    with open(out_file, "w") as out:
        header = ["Match,target time,map,time mean,time std,score mean,score std, fitness mean, fitness std"]
        if write_raw:
            header += ["time"] * nr_runs
            header += ["score"] * nr_runs
            header += ["fitness"] * nr_runs
        header = ",".join(header)
        out.write("{0}\n".format(header))
        for match in match_names:
            map_num = 0
            for target_time in range(len(data_dict[match])):
                t_time = index_to_target[target_time]
                tt_data = data_dict[match][target_time]
                # out.write("{0}".format(match))
                for map_num in range(len(tt_data)):
                    out.write("{0}, {1}, {2}".format(match, t_time, map_num))
                    # compute means first
                    for dim in tt_data[map_num]:
                        mean = np.nanmean(dim)
                        std = np.nanstd(dim)
                        out.write(",{0:.2f}, {1:.2f}".format(mean, std))
                    # then write raw values
                    if write_raw:
                        # rowstring = ("{0},{1},{2}".format(match, list_to_string(row[0]), list_to_string(row[1])))
                        for dim in tt_data[map_num]:
                            out.write(",{0}".format(list_to_string(dim)))
                    out.write("\n")
                    counter += 1
                    print(counter)
                map_num += 1


def write_csv_result_per_target(data_dict, match_names, out_file, write_raw=True):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    nr_runs = len(data_dict[match_names[0]][0][0])
    with open(out_file, "w") as out:
        header = ["Match,target time,time mean,time std,score mean,score std, fitness mean, fitness std"]
        if write_raw:
            header += ["time"] * nr_runs
            header += ["score"] * nr_runs
            header += ["fitness"] * nr_runs
        header = ",".join(header)
        out.write("{0}\n".format(header))
        for match in match_names:
            for target_time in range(len(data_dict[match])):
                t_time = index_to_target[target_time]
                tt_data = data_dict[match][target_time]
                # out.write("{0}".format(match))
                out.write("{0}, {1}".format(match, t_time))
                # compute means first
                for dim in tt_data:
                    mean = np.nanmean(dim)
                    std = np.nanstd(dim)
                    out.write(",{0:.2f}, {1:.2f}".format(mean, std))
                # then write raw values
                if write_raw:
                    # rowstring = ("{0},{1},{2}".format(match, list_to_string(row[0]), list_to_string(row[1])))
                    for dim in tt_data:
                        dim_str = list_to_string(dim)
                        out.write(",{0}".format(dim_str))
                out.write("\n")
                counter += 1
                print(counter)


def save_as_binary(data_dict, output_file):
    print("Writing to numpy : {}.npy".format(output_file))
    data_array = np.asarray([data_dict[m] for m in match_names])
    np.save(output_file + ".npy", data_array)

    print("Writing dict to pickle : {}.pkl".format(output_file))
    with open(output_file + ".pkl", 'wb') as dict_file:
        pickle.dump(data_dict, dict_file)


def main(folder, game_input_folder, output_file, store_per_map=False, write_raw=False):
    print("parsing...")
    # get files
    list_of_files_per_run = parse_folder(folder)

    # get the order in which the games were played
    game_input_list = process_folder(game_input_folder, match_to_param_dict)
    game_input_dict = process_game_input(game_input_list)

    if store_per_map:
        parse_func = parse_files_store_per_map
        csv_func = write_csv_result_per_map
    else:
        parse_func = parse_files_store_per_target
        csv_func = write_csv_result_per_target

    list_of_data_dicts = parse_func(list_of_files_per_run, param_to_match_dict, match_names, game_input_dict)
    print("saving...")
    for level in list_of_data_dicts.keys():
        level_output_fi = "-".join(compress([output_file, level, "per_map", "raw"], [True, True, store_per_map, write_raw]))

        csv_func(list_of_data_dicts[level], match_names, level_output_fi + ".csv", write_raw)
        save_as_binary(list_of_data_dicts[level], level_output_fi)


if __name__ == "__main__":
    # game_input_path = os.getcwd() + "/map_evolution/rev_results/reversed_crs0.2/"
    game_input_path = "/media/daniel/3c8aeac5-6a62-4669-adcb-74bc97540200/daniel/CIG18EvolvedMaps/"
    # game_input_path = os.getcwd() + "/map_evolution/Levels_1-4/"
    # gt_path = os.getcwd() + "/map_evolution/groundtruths/GTRev_crs0.2/"
    # gt_path = "/media/daniel/3c8aeac5-6a62-4669-adcb-74bc97540200/daniel/CIG18ExtraGTS/CIG_Levels14_GT/"
    gt_path = "/media/daniel/3c8aeac5-6a62-4669-adcb-74bc97540200/daniel/CIG18EvolvedGT/"

    result_file = gt_path[:-1] + "-parsed-w_nan"
    main(gt_path, game_input_path, result_file, store_per_map=True, write_raw=True)
    print("NaNs : ", nans)
    # print("Files removed from data:", remove_cnt)

    nan_dict = Counter(nan_files)
    for fi in sorted(nan_dict):
        print(fi)

