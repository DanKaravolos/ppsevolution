from __future__ import print_function
import os
from os import path
import numpy as np
from common.common import list_to_string, normalize_time
from map_evolution.evolution.init_and_select import parse_match_settings


def get_num_from_filename(filename):
    elems = filename.split("_", 2)[1]
    return int(elems)


def parse_folder(folder):
    f_tag = "/Level1/"
    game_folders = [folder + fold + f_tag for fold in os.listdir(folder) if path.isdir(folder + fold)]
    game_folders = sorted(game_folders)
    sum_files = []
    for f in game_folders:
        g_files = [[f + name, get_num_from_filename(name) ] for name in os.listdir(f) if "Summary" in name and path.isfile(f + name)]
        g_files = sorted(g_files, key=lambda x: x[1])
        sum_files.append(g_files)
    return sum_files


def parse_files(files_per_run, param_to_match_dict, match_names):
    # desired data format: match x value (time, score) x run
    data = {}
    for m in match_names:
        data[m] = [[], []]

    for run in range(len(files_per_run)):
        for data_point in range(len(files_per_run[run])):
            filename = files_per_run[run][data_point][0]
            match, time, score = extract_match_info(filename, param_to_match_dict)
            data[match][0].append(time)
            data[match][1].append(score)
    return data


def extract_match_info(filename, params_to_match_dict):
    time = -1.0
    score = -1.0
    t0 = []
    t1 = []
    with open(filename, 'r') as infile:
        lines = infile.readlines()
        for line in lines:
            elems = line.rstrip().split(",")
            key = elems[0]
            if key.startswith("TotalTime"):
                time = normalize_time(float(elems[1]))
            elif key.startswith("Team0Kills"):
                if elems[2] == "NaN":
                    score = 0.5
                else:
                    score = float(elems[2])
            elif key.startswith("Team0Params"):
                t0 = elems[1:9]
            elif key.startswith("Team1Params"):
                t1 = elems[1:9]

    test_str = list_to_string([float(num) for num in t0 + t1])
    match_name = params_to_match_dict[test_str]
    if time == -1:
        print("Error while reading : {0}. No time value found.".format(filename))
    if score == -1:
        print("Error while reading : {0}. No score value found.".format(filename))
    if not t0:
        print("Error while reading : {0}. No Team0Params value found.".format(filename))
    if not t1:
        print("Error while reading : {0}. No Team1Params found.".format(filename))
    return match_name, time, score


def write_csv_result(data_dict, match_names, out_file, write_raw=True):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    nr_runs = len(data_dict[match_names[0]][0])
    with open(out_file, "w") as out:
        header = ["Match,time mean,time std,score mean,score std"]
        if write_raw:
            header += ["time"] * nr_runs
            header += ["score"] * nr_runs
        header = ",".join(header)
        out.write("{0}\n".format(header))
        for match in match_names:
            out.write("{0}".format(match))
            row = data_dict[match]
            # compute means first
            for dim in row:
                mean = np.mean(dim)
                std = np.std(dim)
                out.write(",{0:.2f}, {1:.2f}".format(mean, std))
            # then write raw values
            if write_raw:
                # rowstring = ("{0},{1},{2}".format(match, list_to_string(row[0]), list_to_string(row[1])))
                out.write(",{0},{1}".format(list_to_string(row[0]), list_to_string(row[1])))
            out.write("\n")
            counter += 1


def main(folder, match_file, output_file):
    print("parsing...")
    list_of_files_per_run = parse_folder(folder)
    match_to_param_dict = parse_match_settings(match_file)
    matches = [list_to_string(match_to_param_dict[m][0]) for m in sorted(match_to_param_dict)]
    match_names = [m for m in sorted(match_to_param_dict)]
    param_to_match_dict = dict(zip(matches, match_names))

    data_dict = parse_files(list_of_files_per_run, param_to_match_dict, match_names)
    print("saving...")
    write_csv_result(data_dict, match_names, output_file + ".csv", write_raw=False)

    print("Writing to numpy : {}.npy".format(output_file))
    data_array = np.asarray([data_dict[m] for m in match_names])
    np.save(output_file + ".npy", data_array)

if __name__ == "__main__":
    gt_path = os.getcwd() + "/map_evolution/groundtruths/GT_initial_map/"
    match_file = os.getcwd() + "/data/all_5_class_matches.csv"
    result_file = gt_path[:-1] + "-parsed"
    main(gt_path, match_file, result_file)
