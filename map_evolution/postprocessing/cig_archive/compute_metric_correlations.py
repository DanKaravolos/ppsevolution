from __future__ import print_function
import cPickle as pickle
from scipy.stats import spearmanr
from common.common import *
from map_evolution.postprocessing.cig_archive.compute_map_metrics import ind_to_list, make_header

dims = {0: "duration", 1: "score", 2: "imbalance", 3: "fitness"}


def parse_gt_file(gt_file):
    means = []
    with open(gt_file, 'r') as inpt:
        lines = inpt.readlines()
        for line in lines[1:]:
            elems = line.rstrip().split(",")
            time = float(elems[3])
            score = float(elems[5])
            fitness = float(elems[7])
            score_diff = abs(score - 0.5)
            means.append([time, score, score_diff, fitness])
    means = np.asarray(means)
    return means


def parse_metric_file(metric_file, matches=[]):
    with open(metric_file, 'rb') as inpt:
        met_data = pickle.load(inpt)
    # convert weird dictionary to list of all items
    list_data = []
    for m in met_data:
        l_vals = ind_to_list(m)
        match = l_vals[1] + "_" + l_vals[2]
        new_val = [match] + l_vals[3:]
        if (not matches) or (matches and match in matches):
            list_data.append(new_val)
    # create matching labels to know what is what
    labels = make_header(False)
    sl_labels = ["match"] + labels[3:]
    sorted_list_data = sorted(list_data, key=lambda x: (x[0], x[1], x[2]))
    return sorted_list_data, sl_labels


def compute_correlations(gt_data, m_data, m_labels, specific_dim=False, no_fitness=True):
    if specific_dim:
        return compute_dim_correlations(gt_data[:, 2], m_data, m_labels, dims[2])  # balance
        # return compute_dim_correlations(gt_data[:, 3], m_data, m_labels, dims[3])  # fitness
    elif no_fitness:
        dims_to_correlate = range(gt_data.shape[1])[:3]
    else:
        dims_to_correlate = range(gt_data.shape[1])
    correlations = []
    for i in dims_to_correlate:  # remove [:2] to include fitness
        dim_correlations = compute_dim_correlations(gt_data[:, i], m_data, m_labels, dims[i])
        correlations.extend(dim_correlations)
    return correlations


def compute_dim_correlations(gt_dim, m_data, m_labels, gt_label):
    unconsidered_metrics = ["counts 0Y", "counts 0Z"]
    correlations = []
    for met in range(3, len(m_labels)):  #m_data[0 - 2] are match, target_time and map_num
        met_label = m_labels[met]
        if met_label in unconsidered_metrics:
            continue
        met_vals = [row[met] for row in m_data]
        corr, p_val = spearmanr(met_vals, gt_dim)  # fill in your correlation function here
        corr = 0 if np.isnan(corr) else corr
        print("{0} correlation with {1}: {2:.2f}, p: {3:.4f}".format(met_label, gt_label, corr, p_val))
        correlations.append([gt_label, met_label, corr, p_val])
    return correlations


def write_correlations(correlations, result_file):
        print("writing to ", result_file)
        with open(result_file, 'w') as out:
            header = "gt_value, map metric, correlation, p_value\n"
            out.write(header)
            srtd_corrs = sorted(correlations, key=lambda x: (-abs(x[2]), -x[3]))
            for row in srtd_corrs:
                strow = list_to_string(row)
                out.write(strow + "\n")


def main(metric_file, gt_file, result_file, matches=[]):
    m_data, m_labels = parse_metric_file(metric_file, matches)
    gt_data = parse_gt_file(gt_file)
    correlations = compute_correlations(gt_data, m_data, m_labels)

    # regr_weights = do_linear_regression(gt_data, m_data, m_labels)
    write_correlations(correlations, result_file)


def compute_focus_maps():
    data_name = "focus_maps"
    gt_f = os.getcwd() + "/map_evolution/groundtruths/{}-parsed-per_map-manual.csv".format(data_name)
    data_f = os.getcwd() + "/map_evolution/results/{}-map_metrics-per_map.pkl".format(data_name)
    result_f = os.getcwd() + "/map_evolution/metrics/{}-correlations.csv".format(data_name)
    main(data_f, gt_f, result_f)


def compute_all_maps():
    data_name = "results_20runs_crs0"
    gt_f = os.getcwd() + "/map_evolution/groundtruths/GT_crs0.2-parsed-per_map.csv".format(data_name)
    data_f = os.getcwd() + "/map_evolution/results/{}-map_metrics-per_map.pkl".format(data_name)
    # if per_match:
    #     result_f = os.getcwd() + "/map_evolution/metrics/{}-correlations-per_match.csv".format(data_name)
    # else:
    result_f = os.getcwd() + "/map_evolution/metrics/{}-correlations.csv".format(data_name)

    # m = matches if per_match else []
    main(data_f, gt_f, result_f)


def compute_per_focus_match():
    matches = ["Scout_Scout", "Scout_Pyro", "Sniper_Sniper", "Sniper_Pyro"]
    for m in matches:
        data_name = m
        gt_f = os.getcwd() + "/map_evolution/metrics/input/{}-parsed-per_map-manual.csv".format(data_name)
        data_f = os.getcwd() + "/map_evolution/metrics/input/{}-map_metrics-per_map.pkl".format("focus_maps")
        result_f = os.getcwd() + "/map_evolution/metrics/{}-correlations.csv".format(data_name)
        main(data_f, gt_f, result_f, [m])

if __name__ == "__main__":
    compute_all_maps()
    # compute_focus_maps()
    # compute_per_focus_match()
