from __future__ import print_function
import os
import cPickle as pickle
from collections import defaultdict
from common.common import *
from common.common import csv_to_array
import map_evolution.evolution.individual.grid_functions as gf
from map_evolution.evolution.individual.MapIndividual import MapIndividual
from map_evolution.evolution.init_and_select import parse_match_settings
import numpy as np
from copy import deepcopy
from tqdm import *

pickup_types = ["H", "A", "D"]
pickup_tiles = ['0A', '1A', '0H', '1H', '0D', '1D']
traversables = ['0', '1', '0S', '0Y', '0Z'] + pickup_tiles
type_dict = {
    '0': 0,
    '1': 0,
    '2': 0,
    '0Y': 0,
    '0Z': 0,
    '0S': 0,
    '0A': 0,
    '1A': 0,
    '0H': 0,
    '1H': 0,
    '0D': 0,
    '1D': 0,
}
tile_types = sorted(type_dict)
original_map = MapIndividual(csv_to_array(os.getcwd() + "/data/map_antonios_valid_empty_bases.csv"), 4, 4)
original_map.visualize_bases()
original_map = original_map.to_matrix()


def compute_counts(np_map, broad_def=True, only_bases=True):
    unique, counts = np.unique(np_map, return_counts=True)
    # c_dict = dict(zip(unique, counts))
    count_dict = deepcopy(type_dict)
    for i in range(len(unique)):
        count_dict[unique[i]] = counts[i]

    # maybe we do not only want to count the empty floor tiles as being a certain floor
    if broad_def or only_bases:
        count_dict["0"] += count_dict["0Y"] + count_dict["0Z"]
    if broad_def:
        count_dict["0"] += count_dict["0A"] + count_dict["0D"] + count_dict["0H"]
        count_dict["1"] += count_dict["1A"] + count_dict["1D"] + count_dict["1H"]
    return count_dict


def compute_shortest_distances_to_pickups(np_map, tile_dict, pickup_type, base1, base2):
    p_tiles = ["0" + pickup_type, "1" + pickup_type]

    occurences_in_map = sum([len(tile_dict[p]) for p in pickup_types])
    if occurences_in_map == 0:
        return [-1, -1]

    base1_to_pickup, cost1 = gf.astar(np_map, base1, p_tiles, traversables)
    base2_to_pickup, cost2 = gf.astar(np_map, base2, p_tiles, traversables)

    if cost1 > 500 or cost2 > 500:
        # print("Error: path cost more than 500...")
        return [-1, -1]

    base1_dist = -1 if len(base1_to_pickup) == 0 else len(base1_to_pickup)
    base2_dist = -1 if len(base2_to_pickup) == 0 else len(base2_to_pickup)

    return [base1_dist, base2_dist]


def compute_base2base_dist(np_map, tile_dict, base1, base2):
    base1_to_base2, cost1 = gf.astar(np_map, base1, ["0Z"], traversables)
    base2_to_base1, cost2 = gf.astar(np_map, base2, ["0Y"], traversables)

    b1b2 = len(base1_to_base2) if cost1 < 500 else -1
    b2b1 = len(base2_to_base1) if cost2 < 500 else -1
    # if cost1 > 500 or cost2 > 500:
    #     print("Error: path cost more than 500...")
        # return [-1, -1]
    return [b1b2, b2b1]


def compute_euc_dist(coord1, coord2):
    return np.sqrt((coord2[0] - coord1[0]) ** 2 + (coord2[1] - coord1[1]) ** 2)


def compute_avg_pickup_dist_to_base(np_map, tile_dict, base):
    # total_dist = 0
    # for pt in pickup_tiles:
    #     for pickup in tile_dict[pt]:
    #         dist = compute_euc_dist(pickup, base)
    distances = []
    for pt in pickup_types:
        for pickup in tile_dict[pt]:
            distances.append(compute_euc_dist(pickup, base))
    avg_dist = np.average(distances)
    return avg_dist


def compute_openness(np_map, tile_dict):
    ground_tiles = tile_dict['0'] + tile_dict['0A'] + tile_dict['0D'] + tile_dict['0H'] + tile_dict['0Y'] + tile_dict['0Z']

    counts = []
    for gt in ground_tiles:
        nbrs = gf.get_neighbors(gt, np_map)
        types = [np_map[n] for n in nbrs]
        grounds = types.count("0") + types.count("0A") + types.count("0D") + types.count("0H") + types.count("0Y") + types.count("0Z")
        counts.append(grounds)
    avg = np.average(counts)
    return avg


def compute_distance_between_maps(np_map, original_map):
    not_same = np_map != original_map
    return np.count_nonzero(not_same)


def compute_avg_dist_to_nn_pickups(np_map, tile_dict):
    pickups = [tile_dict[p] for p in pickup_types]
    if len(pickups) <= 1:
        return -1

    nearest_n = []
    # for p in flatten(pickups):  # you know... we could do this with a few loops... since it basically Euc. dist.
    #     nn_path, cost1 = gf.astar(np_map, p, pickup_tiles, traversables, start_is_valid_goal=False)
    #     # if cost1 > 500:
    #     #     print("Error: path cost more than 500...")
    #     #     return -1
    #     if len(nn_path) == 0:
    #         continue  # if no path is found, just continue...
    #     else:
    #         nearest_n.append(len(nn_path))
    pickups = flatten(pickups)
    for p in pickups:
        min_euc_dist = min([compute_euc_dist(p, nbr) for nbr in pickups if nbr != p])
        nearest_n.append(min_euc_dist)
    return np.mean(nearest_n)


def compute_avg_dist_between_pickups(np_map, tile_dict):
    pickups = [tile_dict[p] for p in pickup_types]
    if len(pickups) <= 1:
        return -1

    nearest_n = []
    # for p in flatten(pickups):  # you know... we could do this with a few loops... since it basically Euc. dist.
    #     nn_path, cost1 = gf.astar(np_map, p, pickup_tiles, traversables, start_is_valid_goal=False)
    #     # if cost1 > 500:
    #     #     print("Error: path cost more than 500...")
    #     #     return -1
    #     if len(nn_path) == 0:
    #         continue  # if no path is found, just continue...
    #     else:
    #         nearest_n.append(len(nn_path))
    pickups = flatten(pickups)
    for p in pickups:
        min_euc_dist = np.mean([compute_euc_dist(p, nbr) for nbr in pickups if nbr != p])
        nearest_n.append(min_euc_dist)
    return np.mean(nearest_n)


def get_match_and_time(filename):
    elems = filename.split("-")
    match = ""
    time = ""
    for el in elems:
        if el.startswith("match"):
            match = el.split("_")[1:]
        elif el.startswith("time"):
            time = el.split("_")[1]
    return match, time


def analyze_original_map(fn):
    orig_map = MapIndividual(csv_to_array(fn), 4, 4)
    orig_map.visualize_bases()
    map_data = []
    np_map = orig_map.to_matrix()
    tile_dict = gf.build_dict(np_map)
    base1 = (np_map.shape[0] - 3, 2)
    base2 = (2, np_map.shape[0] - 3)
    center = (np_map.shape[0] / 2.0, np_map.shape[0] / 2.0)

    # gather information
    count_dict = compute_counts(np_map)
    avg_p_distances = [compute_avg_pickup_dist_to_base(np_map, tile_dict, base) for base in [base1, base2]]
    avg_p_dist_center = compute_avg_pickup_dist_to_base(np_map, tile_dict, center)
    shortest_p_distances = dict(zip(pickup_types,
                                    [compute_shortest_distances_to_pickups(np_map, tile_dict, pt, base1, base2)
                                     for pt in pickup_types]))
    openness = compute_openness(np_map, tile_dict)
    b2b = compute_base2base_dist(np_map, tile_dict, base1, base2)
    dist_to_orig = compute_distance_between_maps(np_map, original_map)
    nn_dist = compute_avg_dist_to_nn_pickups(np_map, tile_dict)
    avg_p_dist = compute_avg_dist_between_pickups(np_map, tile_dict)
    fname = fn.rsplit("/", 1)[1].replace(".csv", "")

    mf_data = [fname, count_dict, avg_p_distances, avg_p_dist_center,
               shortest_p_distances, openness, b2b, ["Original", "Map"], "0.315", dist_to_orig, nn_dist, -1, avg_p_dist]
    map_data.append(mf_data)
    return map_data


def analyze_all_in_folder(folder, original_map):
    map_files = get_files_in_folder(folder)
    map_files = [fi for fi in map_files if "last_best" in fi]
    map_data = []

    for mf in tqdm(map_files):
        # print(mf)
        # create necessary datastructures of map
        np_map = csv_to_array(mf)
        tile_dict = gf.build_dict(np_map)
        base1 = (np_map.shape[0] - 3, 2)
        base2 = (2, np_map.shape[0] - 3)
        center = (np_map.shape[0] / 2.0, np_map.shape[0] / 2.0)

        # gather information
        match, time = get_match_and_time(mf)

        count_dict = compute_counts(np_map)
        avg_p_distances = [compute_avg_pickup_dist_to_base(np_map, tile_dict, base) for base in [base1, base2]]
        avg_p_dist_center = compute_avg_pickup_dist_to_base(np_map, tile_dict, center)
        shortest_p_distances = dict(zip(pickup_types, [compute_shortest_distances_to_pickups(np_map, tile_dict, pt, base1, base2) for pt in pickup_types]))
        openness = compute_openness(np_map, tile_dict)
        b2b = compute_base2base_dist(np_map, tile_dict, base1, base2)
        dist_to_orig = compute_distance_between_maps(np_map, original_map)
        nn_dist = compute_avg_dist_to_nn_pickups(np_map, tile_dict)
        avg_p_dist = compute_avg_dist_between_pickups(np_map, tile_dict)
        fname = mf.rsplit("/", 1)[1].replace("-last_best.csv", "")
        run = [int(elem.split("_", 1)[1]) for elem in fname.split("-") if "run" in elem][0]
        mf_data = [fname, count_dict, avg_p_distances, avg_p_dist_center,
                   shortest_p_distances, openness, b2b, match, time, dist_to_orig, nn_dist, run, avg_p_dist]
        map_data.append(mf_data)
    return map_data


def do_aggregate_data(all_data):
    data_dict = {}
    aggr_dict = {}
    for m in match_names:
        data_dict[m] = [[] for i in range(3)]
        aggr_dict[m] = [[] for i in range(3)]

    for ind in all_data:
        match_name = "{0}_{1}".format(ind[7][0], ind[7][1])
        target_time = ind[8]
        # run = ind[11]
        data_dict[match_name][target_to_index[target_time]].append(ind)

    for match_name in data_dict:
        for tt in range(len(data_dict[match_name])):
            lists = [ind_to_list(ind) for ind in data_dict[match_name][tt]]

            if lists:
                num_lists = [l[5:] for l in lists]
                tt_mean = np.mean(num_lists, axis=0)
                aggr_dict[match_name][tt] = tt_mean
    return aggr_dict


def ind_to_list(ind):
    met1 = [ind[1][t] for t in tile_types]
    met2 = ind[2]
    met3 = [ind[3]]
    met4 = [ind[4][pt] for pt in pickup_types]
    met4 = flatten(met4)
    met5 = [ind[5]]
    met6 = ind[6]
    met7 = [ind[7][0], ind[7][1], ind[8]]
    met8 = [ind[9]]  # make sure that everything is a list!
    met9 = [ind[10]]  # make sure that everything is a list!
    met10 = [ind[11]]
    met11 = [ind[12]]
    metrics = [ind[0]]
    for met in [met7, met10, met1, met2, met3, met9, met11, met4, met5, met6, met8]:
        metrics.extend(met)
    return metrics


def make_header(is_aggregated):
    h1 = ["counts {0}".format(t) for t in tile_types]
    h2 = ["avg pickup euc.dist p1", "avg pickup euc.dist p2"]
    h3 = ["avg pickup euc. dist center"]
    h4 = ["shortest dist p1 to {0},shortest dist p2 to {0}".format(pt) for pt in pickup_types]
    h5 = ["openness"]
    h6 = ["base1 to base2 dist", "base2 to base1 dist"]
    h7 = ["p1", "p2", "time"]
    h8 = ["diff with original"]
    h9 = ["pickup min dist to nbrs"]
    h10 = ["run"]
    h11 = ["pickup avg dist to nbrs"]
    if is_aggregated:
        header = ["match name,target_time"]
        for h in [h1, h2, h3, h9, h11, h4, h5, h6, h8]:
            header.extend(h)
    else:
        header = ["map name"]
        for h in [h7, h10, h1, h2, h3, h9, h11, h4, h5, h6, h8]:
            for el in h:
                hs = [s for s in el.split(",")]
                header.extend(hs)
    return header


def write_all_data(data, filename):
    print("writing to ", filename)
    with open(filename, 'w') as out:
        header = make_header(False)
        header = ",".join(header)
        out.write(header + "\n")

        for ind in data:
            metrics = ind_to_list(ind)
            metrics = ",".join(["{:.2f}".format(m) if not isinstance(m, basestring) else m for m in metrics])
            out.write(metrics + "\n")


def write_aggr_data(aggr_data_dict, filename):
    print("writing to ", filename)
    with open(filename, 'w') as out:
        header = make_header(True)
        header = ",".join(header)
        out.write(header + "\n")

        for match in aggr_data_dict:
            match_data = aggr_data_dict[match]
            for tt in range(len(match_data)):
                target_time = index_to_target[tt]
                metrics = match_data[tt]
                metrics = ",".join(["{:.2f}".format(m) for m in metrics])
                out.write("{0},{1},".format(match, target_time))
                out.write(metrics + "\n")


def main_analyze_folder(data_folder, aggregate_data):
    all_data = analyze_all_in_folder(data_folder, original_map)

    if aggregate_data:
        result_filename = data_folder[:-1] + "-map_metrics.csv"
        aggr_data = do_aggregate_data(all_data)
        write_aggr_data(aggr_data, result_filename)
        all_data = aggr_data
    else:
        result_filename = data_folder[:-1] + "-map_metrics-per_map.csv"
        write_all_data(all_data, result_filename)

    with open(result_filename.replace(".csv", ".pkl"), 'wb') as out:
        pickle.dump(all_data, out)


def main_analyze_single_map(input_f, result_f):
    metrics = analyze_original_map(input_f)
    write_all_data(metrics, result_f)

if __name__ == "__main__":
    # option 1: analyze a whole folder (for evolution results)
    # folder = os.getcwd() + "/map_evolution/results/results_20runs_crs0/"
    # folder = os.getcwd() + "/map_evolution/results/results_single_run/"
    # folder = os.getcwd() + "/map_evolution/results/focus_maps/"
    folder = os.getcwd() + "/map_evolution/rev_results/reversed_crs0.2/"
    # folder = os.getcwd() + "/map_evolution/rev_results/reversed_crs0/"
    main_analyze_folder(folder, aggregate_data=True)

    # option 2: analyze a single map (for the population seed)
    # original_file = os.getcwd() + "/data/map_antonios_valid_empty_bases.csv"
    # result_file = os.getcwd() + "/map_evolution/results/map5-map_metrics.csv"
    # main_analyze_single_map(original_file, result_file)
