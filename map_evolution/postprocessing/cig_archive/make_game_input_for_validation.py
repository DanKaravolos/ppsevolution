from __future__ import print_function
import os
from common.common import list_to_string, get_specific_files_in_folder, parse_match_settings


def process_initial_map(match_dict, nr_runs):
    matches = [match_dict[m] for m in sorted(match_dict)]
    # match_names = [m for m in sorted(match_dict)]
    game_input_file = os.getcwd() + "/map_evolution/postprocessing/reversed_matches-baseline-game_input-{0}runs.csv".format(nr_runs)
    maps = ["/data/map_antonios_valid_empty_bases.csv"]
    with open(game_input_file, 'w') as out:
        count = 0
        for map in maps:
            for match in matches:
                for run in range(nr_runs):
                    print(count)
                    paramstring = list_to_string(match[0])
                    line = map + "," + paramstring + "\n"
                    out.write(line)
                    count += 1


def parse_lb_files(lb_files, match_to_param_dict):
    print("parsing")
    data = []
    for f in lb_files:
        name = f.rsplit("/", 1)[1]

        match_name = [elem.split("_", 1)[1] for elem in name.split("-") if "match" in elem][0]
        try:
            match_params = match_to_param_dict[match_name]
            data.append([name, match_params])
        except KeyError:
            print("ERROR! {0} IS NOT FOUND IN DICT".format(match_name))
    return data


def write_game_input_file(data, nr_gt_runs, game_input_file):
    print("Writing game input to ", game_input_file)
    with open(game_input_file, 'w') as out:
        count = 0
        for d in data:
            map_name = d[0]
            params = d[1]
            for run in range(nr_gt_runs):
                # print(count)
                paramstring = list_to_string(params[0])
                line = "/" + map_name + "," + paramstring + "\n"
                out.write(line)
                count += 1
    print("Nr of matches: ", count)


def process_folder(folder, match_to_param_dict):
    best_files = get_specific_files_in_folder(folder, "last_best")
    best_files =sorted(best_files)
    data = parse_lb_files(best_files, match_to_param_dict)
    data = sorted(data)
    return data


def single_folder():
    nr_gt_runs = 1
    # input_folder = os.getcwd() + "/map_evolution/Levels_1-4/"
    input_folder = os.getcwd() + "/data/maps/custom_maps/"
    result_file = input_folder[:-1] + "-game_input-{0}_runs.csv".format(nr_gt_runs)
    data = process_folder(input_folder, match_dict)
    write_game_input_file(data, nr_gt_runs, result_file)


def multi_folder_in_folder():
    nr_gt_runs = 1
    input_folder = os.getcwd() + "/map_evolution/CIG18Extra/"
    result_file = input_folder[:-1] + "-game_input-{0}_runs.csv".format(nr_gt_runs)

    folders = [input_folder + fo + "/results/" for fo in os.listdir(input_folder) if os.path.isdir(input_folder + fo)]
    data = []
    for ifo in folders:
        data += process_folder(ifo, match_dict)
    write_game_input_file(data, nr_gt_runs, result_file)


if __name__ == "__main__":
    # nr_gt_runs = 1
    # match_file = os.getcwd() + "/data/all_5_class_matches.csv"
    # match_file = os.getcwd() + "/data/spy_matches.csv"
    match_file = os.getcwd() + "/data/all_4_damage_class_matches.csv"
    match_dict = parse_match_settings(match_file)
    # process_initial_map(match_dict, nr_gt_runs)

    # single_folder()
    multi_folder_in_folder()