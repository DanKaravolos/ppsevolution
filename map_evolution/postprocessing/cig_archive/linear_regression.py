from __future__ import print_function
from sklearn.metrics import r2_score, mean_absolute_error
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.pipeline import make_pipeline
dims = {0: "duration", 1: "score", 2: "fitness"}


def do_linear_regression(gt_data, m_data, m_labels, standardize=True, normalize=False):
    coeffs = []
    alg = LinearRegression(normalize=normalize)
    # alg = make_pipeline(PolynomialFeatures(2), LinearRegression())
    X = [v[3:] for v in m_data]
    if standardize:
        print("scaling the data...")
        X = StandardScaler().fit_transform(X)

    nr_dims = gt_data.shape[1]
    for i in range(nr_dims):
        print("\ndim: ", dims[i])
        y = gt_data[:, i]
        alg.fit(X, y)
        y_pred = alg.predict(X)
        r2_score_alg = r2_score(y, y_pred)
        mae = mean_absolute_error(y, y_pred)
        print("R2: {0}, MAE: {1}".format(r2_score_alg, mae))
        coefficients = alg.coef_

        # coefficients = alg.steps[1][1].coef_
        for name, val in zip(m_labels[3:], coefficients):
            print("{2}, {0}, weight {1:.2f}".format(name, val, dims[i]))
        coeffs.append(coefficients)
    return coeffs
