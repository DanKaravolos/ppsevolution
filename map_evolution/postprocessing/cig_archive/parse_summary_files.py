from __future__ import print_function
import os
import cPickle as pickle
from os import path
import numpy as np
from common.common import list_to_string, get_specific_files_in_folder, index_to_target, target_to_index
from map_evolution.evolution.init_and_select import parse_match_settings


def extract_match_info(sum_line):
    elems = sum_line.split(",")
    match_name = elems[1]
    target_time = elems[3]
    time = float(elems[4])
    score = float(elems[7])
    fitness = float(elems[-2])
    run = int(elems[-1])
    return match_name, target_time, time, score, fitness, run


def parse_summary_files(list_of_files, match_names):
    nr_runs = 20
    data = {}
    for m in match_names:
        data[m] = [[np.zeros(nr_runs), np.zeros(nr_runs), np.zeros(nr_runs)] for i in range(3)]
        # per match: 3 time targets, 3 metrics (time, score, fitness), X runs
        # actually, the summary file results are sorted per run, so we don't need this per se.

    for summary in list_of_files:
        with open(summary, 'r') as inpt:
            lines = inpt.readlines()
            for line in lines:
                line = line.rstrip("\r\n")
                if "," in line:  # naive check for empty lines
                    match_name, target_time, time, score, fitness, run = extract_match_info(line)

                    # data[match_name][target_to_index[target_time]][0].append(time)
                    # data[match_name][target_to_index[target_time]][1].append(score)
                    # data[match_name][target_to_index[target_time]][2].append(fitness)
                    data[match_name][target_to_index[target_time]][0][run] = time
                    data[match_name][target_to_index[target_time]][1][run] = score
                    data[match_name][target_to_index[target_time]][2][run] = fitness
    print("data parsed")
    return data


def write_csv_result(data_dict, match_names, out_file):
    print("Writing to csv : {}".format(out_file))
    counter = 0
    nr_runs = len(data_dict[match_names[0]][0][0])
    with open(out_file, "w") as out:
        header = ["Match,target_time,time mean,time std,score mean,score std, fitness mean, fitness std"]
        header += ["time"] * nr_runs
        header += ["score"] * nr_runs
        header += ["fitness"] * nr_runs
        header = ",".join(header)
        out.write("{0}\n".format(header))
        for match in match_names:
            target_times = data_dict[match]
            idx = 0
            for target in target_times:
                out.write("{0}, {1}".format(match, index_to_target[idx]))
                # compute means first
                for dim in target:
                    mean = np.mean(dim)
                    std = np.std(dim)
                    out.write(",{0:.2f}, {1:.2f}".format(mean, std))
                # then write raw values
                # rowstring = ("{0},{1},{2}".format(match, list_to_string(row[0]), list_to_string(row[1])))
                out.write(",{0},{1},{2}".format(list_to_string(target[0]), list_to_string(target[1]), list_to_string(target[2])))
                out.write("\n")
                idx += 1
            counter += 1


def main(folder, match_file, output_file):
    print("parsing...")
    list_of_summary_files = get_specific_files_in_folder(folder, "summary.csv")
    match_to_param_dict = parse_match_settings(match_file)
    # matches = [list_to_string(match_to_param_dict[m][0]) for m in sorted(match_to_param_dict)]
    match_names = [m for m in sorted(match_to_param_dict)]
    # param_to_match_dict = dict(zip(matches, match_names))

    data_dict = parse_summary_files(list_of_summary_files, match_names)
    print("saving...")
    write_csv_result(data_dict, match_names, output_file + ".csv")

    print("Writing to numpy : {}.npy".format(output_file))
    data_array = np.asarray([data_dict[m] for m in match_names])
    np.save(output_file + ".npy", data_array)

    print("Writing dict to pickle : {}.pkl".format(output_file))
    with open(output_file + ".pkl", 'wb') as dict_file:
        pickle.dump(data_dict, dict_file)

if __name__ == "__main__":
    # gt_path = os.getcwd() + "/map_evolution/rev_results/reversed_crs0.2/"
    # gt_path = os.getcwd() + "/map_evolution/results/focus_maps/"
    gt_path = os.getcwd() + "/map_evolution/Custom/Custom_1_1runs/results/"
    result_file = gt_path[:-1] + "-parsed"

    match_file = os.getcwd() + "/data/all_5_class_matches.csv"
    # match_file = os.getcwd() + "/data/5_class_matches.csv"
    # match_file = os.getcwd() + "/data/5_class_matches_reversed.csv"
    main(gt_path, match_file, result_file)