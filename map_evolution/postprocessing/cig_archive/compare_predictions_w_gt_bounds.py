from __future__ import print_function
import numpy as np
from scipy.stats import normaltest, ttest_ind, mannwhitneyu
import scipy.stats as st
import cPickle as pickle
from common.common import *
dim_to_name_dict = {0:"duration", 1:"score", 2:"mse"}


def confidence_bounds(gt, confidence="0.95"):
    conf_dict = {
        "0.99": 2.576,
        "0.98": 2.326,
        "0.95": 1.96,
        "0.90": 1.645
    }
    mn = np.mean(gt)
    std = np.std(gt)
    z = conf_dict[confidence]
    n = len(gt)
    iv = z * (std / np.sqrt(n))
    ci_low = mn - iv
    ci_high = mn + iv
    return ci_low, ci_high, mn


def compare_values(preds, gts):
    pred_array = np.asarray(preds)
    nr_targets, nr_dims, nr_maps = pred_array.shape
    # gt_array = np.asarray(gts)
    gt_rolled = np.moveaxis(np.asarray(gts), 1, 2)

    cb_list = []
    in_bounds_list = []
    mse_diff = np.zeros((20, 3), dtype=np.float32)

    for target_time in range(nr_targets):
        nr_in_bounds = np.zeros(3, dtype=np.float32)
        print("Looking at {0}".format(index_to_target[target_time]))
        for run in range(nr_maps):
            tt_data = []
            for dimension in range(3):
                prediction = pred_array[target_time, dimension, run]
                lcb, ucb, mean = confidence_bounds(gt_rolled[target_time, dimension, run])
                is_true = lcb <= prediction <= ucb
                # print("run: {4}. LCB {0:.2f} <= P: {1:.2f} <= UCB {2:.2f}, {3}".format(lcb, prediction, ucb, is_true, run))
                tt_data.extend([prediction, lcb, ucb])
                if is_true:
                    nr_in_bounds[dimension] += 1
                if dimension == 2:
                    mse_diff[run, target_time] = prediction - mean
            cb_list.append(tt_data)
        perc_in_bounds = nr_in_bounds / nr_maps
        in_bounds_list.append(perc_in_bounds)
        print("percentage within bounds: duration {0:.2f}, score {1:.2f}".format(perc_in_bounds[0], perc_in_bounds[1]))
    cb_array = np.asarray(cb_list)
    return cb_array, in_bounds_list, np.asarray(mse_diff)


def main(data_file, gt_file, result_file, ucb_file, mean_file, mse_file):
    with open(data_file, 'rb') as data_in:
        preds = pickle.load(data_in)
    with open(gt_file, 'rb') as data_in:
        gt_data = pickle.load(data_in)

    cb_dict = {}
    ib_dict = {}
    mse_dict = {}
    for match in match_names:
        print("Match: ", match)
        match_cbs, in_bounds, mse_diff = compare_values(preds[match], gt_data[match])
        cb_dict[match] = match_cbs
        ib_dict[match] = in_bounds
        mse_dict[match] = mse_diff
    write_means(cb_dict, mean_file)
    write_error(mse_dict, mse_file)
    write_cbs(cb_dict, ucb_file)
    write_result(ib_dict, result_file)


def write_means(cb_dict, output_file):
    print("writing means to ", output_file)
    with open(output_file, 'w') as out:
        header = "Match, Run, Pred.Time, LCB, UCB, Pred.Score,LCB,UCB, Pred.Fitness,LCB,UCB\n"
        out.write(header)
        for match in match_names:
            cb_arr = cb_dict[match]
            for i in range(3):
                start = i * 20
                end = (i + 1) * 20
                mean_arr = np.mean(cb_arr[start:end], axis=0)
                str_vals = list_to_string(mean_arr)
                line = "{0},{1},{2}\n".format(match, index_to_target[i], str_vals)
                out.write(line)


def write_error(mse_dict, mse_file):
    print("writing means to ", mse_file)
    with open(mse_file, 'w') as out:
        header = "Match, 0.085, 0.315, 1\n"
        out.write(header)
        for match in match_names:
            mse_array = mse_dict[match]
            for run in range(20):
                errors = [mse_array[run, target] for target in range(3)]
                err_str = list_to_string(errors)
                line = "{0},{1}\n".format(match, err_str)
                out.write(line)


def write_cbs(cb_dict, ucb_file):
    print("writing UCB to ", ucb_file)
    with open(ucb_file, 'w') as out:
        header = "Match, Run, Pred.Time, LCB, UCB, Pred.Score,LCB,UCB, Pred.Fitness,LCB,UCB\n"
        out.write(header)
        for match in match_names:
            cb_arr = cb_dict[match]
            cntr = 0
            for vals in cb_arr:
                str_vals = list_to_string(vals)
                line = "{0},{1},{2}\n".format(match, cntr, str_vals)
                out.write(line)
                cntr += 1


def write_result(ib_dict, result_file):
    print("writing result to ", result_file)
    with open(result_file, 'w') as out:
        header = "match, target, duration in bounds ratio, score in bounds ratio\n"
        out.write(header)
        for match in match_names:
            ib_list = ib_dict[match]
            for i in range(len(ib_list)):
                vals = list_to_string(ib_list[i])
                line = "{0},{1},{2}\n".format(match, index_to_target[i], vals)
                out.write(line)

if __name__ == "__main__":
    gt_f = os.getcwd() + "/map_evolution/groundtruths/GT_crs0.2-parsed-per_map-raw.pkl"
    data_name = "results_20runs_crs0.2"
    data_f = os.getcwd() + "/map_evolution/results/{}-parsed.pkl".format(data_name)

    result_f = os.getcwd() + "/map_evolution/box_plots/{}-gt_comp.csv".format(data_name)
    ucb_f = os.getcwd() + "/map_evolution/box_plots/{}-UCB.csv".format(data_name)
    means_f = os.getcwd() + "/map_evolution/box_plots/{}-means.csv".format(data_name)
    mse_f = os.getcwd() + "/map_evolution/box_plots/{}-error.csv".format(data_name)

    main(data_f, gt_f, result_f, ucb_f, means_f, mse_f)
