import os
import numpy as np
from tqdm import tqdm
from collections import defaultdict
from map_evolution.postprocessing.EvolvedMapData import EvolvedMap
from character_evolution.postprocessing.GridCellData import GridCellData


def parse_result_files(folder,  nr_ea_targets, from_classification):
    keyword = "result"
    print("Searching for {0} in folder: {1} ".format(keyword, folder))
    files = [folder + f for f in os.listdir(folder) if keyword in f and not "summary" in f]
    grid_cell_data = []
    for fi in tqdm(sorted(files)):
        ea_data = parse_result_file(fi, nr_ea_targets, from_classification)
        fi_name_elems = fi.split("-")
        mut = 0
        crs = 0
        for el in fi_name_elems:
            if "mut" in el:
                mut = float(el.split("_")[1])
            if "crs" in el:
                crs = float(el.split("_")[1])
        grid_cell_data.append(GridCellData(mut, crs, ea_data))
    return grid_cell_data


def extract_targets_from_header(header_elems, nr_ea_targets, game_metric_names):
    targets_elems = header_elems[-nr_ea_targets:]
    targets = {}
    for el in targets_elems:
        target_els = el.split(":")
        target_name = target_els[0].replace("target ", "")
        if target_name in game_metric_names:
            targets[target_name] = float(target_els[1].strip())
    return targets


def extract_targets_from_filename(filename, game_metric_names):
    fi_elems = filename.split("-")
    targets = {}
    for el in fi_elems:
        target_els = el.split("_")
        if target_els[0] in game_metric_names:
            targets[target_els[0]] = float(target_els[1])
    return targets


def parse_result_file(gi_file, nr_ea_targets=2, from_classification=False):
    ea_classes = []
    with open(gi_file, 'r') as inpt:
        lines = inpt.readlines()
        header_elems = lines[0].rstrip().split(",")
        header_elems = [h for h in header_elems if h]
        fit_idx = header_elems.index("fitness")
        # I am assuming the following layout:
        # initial map, match, fitness,team0ratio,time,pred. initial team0ratio,pred. initial time,is_invalid,nr_generations,ind_name,Run
        # MOEA: initial map,ind,fitness,MSE,multi_squared_error per target ,preds per target, Sqrt. sum of fitnesses,initial multi_squared_error per target,evolved map
        for line in lines[1:]:
            if line.startswith("initial map"):  # these are header lines. This might introduce new targets, so we copy it.
                header_elems = line.rstrip().split(",")
                header_elems = [h for h in header_elems if h]
                continue
            elems = line.rstrip().split(",")
            map_name = elems[0]
            match = elems[1]
            class1, class2 = match.split("_")
            is_moea = "multi" in elems[fit_idx]
            t_id = fit_idx + 1 if not is_moea else fit_idx + nr_ea_targets
            # game_metric_names = header_elems[4:4 + nr_ea_targets] if "multi" not in elems[2] \
            #                                                       else elems[4 + nr_ea_targets:4 + 2 * nr_ea_targets]
            # game_metrics = elems[4:4 + nr_ea_targets] if "multi" not in elems[2] \
            #                                           else elems[ 4 + nr_ea_targets:4 + 2 * nr_ea_targets]
            game_metric_names = header_elems[t_id:t_id + nr_ea_targets] if not is_moea else header_elems[t_id:t_id * nr_ea_targets]
            game_metrics = elems[t_id:t_id + nr_ea_targets] if not is_moea else elems[t_id:t_id * nr_ea_targets]
            targets = extract_targets_from_header(header_elems, nr_ea_targets, game_metric_names)
            if targets == {}:
                print(lines[0].rstrip())
                print(header_elems)
                print("How can your targets be empty??")
                quit()
            infeasible = elems[header_elems.index("is_invalid")] == "True"
            evolved_map = gi_file.rsplit("/", 1)[0] + "/best_ind/" + elems[header_elems.index("ind_name")]
            if ".csv" not in evolved_map:
                evolved_map += ".csv"
            ea_classes.append(EvolvedMap(map_name, class1, class2, elems[3], game_metric_names, game_metrics, targets,
                                         infeasible, evolved_map,
                                         from_classification=from_classification))
    return ea_classes


def reshape_grid_data(grid_data):
    grid_dict = defaultdict(lambda: defaultdict(dict))
    for dp in grid_data:
        m = "{:.2f}".format(dp.mut)
        c = "{:.2f}".format(dp.cross)
        time = list(dp.per_duration_summary.keys())[0]  # assuming there is only one target per data point
        grid_dict[m][time][c] = dp
        # grid_dict[m][c].append(dp)
    return grid_dict


def write_value_list(grid_data, out_file, value):
    with open(out_file, 'w') as out:
        header = "mut, crs, key, {0}".format(value)
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.get_value_string(value, "duration")
            out.write(lines + "\n")


def write_value_grid(grid_dict, out_file, value):
    with open(out_file, 'w') as out:
        crsprob = ["{:.2f}".format(i) for i in np.arange(0, 1.1, 0.1)]
        header = "mut, time, {0}, <= crs".format(",".join(crsprob))
        out.write(header + "\n")
        for mut, time_dict in sorted(grid_dict.items()):
            line = [mut]
            for time, crs_dict in reversed(sorted(time_dict.items())):
                line.append(time)
                # for crs, cell in sorted(crs_dict.items(), key=lambda x: x[0]):
                #     line.extend([cell.get_value_string(value, "duration", value_only=True)])
                for crs in crsprob:
                    if crs in crs_dict.keys():
                        line.extend([crs_dict[crs].get_value_string(value, "duration", value_only=True)])
                    else:
                        line.extend([" "])
                # line.append("\n")
                out.write(",".join(line) + "\n")
                line = [mut]
            out.write("\n")


def write_output(grid_data, out_file):
    with open(out_file, 'w') as out:
        header = grid_data[0].get_summary_header()
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.get_summary_string()
            out.write(lines + "\n")


def write_all_values(grid_data, out_file):
    with open(out_file, 'w') as out:
        # count = 1
        header = grid_data[0].get_header()
        out.write(header + "\n")
        for cell in grid_data:
            lines = cell.to_string()
            out.write(lines)
            # out.write("{0},{1}".format(count, line))
            # count += 1


def main():
    # result_folder = os.getcwd() + "/Results/Scout_Heavy_GS/result_files_only/"
    # output_file = os.getcwd() + "/Results/Scout_Heavy_GS/Scout_Heavy_GS-evaluation6.csv"
    #
    # result_folder = os.getcwd() + "/Results/Scout_Heavy_Best/results_from_grid_search/"
    # output_file = os.getcwd() + "/Results/Scout_Heavy_Best/Scout_Heavy_Best-from_gs-evaluation.csv"

    result_folder = os.getcwd() + "/Results/New_Scout_Heavy_GS/"
    output_file = os.getcwd() + "/Results/New_Scout_Heavy_GS-evaluation.csv"
    from_classification = False

    nr_ea_targets = 2
    grid_data = parse_result_files(result_folder, nr_ea_targets, from_classification)
    grid_dict = reshape_grid_data(grid_data)

    # write_output(grid_data, output_file)
    write_value_grid(grid_dict, output_file.replace("evaluation", "fitness_grid"), "fitness")
    # write_value_grid(grid_dict, output_file.replace("evaluation", "delta_team0ratio_grid"), "delta_team0ratio")
    # write_value_grid(grid_dict, output_file.replace("evaluation", "delta_time_grid"), "delta_time")


if __name__ == "__main__":
    # Running the code from ppsEvolution/map_evolution/
    main()