import numpy as np
from common.EvolvedIndividualSummary import EvolvedIndividualSummary
from collections import defaultdict
from warnings import warn


possible_tiles = ["0", "1", "2", "S", "0A", "1A", "0D", "1D", "0H", "1H", "A", "D", "H"]


class EvolvedMapSummary(EvolvedIndividualSummary):
    def __init__(self, evolved_map_data_list):
        # needed for general evaluations:
        initial_len = len(evolved_map_data_list)
        evolved_map_data_list = [dp for dp in evolved_map_data_list if dp.has_gt]
        if len(evolved_map_data_list) < initial_len:
            print("Ignoring Maps without GT runs...")
        super().__init__(evolved_map_data_list)
        self.infeasible_count = self.count_infeasibles()
        self.mean_tile_count = self.compute_mean_tc([dp.tile_counts for dp in self.data])
        self.mean_tc_diff = self.compute_mean_tc([dp.tc_diff_w_initial for dp in self.data])

        self.bdi = [dp.tile_diff_w_initial for dp in self.data]
        self.mean_bin_dist_to_initial = np.mean(self.bdi)

    # print("MapSummary")

    def count_infeasibles(self):
        infeasibles = sum([1 if dp.infeasible else 0 for dp in self.data])
        if infeasibles > 0:
            warn("ATTENTION! Nr of infeasible evolved maps == {0}! Not 0.".format(infeasibles))
        return infeasibles

    def compute_mean_tc(self, tile_count_dicts):
        mean_tc_dict = defaultdict(float)
        nr_dicts = len(tile_count_dicts)
        for tc_dict in tile_count_dicts:
            for tile in possible_tiles:
                mean_tc_dict[tile] += tc_dict[tile]
        for key in possible_tiles:
            mean_tc_dict[key] = mean_tc_dict[key] / nr_dicts

        return mean_tc_dict

    def get_header(self):
        line = super().get_header()
        map_elems = ["mean tile diff to initial"] + \
                    ["mean " + tile for tile in possible_tiles] + \
                    ["mean {0} diff".format(tile) for tile in possible_tiles]
        str_line = ",".join(map_elems)
        header = line + "," + str_line
        return header

    def to_string(self):
        line = super().to_string()
        map_elems = ["{0:.1f}".format(self.mean_bin_dist_to_initial)] + \
                    ["{0:.1f}".format(self.mean_tile_count[tile]) for tile in possible_tiles] +\
                    ["{0:.1f}".format(self.mean_tc_diff[tile]) for tile in possible_tiles]
        str_line = ",".join(map_elems)
        line = line + "," + str_line
        return line


